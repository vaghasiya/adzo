$(document).ready(function() {
    $( "body" ).on("keypress", ".AlphabetNumericOnly", function( event ) {
        var key = window.event ? event.keyCode : event.which;
        var keychar = String.fromCharCode(key);
        if (key == 0) {
            return;
        }
        reg = /[a-zA-Z\t\d\b]+$/;
        return reg.test(keychar);
    });

    $( "body" ).on("keypress", ".IntegerOnly", function( event ) {
        var key = window.event ? event.keyCode : event.which;
        var keychar = String.fromCharCode(key);
        if (key == 0) {
            return;
        }
        reg = /[\t\d\b]+$/;
        return reg.test(keychar);
    });

    $( "body" ).on("keypress", ".DecimalOnly", function( event ) {
        var key = window.event ? event.keyCode : event.which;
        var keychar = String.fromCharCode(key);
        var newValue = $(this).val() + keychar;
        if (key == 0 || key == 8) {
            return;
        }
        if(key == 46){
            var str = $(this).val().toString();
            if(str.indexOf('.') != -1){
                event.preventDefault();
                event.stopPropagation();
                return false;
            }
        }
        if(parseFloat(newValue) * 100 % 1 > 0) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        }

        reg = /[\t\d\b.]+$/;
        return reg.test(keychar);
    });
    $('[data-tooltip="true"]').tooltip();
});
function ajax_request(URL, request_data, response_function, element){
    $.ajax({
        type: "POST",
        datatype:"json",
        url: URL,
        data: request_data,
        cache: false,
        success: function(result){
            if(response_function != undefined){
                response_function(result, element);
            }
        },
        error: function() {
            swal('Data Request Failed.');
            if(response_function != undefined) {
                response_function(undefined, element);
            }
        }
    });
}