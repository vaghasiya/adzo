<?php $this->load->view('elements/worker_header', array("title"=>"Withdrawal Summary","active_menu"=>"summary","sub_menu"=>"withdrawal")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $('#list_table').DataTable({
                responsive: true
            });

            $("#withdrawal_amount").on("change",function(){
                var adzo_balance = parseFloat($("#adzo_balance").val());
                var minimum_balance = parseFloat($("#minimum_balance").val());
                if(adzo_balance >= minimum_balance) {
                    var withdrawal_amount = parseFloat($("#withdrawal_amount").val());
                    var adzo_fees_amount = parseFloat($("#adzo_fees_amount").val());
                    var total_amount = 0;
                    if ((withdrawal_amount > 0)) {
                        if (withdrawal_amount < minimum_balance) {
                            withdrawal_amount = minimum_balance;
                            $(this).val(withdrawal_amount);
                            swal("Opps, Error Occurred", "Sorry, Minimum "+minimum_balance+" is required for withdrawal transaction", "error");
                        }
                        if (withdrawal_amount > adzo_balance) {
                            withdrawal_amount = adzo_balance;
                            $(this).val(withdrawal_amount);
                            swal("Opps, Error Occurred", "Sorry, You do not have enough balance", "error");
                        }
                        total_amount = withdrawal_amount - adzo_fees_amount;
                    } else {
                        $("#withdrawal_amount").val("");
                        total_amount = 0;
                    }
                    $("#total_amount").val(total_amount.toFixed(2));
                } else {
                    swal("Opps, Error Occurred","Sorry, Minimum "+minimum_balance+" Balance is Required in Adzo Account","error");
                }
            });

            $("#withdrawal_submit_button").on("click",function(){
                var withdrawal_amount = parseFloat($("#withdrawal_amount").val());
                var total_amount = parseFloat($("#total_amount").val());
                if(withdrawal_amount <= 0 || withdrawal_amount == "" || isNaN(withdrawal_amount)){
                    swal("Opps, Error Occurred","Invalid Withdrawal Amount","error");
                    return false;
                } else if(total_amount <= 0 || total_amount == "" || isNaN(total_amount)){
                    swal("Opps, Error Occurred","Invalid Withdrawal Total Amount","error");
                    return false;
                } else {
                    swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this process.!!!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, do it!'
                    }).then(function () {
                        $("#withdrawal_form").submit();
                    }, function(dismiss) {
                        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                        $("#withdrawal_amount").val("");
                        $("#total_amount").val("");
                        $("#withdraw_modal").modal("hide");
                    });
                }
            });
        });
    </script>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-badge font-yellow-gold"></i>
                        <span class="caption-subject bold uppercase text-info">Withdrawal Summary</span>
                    </div>
                    <div class="tools"><button class="btn btn-info" data-toggle="modal" href="#withdraw_modal">Click Here to Withdraw</button></div>
                </div>
                <div class="portlet-body">
                    <?php
                    $class = $this -> session -> flashdata('class');
                    $message = $this -> session -> flashdata('message');
                    if(empty($class)) $class = "danger";
                    if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $class; ?>">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <span class="message-text"><?php echo $message; ?></span>
                        </div>
                    <?php } ?>
                    <table id="list_table" class="table display nowrap table-striped table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="text-center">Date-Time</th>
                            <th class="text-center">Withdraw ID</th>
                            <th class="text-center">Transaction ID</th>
                            <th class="text-center">Details</th>
                            <th class="text-right">Withdrawal<br>Amount</th>
                            <th class="text-right">Adzo<br>Charges</th>
                            <th class="text-right">Net Amount</th>
                            <th class="text-right">Status</th>
                            <th class="text-center">Completed<br>Date-Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($withdrawal_list as $row) { ?>
                            <tr>
                                <td><?php echo date("Y-m-d", strtotime($row['created_date']))."<br>".date("H:i:s", strtotime($row['created_date'])); ?></td>
                                <td><?php echo $row['withdraw_reference_no']; ?></td>
                                <td><?php echo $row['transaction_reference_no']; ?></td>
                                <td><?php echo $row['comments']; ?></td>
                                <td class="text-right"><?php echo $row['withdrawal_amount']; ?></td>
                                <td class="text-right"><?php echo $row['adzo_fees_amount']; ?></td>
                                <td class="text-right"><?php echo $row['total_amount']; ?></td>
                                <td class="text-center"><?php echo $row['status']; ?></td>
                                <td><?php if((empty($row['updated_date'])) || ($row['updated_date'] == "0000-00-00 00:00:00")){ echo "&nbsp;"; } else { echo date("Y-m-d", strtotime($row['updated_date']))."<br>".date("H:i:s", strtotime($row['updated_date'])); } ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="withdraw_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color: #EAF3FE;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Withdraw Amount From Adzo Account</h4>
                </div>
                <div class="modal-body">
                    <form action="<?php echo base_url(); ?>worker/withdraw-request" method="post" id="withdrawal_form">
                        <input type="hidden" id="minimum_balance" value="<?php echo MINIMUM_BALANCE_FOR_WITHDRAWAL; ?>"/>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="adzo_balance">Adzo Account Balance</label>
                                    <input id="adzo_balance" class="form-control" value="<?php echo $user['balance']; ?>" type="text" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label for="withdrawal_amount">Withdraw Amount</label>
                                    <input name="withdrawal_amount" id="withdrawal_amount" class="form-control DecimalOnly" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="adzo_fees_amount">Adzo Fees</label>
                                    <input name="adzo_fees_amount" id="adzo_fees_amount" class="form-control" type="text" value="<?php echo ADZO_WITHDRAW_CHARGES; ?>" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label for="total_amount">Total Amount</label>
                                    <input name="total_amount" id="total_amount" class="form-control" type="text" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="button" class="form-control btn btn-info" value="Submit Request" id="withdrawal_submit_button">
                                        </div>
                                        <div class="col-sm-4">&nbsp;</div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('elements/worker_footer'); ?>