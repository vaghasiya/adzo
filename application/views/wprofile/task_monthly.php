<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?><div class="container">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title alert alert-info">
                <div class="caption ">
                    <i class="icon-tasks font-dark"></i>
                    <span class="caption-subject bold ">Tasks Status</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <span class="dashboard-stat dashboard-stat-v2 yellow" >
                        <div class="visual">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="1349">5665</span>
                            </div>
                            <div class="desc"> Submitted </div>
                        </div>
                    </span>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <span class="dashboard-stat dashboard-stat-v2 red">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="12,5">1258</span></div>
                            <div class="desc"> Approved </div>
                        </div>
                    </span>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <span class="dashboard-stat dashboard-stat-v2 green" >
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="549">549</span>
                            </div>
                            <div class="desc"> Rejected</div>
                        </div>
                    </span>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <span class="dashboard-stat dashboard-stat-v2 purple" >
                        <div class="visual">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="89">89</span></div>
                            <div class="desc">Pending</div>
                        </div>
                    </span>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <span class="dashboard-stat dashboard-stat-v2 blue" >
                        <div class="visual">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="$1349">$1349</span>
                            </div>
                            <div class="desc">Earnings </div>
                        </div>
                    </span>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <span class="dashboard-stat dashboard-stat-v2 green" >
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="96%">96%</span>
                            </div>
                            <div class="desc">Approval rate</div>
                        </div>
                    </span>
                </div>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class=" bold alert-danger"><a  href="<?php echo base_url(); ?>worker/tasks">30 Days</a></li>
                    <li class=" active bold alert-warning"><a>Monthly</a></li>                        
                </ul>
            </div>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in ">                               
                </div>
                <div id="menu1" class="tab-pane fade in active">
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="example" width="100%" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="all">Month</th>
                                    <th class="min-phone-l">Submitted</th>
                                    <th class="min-tablet">Approved</th>
                                    <th class="">Rejected</th>
                                    <th class="">Pending</th>
                                    <th class="text-center">Payment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="bold">April 2016</td>
                                    <td>1200</td>
                                    <td>2588</td>
                                    <td>858</td>
                                    <td>568</td>
                                    <td class="text-center">125</td>
                                </tr> 
                                <tr>
                                    <td class="bold">Aug 2016</td>
                                    <td>1200</td>
                                    <td>2588</td>
                                    <td>858</td>
                                    <td>568</td>
                                    <td class="text-center">125</td>
                                </tr> 
                                <tr>
                                    <td class="bold">Sep 2016</td>
                                    <td>1200</td>
                                    <td>2588</td>
                                    <td>858</td>
                                    <td>568</td>
                                    <td class="text-center">125</td>
                                </tr>
                                <tr>
                                    <td class="bold">June 2016</td>
                                    <td>1200</td>
                                    <td>2588</td>
                                    <td>858</td>
                                    <td>568</td>
                                    <td class="text-center">125</td>
                                </tr>
                            </tbody>                
                        </table>
                    </div>                    
                </div>
            </div>        
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>