<?php $this->load->view('elements/worker_header', array("title"=>"Publisher Summary","active_menu"=>"summary", "sub_menu"=>"publisher")); ?><div class="container">
    <script type="application/javascript">
        $(document).ready(function() {
            $('#life_time_table').DataTable();
            $('#week_table').DataTable();
            $('#months_table').DataTable();
        });
    </script>
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title alert alert-info">
                <div class="caption ">
                    <i class="icon-tasks font-dark"></i>
                    <span class="caption-subject bold ">Publisher Summary</span>
                </div>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active bold alert-warning"><a href="#week_tab" data-toggle="tab">7 Days</a></li>
                    <li class="bold alert-info"><a href="#months_tab" data-toggle="tab">30 Days</a></li>
                    <li class="bold alert-danger"><a href="#life_time_tab" data-toggle="tab">Life Time</a></li>
                </ul>
                <div class="tab-content">
                    <div id="week_tab" class="tab-pane fade in active">
                        <table class="table table-striped table-bordered table-hover" width="100%" id="week_table" cellspacing="0">
                            <thead>
                            <tr>
                                <th class="text-center">Publisher</th>
                                <th class="text-center">Submitted</th>
                                <th class="text-center">Approved</th>
                                <th class="text-center">Rejected</th>
                                <th class="text-center">Review<br>Pending</th>
                                <th class="text-center">Task<br>In Progress</th>
                                <th class="text-center">Earnings</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($week_list as $row) { ?>
                                <tr>
                                    <td class=""><?php echo $row['publisher_name']; ?></td>
                                    <td class="text-center"><?php echo $row['submitted']; ?></td>
                                    <td class="text-center"><?php echo $row['approved']; ?></td>
                                    <td class="text-center"><?php echo $row['rejected']; ?></td>
                                    <td class="text-center"><?php echo $row['review_pending']; ?></td>
                                    <td class="text-center"><?php echo $row['in_progress']; ?></td>
                                    <td class="text-right">$&nbsp;<?php if(empty($row['earnings'])){ echo "0"; } else { echo $row['earnings']; } ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div id="months_tab" class="tab-pane fade">
                        <table class="table table-striped table-bordered table-hover" width="100%" id="months_table" cellspacing="0">
                            <thead>
                            <tr>
                                <th class="text-center">Publisher</th>
                                <th class="text-center">Submitted</th>
                                <th class="text-center">Approved</th>
                                <th class="text-center">Rejected</th>
                                <th class="text-center">Review<br>Pending</th>
                                <th class="text-center">Task<br>In Progress</th>
                                <th class="text-center">Earnings</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($month_list as $row) { ?>
                                <tr>
                                    <td class=""><?php echo $row['publisher_name']; ?></td>
                                    <td class="text-center"><?php echo $row['submitted']; ?></td>
                                    <td class="text-center"><?php echo $row['approved']; ?></td>
                                    <td class="text-center"><?php echo $row['rejected']; ?></td>
                                    <td class="text-center"><?php echo $row['review_pending']; ?></td>
                                    <td class="text-center"><?php echo $row['in_progress']; ?></td>
                                    <td class="text-right">$&nbsp;<?php if(empty($row['earnings'])){ echo "0"; } else { echo $row['earnings']; } ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div id="life_time_tab" class="tab-pane fade">
                        <table class="table table-striped table-bordered table-hover" width="100%" id="life_time_table" cellspacing="0">
                            <thead>
                            <tr>
                                <th class="text-center">Publisher</th>
                                <th class="text-center">Submitted</th>
                                <th class="text-center">Approved</th>
                                <th class="text-center">Rejected</th>
                                <th class="text-center">Review<br>Pending</th>
                                <th class="text-center">Task<br>In Progress</th>
                                <th class="text-center">Earnings</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($life_time_list as $row) { ?>
                                <tr>
                                    <td class=""><?php echo $row['publisher_name']; ?></td>
                                    <td class="text-center"><?php echo $row['submitted']; ?></td>
                                    <td class="text-center"><?php echo $row['approved']; ?></td>
                                    <td class="text-center"><?php echo $row['rejected']; ?></td>
                                    <td class="text-center"><?php echo $row['review_pending']; ?></td>
                                    <td class="text-center"><?php echo $row['in_progress']; ?></td>
                                    <td class="text-right">$&nbsp;<?php if(empty($row['earnings'])){ echo "0"; } else { echo $row['earnings']; } ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>