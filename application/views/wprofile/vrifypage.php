<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="container">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-pin font-green"></i>
                <span class="caption-subject bold font-blue-sharp uppercase">Verify my account</span>
            </div>        
        </div>
        <div class="row">            
            <div class="col-md-4">
                <div class="thumbnail">
                    <a href="/assets/pages/media/profile/profile_user.jpg" target="_blank">
                        <img src="/assets/pages/media/profile/profile_user.jpg" alt="Fjords" style="width:50%">
                        <div class="caption">
                            <p>A scan copy of your Photo ID.</p>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="thumbnail">
                    <a href="/assets/pages/media/profile/profile_user.jpg" target="_blank">
                        <img src="/assets/pages/media/profile/profile_user.jpg" alt="Fjords" style="width:50%">
                        <div class="caption">
                            <p>A clear picture of you holding your Photo ID next to your face.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="thumbnail">
                    <a href="/assets/pages/media/profile/profile_user.jpg" target="_blank">
                        <img src="/assets/pages/media/profile/profile_user.jpg" alt="Fjords" style="width:50%">
                        <div class="caption">
                            <p>A more clear Bio picture of yourself showing your face.</p>
                        </div>
                    </a>
                </div>
            </div>
            <table>
                <tr>
                    <td><h4>Full Name :</h4></td>
                    <td><h4><b>Adxoxo </b></h4></td>
                </tr>
                <tr>
                    <td><h4>Document Number:</h4></td>
                    <td><h4><b>6776UU67876</b> </h4></td>
                </tr>
                <tr>
                    <td><h4>Date of birth :</h4></td>
                    <td><h4><b>21-45-2017</b></h4></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#edit").click(function () {
            $("#form_id").show();
        });
    });
</script>
<?php $this->load->view('elements/worker_footer'); ?>