<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?><div  class="container">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-pin font-red"></i>
                <span class="caption-subject bold uppercase font-red-haze"> Change Mail</span>
            </div>        
        </div>
        <div class="row">
            <div class="col-md-2">

            </div>
            <div class="col-md-6">
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input class="form-control edited" id="form_control_1"  type="email">
                                <label for="form_control_1">Current Email Address</label>
                                <span class="help-block">Please Enter Current Email Address...</span>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input class="form-control edited" id="form_control_1"  type="email">
                                <label for="form_control_1">New Email</label>
                                <span class="help-block">Please Email New Email...</span>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input class="form-control" id="form_control_1" type="email">
                                <label for="form_control_1">Confirm Email</label>
                                <span class="help-block">Confirm New Email here...</span>
                            </div>   
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input class="form-control" id="form_control_1" type="password">
                                <label for="form_control_1">Your Password</label>
                                <span class="help-block">Your Password here...</span>
                            </div>  
                            <div class="form-actions noborder">
                                <button type="button" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>