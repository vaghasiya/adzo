<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="container">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-bulb font-red-haze"></i>
                <span class="caption-subject bold font-red uppercase">Transfer Earnings</span>
            </div>        
        </div>
        <div class="row">                       
            <div class="portlet-body">
                <div class="col-md-2">
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover dt-responsive">
                        <tr>
                            <td width="40%">Available balance:</td>
                            <td >$350.00</td>
                        </tr>
                        <tr>
                            <td>Transfer amount:</td>
                            <td>$100.00</td>
                        </tr>
                        <tr>
                            <td>Transfer fee:</td>
                            <td>$3.00</td>
                        </tr>
                        <tr>
                            <td>Balance:</td>
                            <td>$97</td>
                        </tr>
                    </table>
                </div>                        
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <a class="bold">PayPal account</a>
            </div>
            <div class="col-md-3">
                <a class="bold">UPI</a>
            </div>
            <div class="col-md-3">
                <a class="bold">Bank account(2 weeks time)</a>
            </div>
            <div class="col-md-3">
                <a class="bold">Publisher account</a>
            </div>
        </div>
        <p></p>
        <div class="text-center">
            <a class="btn red btn-outline sbold" data-toggle="modal" href="#basic">Transfer</a>
        </div>
        
        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Bank List</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>  Allahabad Bank</option>
                                        <option>  Andhra Bank</option>
                                        <option>  Axis Bank</option>
                                        <option>  Bank of Bahrain and Kuwait</option>
                                        <option>  Bank of Baroda - Corporate Banking</option>
                                        <option>  Bank of Baroda - Retail Banking</option>
                                        <option>  Bank of India</option>
                                        <option>  Bank of Maharashtra</option>
                                        <option>  Canara Bank</option>
                                        <option>  Central Bank of India</option>
                                        <option>  City Union Bank</option>
                                        <option>  Corporation Bank</option>
                                        <option>  Deutsche Bank</option>
                                        <option>  Development Credit Bank</option>
                                        <option>  Dhanlaxmi Bank</option>
                                        <option>  Federal Bank</option>
                                        <option>  ICICI Bank</option>
                                        <option>  IDBI Bank</option>
                                        <option>  Indian Bank</option>
                                        <option>  Indian Overseas Bank</option>
                                        <option>  IndusInd Bank</option>
                                        <option>  ING Vysya Bank</option>
                                        <option>  Jammu and Kashmir Bank</option>
                                        <option>  Karnataka Bank Ltd</option>
                                        <option>  Karur Vysya Bank</option>
                                        <option>  Kotak Bank</option>
                                        <option>  Laxmi Vilas Bank</option>
                                        <option>  Oriental Bank of Commerce</option>
                                        <option>  Punjab National Bank - Corporate Banking</option>
                                        <option>  Punjab National Bank - Retail Banking</option>
                                        <option>  Punjab & Sind Bank</option>
                                        <option>  Royal Bank of Scotland</option>
                                        <option>  Shamrao Vitthal Co-operative Bank</option>
                                        <option>  South Indian Bank</option>
                                        <option>  State Bank of Bikaner & Jaipur</option>
                                        <option>  State Bank of Hyderabad</option>
                                        <option>  State Bank of India</option>
                                        <option>  State Bank of Mysore</option>
                                        <option>  State Bank of Patiala</option>
                                        <option>  State Bank of Travancore</option>
                                        <option>  Syndicate Bank</option>
                                        <option>  Tamilnad Mercantile Bank Ltd.</option>
                                        <option>  UCO Bank</option>
                                        <option>  Union Bank of India</option>
                                        <option>  United Bank of India</option>
                                        <option>  Vijaya Bank</option>
                                        <option>  Yes Bank Ltd</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="text-center">
                                <a href="#" class="btn btn-warning ">Submit</a>
                            </div>
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                        <button type="button" class="btn green">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>