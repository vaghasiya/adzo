<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?><div class="container">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title alert alert-success">
                <div class="caption ">
                    <i class="icon-tasks font-dark"></i>
                    <span class="caption-subject bold ">Publisher</span>
                </div>
            </div>        
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="bold alert-info"><a href="<?php echo base_url(); ?>worker/publisher">LifeTime</a></li>
                    <li class=" bold alert-warning"><a href="<?php echo base_url(); ?>worker/p7days">7 Days</a></li>
                    <li class="active bold alert-danger"><a href="<?php echo base_url(); ?>worker/p30days">30 Days</a></li>

                </ul>
            </div>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="example" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all text-center">Publisher ID</th>
                                <th class="min-phone-l text-center">Submitted</th>
                                <th class="min-tablet text-center">Approved</th>
                                <th class="text-center">Rejected</th>
                                <th class="text-center">Pending</th>
                                <th class="text-center">Earnings</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">TYJUKIJ7836YU</td>
                                <td class="text-center">1200</td>
                                <td class="text-center">2588</td>
                                 <td class="text-center">2588</td>
                                <td class="text-center">125</td>
                                <td class="text-center">$700</td>
                            </tr> 
                             <tr>
                                <td class="text-center">TYJUKIJ7836YU</td>
                                <td class="text-center">1200</td>
                                <td class="text-center">2588</td>
                                 <td class="text-center">2588</td>
                                <td class="text-center">125</td>
                                <td class="text-center">$700</td>
                            </tr>
                             <tr>
                                <td class="text-center">TYJUKIJ7836YU</td>
                                <td class="text-center">1200</td>
                                <td class="text-center">2588</td>
                                 <td class="text-center">2588</td>
                                <td class="text-center">125</td>
                                <td class="text-center">$700</td>
                            </tr>
                             <tr>
                                <td class="text-center">TYJUKIJ7836YU</td>
                                <td class="text-center">1200</td>
                                <td class="text-center">2588</td>
                                 <td class="text-center">2588</td>
                                <td class="text-center">125</td>
                                <td class="text-center">$700</td>
                            </tr>
                             <tr>
                                <td class="text-center">HTHUJUKUH</td>
                                <td class="text-center">1200</td>
                                <td class="text-center">2588</td>
                                 <td class="text-center">2588</td>
                                <td class="text-center">125</td>
                                <td class="text-center">$700</td>
                            </tr>
                             <tr>
                                <td class="text-center">TYJUKIJ7836YU</td>
                                <td class="text-center">1200</td>
                                <td class="text-center">2588</td>
                                 <td class="text-center">2588</td>
                                <td class="text-center">125</td>
                                <td class="text-center">$700</td>
                            </tr>
                             <tr>
                                <td class="text-center">TYJUKIJ7836YU</td>
                                <td class="text-center">1200</td>
                                <td class="text-center">2588</td>
                                 <td class="text-center">2588</td>
                                <td class="text-center">125</td>
                                <td class="text-center">$700</td>
                            </tr>
                        </tbody>                
                    </table>        
                </div>                
            </div>        
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>