<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?><div class="col-md-12">
    <div class="portlet light bordered">
        <div class="portlet-title alert alert-success">
            <div class="caption ">
                <i class="icon-tasks font-dark"></i>
                <span class="caption-subject bold ">Transaction</span>
            </div>
        </div>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="example" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="all text-center">Date</th>
                            <th class="min-phone-l text-center">Publisher Name</th>
                            <th class="min-tablet text-center">Project Name</th>
                            <th class="text-center">Transaction ID</th>
                            <th class="text-center">Reward</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">21-05-2017</td>
                            <td class="text-center">LIJO JOHN </td>
                            <td class="text-center">Simple project</td>
                            <td class="text-center">RTYBHGRTY</td>
                            <td class="text-center">$758</td>
                        </tr>   
                        <tr>
                            <td class="text-center">21-05-2017</td>
                            <td class="text-center">LIJO JOHN </td>
                            <td class="text-center">Simple project</td>
                            <td class="text-center">RTYBHGRTY</td>
                            <td class="text-center">$758</td>
                        </tr>
                        <tr>
                            <td class="text-center">21-05-2017</td>
                            <td class="text-center">LIJO JOHN </td>
                            <td class="text-center">Simple project</td>
                            <td class="text-center">RTYBHGRTY</td>
                            <td class="text-center">$758</td>
                        </tr> 
                        <tr>
                            <td class="text-center">21-05-2017</td>
                            <td class="text-center">LIJO JOHN </td>
                            <td class="text-center">Simple project</td>
                            <td class="text-center">RTYBHGRTY</td>
                            <td class="text-center">$758</td>
                        </tr> 
                        <tr>
                            <td class="text-center">21-05-2017</td>
                            <td class="text-center">LIJO JOHN </td>
                            <td class="text-center">Simple project</td>
                            <td class="text-center">RTYBHGRTY</td>
                            <td class="text-center">$758</td>
                        </tr> 
                        <tr>
                            <td class="text-center">21-05-2017</td>
                            <td class="text-center">LIJO JOHN </td>
                            <td class="text-center">Simple project</td>
                            <td class="text-center">RTYBHGRTY</td>
                            <td class="text-center">$758</td>
                        </tr> 
                    </tbody>                
                </table>            
            </div>                
        </div>        
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>