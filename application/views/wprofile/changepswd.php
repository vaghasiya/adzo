<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?><div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green">
            <i class="icon-pin font-green"></i>
            <span class="caption-subject bold uppercase"> Change Password</span>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-6">
            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <input class="form-control edited" id="form_control_1"  type="email">
                            <label for="form_control_1">Email Address</label>
                            <span class="help-block">Please Enter Email Address...</span>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <input class="form-control edited" id="form_control_1"  type="password">
                            <label for="form_control_1">Old Password</label>
                            <span class="help-block">Please Enter Old Password...</span>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <input class="form-control" id="form_control_1" type="password">
                            <label for="form_control_1">New Password</label>
                            <span class="help-block">Please Enter New Password here...</span>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                            <input class="form-control" id="form_control_1" type="password">
                            <label for="form_control_1">Confirm Password</label>
                            <span class="help-block">Confirm New Password here...</span>
                        </div>                        
                        <div class="form-actions noborder">
                            <button type="button" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>