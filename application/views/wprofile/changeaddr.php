<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="container">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-pin font-green"></i>
                <span class="caption-subject bold font-blue-sharp uppercase">Change Address</span>
            </div>        
        </div>
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <textarea class="form-control" rows="3">
                            Adzo Info Com  
                            3997  Bhanshankari,
                            Bangalore -560085.
                                </textarea>
                                <label for="form_control_1">Current Address</label>
                            </div>
                            <button type="button" id="edit" class="btn blue">Edit</button>
                            <div class="form-group form-md-line-input form-md-floating-label" id="form_id" style="display: none">
                                <textarea class="form-control" rows="3">                               
                                </textarea>
                                <label for="form_control_1">New Address</label>
                            </div>
                            <div class="form-actions noborder">
                                <button type="button" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#edit").click(function () {
            $("#form_id").show();
        });
    });
</script>
<?php $this->load->view('elements/worker_footer'); ?>