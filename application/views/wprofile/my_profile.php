<?php $this->load->view('elements/worker_header', array("title"=>"My Profile","active_menu"=>"dashboard")); ?>
    <script type="application/javascript">
        var india_state_list = <?php echo json_encode($india_state_list); ?>;
        var usa_state_list = <?php echo json_encode($usa_state_list); ?>;
        $(document).ready(function() {
            $('#birthdate').daterangepicker({
                "locale": {
                    "format": 'DD-MM-YYYY'
                },
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "minDate": "01-01-1950",
                "maxDate": "<?php echo date("d-m-Y"); ?>"
            }, function(start, end, label) {
                $("#birthdate").val(start.format('DD-MM-YYYY'));
            });

            $("#country").on("change",function(){
                var country = $(this).val();
                if(country == "India"){
                    load_state_data(india_state_list);
                } else if (country == "United States of America"){
                    load_state_data(usa_state_list);
                } else {
                    $("#state").html('<option value="">None</option>');
                }
            });

            $(".ImageOnly").on("change", function (event) {
                filename = event.target.files[0].name;
                file = filename.split(".");
                if(file[1] == "jpg" || file[1] == "JPG" || file[1] == "jpeg" || file[1] == "JPEG" || file[1] == "png" || file[1] == "PNG"){
                    console.log("valid file");
                } else {
                    swal("Oops Error Found","Sorry, Please select correct file only.!!!","error");
                    $(this).val("");
                }
            });
        });

        function load_state_data(state_list) {
            var option_html = "";
            $(state_list).each(function (index, value) {
                option_html += '<option value="' + value.state_name + '">' + value.state_name + '</option>';
            });
            $("#state").html(option_html);
        }
    </script>
<div class="container">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title alert alert-info">
                <div class="caption ">
                    <i class="icon-tasks font-dark"></i>
                    <span class="caption-subject bold ">My Profile</span>
                </div>
            </div>
            <div class="panel-body">
                <?php
                $class = $this -> session -> flashdata('class');
                $message = $this -> session -> flashdata('message');
                if(empty($class)) $class = "danger";
                if(!empty($message)){ ?>
                    <div class="alert alert-<?php echo $class; ?>">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span class="message-text"><?php echo $message; ?></span>
                    </div>
                <?php } ?>
                <ul class="nav-responsive nav nav-tabs">
                    <li class="active">
                        <a href="#tab1" data-toggle="tab"><strong>Account Details</strong></a>
                    </li>
                    <li>
                        <a href="#tab2" data-toggle="tab"><strong>Bank Details</strong></a>
                    </li>
                    <li>
                        <a href="#tab3" data-toggle="tab"><strong>Verify Account</strong></a>
                    </li>
                </ul>
                <form action="<?php echo base_url(); ?>worker/update-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="first_name">* First Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="first_name" value="<?php echo $first_name; ?>" id="first_name" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="last_name">* Last Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="last_name" value="<?php echo $last_name; ?>" id="last_name" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="company_name">Company / Institute Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="company_name" value="<?php echo $company_name; ?>" id="company_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="country">* Country:</label>
                                    <div class="col-sm-8">
                                        <select id="country" class="form-control" style="border: 1px solid #ccc;" disabled="disabled">
                                            <option value="" style="display: none;">Select Here</option>
                                            <option <?php if($country == "India"){ echo 'selected="selected"'; } ?> value="India">India</option>
                                            <option <?php if($country == "United States of America"){ echo 'selected="selected"'; } ?> value="United States of America">United States of America</option>
                                            <option disabled>─────────────────</option>
                                            <?php foreach ($country_list as $row) { if($row['country_nice_name'] == "India" || $row['country_nice_name'] == "United States of America"){ continue; } ?>
                                                <option <?php if($country == $row['country_nice_name']){ echo 'selected="selected"'; } ?> value="<?php echo $row['country_nice_name']; ?>"><?php echo $row['country_nice_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="state">State:</label>
                                    <div class="col-sm-8">
                                        <select id="state" class="form-control" style="border: 1px solid #ccc;" disabled="disabled">
                                            <?php if($country == "India"){
                                                foreach ($india_state_list as $row) { ?>
                                                    <option <?php if($state == $row['state_name']){ echo 'selected="selected"'; } ?> value="<?php echo $row['state_name']; ?>"><?php echo $row['state_name']; ?></option>
                                            <?php }
                                            } else if($country == "United States of America"){
                                                foreach ($usa_state_list as $row) {?>
                                                    <option <?php if($state == $row['state_name']){ echo 'selected="selected"'; } ?> value="<?php echo $row['state_name']; ?>"><?php echo $row['state_name']; ?></option>
                                            <?php }
                                            } else { ?>
                                                <option>None</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <br/><br/>
                                <div class="form-group">
                                    <label class="control-label col-sm-1">&nbsp;</label>
                                    <button type="submit" class="btn btn-success btn-md">Update Details</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="address1">Address 1:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="address1" value="<?php echo $address1; ?>" id="address1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="address2">Address 2:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="address2" value="<?php echo $address2; ?>" id="address2">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="address3">Address 3:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="address3" value="<?php echo $address3; ?>" id="address3">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="city">City :</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="city" value="<?php echo $city; ?>" id="city" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="pin">Pin :</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="pin" value="<?php echo $pin; ?>" id="pin">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="mobile_number">Mobile Number :</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="mobile_number" value="<?php echo $mobile_number; ?>" id="mobile_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="profile_pic">Profile Photo :</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control ImageOnly" name="profile_pic" id="profile_pic"  accept=".jpeg, .jpg, .png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            <?php if($country == "India"){ ?>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="account_holder_name">Name of Account Holder (As per bank records):</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="account_holder_name" value="<?php echo $account_holder_name; ?>" id="account_holder_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="account_number">Account Number :</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="account_number" value="<?php echo $account_number; ?>" id="account_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="state">Bank Name:</label>
                                    <div class="col-sm-8">
                                        <select id="bank_name" name="bank_name" class="form-control">
                                            <option value="" style="display: none;">Select Here</option>
                                            <?php foreach ($banks_list as $row) { ?>
                                                <option <?php if($bank_name == $row['bank_name']){ echo 'selected="selected"'; } ?> value="<?php echo $row['bank_name']; ?>"><?php echo $row['bank_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="branch_location">Branch Location:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="branch_location" value="<?php echo $branch_location; ?>" id="branch_location">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="ifsc_code">IFSC Code:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="ifsc_code" value="<?php echo $ifsc_code; ?>" id="ifsc_code">
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="paypal_name">Name Associated with Paypal Account:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="paypal_name" value="<?php echo $paypal_name; ?>" id="paypal_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="paypal_email">Email Associated with Paypal Account:</label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control" name="paypal_email" value="<?php echo $paypal_email; ?>" id="paypal_email">
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label class="control-label col-sm-1">&nbsp;</label>
                                <button type="submit" class="btn btn-success btn-md">Update Details</button>
                            </div>
                        </div>
                        <div id="tab3" class="tab-pane fade">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="full_name">Full Name (As per Document):</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="full_name" value="" id="full_name" form="verification_form" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="document_number">Document Number:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="document_number" value="" id="document_number" form="verification_form" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="birthdate">Date of Birth:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="birthdate" value="" id="birthdate" form="verification_form" required="required" style="background-color: #FFFFFF;" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="document1">Document 1:</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control ImageOnly" name="document1" id="document1"  accept=".jpeg, .jpg, .png" form="verification_form" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="document2">Document 2:</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control ImageOnly" name="document2" id="document2"  accept=".jpeg, .jpg, .png" form="verification_form" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="document3">Document 3:</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control ImageOnly" name="document3" id="document3"  accept=".jpeg, .jpg, .png" form="verification_form" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-1">&nbsp;</label>
                                <button type="submit" class="btn btn-success btn-md" form="verification_form">Submit Request</button>
                            </div>
                        </div>
                    </div>
                </form>
                <form action="<?php echo base_url(); ?>worker/verification-request" class="form-horizontal" method="post" enctype="multipart/form-data" id="verification_form"></form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>