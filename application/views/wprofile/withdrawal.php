<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?><div class="container">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title alert alert-success">
                <div class="caption ">
                    <i class="icon-tasks font-dark"></i>
                    <span class="caption-subject bold ">Withdrawals</span>
                </div>
            </div>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="example" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all text-center">Date</th>
                                <th class="min-phone-l text-center">Worker ID</th>
                                <th class="min-tablet text-center">Amount</th>
                                <th class="text-center">Deduction</th>
                                <th class="text-center">Gross amount</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">21-05-2017</td>
                                <td class="text-center">HYHJUYB56D</td>
                                <td class="text-center">$250</td>
                                <td class="text-center">$85</td>
                                <td class="text-center">$450</td>
                                <td class="text-center">Success</td>
                            </tr>
                            <tr>
                                <td class="text-center">21-05-2017</td>
                                <td class="text-center">HYHJUYB56D</td>
                                <td class="text-center">$250</td>
                                <td class="text-center">$85</td>
                                <td class="text-center">$450</td>
                                <td class="text-center">Success</td>
                            </tr>
                            <tr>
                                <td class="text-center">21-05-2017</td>
                                <td class="text-center">HYHJUYB56D</td>
                                <td class="text-center">$250</td>
                                <td class="text-center">$85</td>
                                <td class="text-center">$450</td>
                                <td class="text-center text-warning">Failed</td>
                            </tr>
                            <tr>
                                <td class="text-center">21-05-2017</td>
                                <td class="text-center">HYHJUYB56D</td>
                                <td class="text-center">$250</td>
                                <td class="text-center">$85</td>
                                <td class="text-center">$450</td>
                                <td class="text-center text-success">Success</td>
                            </tr>
                            <tr>
                                <td class="text-center">21-05-2017</td>
                                <td class="text-center">HYHJUYB56D</td>
                                <td class="text-center">$250</td>
                                <td class="text-center">$85</td>
                                <td class="text-center">$450</td>
                                <td class="text-center text-success">Success</td>
                            </tr>
                        </tbody>                
                    </table>            
                </div>                
            </div>        
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>