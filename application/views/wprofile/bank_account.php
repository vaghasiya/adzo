<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="container">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-envelope bold font-green"></i>
                <span class="caption-subject bold font-blue-sharp uppercase">Bank Account</span>
            </div>        
        </div>
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive">
                                <tr>
                                    <td>Name of the account holder</td>
                                    <td>Adzo Ltd</td>
                                </tr>
                                <tr>
                                    <td>Account Number</td>
                                    <td>125214252000125</td>
                                </tr>                                
                                <tr>
                                    <td>Bank name</td>
                                    <td>ICIC Bank</td>
                                </tr>
                                <tr>
                                    <td>IFSC Code</td>
                                    <td>ICCIC004</td>
                                </tr>
                                <tr>
                                    <td>Branch Location</td>
                                    <td>Sreengagara</td>
                                </tr>
                            </table>
                            
                            <button type="button" class="btn red" id="edit" >Edit</button> 
                            <div id="form_id" style="display: none;">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control edited" id="form_control_1" value="12542522522514" type="number">
                                    <label for="form_control_1">Account number</label>
                                    <span class="help-block">Please Enter Account number...</span>
                                </div>
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control edited" id="form_control_1" value="12542522522514" type="number">
                                    <label for="form_control_1">Re-type account number...</label>
                                    <span class="help-block">Please Confirm New PayPal Email...</span>
                                </div>
                                <div class="form-group form-md-line-input form-md-floating-label has-info">
                                    <select class="form-control edited" id="form_control_1">
                                        <option value=""></option>
                                        <option value="1" selected="">State Bank Of India</option>
                                        <option value="2">HDFC Bnak Ltd</option>
                                        <option value="3">ICICI Bnak Ltd</option>
                                        <option value="4">KARNATAKA Bank</option>
                                    </select>
                                    <label for="form_control_1">Bank Name</label>
                                </div>
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control edited" id="form_control_1" value="ICICIC9999" type="text">
                                    <label for="form_control_1">IFSC Code</label>
                                    <span class="help-block">Please Enter IFSC Code ...</span>
                                </div>
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control edited" id="form_control_1" value="Bangalore" type="text">
                                    <label for="form_control_1">Branch Location</label>
                                    <span class="help-block">Please Branch Location ...</span>
                                </div>
                            </div>
                            <div class="form-actions noborder">
                                <button type="button" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#edit").click(function () {
            $("#form_id").show();
        });
    });
</script>
<?php $this->load->view('elements/worker_footer'); ?>