<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?><div class="container">
    <div class="modal-body">
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" id="example" width="100%" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="all">Publisher Name</th>
                        <th class="min-phone-l">Project Name</th>
                        <th class="min-tablet">Amount</th>
                        <th class="">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Microsoft</td>
                        <td>Data collections</td>
                        <td>$100.44</td>
                        <td>Pending</td>
                        <th class="text-center">
                            <a href="#" class="text-warning">Contact worker</a>
                        </th>
                    </tr>
                    <tr>
                        <td>Amazon</td>
                        <td>Data collections</td>
                        <td>$100.44</td>
                        <td>Accepted</td>
                        <th class="text-center">
                            <a href="#" class="text-warning">Contact worker</a>
                        </th>
                    </tr>
                    <tr>
                        <td>Google</td>
                        <td>Data collections</td>
                        <td>$100.44</td>
                        <td>Accepted</td>
                        <th class="text-center">
                            <a href="#" class="text-warning">Contact worker</a>
                        </th>
                    </tr>
                    <tr>
                        <td>Bing</td>
                        <td>Data collections</td>
                        <td>$100.44</td>
                        <td>Accepted</td>
                        <th class="text-center">
                            <a href="#" class="text-warning">Contact worker</a>
                        </th>
                    </tr>
                    <tr>
                        <td>Yahoo</td>
                        <td>Data collections</td>
                        <td>$100.44</td>
                        <td>Accepted</td>
                        <th class="text-center">
                            <a href="#" class="text-warning">Contact worker</a>
                        </th>
                    </tr>
                </tbody>                
            </table>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>