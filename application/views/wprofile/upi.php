<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="container">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-bulb font-red-haze"></i>
                <span class="caption-subject bold font-red uppercase">UPI</span>
            </div>        
        </div>
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input class="form-control edited" id="form_control_1" disabled="" value="Adzo dvff" type="text">
                                <label for="form_control_1">Name</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input class="form-control edited" id="form_control_1" disabled="" value="9025444834" >
                                <label for="form_control_1">Phone Number</label>
                            </div>
                            <button type="button" class="btn red" id="edit" >Edit</button> 
                            <div id="form_id" style="display: none;">s                                
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control edited" id="form_control_1" value="xxxxxrbs@gmail.com" type="email">
                                    <label for="form_control_1">Confirm Virtual address...</label>
                                    <span class="help-block">Please Virtual address...</span>
                                </div>
                            </div>
                            <div class="form-actions noborder">
                                <button type="button" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#edit").click(function () {
            $("#form_id").show();
        });
    });
</script>
<?php $this->load->view('elements/worker_footer'); ?>