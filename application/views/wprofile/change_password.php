<?php $this->load->view('elements/worker_header', array("title"=>"Change Password","active_menu"=>"dashboard")); ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green">
            <i class="icon-pin font-green"></i>
            <span class="caption-subject bold uppercase"> Change Password</span>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-6">
            <?php
            $class = $this -> session -> flashdata('class');
            $message = $this -> session -> flashdata('message');
            if(empty($class)) $class = "danger";
            if(!empty($message)){ ?>
                <div class="alert alert-<?php echo $class; ?>">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span class="message-text"><?php echo $message; ?></span>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <form role="form" method="post" action="<?php echo base_url(); ?>worker/update-password">
                    <div class="form-group">
                        <label for="password">Old Password</label>
                        <input class="form-control" name="password" id="password" type="password" required="required">
                    </div>
                    <div class="form-group">
                        <label for="new_password">New Password</label>
                        <input class="form-control" name="new_password" id="new_password" type="password" required="required">
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <input class="form-control" name="confirm_password" id="confirm_password" type="password" required="required">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn blue">Submit</button>
                        <a href="<?php echo base_url(); ?>worker/dashboard" class="btn red pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>