<?php $this->load->view('elements/admin_header'); ?>
<div class="container-fluid">
    <div class="page-content">
        <!--<h2>Manage Batch</h2>-->
        <div class="container">
            <ul class="nav nav-tabs">
                <li class="active h4"><a data-toggle="tab" href="#home">Batches in Progress &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                <li class="h4"><a data-toggle="tab" href="#menu1">Batches ready for review &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                <li class="h4"><a data-toggle="tab" href="#menu2">Batches already reviewed &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="portlet-body flip-scroll">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content">
                                <tr>
                                    <th class="text-center" width="15%"> Date</th>
                                    <th class="text"> Publisher ID</th>
                                    <th class="numeric">Project Name</th>
                                    <th class="numeric">Total Assignment</th>
                                    <th class="numeric">Submitted</th>
                                    <th class="numeric">Pending</th>
                                    <th class="text-center"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Summary</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="page-content-container">
                                            <div class="page-content-row">
                                                <div class="page-sidebar">
                                                    <div class="page-content-col">
                                                        <div class="row">      
                                                            <h4><b>Batch Details</b> </h4>  
                                                            <h4> <b>Project Name:</b> Data Collection in the email   </h4>
                                                            <h4> <b>Title :</b> Collect data </h4>
                                                            <h4> <b>Description :</b>  </h4>  
                                                            <h4> <b>Keyword:</b> </h4>
                                                            <h4> <b>Description:</b> </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5><b> Total assignments:</b>5258</h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5><b>Tasks expires in:</b>10-Feb-2016</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5><b>Reward per Tasks:</b>$0.03</h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5><b>Total cost:</b>$252</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5><b>Tasks submitted:</b> 41</h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5><b>Tasks Approved:</b> 15</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5><b>Task Pending:</b>14</h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h5><b>File:</b>14</h5>
                                            </div>
                                        </div>
                                    </div>     
                                        <h3 class="text-info">Worker Summary</h3>
                                        <table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead class="flip-content">
                                                <tr>
                                                    <th > Worker ID </th>
                                                    <th class="text"> No of Tasks submitted </th>
                                                    <th class="numeric"> Approved </th>
                                                    <th class="numeric"> Rejected</th>
                                                    <th class="numeric"> Pending</th>
                                                    <th class="numeric"> Total Payment</th>
                                                    <th class="text-center"> Actions </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td> 0000252215 </td>
                                                    <td> 514545 </td>
                                                    <td class="numeric"> 252514 </td>
                                                    <td class="numeric"> 121245</td>
                                                    <td class="numeric"> 461821 </td>
                                                    <td class="numeric"> 25851 </td>
                                                    <td class="text-center"><a href="#" class="btn red btn-xs">Block</a>
                                                        <a href="#" class="btn blue btn-xs">Pay Bonus</a>
                                                    </td>
                                                </tr>    
                                                <tr>
                                                    <td> 0000252215 </td>
                                                    <td> 514545 </td>
                                                    <td class="numeric"> 252514 </td>
                                                    <td class="numeric"> 121245</td>
                                                    <td class="numeric"> 461821 </td>
                                                    <td class="numeric"> 25851 </td>
                                                    <td class="text-center"><a href="#" class="btn red btn-xs">Block</a>
                                                        <a href="#" class="btn blue btn-xs">Pay Bonus</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> 0000252215 </td>
                                                    <td> 514545 </td>
                                                    <td class="numeric"> 252514 </td>
                                                    <td class="numeric"> 121245</td>
                                                    <td class="numeric"> 461821 </td>
                                                    <td class="numeric"> 25851 </td>
                                                    <td class="text-center"><a href="#" class="btn red btn-xs">Block</a>
                                                        <a href="#" class="btn blue btn-xs">Pay Bonus</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> 0000252215 </td>
                                                    <td> 514545 </td>
                                                    <td class="numeric"> 252514 </td>
                                                    <td class="numeric"> 121245</td>
                                                    <td class="numeric"> 461821 </td>
                                                    <td class="numeric"> 25851 </td>
                                                    <td class="text-center"><a href="#" class="btn red btn-xs">Block</a>
                                                        <a href="#" class="btn blue btn-xs">Pay Bonus</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> 0000252215 </td>
                                                    <td> 514545 </td>
                                                    <td class="numeric"> 252514 </td>
                                                    <td class="numeric"> 121245</td>
                                                    <td class="numeric"> 461821 </td>
                                                    <td class="numeric"> 25851 </td>
                                                    <td class="text-center"><a href="#" class="btn red btn-xs">Block</a>
                                                        <a href="#" class="btn blue btn-xs">Pay Bonus</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="portlet-body flip-scroll">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content">
                                <tr>
                                    <th class="text-center" width="15%"> Date</th>
                                    <th class="text"> Publisher ID</th>
                                    <th class="numeric">Project Name</th>
                                    <th class="numeric">Total Assignment</th>
                                    <th class="numeric">Submitted</th>
                                    <th class="numeric">Pending</th>
                                    <th class="text-center"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <div class="portlet-body flip-scroll">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content">
                                <tr>
                                    <th class="text-center" width="15%"> Date</th>
                                    <th class="text"> Publisher ID</th>
                                    <th class="numeric">Project Name</th>
                                    <th class="numeric">Total Assignment</th>
                                    <th class="numeric">Submitted</th>
                                    <th class="numeric">Pending</th>
                                    <th class="text-center"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> 12-12-2016 </td>
                                    <td> 252555 </td>
                                    <td class="numeric"> Adzo India Pvt Ltd.</td>
                                    <td class="numeric"> admin@exaample.com </td>
                                    <td class="numeric"> 61821 </td>
                                    <td class="numeric"> 25585 </td>
                                    <td class="text-center"><a href="#" class="btn red btn-xs">Result</a>
                                        <a href="#" class="btn blue btn-xs">Cancel Batches</a>
                                        <a class="btn purple btn-xs" data-toggle="modal" href="#basic">Summary</a>
                                        <a href="#" class="btn green btn-xs">hold/revoke</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/admin_footer'); ?>