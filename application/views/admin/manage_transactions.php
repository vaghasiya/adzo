<?php $this->load->view('elements/admin_header', array("title"=>"Manage Transactions","active_menu"=>"manage","sub_menu"=>"manage_transactions")); ?>
    <script>
        $(document).ready(function() {
            $('#transactions_table').DataTable({
                responsive: true,
                order: [[ 0, "desc" ]]
            });
        });
    </script>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 style="margin-top: 10px; margin-bottom: 20px;">Manage Transactions</h3>
                            <?php
                            $class = $this -> session -> flashdata('class');
                            $message = $this -> session -> flashdata('message');
                            if(empty($class)) $class = "danger";
                            if(!empty($message)){ ?>
                                <div class="alert alert-<?php echo $class; ?>">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <span class="message-text"><?php echo $message; ?></span>
                                </div>
                            <?php } ?>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="transactions_table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Date-Time</th>
                                        <th class="text-center">Transaction ID</th>
                                        <th>From<br>To</th>
                                        <th>Party Name</th>
                                        <th class="text-center">Details</th>
                                        <th class="text-right">Debit</th>
                                        <th class="text-right">Credit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($transactions_list as $row) { ?>
                                        <tr>
                                            <td><?php echo date("Y-m-d", strtotime($row['created_date']))."<br>".date("H:i:s", strtotime($row['created_date'])); ?></td>
                                            <td><?php echo $row['transaction_reference_no']; ?></td>
                                            <td><?php if($row['type'] == "debit"){ echo "To"; } else if($row['type'] == "credit") { echo "From"; } ?></td>
                                            <td><?php echo $row['first_name']." ".$row['last_name']; ?></td>
                                            <td><?php echo $row['details']; ?></td>
                                            <td class="text-right"><?php if($row['type'] == "debit"){ echo $row['transaction_amount']; } else { echo "&nbsp;"; } ?></td>
                                            <td class="text-right"><?php if($row['type'] == "credit"){ echo $row['transaction_amount']; } else { echo "&nbsp;"; } ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="add_qualification_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Qualification</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="<?php echo base_url(); ?>admin-qualification/create" method="post" role="form" style="display: block;">
                            <div class="form-group">
                                <label for="qualification_name">Qualification Name</label>
                                <input type="text" id="qualification_name" name="name" class="form-control" maxlength="60" required="required"/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" id="description" name="description" class="form-control" maxlength="250" required="required"/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="submit" class="form-control btn btn-info" value="Add Now">
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="edit_qualification_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Qualification</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="<?php echo base_url(); ?>admin-qualification/update-details" method="post" role="form">
                            <input type="hidden" id="edit_qualification_id" name="qualification_id" value=""/>
                            <div class="form-group">
                                <label for="edit_qualification_name">Qualification Name</label>
                                <input type="text" id="edit_qualification_name" name="name" class="form-control" maxlength="60" required="required"/>
                            </div>
                            <div class="form-group">
                                <label for="edit_description">Description</label>
                                <input type="text" id="edit_description" name="description" class="form-control" maxlength="250" required="required"/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="submit" class="form-control btn btn-info" value="Update">
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/admin_footer'); ?>