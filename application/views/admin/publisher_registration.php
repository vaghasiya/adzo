<?php $this->load->view('elements/admin_header'); ?>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="25258">0</span>
                        </div>
                        <div class="desc"> Total Registered Publishers </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="2525">0</span></div>
                        <div class="desc"> Active Publishers </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="43535">0</span>
                        </div>
                        <div class="desc"> Suspended </div> 
                    </div>
                </a>
            </div>   
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="1549">0</span>
                        </div>
                        <div class="desc"> Success </div>
                    </div>
                </a>
            </div> 
        </div>

        <div class="container">
            <ul class="nav nav-tabs">
                <li class="active h4"><a data-toggle="tab" href="#home">Registration &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                <li class="h4"><a data-toggle="tab" href="#menu2">Country – Summary </a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="page-content-col">
                        <div class="mt-bootstrap-tables">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-social-dribbble font-dark hide"></i>
                                                <span class="caption-subject font-dark bold uppercase">Registration</span>
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body flip-scroll">
                                            <table class="table table-bordered table-striped table-condensed flip-content">
                                                <thead class="flip-content">
                                                    <tr>
                                                        <th width="20%"> Full Name </th>
                                                        <th class="text"> Publisher ID </th>
                                                        <th class="numeric"> Country </th>
                                                        <th class="numeric"> Email address </th>
                                                        <th class="numeric"> Phone Number </th>
                                                        <th class="text-center"> Actions </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td> Adzo India Pvt Ltd. </td>
                                                        <td> 25255514545 </td>
                                                        <td class="numeric"> India </td>
                                                        <td class="numeric"> admin@exaample.com </td>
                                                        <td class="numeric"> 2585461821 </td>
                                                        <td class="text-center"><a href="#" class="btn red btn-xs">Suspend/Revoke</a>
                                                            <a href="#" class="btn blue btn-xs">Message</a>
                                                            <a class="btn purple btn-xs" data-toggle="modal" href="#basic"> View more</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Adzo India Pvt Ltd. </td>
                                                        <td> 25255514545 </td>
                                                        <td class="numeric"> India </td>
                                                        <td class="numeric"> admin@exaample.com </td>
                                                        <td class="numeric"> 2585461821 </td>
                                                        <td class="text-center"><a href="#" class="btn red btn-xs">Suspend/Revoke</a>
                                                            <a href="#" class="btn blue btn-xs">Message</a>
                                                            <a class="btn purple btn-xs" data-toggle="modal" href="#basic"> View more</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Adzo India Pvt Ltd. </td>
                                                        <td> 25255514545 </td>
                                                        <td class="numeric"> India </td>
                                                        <td class="numeric"> admin@exaample.com </td>
                                                        <td class="numeric"> 2585461821 </td>
                                                        <td class="text-center"><a href="#" class="btn red btn-xs">Suspend/Revoke</a>
                                                            <a href="#" class="btn blue btn-xs">Message</a>
                                                            <a class="btn purple btn-xs" data-toggle="modal" href="#basic"> View more</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Adzo India Pvt Ltd. </td>
                                                        <td> 25255514545 </td>
                                                        <td class="numeric"> India </td>
                                                        <td class="numeric"> admin@exaample.com </td>
                                                        <td class="numeric"> 2585461821 </td>
                                                        <td class="text-center"><a href="#" class="btn red btn-xs">Suspend/Revoke</a>
                                                            <a href="#" class="btn blue btn-xs">Message</a>
                                                            <a class="btn purple btn-xs" data-toggle="modal" href="#basic"> View more</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Adzo India Pvt Ltd. </td>
                                                        <td> 25255514545 </td>
                                                        <td class="numeric"> India </td>
                                                        <td class="numeric"> admin@exaample.com </td>
                                                        <td class="numeric"> 2585461821 </td>
                                                        <td class="text-center"><a href="#" class="btn red btn-xs">Suspend/Revoke</a>
                                                            <a href="#" class="btn blue btn-xs">Message</a>
                                                            <a class="btn purple btn-xs" data-toggle="modal" href="#basic"> View more</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> ARDENT LEISURE GROUP </td>
                                                        <td> 25255514545 </td>
                                                        <td class="numeric"> India </td>
                                                        <td class="numeric"> admin@exaample.com </td>
                                                        <td class="numeric"> 2585461821 </td>
                                                        <td class="text-center"><a href="#" class="btn red btn-xs">Suspend/Revoke</a>                                                                <a href="#" class="btn blue btn-xs">Message</a>
                                                            <a class="btn purple btn-xs" data-toggle="modal" href="#basic"> View more</a>
                                                        </td>
                                                    </tr>                                                        
                                                </tbody>
                                            </table>
                                            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">View More</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="page-content-container">
                                                                <div class="page-content-row">
                                                                    <div class="page-sidebar">
                                                                        <div class="page-content-col">
                                                                            <div class="row">
                                                                                <div class="col-md-12">                                                        
                                                                                    <div class="profile-content">
                                                                                        <div class="row">
                                                                                            <div class="col-md-3">
                                                                                                <div class="profile-sidebar">
                                                                                                    <div class="portlet light profile-sidebar-portlet bordered">
                                                                                                        <div class="profile-userpic">
                                                                                                            <img src="/assets/pages/media/profile/profile_user.jpg"  class="img-responsive" alt=""> </div>
                                                                                                        <div class="profile-usertitle">
                                                                                                            <p></p>
                                                                                                            <p></p>
                                                                                                            <div class="text-info"><b>Name:</b>Adminuser</div>
                                                                                                            <div class="text-info"><b>Username :</b>unhdnfns</div>
                                                                                                            <div class="text-info"><b>Publisher ID:</b>252515452</div>
                                                                                                            <div class="text-info"><b>Email :</b>admin@gamil.com</div>
                                                                                                            <div class="text-info"><b>Address:</b>3997</div>
                                                                                                            <div class="text-info"><b>State:</b>Karnataka</div>
                                                                                                            <div class="text-info"><b>Country:</b>India</div>
                                                                                                            <div class="text-info"><b>Zip code:</b>560085</div>
                                                                                                            <div class="text-info"><b>Phone :</b>21514253614</div>
                                                                                                            <div class="text-info"><b>LinkedIn:</b>xdvdvdz</div>
                                                                                                            <div class="text-info"><b>Facebook:</b>vcv</div>
                                                                                                            <div class="text-info"><b>Twitter:</b>vcvdfdv</div>
                                                                                                            <div class="text-info"><b>Created:</b>65-12-1205</div>
                                                                                                            <div class="text-info"><b>Status:</b>Active</div>
                                                                                                        </div>                                                
                                                                                                    </div>                                
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-9">
                                                                                                <div class="row">
                                                                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                                        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                                                                                            <div class="visual">
                                                                                                                <i class="fa fa-comments"></i>
                                                                                                            </div>
                                                                                                            <div class="details">
                                                                                                                <div class="number">
                                                                                                                    <span data-counter="counterup" data-value="1349">0</span>
                                                                                                                </div>
                                                                                                                <div class="desc"> Total Task Published </div>
                                                                                                            </div>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                                        <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                                                                                            <div class="visual">
                                                                                                                <i class="fa fa-bar-chart-o"></i>
                                                                                                            </div>
                                                                                                            <div class="details">
                                                                                                                <div class="number">
                                                                                                                    <span data-counter="counterup" data-value="12258">0</span></div>
                                                                                                                <div class="desc"> Approved </div>
                                                                                                            </div>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                                        <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                                                                                            <div class="visual">
                                                                                                                <i class="fa fa-shopping-cart"></i>
                                                                                                            </div>
                                                                                                            <div class="details">
                                                                                                                <div class="number">
                                                                                                                    <span data-counter="counterup" data-value="549">0</span>
                                                                                                                </div>
                                                                                                                <div class="desc"> Rejected </div>
                                                                                                            </div>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                                        <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                                                                                            <div class="visual">
                                                                                                                <i class="fa fa-globe"></i>
                                                                                                            </div>
                                                                                                            <div class="details">
                                                                                                                <div class="number"> +
                                                                                                                    <span data-counter="counterup" data-value="8958"></span>% </div>
                                                                                                                <div class="desc"> Pending </div>
                                                                                                            </div>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                                        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                                                                                            <div class="visual">
                                                                                                                <i class="fa fa-comments"></i>
                                                                                                            </div>
                                                                                                            <div class="details">
                                                                                                                <div class="number">
                                                                                                                    <span data-counter="counterup" data-value="1349">0</span>
                                                                                                                </div>
                                                                                                                <div class="desc">Total Payment</div>
                                                                                                            </div>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                                                                        <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                                                                                            <div class="visual">
                                                                                                                <i class="fa fa-bar-chart-o"></i>
                                                                                                            </div>
                                                                                                            <div class="details">
                                                                                                                <div class="number">
                                                                                                                    <span data-counter="counterup" data-value="12,5">0</span></div>
                                                                                                                <div class="desc">Account Balance</div>
                                                                                                            </div>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="portlet light bordered">                                                
                                                                                                        <div class="table-scrollable table-scrollable-borderless">
                                                                                                            <table class="table table-hover table-light">
                                                                                                                <thead>
                                                                                                                    <tr class="uppercase">
                                                                                                                        <th colspan="2"> Date/Month </th>
                                                                                                                        <th> Tasks published </th>
                                                                                                                        <th> Approved </th>
                                                                                                                        <th> Rejected </th>
                                                                                                                        <th> Pending </th>
                                                                                                                        <th> Amount paid </th>
                                                                                                                    </tr>
                                                                                                                </thead>
                                                                                                                <tr>
                                                                                                                    <td class="fit">
                                                                                                                        <img class="user-pic" src="/assets/pages/media/users/avatar4.jpg"> </td>
                                                                                                                    <td>
                                                                                                                        <a href="javascript:;" class="primary-link">Brain</a>
                                                                                                                    </td>
                                                                                                                    <td> $345 </td>
                                                                                                                    <td> 45 </td>
                                                                                                                    <td> 124 </td>
                                                                                                                    <td> 124 </td>
                                                                                                                    <td>
                                                                                                                        <span class="bold theme-font">80%</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="fit">
                                                                                                                        <img class="user-pic" src="../assets/pages/media/users/avatar5.jpg"> </td>
                                                                                                                    <td>
                                                                                                                        <a href="javascript:;" class="primary-link">Nick</a>
                                                                                                                    </td>
                                                                                                                    <td> $560 </td>
                                                                                                                    <td> 12 </td>
                                                                                                                    <td> 24 </td>
                                                                                                                    <td> 124 </td>
                                                                                                                    <td>
                                                                                                                        <span class="bold theme-font">67%</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="fit">
                                                                                                                        <img class="user-pic" src="../assets/pages/media/users/avatar6.jpg"> </td>
                                                                                                                    <td>
                                                                                                                        <a href="javascript:;" class="primary-link">Tim</a>
                                                                                                                    </td>
                                                                                                                    <td> $1,345 </td>
                                                                                                                    <td> 450 </td>
                                                                                                                    <td> 46 </td>
                                                                                                                    <td> 124 </td>
                                                                                                                    <td>
                                                                                                                        <span class="bold theme-font">98%</span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="fit">
                                                                                                                        <img class="user-pic" src="../assets/pages/media/users/avatar7.jpg"> </td>
                                                                                                                    <td>
                                                                                                                        <a href="javascript:;" class="primary-link">Tom</a>
                                                                                                                    </td>
                                                                                                                    <td> $645 </td>
                                                                                                                    <td> 50 </td>
                                                                                                                    <td> 89 </td>
                                                                                                                    <td> 124 </td>
                                                                                                                    <td>
                                                                                                                        <span class="bold theme-font">58%</span>
                                                                                                                    </td>
                                                                                                                </tr>                                                                                                                
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>                                    
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!---------------------tab 3 ------------->

                <div id="menu2" class="tab-pane fade">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-social-dribbble font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Country – Summary</span>
                            </div>                            
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <th width="20%"> Country Name </th>
                                        <th class="text"> Registered users </th>
                                        <th class="text">Active users</th>
                                        <th class="numeric text-center">Suspended users</th>
                                    </tr>
                                </thead>
                                <tbody>                                                    
                                    <tr>
                                        <td> India</td>
                                        <td> 12586 </td>
                                        <td class="numeric"> 585456 </td>
                                        <td class="numeric text-center"> 25486 </td>
                                    </tr>
                                    <tr>
                                        <td> India</td>
                                        <td> 12586 </td>
                                        <td class="numeric"> 585456 </td>
                                        <td class="numeric text-center"> 25486 </td>
                                    </tr>
                                    <tr>
                                        <td> India</td>
                                        <td> 12586 </td>
                                        <td class="numeric"> 585456 </td>
                                        <td class="numeric text-center"> 25486 </td>
                                    </tr>
                                    <tr>
                                        <td> India</td>
                                        <td> 12586 </td>
                                        <td class="numeric"> 585456 </td>
                                        <td class="numeric text-center"> 25486 </td>
                                    </tr>
                                    <tr>
                                        <td> India</td>
                                        <td> 12586 </td>
                                        <td class="numeric"> 585456 </td>
                                        <td class="numeric text-center"> 25486 </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/admin_footer'); ?>
