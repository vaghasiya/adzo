<?php $this->load->view('elements/admin_header',array("title"=>"Manage Publishers","active_menu"=>"manage","sub_menu" => "manage_publishers")); ?>
<script type="application/javascript">
    $(document).ready(function() {
        $('#users_table').DataTable({
            order: [[ 0, "desc" ]]
        });

        $('#country_summary_table').DataTable({
            order: [[ 0, "desc" ]]
        });

        $("#users_table").on("click",".user_details",function(){
            var base_path = "<?php echo base_url().PATH_PROFILE_PIC; ?>";
            var user = $(this).data('details');
            console.log(user);
            var address = "";
            if(user.address1 != ""){
                address += user.address1;
            }
            if(user.address2 != ""){
                address += " "+user.address2;
            }
            if(user.address3 != ""){
                address += " ".user.address3;
            }
            $("#user_modal_profile_pic").attr("src",base_path+"/"+user.profile_pic);
            $("#user_modal_unique_id").text(user.unique_user_id);
            $("#user_modal_full_name").text(user.first_name+" "+user.last_name);
            $("#user_modal_user_name").text(user.user_name);
            $("#user_modal_email").text(user.email);
            $("#user_modal_address").html("<br>"+address);
            $("#user_modal_state").text(user.state);
            $("#user_modal_country").text(user.country);
            $("#user_modal_pin").text(user.pin);
            $("#user_modal_mobile_number").text(user.mobile_number);
            $("#user_modal_register_date").html("<br>"+user.register_date);
            $("#user_modal_submitted").text(user.submitted);
            $("#user_modal_approved").text(user.approved);
            $("#user_modal_rejected").text(user.rejected);
            $("#user_modal_review_pending").text(user.review_pending);
            $("#user_modal_in_progress").text(user.in_progress);
//            $("#user_modal_expired").text(user.expired);
            $("#user_modal_reassigned").text(user.reassigned);
            $("#user_modal_published").text(user.published);
            $("#user_modal_payments").text(user.payments);
            $("#user_modal_balance").text(user.balance);

            $("#user_modal_submitted").data("value",user.submitted);
            $("#user_modal_approved").data("value",user.approved);
            $("#user_modal_rejected").data("value",user.rejected);
            $("#user_modal_review_pending").data("value",user.review_pending);
            $("#user_modal_in_progress").data("value",user.in_progress);
//            $("#user_modal_expired").data("value",user.expired);
            $("#user_modal_reassigned").data("value",user.reassigned);
            $("#user_modal_published").data("value",user.published);
            $("#user_modal_payments").data("value",user.payments);
            $("#user_modal_balance").data("value",user.balance);
            $("#user_details").modal("show");
        });

        $("#users_table").on("click",".suspend_user_button",function(){
            var id = $(this).data("id");
            var post_data = {"user_id": id};
            ajax_request("<?php echo base_url(); ?>admin/ajax-suspend-user", post_data, function (result, element) {
                result = JSON.parse(result);
                if(result.status == "error"){
                    swal("Opps, Error Occurred",result.message,"error");
                } else {
                    $(element).addClass("revoke_user_button");
                    $(element).addClass("green");
                    $(element).removeClass("suspend_user_button");
                    $(element).removeClass("red");
                    $(element).html("Revoke");
                    swal("Process Completed",result.message,"success");
                }
            }, $(this));
        });

        $("#users_table").on("click",".revoke_user_button",function(){
            var id = $(this).data("id");
            var post_data = {"user_id": id};
            ajax_request("<?php echo base_url(); ?>admin/ajax-revoke-user", post_data, function (result, element) {
                result = JSON.parse(result);
                if(result.status == "error"){
                    swal("Opps, Error Occurred",result.message,"error");
                } else {
                    $(element).addClass("suspend_user_button");
                    $(element).addClass("red");
                    $(element).removeClass("revoke_user_button");
                    $(element).removeClass("green");
                    $(element).html("Suspend");
                    swal("Process Completed",result.message,"success");
                }
            }, $(this));
        });
    });
</script>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['registered']; ?>"><?php echo $users_count['registered']; ?></span>
                        </div>
                        <div class="desc">Registered Publishers</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['verified']; ?>"><?php echo $users_count['verified']; ?></span>
                        </div>
                        <div class="desc">Verified</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['unverified']; ?>"><?php echo $users_count['unverified']; ?></span>
                        </div>
                        <div class="desc">Unverified</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['suspended']; ?>"><?php echo $users_count['suspended']; ?></span>
                        </div>
                        <div class="desc">Suspended</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['active']; ?>"><?php echo $users_count['active']; ?></span></div>
                        <div class="desc">Active Publishers</div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row">
            <?php
            $class = $this -> session -> flashdata('class');
            $message = $this -> session -> flashdata('message');
            if(empty($class)) $class = "danger";
            if(!empty($message)){ ?>
                <div class="alert alert-<?php echo $class; ?>">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span class="message-text"><?php echo $message; ?></span>
                </div>
            <?php } ?>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#users_tab">Publisher Details</a></li>
                <li><a data-toggle="tab" href="#country_summary_tab">Country – Summary</a></li>
            </ul>
            <div class="tab-content">
                <div id="users_tab" class="tab-pane fade in active">
                    <div class="page-content-col">
                        <div class="mt-bootstrap-tables">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body flip-scroll">
                                            <table class="table table-bordered table-striped table-condensed" id="users_table">
                                                <thead>
                                                <tr>
                                                    <th>Registration<br>Date-Time</th>
                                                    <th>Publisher ID</th>
                                                    <th>Full Name</th>
                                                    <th>Country</th>
                                                    <th>Email Address</th>
                                                    <th>Mobile Number</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($users_list as $row) { $row['register_date'] = date("d-m-Y H:i:s", strtotime($row['register_date'])); ?>
                                                    <tr>
                                                        <td><?php echo date("d-m-Y", strtotime($row['register_date']))."<br>".date("H:i:s", strtotime($row['register_date'])); ?></td>
                                                        <td><?php echo $row['unique_user_id']; ?></td>
                                                        <td><?php echo $row['first_name'] . " " . $row['last_name']; ?></td>
                                                        <td><?php echo $row['country']; ?></td>
                                                        <td><?php echo $row['email']; ?></td>
                                                        <td><?php echo $row['mobile_number']; ?></td>
                                                        <td class="text-center">
                                                            <a href="javascript:;" class="btn btn-xs <?php if(empty($row['status'])){ echo "green revoke_user_button"; } else { echo "red suspend_user_button"; } ?>" data-id="<?php echo $row['user_id']; ?>"><?php if(empty($row['status'])){ echo "Revoke"; } else { echo "Suspend"; } ?></a>
                                                            <a href="javascript:;" class="btn blue btn-xs" data-id="<?php echo $row['user_id']; ?>">Message</a>
                                                            <a class="btn purple btn-xs user_details" data-details='<?php echo json_encode($row); ?>'>View</a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="country_summary_tab" class="tab-pane fade">
                    <div class="portlet light bordered">
                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed" id="country_summary_table">
                                <thead>
                                <tr>
                                    <th>Country Name</th>
                                    <th>Registered Users</th>
                                    <th>Verified Users</th>
                                    <th>Unverified Users</th>
                                    <th>Suspended Users</th>
                                    <th>Active Users</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($country_summary_list as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['country']; ?></td>
                                        <td><?php echo $row['registered']; ?></td>
                                        <td><?php echo $row['verified']; ?></td>
                                        <td><?php echo $row['unverified']; ?></td>
                                        <td><?php echo $row['suspended']; ?></td>
                                        <td><?php echo $row['active']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="user_details" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Worker Details</h4>
            </div>
            <div class="modal-body">
                <div class="page-content-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="profile-sidebar">
                                            <div class="portlet light profile-sidebar-portlet bordered">
                                                <img id="user_modal_profile_pic" src="<?php echo base_url(); ?>/assets/pages/media/profile/profile_user.jpg"  class="img-responsive" alt="" style="max-width: 70%; margin: 0px auto 10px;">
                                                <div class="profile-usertitle">
                                                    <div class="text-info"><b>Worker ID:&nbsp;</b><span id="user_modal_unique_id"></span></div>
                                                    <div class="text-info"><b>Name:&nbsp;</b><span id="user_modal_full_name"></span></div>
                                                    <div class="text-info"><b>User Name:&nbsp;</b><span id="user_modal_user_name"></span></div>
                                                    <div class="text-info"><b>Email:&nbsp;</b><span id="user_modal_email"></span></div>
                                                    <div class="text-info"><b>Address:&nbsp;</b><span id="user_modal_address"></span></div>
                                                    <div class="text-info"><b>State:&nbsp;</b><span id="user_modal_state"></span></div>
                                                    <div class="text-info"><b>Country:&nbsp;</b><span id="user_modal_country"></span></div>
                                                    <div class="text-info"><b>Pin Code:&nbsp;</b><span id="user_modal_pin"></span></div>
                                                    <div class="text-info"><b>Mobile:&nbsp;</b><span id="user_modal_mobile_number"></span></div>
                                                    <div class="text-info"><b>Registered On:&nbsp;</b><span id="user_modal_register_date"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-comments"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_submitted">0</span>
                                                        </div>
                                                        <div class="desc">Submitted</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-bar-chart-o"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_approved">0</span>
                                                        </div>
                                                        <div class="desc">Approved</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_rejected">0</span>
                                                        </div>
                                                        <div class="desc">Rejected</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-globe"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_review_pending"></span>
                                                        </div>
                                                        <div class="desc">Review Pending</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-globe"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_in_progress"></span>
                                                        </div>
                                                        <div class="desc">In Progress</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-globe"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_reassigned"></span>
                                                        </div>
                                                        <div class="desc">Reassigned</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-globe"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_published"></span>
                                                        </div>
                                                        <div class="desc">Published</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-comments"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_payments">0</span>
                                                        </div>
                                                        <div class="desc">Total Paid</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-bar-chart-o"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_balance">0</span>
                                                        </div>
                                                        <div class="desc">Account Balance</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/admin_footer'); ?>
