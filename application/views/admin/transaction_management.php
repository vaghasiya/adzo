<?php $this->load->view('elements/admin_header'); ?>
<div class="container-fluid">
    <div class="page-content">
        <div class="container">
            <ul class="nav nav-tabs">
                <li class="active h4"><a data-toggle="tab" href="#home">Publisher &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                <li class="h4"><a data-toggle="tab" href="#menu1">Worker &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">

                    <ul class="nav nav-tabs">
                        <li class="active h6"><a data-toggle="tab" href="#subm1">Batches in Progress &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                        <li class="h6"><a data-toggle="tab" href="#subm2">Transaction history - Payment &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                        <li class="h6"><a data-toggle="tab" href="#subm3">Transaction History – Tasks &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                        <li class="h6"><a data-toggle="tab" href="#subm4">Bonus withdrawal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                        <li class="h6"><a data-toggle="tab" href="#subm5">Bonus Payment &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="subm1" class="tab-pane fade in active">
                            <div class="portlet-body flip-scroll">
                                <div class="row">                                    
                                    <div class="col-md-6">
                                        <table class="table table-bordered table-striped table-condensed flip-content">                                    
                                            <tbody>
                                                <tr>
                                                    <td><b>Total Balance:</b></td>  
                                                    <td> $1100.00</td>
                                                    <td><a class="btn btn-success" href="#">Add/Deduct Funds</a> </td>  
                                                </tr> 
                                                <tr>
                                                    <td><b>Fund held for liability :</b></td>
                                                    <td>$2525</td>     
                                                    <td><a class="btn btn-warning" href="#">Add/Deduct Funds</a> </td>                                       
                                                </tr> 
                                                <tr>
                                                    <td><b>Total amount for Adzo:</b></td>
                                                    <td>$25</td>
                                                    <td><a class="btn btn-danger" href="#">Add/Deduct Funds</a> </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Bonus Fund:</b></td>
                                                    <td>$25</td>
                                                    <td><a class="btn btn-info" href="#">Add/Deduct Funds</a> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body flip-scroll">
                                <div class="row">                                    
                                    <div class="col-md-4">
                                        <h4 class="text-info"><strong>Pending</strong></h4>
                                        <table  class="table table-bordered table-striped table-condensed flip-content" border="2">                                    
                                            <tbody>
                                                <tr>
                                                    <td><b>Total Tasks approved:</b></td>
                                                    <td>85054</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Amount to transfer:</b></td>
                                                    <td>$545454.00</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Adzo fee:</b></td>
                                                    <td>$212.00</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Bonus:</b></td>
                                                    <td>$255</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Total:</b></td>
                                                    <td>$2525</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Transfer:</b></td>
                                                    <td>$2525</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-success"><strong>Transaction History</strong></h4>
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th width="15%"> Date</th>
                                                <th class="text">Task approved</th>
                                                <th class="numeric">Worker’s reward</th>
                                                <th class="numeric">Adzo Fee</th>
                                                <th class="numeric">Grant Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center"> 12-12-2016 </td>
                                                <td> 252555 </td>
                                                <td class="numeric">$545454.00 </td>
                                                <td class="numeric">$212.00</td>
                                                <td class="numeric"> $56465545.00 </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center"> 12-12-2016 </td>
                                                <td> 252555 </td>
                                                <td class="numeric">$545454.00 </td>
                                                <td class="numeric">$212.00</td>
                                                <td class="numeric"> $56465545.00 </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center"> 12-12-2016 </td>
                                                <td> 252555 </td>
                                                <td class="numeric">$545454.00 </td>
                                                <td class="numeric">$212.00</td>
                                                <td class="numeric"> $56465545.00 </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center"> 12-12-2016 </td>
                                                <td> 252555 </td>
                                                <td class="numeric">$545454.00 </td>
                                                <td class="numeric">$212.00</td>
                                                <td class="numeric"> $56465545.00 </td>
                                            </tr>
                                        </tbody>
                                    </table>   
                                </div>
                            </div>                            
                        </div>
                        <div id="subm2" class="tab-pane fade in">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <th class="text-center" width="15%"> Date</th>
                                        <th class="text"> Publisher ID</th>
                                        <th class="numeric">Transaction ID</th>
                                        <th class="numeric">Amount</th>
                                        <th class="text-center"> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td> 252555 </td>
                                        <td class="numeric">125472</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="text-center">
                                            <a href="#" class="btn blue btn-xs">Add/Deduct</a>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td> 252555 </td>
                                        <td class="numeric">125472</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="text-center">
                                            <a href="#" class="btn blue btn-xs">Add/Deduct</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td> 252555 </td>
                                        <td class="numeric">125472</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="text-center">
                                            <a href="#" class="btn blue btn-xs">Add/Deduct</a>
                                        </td>
                                    </tr><tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td> 252555 </td>
                                        <td class="numeric">125472</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="text-center">
                                            <a href="#" class="btn blue btn-xs">Add/Deduct</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td> 252555 </td>
                                        <td class="numeric">125472</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="text-center">
                                            <a href="#" class="btn blue btn-xs">Add/Deduct</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td> 252555 </td>
                                        <td class="numeric">125472</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="text-center">
                                            <a href="#" class="btn blue btn-xs">Add/Deduct</a>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <div id="subm3" class="tab-pane fade in">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <th class="text-center" width="15%"> Date</th>
                                        <th class="text"> Publisher</th>
                                        <th class="numeric">Batch ID</th>
                                        <th class="numeric">Published Tasks</th>
                                        <th class="numeric">Submitted</th>
                                        <th class="numeric">Rejected</th>
                                        <th class="numeric">Pending</th>
                                        <th class="numeric">Reward</th>
                                        <th class="numeric">Reward</th>
                                        <th class="numeric">Adzo Fee</th>
                                        <th class="numeric">Grant fee</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric">LIJO JOHN</td>
                                        <td> 252555 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> 520 </td>
                                        <td class="numeric"> 500 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> 10 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="numeric">$252</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric">LIJO JOHN</td>
                                        <td> 252555 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> 520 </td>
                                        <td class="numeric"> 500 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> 10 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="numeric">$252</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric">LIJO JOHN</td>
                                        <td> 252555 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> 520 </td>
                                        <td class="numeric"> 500 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> 10 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="numeric">$252</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric">LIJO JOHN</td>
                                        <td> 252555 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> 520 </td>
                                        <td class="numeric"> 500 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> 10 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="numeric">$252</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric">LIJO JOHN</td>
                                        <td> 252555 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> 520 </td>
                                        <td class="numeric"> 500 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> 10 </td>
                                        <td class="numeric">10</td>
                                        <td class="numeric"> $25 </td>
                                        <td class="numeric">$252</td>
                                    </tr>                                    
                                </tbody>
                            </table>                                                                                    
                        </div>
                        <div id="subm4" class="tab-pane fade in">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <th class="text-center" width="15%"> Date</th>
                                        <th class="text"> Publisher ID</th>
                                        <th class="numeric">Transaction ID</th>
                                        <th class="numeric">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric"> 2252521</td>
                                        <td class="text"> 12541 </td>
                                        <td class="numeric"> 82255 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric"> 2252521</td>
                                        <td class="text"> 12541 </td>
                                        <td class="numeric"> 82255 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric"> 2252521</td>
                                        <td class="text"> 12541 </td>
                                        <td class="numeric"> 82255 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric"> 2252521</td>
                                        <td class="text"> 12541 </td>
                                        <td class="numeric"> 82255 </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="subm5" class="tab-pane fade in">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <th class="text-center" width="15%"> Date</th>
                                        <th class="text"> Assignment ID</th>
                                        <th class="numeric">Publisher ID</th>
                                        <th class="numeric">Worker ID</th>
                                        <th class="numeric">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric"> 2252521</td>
                                        <td class="text"> 12541 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> $520 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric"> 2252521</td>
                                        <td class="text"> 12541 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> $520 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric"> 2252521</td>
                                        <td class="text"> 12541 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> $520 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"> 12-12-2016 </td>
                                        <td class="numeric"> 2252521</td>
                                        <td class="text"> 12541 </td>
                                        <td class="numeric"> 82255 </td>
                                        <td class="numeric"> $520 </td>
                                    </tr>
                                </tbody>
                            </table>                            
                        </div>
                    </div>                        
                </div>
                <div id="menu1" class="tab-pane fade in">
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content">
                            <tr>
                                <th class="text-center" width="15%"> Date</th>
                                <th class="text"> Worker ID</th>
                                <th class="numeric">Assignment ID</th>
                                <th class="numeric">Publisher ID</th>
                                <th class="numeric">Transaction ID</th>
                                <th class="numeric">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> 1254112 </td>
                                <td class="numeric"> 1282255 </td>
                                <td class="numeric"> 986325 </td>
                                <td class="numeric"> $520 </td>
                            </tr>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> 1254112 </td>
                                <td class="numeric"> 1282255 </td>
                                <td class="numeric"> 986325 </td>
                                <td class="numeric"> $520 </td>
                            </tr>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> 1254112 </td>
                                <td class="numeric"> 1282255 </td>
                                <td class="numeric"> 986325 </td>
                                <td class="numeric"> $520 </td>
                            </tr>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> 1254112 </td>
                                <td class="numeric"> 1282255 </td>
                                <td class="numeric"> 986325 </td>
                                <td class="numeric"> $520 </td>
                            </tr>
                        </tbody>
                    </table>
                    <h4 class="text-info"><strong>Workers – Withdraw Request</strong></h4>
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content">
                            <tr>
                                <th class="text-center" width="15%"> Date</th>
                                <th class="text"> Worker ID</th>
                                <th class="numeric">Withdrawn amount</th>
                                <th class="numeric">Charges</th>
                                <th class="numeric">Balance</th>
                                <th class="numeric text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> $450 </td>
                                <td class="numeric"> $25 </td>
                                <td class="numeric"> $55 </td>
                                <td class="text-center">
                                    <a href="#" class="btn blue btn-xs">Add fund</a>
                                    <a href="#" class="btn green btn-xs">Completed/pending</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> $450 </td>
                                <td class="numeric"> $25 </td>
                                <td class="numeric"> $55 </td>
                                <td class="text-center">
                                    <a href="#" class="btn blue btn-xs">Add fund</a>
                                    <a href="#" class="btn green btn-xs">Completed/pending</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> $450 </td>
                                <td class="numeric"> $25 </td>
                                <td class="numeric"> $55 </td>
                                <td class="text-center">
                                    <a href="#" class="btn blue btn-xs">Add fund</a>
                                    <a href="#" class="btn green btn-xs">Completed/pending</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> $450 </td>
                                <td class="numeric"> $25 </td>
                                <td class="numeric"> $55 </td>
                                <td class="text-center">
                                    <a href="#" class="btn blue btn-xs">Add fund</a>
                                    <a href="#" class="btn green btn-xs">Completed/pending</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"> 12-12-2016 </td>
                                <td class="numeric"> 2252521</td>
                                <td class="text"> $450 </td>
                                <td class="numeric"> $25 </td>
                                <td class="numeric"> $55 </td>
                                <td class="text-center">
                                    <a href="#" class="btn blue btn-xs">Add fund</a>
                                    <a href="#" class="btn green btn-xs">Completed/pending</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>                                        
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/admin_footer'); ?>