<?php $this->load->view('elements/admin_header',array("title"=>"Manage Workers","active_menu"=>"manage","sub_menu" => "manage_workers")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $('#users_table').DataTable({
                order: [[ 0, "desc" ]]
            });

            $('#country_summary_table').DataTable({
                order: [[ 0, "desc" ]]
            });

            $('#verification_table').DataTable({
                order: [[ 0, "desc" ]]
            });

            $('#withdrawals_table').DataTable({
                order: [[ 0, "desc" ]]
            });

            $("#users_table").on("click",".user_details",function(){
                var base_path = "<?php echo base_url().PATH_PROFILE_PIC; ?>";
                var user = $(this).data('details');
                console.log(user);
                var address = "";
                if(user.address1 != ""){
                    address += user.address1;
                }
                if(user.address2 != ""){
                    address += " "+user.address2;
                }
                if(user.address3 != ""){
                    address += " ".user.address3;
                }
                $("#user_modal_profile_pic").attr("src",base_path+"/"+user.profile_pic);
                $("#user_modal_unique_id").text(user.unique_user_id);
                $("#user_modal_full_name").text(user.first_name+" "+user.last_name);
                $("#user_modal_user_name").text(user.user_name);
                $("#user_modal_email").text(user.email);
                $("#user_modal_address").html("<br>"+address);
                $("#user_modal_state").text(user.state);
                $("#user_modal_country").text(user.country);
                $("#user_modal_pin").text(user.pin);
                $("#user_modal_mobile_number").text(user.mobile_number);
                $("#user_modal_register_date").html("<br>"+user.register_date);
                $("#user_modal_submitted").text(user.submitted);
                $("#user_modal_approved").text(user.approved);
                $("#user_modal_rejected").text(user.rejected);
                $("#user_modal_review_pending").text(user.review_pending);
                $("#user_modal_in_progress").text(user.in_progress);
                $("#user_modal_expired").text(user.expired);
                $("#user_modal_reassigned").text(user.reassigned);
                $("#user_modal_earning").text(user.earning);
                $("#user_modal_balance").text(user.balance);

                $("#user_modal_submitted").data("value",user.submitted);
                $("#user_modal_approved").data("value",user.approved);
                $("#user_modal_rejected").data("value",user.rejected);
                $("#user_modal_review_pending").data("value",user.review_pending);
                $("#user_modal_in_progress").data("value",user.in_progress);
                $("#user_modal_expired").data("value",user.expired);
                $("#user_modal_reassigned").data("value",user.reassigned);
                $("#user_modal_earning").data("value",user.earning);
                $("#user_modal_balance").data("value",user.balance);
                $("#user_details").modal("show");
            });

            $("#users_table").on("click",".suspend_user_button",function(){
                var id = $(this).data("id");
                var post_data = {"user_id": id};
                ajax_request("<?php echo base_url(); ?>admin/ajax-suspend-user", post_data, function (result, element) {
                    result = JSON.parse(result);
                    if(result.status == "error"){
                        swal("Opps, Error Occurred",result.message,"error");
                    } else {
                        $(element).addClass("revoke_user_button");
                        $(element).addClass("green");
                        $(element).removeClass("suspend_user_button");
                        $(element).removeClass("red");
                        $(element).html("Revoke");
                        swal("Process Completed",result.message,"success");
                    }
                }, $(this));
            });

            $("#users_table").on("click",".revoke_user_button",function(){
                var id = $(this).data("id");
                var post_data = {"user_id": id};
                ajax_request("<?php echo base_url(); ?>admin/ajax-revoke-user", post_data, function (result, element) {
                    result = JSON.parse(result);
                    if(result.status == "error"){
                        swal("Opps, Error Occurred",result.message,"error");
                    } else {
                        $(element).addClass("suspend_user_button");
                        $(element).addClass("red");
                        $(element).removeClass("revoke_user_button");
                        $(element).removeClass("green");
                        $(element).html("Suspend");
                        swal("Process Completed",result.message,"success");
                    }
                }, $(this));
            });

            $(".approve_verification_button").on("click",function(){
                var id = $(this).data('id');
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this process.!!!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, approve now.!!!'
                }).then(function () {
                    $("#approve_verification_id").val(id);
                    $("#approve_verification_form").submit();
                }, function(dismiss) {
                    $("#approve_verification_id").val("");
                });
            });

            $(".reject_verification_button").on("click",function(){
                var id = $(this).data('id');
                $("#reject_verification_id").val(id);
                $("#reject_verification_modal").modal("show");
            });

            $("#reject_verification_modal_submit_button").on("click",function(){
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this process.!!!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, reject now.!!!'
                }).then(function () {
                    var verification_rejection_comments = $("#verification_rejection_comments").val();
                    $("#verification_reject_comments").val(verification_rejection_comments);
                    $("#reject_verification_form").submit();
                }, function(dismiss) {
                    $("#reject_verification_id").val("");
                });
            });

            $(".view_documents_button").on("click",function(){
                var base_url = "<?php echo base_url().PATH_USER_DOCUMENTS."/"; ?>";
                var document1 = $(this).data('document1');
                var document2 = $(this).data('document2');
                var document3 = $(this).data('document3');
                $("#document1_image").attr("src",base_url+document1);
                $("#document2_image").attr("src",base_url+document2);
                $("#document3_image").attr("src",base_url+document3);
                $("#view_documents_modal").modal("show");
            });

            $(".approve_withdrawal_button").on("click",function(){
                var id = $(this).data('id');
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this process.!!!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, approve now.!!!'
                }).then(function () {
                    $("#approve_withdraw_id").val(id);
                    $("#approve_withdrawal_form").submit();
                }, function(dismiss) {
                    $("#approve_withdraw_id").val("");
                });
            });

            $(".reject_withdrawal_button").on("click",function(){
                var id = $(this).data('id');
                $("#reject_withdraw_id").val(id);
                $("#reject_withdrawal_modal").modal("show");
            });

            $("#reject_withdrawal_modal_submit_button").on("click",function(){
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this process.!!!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, reject now.!!!'
                }).then(function () {
                    var withdrawal_rejection_comments = $("#withdrawal_rejection_comments").val();
                    $("#withdrawal_reject_comments").val(withdrawal_rejection_comments);
                    $("#reject_withdrawal_form").submit();
                }, function(dismiss) {
                    $("#reject_withdraw_id").val("");
                });
            });
        });
    </script>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['registered']; ?>"><?php echo $users_count['registered']; ?></span>
                        </div>
                        <div class="desc">Registered Workers</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['verified']; ?>"><?php echo $users_count['verified']; ?></span>
                        </div>
                        <div class="desc">Verified</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['unverified']; ?>"><?php echo $users_count['unverified']; ?></span>
                        </div>
                        <div class="desc">Unverified</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['suspended']; ?>"><?php echo $users_count['suspended']; ?></span>
                        </div>
                        <div class="desc">Suspended</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?php echo $users_count['active']; ?>"><?php echo $users_count['active']; ?></span></div>
                        <div class="desc">Active Workers</div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row">
            <?php
            $class = $this -> session -> flashdata('class');
            $message = $this -> session -> flashdata('message');
            if(empty($class)) $class = "danger";
            if(!empty($message)){ ?>
                <div class="alert alert-<?php echo $class; ?>">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span class="message-text"><?php echo $message; ?></span>
                </div>
            <?php } ?>
            <ul class="nav nav-tabs">
                <li class="active "><a data-toggle="tab" href="#users_tab">Worker Details</a></li>
                <li><a data-toggle="tab" href="#country_summary_tab">Country – Summary</a></li>
                <li><a data-toggle="tab" href="#verification_tab">Verification Request</a></li>
                <li><a data-toggle="tab" href="#withdrawals_tab">Withdrawals Request</a></li>
            </ul>
            <div class="tab-content">
                <div id="users_tab" class="tab-pane fade in active">
                    <div class="page-content-col">
                        <div class="mt-bootstrap-tables">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-social-dribbble font-dark hide"></i>
                                                <span class="caption-subject font-dark bold uppercase">Worker Details</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <table class="table table-bordered table-striped table-condensed" id="users_table">
                                                <thead>
                                                    <tr>
                                                        <th>Registration<br>Date-Time</th>
                                                        <th>Worker ID</th>
                                                        <th>Full Name</th>
                                                        <th>Country</th>
                                                        <th>Email Address</th>
                                                        <th>Mobile Number</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($users_list as $row) { $row['register_date'] = date("d-m-Y H:i:s", strtotime($row['register_date'])); ?>
                                                    <tr>
                                                        <td><?php echo date("d-m-Y", strtotime($row['register_date']))."<br>".date("H:i:s", strtotime($row['register_date'])); ?></td>
                                                        <td><?php echo $row['unique_user_id']; ?></td>
                                                        <td><?php echo $row['first_name'] . " " . $row['last_name']; ?></td>
                                                        <td><?php echo $row['country']; ?></td>
                                                        <td><?php echo $row['email']; ?></td>
                                                        <td><?php echo $row['mobile_number']; ?></td>
                                                        <td class="text-center">
                                                            <a href="javascript:;" class="btn btn-xs <?php if(empty($row['status'])){ echo "green revoke_user_button"; } else { echo "red suspend_user_button"; } ?>" data-id="<?php echo $row['user_id']; ?>"><?php if(empty($row['status'])){ echo "Revoke"; } else { echo "Suspend"; } ?></a>
                                                            <a href="javascript:;" class="btn blue btn-xs" data-id="<?php echo $row['user_id']; ?>">Message</a>
                                                            <a class="btn purple btn-xs user_details" data-details='<?php echo json_encode($row); ?>'>View</a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="country_summary_tab" class="tab-pane fade">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-social-dribbble font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Country – Summary</span>
                            </div>                            
                        </div>
                        <div class="portlet-body">
                            <table class="table table-bordered table-striped table-condensed" id="country_summary_table">
                                <thead>
                                    <tr>
                                        <th>Country Name</th>
                                        <th>Registered Users</th>
                                        <th>Verified Users</th>
                                        <th>Unverified Users</th>
                                        <th>Suspended Users</th>
                                        <th>Active Users</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($country_summary_list as $row) { ?>
                                        <tr>
                                            <td><?php echo $row['country']; ?></td>
                                            <td><?php echo $row['registered']; ?></td>
                                            <td><?php echo $row['verified']; ?></td>
                                            <td><?php echo $row['unverified']; ?></td>
                                            <td><?php echo $row['suspended']; ?></td>
                                            <td><?php echo $row['active']; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="verification_tab" class="tab-pane fade">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-social-dribbble font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Verification Request</span>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table id="verification_table" class="table table-bordered table-striped" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Date<br>Time</th>
                                    <th>Worker ID</th>
                                    <th>Full Name</th>
                                    <th>Country</th>
                                    <th>Document<br>Number</th>
                                    <th>Date of Birth</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($verification_list as $row) { ?>
                                    <tr>
                                        <td><?php echo date("Y-m-d", strtotime($row['created_date']))."<br>".date("H:i:s", strtotime($row['created_date'])); ?></td>
                                        <td><?php echo $row['unique_user_id']; ?></td>
                                        <td><?php echo $row['full_name']; ?></td>
                                        <td><?php echo $row['country']; ?></td>
                                        <td><?php echo $row['document_number']; ?></td>
                                        <td><?php echo date("d-m-Y", strtotime($row['birthdate'])); ?></td>
                                        <td><?php echo $row['status']; ?></td>
                                        <td class="text-center">
                                            <?php if($row['status'] == "pending"){ ?>
                                                <a class="btn btn-success btn-xs approve_verification_button" data-id="<?php echo $row['verification_id']; ?>" href="javascript:;">Approve</a>
                                                <br/>
                                                <a class="btn btn-danger btn-xs reject_verification_button" data-id="<?php echo $row['verification_id']; ?>" href="javascript:;">Reject</a>
                                                <br/>
                                            <?php } ?>
                                            <a class="btn btn-info btn-xs view_documents_button" data-document1="<?php echo $row['document1']; ?>" data-document2="<?php echo $row['document2']; ?>" data-document3="<?php echo $row['document3']; ?>" href="javascript:;">View Documents</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="withdrawals_tab" class="tab-pane fade">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-social-dribbble font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Withdrawals Request</span>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table id="withdrawals_table" class="table table-bordered table-striped" width="100%" cellspacing="0" >
                                <thead>
                                    <tr>
                                        <th class="text-center">Date<br>Time</th>
                                        <th class="text-center">Worker ID</th>
                                        <th class="text-center">Worker Name</th>
                                        <th class="text-center">Withdraw ID</th>
                                        <th class="text-center">Transaction ID</th>
                                        <th class="text-center">Details</th>
                                        <th class="text-right">Withdrawal<br>Amount</th>
                                        <th class="text-right">Adzo<br>Charges</th>
                                        <th class="text-right">Net<br>Amount</th>
                                        <th class="text-right">Status</th>
                                        <th class="text-center">Completed<br>Date-Time</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($withdrawal_list as $row) { ?>
                                    <tr>
                                        <td><?php echo date("Y-m-d", strtotime($row['created_date']))."<br>".date("H:i:s", strtotime($row['created_date'])); ?></td>
                                        <td><?php echo $row['unique_user_id']; ?></td>
                                        <td><?php echo $row['first_name']."".$row['last_name']; ?></td>
                                        <td><?php echo $row['withdraw_reference_no']; ?></td>
                                        <td><?php echo $row['transaction_reference_no']; ?></td>
                                        <td><?php echo $row['comments']; ?></td>
                                        <td class="text-right"><?php echo $row['withdrawal_amount']; ?></td>
                                        <td class="text-right"><?php echo $row['adzo_fees_amount']; ?></td>
                                        <td class="text-right"><?php echo $row['total_amount']; ?></td>
                                        <td class="text-center"><?php echo $row['status']; ?></td>
                                        <td><?php if((empty($row['updated_date'])) || ($row['updated_date'] == "0000-00-00 00:00:00")){ echo "&nbsp;"; } else { echo date("Y-m-d", strtotime($row['updated_date']))."<br>".date("H:i:s", strtotime($row['updated_date'])); } ?></td>
                                        <td class="text-center">
                                            <?php if($row['status'] == "pending"){ ?>
                                            <a class="btn btn-success btn-xs approve_withdrawal_button" data-id="<?php echo $row['withdraw_id']; ?>" href="javascript:;">Approve</a>
                                            <br/>
                                            <a class="btn btn-danger btn-xs reject_withdrawal_button" data-id="<?php echo $row['withdraw_id']; ?>" href="javascript:;">Reject</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="user_details" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Worker Details</h4>
            </div>
            <div class="modal-body">
                <div class="page-content-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="profile-sidebar">
                                            <div class="portlet light profile-sidebar-portlet bordered">
                                                <img id="user_modal_profile_pic" src="<?php echo base_url(); ?>/assets/pages/media/profile/profile_user.jpg"  class="img-responsive" alt="" style="max-width: 70%; margin: 0px auto 10px;">
                                                <div class="profile-usertitle">
                                                    <div class="text-info"><b>Worker ID:&nbsp;</b><span id="user_modal_unique_id"></span></div>
                                                    <div class="text-info"><b>Name:&nbsp;</b><span id="user_modal_full_name"></span></div>
                                                    <div class="text-info"><b>User Name:&nbsp;</b><span id="user_modal_user_name"></span></div>
                                                    <div class="text-info"><b>Email:&nbsp;</b><span id="user_modal_email"></span></div>
                                                    <div class="text-info"><b>Address:&nbsp;</b><span id="user_modal_address"></span></div>
                                                    <div class="text-info"><b>State:&nbsp;</b><span id="user_modal_state"></span></div>
                                                    <div class="text-info"><b>Country:&nbsp;</b><span id="user_modal_country"></span></div>
                                                    <div class="text-info"><b>Pin Code:&nbsp;</b><span id="user_modal_pin"></span></div>
                                                    <div class="text-info"><b>Mobile:&nbsp;</b><span id="user_modal_mobile_number"></span></div>
                                                    <div class="text-info"><b>Registered On:&nbsp;</b><span id="user_modal_register_date"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-comments"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_submitted">0</span>
                                                        </div>
                                                        <div class="desc">Submitted</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-bar-chart-o"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_approved">0</span>
                                                        </div>
                                                        <div class="desc">Approved</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_rejected">0</span>
                                                        </div>
                                                        <div class="desc">Rejected</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-globe"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_review_pending"></span>
                                                        </div>
                                                        <div class="desc">Review Pending</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-globe"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_in_progress"></span>
                                                        </div>
                                                        <div class="desc">In Progress</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-globe"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_expired"></span>
                                                        </div>
                                                        <div class="desc">Expired</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-globe"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_reassigned"></span>
                                                        </div>
                                                        <div class="desc">Reassigned</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-comments"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_earning">0</span>
                                                        </div>
                                                        <div class="desc">Total Earned</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:;">
                                                    <div class="visual">
                                                        <i class="fa fa-bar-chart-o"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            <span data-counter="counterup" data-value="1" id="user_modal_balance">0</span>
                                                        </div>
                                                        <div class="desc">Account Balance</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="reject_withdrawal_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reject Withdrawal Request</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="withdrawal_rejection_comments">Rejection Comments</label>
                            <textarea name="comments" id="withdrawal_rejection_comments" class="form-control" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="button" class="form-control btn btn-info" value="Submit" id="reject_withdrawal_modal_submit_button">
                                </div>
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="view_documents_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">View Documents</h4>
            </div>
            <div class="modal-body">
                <div class="page-content-container">
                    <div class="row">
                        <div class="col-lg-4">
                            <img id="document1_image" src="" class="img-responsive" alt="No Image Found">
                        </div>
                        <div class="col-lg-4">
                            <img id="document2_image" src="" class="img-responsive" alt="No Image Found">
                        </div>
                        <div class="col-lg-4">
                            <img id="document3_image" src="" class="img-responsive" alt="No Image Found">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="reject_verification_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reject Verification Request</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="verification_rejection_comments">Rejection Comments</label>
                            <textarea name="comments" id="verification_rejection_comments" class="form-control" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="button" class="form-control btn btn-info" value="Submit" id="reject_verification_modal_submit_button">
                                </div>
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>admin-manage-workers/approve-verification-request" method="post" id="approve_verification_form">
    <input type="hidden" name="verification_id" id="approve_verification_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>admin-manage-workers/reject-verification-request" method="post" id="reject_verification_form">
    <input type="hidden" name="verification_id" id="reject_verification_id" value=""/>
    <input type="hidden" name="comments" id="verification_reject_comments" value=""/>
</form>
<form action="<?php echo base_url(); ?>admin-manage-workers/approve-withdrawal-request" method="post" id="approve_withdrawal_form">
    <input type="hidden" name="withdraw_id" id="approve_withdraw_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>admin-manage-workers/reject-withdrawal-request" method="post" id="reject_withdrawal_form">
    <input type="hidden" name="withdraw_id" id="reject_withdraw_id" value=""/>
    <input type="hidden" name="comments" id="withdrawal_reject_comments" value=""/>
</form>
<?php $this->load->view('elements/admin_footer'); ?>