<?php $this->load->view('elements/publisher_header', array("title"=>"Qualification Details","active_menu"=>"manage","sub_menu"=>"qualification")); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 style="margin-top: 0px;">Qualification Details:</h3>
                    <h4 style="margin-top: 10px; margin-bottom: 20px;"><strong>Name: </strong><?php echo $name; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Description: </strong><?php echo $description; ?></h4>
                    <?php
                    $class = $this -> session -> flashdata('class');
                    $message = $this -> session -> flashdata('message');
                    if(empty($class)) $class = "danger";
                    if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $class; ?>">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <span class="message-text"><?php echo $message; ?></span>
                        </div>
                    <?php } ?>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="request_table">
                            <thead>
                            <tr>
                                <th>Date-Time</th>
                                <th>Worker ID</th>
                                <th>Qualification Score</th>
                                <th>Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($request_list as $request){ ?>
                                <tr>
                                    <td><?php echo date("d-m-Y H:i:s", strtotime($request['created_date'])); ?></td>
                                    <td><?php echo $request['unique_user_id']; ?></td>
                                    <td><input type="text" value="<?php echo $request['score']; ?>" class="form-control IntegerOnly" id="score_<?php echo $request['qualification_request_id']; ?>"/></td>
                                    <td><?php echo $request['status']; ?></td>
                                    <th class="text-center">
                                        <a class="text-success update_request" data-id="<?php echo $request['qualification_request_id']; ?>" href="javascript:;">Update</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a class="text-danger remove_request" data-id="<?php echo $request['qualification_request_id']; ?>" href="javascript:;">Remove</a>
                                    </th>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>qualification/remove-request" method="post" id="remove_request_form">
    <input type="hidden" name="qualification_id" value="<?php echo $qualification_id; ?>"/>
    <input type="hidden" id="remove_request_id" name="qualification_request_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>qualification/update-request" method="post" id="update_request_form">
    <input type="hidden" name="qualification_id" value="<?php echo $qualification_id; ?>"/>
    <input type="hidden" id="update_request_id" name="qualification_request_id" value=""/>
    <input type="hidden" id="score" name="score" value=""/>
</form>
<?php $this->load->view('elements/publisher_footer'); ?>
<script>
    $(document).ready(function() {

        $('#request_table').DataTable({
            responsive: true,
            autoWidth: false,
            columnDefs: [
                { width: 200, targets: [1,2] }
            ]
        });

        $(".update_request").on("click",function(){
            var id = $(this).data("id");
            var score = parseInt($("#score_"+id).val());
            if(score >= 0){
                $("#update_request_id").val(id);
                $("#score").val(score);
                $("#update_request_form").submit();
            } else {
                swal("Opps, You have Error","Invalid Score, Please Enter Valid Number.! ! !","error");
            }
        });

        $(".remove_request").on("click",function(){
            var id = $(this).data("id");
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this process.!!!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, remove it!'
            }).then(function () {
                $("#remove_request_id").val(id);
                $("#remove_request_form").submit();
            });
        });
    });
</script>