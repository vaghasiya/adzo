<?php $this->load->view('elements/admin_header', array("title"=>"Manage Qualifications","active_menu"=>"qualification")); ?>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 style="margin-top: 10px; margin-bottom: 20px;">Manage Qualifications & Request</h3>
                            <?php
                            $class = $this -> session -> flashdata('class');
                            $message = $this -> session -> flashdata('message');
                            if(empty($class)) $class = "danger";
                            if(!empty($message)){ ?>
                                <div class="alert alert-<?php echo $class; ?>">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <span class="message-text"><?php echo $message; ?></span>
                                </div>
                            <?php } ?>
                            <ul class="nav-responsive nav nav-tabs">
                                <li class="pull-right">
                                    <a href="#add_qualification_modal" data-toggle="modal"><i class="fa fa-plus-square-o" aria-hidden="true" style="color: #275c8c;"></i>Add Qualification</a>
                                </li>
                                <li class="active">
                                    <a href="#qualifications_tab" data-toggle="tab"><strong> Qualification </strong></a>
                                </li>
                                <li>
                                    <a href="#request_tab" data-toggle="tab"><strong>Request</strong></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="qualifications_tab" class="tab-pane fade in active">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="qualifications_table">
                                            <thead>
                                            <tr>
                                                <th>Qualification Name</th>
                                                <th>Qualified Workers</th>
                                                <th>Request Pending</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($qualifications_list as $qualification){ ?>
                                                <tr>
                                                    <td><?php echo $qualification['name']; ?></td>
                                                    <td><?php echo $qualification['request_approved']; ?></td>
                                                    <td><?php echo $qualification['request_pending']; ?></td>
                                                    <th class="text-center">
                                                        <a class="text-warning qualification_details" data-id="<?php echo $qualification['qualification_id']; ?>" href="javascript:;">Details</a>&nbsp;&nbsp;&nbsp;
                                                        <a class="text-info qualification_edit" data-id="<?php echo $qualification['qualification_id']; ?>" data-name="<?php echo $qualification['name']; ?>" data-description="<?php echo $qualification['description']; ?>" href="javascript:;">Edit</a>&nbsp;&nbsp;&nbsp;
                                                        <a class="text-danger qualification_delete" data-id="<?php echo $qualification['qualification_id']; ?>" href="javascript:;">Delete</a>
                                                    </th>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="request_tab" class="tab-pane fade">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="request_table">
                                            <thead>
                                            <tr>
                                                <th>Date-Time</th>
                                                <th>Qualification Name</th>
                                                <th>Worker ID</th>
                                                <th>Score</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($request_list as $request){ ?>
                                                <tr>
                                                    <td><?php echo date("d-m-Y H:i:s", strtotime($request['created_date'])); ?></td>
                                                    <td><?php echo $request['name']; ?></td>
                                                    <td><?php echo $request['unique_user_id']; ?></td>
                                                    <td><input type="text" value="<?php echo $request['score']; ?>" class="form-control IntegerOnly" id="score_<?php echo $request['qualification_request_id']; ?>"/></td>
                                                    <th class="text-center">
                                                        <a class="text-success update_request" data-id="<?php echo $request['qualification_request_id']; ?>" href="javascript:;">Approve & Update</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                        <a class="text-danger reject_request" data-id="<?php echo $request['qualification_request_id']; ?>" href="javascript:;">Reject</a>
                                                    </th>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="add_qualification_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Qualification</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="<?php echo base_url(); ?>admin-qualification/create" method="post" role="form" style="display: block;">
                            <div class="form-group">
                                <label for="qualification_name">Qualification Name</label>
                                <input type="text" id="qualification_name" name="name" class="form-control" maxlength="60" required="required"/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" id="description" name="description" class="form-control" maxlength="250" required="required"/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="submit" class="form-control btn btn-info" value="Add Now">
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="edit_qualification_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Qualification</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="<?php echo base_url(); ?>admin-qualification/update-details" method="post" role="form">
                            <input type="hidden" id="edit_qualification_id" name="qualification_id" value=""/>
                            <div class="form-group">
                                <label for="edit_qualification_name">Qualification Name</label>
                                <input type="text" id="edit_qualification_name" name="name" class="form-control" maxlength="60" required="required"/>
                            </div>
                            <div class="form-group">
                                <label for="edit_description">Description</label>
                                <input type="text" id="edit_description" name="description" class="form-control" maxlength="250" required="required"/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="submit" class="form-control btn btn-info" value="Update">
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>admin-qualification/details" method="post" id="details_qualification_form" target="_blank">
    <input type="hidden" id="details_qualification_id" name="qualification_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>admin-qualification/delete-qualification" method="post" id="delete_qualification_form">
    <input type="hidden" id="delete_qualification_id" name="qualification_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>admin-qualification/reject-request" method="post" id="reject_request_form">
    <input type="hidden" id="reject_request_id" name="qualification_request_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>admin-qualification/update-request" method="post" id="update_request_form">
    <input type="hidden" id="update_request_id" name="qualification_request_id" value=""/>
    <input type="hidden" id="score" name="score" value=""/>
</form>
<?php $this->load->view('elements/admin_footer'); ?>
<script>
    $(document).ready(function() {
        $('#qualifications_table').DataTable({
            responsive: true,
            autoWidth: false,
            columnDefs: [
                { width: 150, targets: [1,2,3] }
            ]
        });

        $('#request_table').DataTable({
            responsive: true,
            autoWidth: false,
            columnDefs: [
                { width: 120, targets: [0,2,3] },
                { width: 160, targets: [4] }
            ]
        });

        $(".qualification_details").on("click",function(){
            var qualification_id = $(this).data("id");
            $("#details_qualification_id").val(qualification_id);
            $("#details_qualification_form").submit();
        });

        $(".qualification_edit").on("click",function(){
            var qualification_id = $(this).data("id");
            var name = $(this).data("name");
            var description = $(this).data("description");
            $("#edit_qualification_id").val(qualification_id);
            $("#edit_qualification_name").val(name);
            $("#edit_description").val(description);
            $("#edit_qualification_modal").modal("show");
        });

        $(".qualification_delete").on("click",function(){
            var qualification_id = $(this).data("id");
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this process.!!!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                $("#delete_qualification_id").val(qualification_id);
                $("#delete_qualification_form").submit();
            });
        });

        $(".reject_request").on("click",function(){
            var id = $(this).data("id");
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this process.!!!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, reject it!'
            }).then(function () {
                $("#reject_request_id").val(id);
                $("#reject_request_form").submit();
            });
        });

        $(".update_request").on("click",function(){
            var id = $(this).data("id");
            var score = parseInt($("#score_"+id).val());
            if(score >= 0){
                $("#update_request_id").val(id);
                $("#score").val(score);
                $("#update_request_form").submit();
            } else {
                swal("Opps, You have Error","Invalid Score, Please Enter Valid Number.! ! !","error");
            }
        });
    });
</script>