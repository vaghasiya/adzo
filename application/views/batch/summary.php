<?php $this->load->view('elements/publisher_header', array("title"=>"Batch Summary","active_menu"=>"manage","sub_menu"=>"batches")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $("#list_table").DataTable();

            $(".Max999").on("change", function(){
                if($(this).val() > 999){
                    $(this).val("999");
                }
            });

            $("#list_table").on("click",".block_worker_button",function(){
                var id = $(this).data("id");
                var post_data = {"worker_id": id};
                ajax_request("<?php echo base_url(); ?>publisher/ajax-block-worker", post_data, function (result, element) {
                    result = JSON.parse(result);
                    if(result.status == "error"){
                        swal("Opps, Error Occurred",result.message,"error");
                    } else {
                        $(element).addClass("unblock_worker_button");
                        $(element).removeClass("block_worker_button");
                        $(element).html("Unblock Worker");
                        swal("Process Completed",result.message,"success");
                    }
                }, $(this));
            });

            $("#list_table").on("click",".unblock_worker_button",function(){
                var id = $(this).data("id");
                var post_data = {"worker_id": id};
                ajax_request("<?php echo base_url(); ?>publisher/ajax-unblock-worker", post_data, function (result, element) {
                    result = JSON.parse(result);
                    if(result.status == "error"){
                        swal("Opps, Error Occurred",result.message,"error");
                    } else {
                        $(element).addClass("block_worker_button");
                        $(element).removeClass("unblock_worker_button");
                        $(element).html("Block Worker");
                        swal("Process Completed",result.message,"success");
                    }
                }, $(this));
            });

            $(".pay_bonus_button").on("click",function(){
                <?php if($user['balance'] > 0) { ?>
                var id = $(this).data("id");
                $("#bonus_worker_id").val(id);
                $("#pay_bonus_modal").modal("show");
                <?php } else { ?>
                swal("Opps, You have Error","Insufficient Adzo Account Balance.! ! !","error");
                <?php } ?>
            });

            $("#bonus_amount").on("change",function(){
                var bonus_amount = parseFloat($("#bonus_amount").val());
                var adzo_fees_percentage = parseFloat($("#adzo_fees_percentage").val());
                var total_amount = 0;
                var adzo_fees_amount = 0;
                if(bonus_amount > 0){
                    adzo_fees_amount = ((bonus_amount * adzo_fees_percentage)/100);
                    total_amount = bonus_amount + adzo_fees_amount;
                    $("#adzo_fees_amount").val(adzo_fees_amount.toFixed(2));
                } else {
                    $("#bonus_amount").val("");
                    $("#adzo_fees_amount").val("");
                    total_amount = 0
                }
                $("#total_amount").val(total_amount.toFixed(2));
            });

            $("#pay_bonus_modal_button").on("click",function(){
                var bonus_amount = parseFloat($("#bonus_amount").val());
                var adzo_fees_percentage = parseFloat($("#adzo_fees_percentage").val());
                var adzo_fees_amount = parseFloat($("#adzo_fees_amount").val());
                var total_amount = parseFloat($("#total_amount").val());
                if(total_amount > 0){
                    swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this process.!!!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, pay now.!!!'
                    }).then(function () {
                        var worker_id = $("#bonus_worker_id").val();
                        var bonus_batch_id = $("#bonus_batch_id").val();
                        var post_data = {"batch_id": bonus_batch_id,"worker_id": worker_id,"bonus_amount": bonus_amount, "adzo_fees_percentage": adzo_fees_percentage, "adzo_fees_amount": adzo_fees_amount, "total_amount": total_amount};
                        ajax_request("<?php echo base_url(); ?>publisher/ajax-pay-bonus", post_data, function (result, element) {
                            result = JSON.parse(result);
                            if(result.status == "error"){
                                swal("Opps, Error Occurred",result.message,"error");
                            } else {
                                swal("Process Completed",result.message,"success");
                            }
                            $("#bonus_amount").val("");
                            $("#adzo_fees_amount").val("");
                            $("#total_amount").val("");
                            $("#bonus_worker_id").val("");
                            $("#pay_bonus_modal").modal("hide");
                        }, undefined);
                    }, function(dismiss) {
                        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                        $("#bonus_amount").val("");
                        $("#adzo_fees_amount").val("");
                        $("#total_amount").val("");
                        $("#bonus_worker_id").val("");
                        $("#pay_bonus_modal").modal("hide");
                    });
                } else {
                    swal("Opps, You have Error","Invalid Bonus Total.! ! !","error");
                }
            });
        });
    </script>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Batch Summary</h3>
                    <?php
                    $class = $this -> session -> flashdata('class');
                    $message = $this -> session -> flashdata('message');
                    if(empty($class)) $class = "danger";
                    if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $class; ?>">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <span class="message-text"><?php echo $message; ?></span>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover dt-responsive">
                                <tbody>
                                    <tr>
                                        <th>Batch Name</th>
                                        <td><?php echo $batch_name; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Batch Description</th>
                                        <td><?php echo $batch_description; ?></td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td class="text">
                                            <?php if($assignments_submitted > 0){ ?>
                                                <form action="<?php echo base_url(); ?>batch/download-batch-result" style="display: inline;" method="post">
                                                    <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                                                    <button class="btn btn-success btn-xs">Download Result</button>
                                                </form>
                                            <?php } ?>
                                            <?php if($status == "in progress"){ ?>
                                            <form action="<?php echo base_url(); ?>batch/cancel-batch" style="display: inline;" method="post">
                                                <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                                                <button class="btn red btn-xs">Cancel This Batch</button>
                                            </form>
                                            <?php } ?>
                                            <form action="<?php echo base_url(); ?>batch/result" style="display: inline;" method="post">
                                                <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                                                <button class="btn blue btn-xs">View Result</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Project Name</th>
                                        <td><?php echo $project_name; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Title</th>
                                        <td><?php echo $title; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Description</th>
                                        <td><?php echo $description; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Keywords</th>
                                        <td><?php echo $keywords_description; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Tasks expires on</th>
                                        <td>
                                            <span id="task_expires_time_span"><?php echo date("d-m-Y H:i:s", strtotime($expire_date_time)); ?></span>&nbsp;&nbsp;&nbsp;
                                            <a class="btn red btn-xs" data-toggle="modal" href="#extend_batch_modal">Extend</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover dt-responsive">
                                <tbody>
                                    <tr>
                                        <th>Total number of task</th>
                                        <td><?php echo $total_hits; ?></td>
                                        <th>Assignments Pending (In-Progress)</th>
                                        <td style="width: 100px;"><?php echo $assignments_pending; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Assignments per task</th>
                                        <td><?php echo $number_of_assignment; ?></td>
                                        <th>Assignments Submitted</th>
                                        <td style="width: 100px;"><?php echo $assignments_submitted; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Total number of assignments</th>
                                        <td><?php echo $total_assignments; ?></td>
                                        <th>Assignments Approved</th>
                                        <td style="width: 100px;"><?php echo $assignments_approved; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Reward per assignment</th>
                                        <td>$<?php echo $reward_per_assignment; ?></td>
                                        <th>Assignments Rejected</th>
                                        <td style="width: 100px;"><?php echo $assignments_rejected; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Total cost</th>
                                        <td>$<?php echo $total_amount; ?></td>
                                        <th>Assignments Pending for Review</th>
                                        <td style="width: 100px;"><?php echo $assignments_pending_review; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Batch Data File</th>
                                        <td>
                                            <?php
                                            if($category_type=="multiple"){
                                                echo $csv_file; ?>
                                                <form action="<?php echo base_url(); ?>batch/download-batch-data" method="post" target="_blank">
                                                    <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                                                    <button type="submit" class="btn blue btn-xs">Download</button>
                                                </form>
                                            <?php } else { echo "---"; } ?>
                                        </td>
                                        <th>Assignments Cancelled</th>
                                        <td style="width: 100px;"><?php echo $assignments_cancelled; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Assignments Remaining (Not Accepted)</th>
                                        <td><?php echo $total_assignments + $assignments_republished - $assignments_submitted - $assignments_pending - $assignments_cancelled; ?></td>
                                        <th>Assignments Republished</th>
                                        <td style="width: 100px;"><?php echo $assignments_republished; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Worker Summary</h3>
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="list_table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Worker ID</th>
                                        <th class="text-center">Submitted</th>
                                        <th class="text-center">Approved</th>
                                        <th class="text-center">Rejected</th>
                                        <th class="text-center">Pending</th>
                                        <th class="text-center">Reassigned</th>
                                        <th class="text-center">Expired</th>
                                        <th class="text-center">Returned</th>
                                        <th class="text-center">Earnings</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($worker_summary_list as $row){ ?>
                                    <tr>
                                        <td><?php echo $row['unique_user_id']; ?></td>
                                        <td class="text-center"><?php echo $row['submitted']; ?></td>
                                        <td class="text-center"><?php echo $row['approved']; ?></td>
                                        <td class="text-center"><?php echo $row['rejected']; ?></td>
                                        <td class="text-center"><?php echo $row['pending']; ?></td>
                                        <td class="text-center"><?php echo $row['reassigned']; ?></td>
                                        <td class="text-center"><?php echo $row['expired']; ?></td>
                                        <td class="text-center"><?php echo $row['returned_task']; ?></td>
                                        <td class="text-right"><?php echo $row['earnings']; ?></td>
                                        <th class="text-center">
                                            <a class="text-success pay_bonus_button" data-id="<?php echo $row['user_id']; ?>" href="javascript:;">Pay Bonus</a><br/>
                                            <a class="text-danger <?php if(empty($row['block_id'])){ echo "block_worker_button"; } else { echo "unblock_worker_button"; } ?>" data-id="<?php echo $row['user_id']; ?>" href="javascript:;"><?php if(empty($row['block_id'])){ echo "Block"; } else { echo "Unblock"; } ?> Worker</a>
                                        </th>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="extend_batch_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Extend Batch Time</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form id="extend_batch_form" action="<?php echo base_url(); ?>batch/extend-task-time" method="post" role="form" style="display: block;">
                            <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                            <div class="form-group">
                                Extend Batch Time By,
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input style="width:40%;" name="extend_by" step="1" min="1" value="7" max="999" class="form-control col-md-2 IntegerOnly Max999" type="number" required="required">
                                    <select class="form-control" name="extend_by_unit" style="width:60%; float:right;">
                                        <option value="Days">Days</option>
                                        <option selected="selected" value="Hours">Hours</option>
                                        <option value="Minutes">Minutes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="submit" class="form-control btn btn-info" value="Extend Now">
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="pay_bonus_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pay Bonus</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" name="worker_id" id="bonus_worker_id" value=""/>
                        <input type="hidden" name="batch_id" id="bonus_batch_id" value="<?php echo $batch_id; ?>"/>
                        <input type="hidden" name="adzo_fees_percentage" id="adzo_fees_percentage" value="<?php echo ADZO_FEES_PERCENTAGE; ?>"/>
                        <div class="form-group">
                            <label for="bonus_amount">Bonus Amount</label>
                            <input name="bonus_amount" id="bonus_amount" class="form-control DecimalOnly" type="text" required="required">
                        </div>
                        <div class="form-group">
                            <label for="adzo_fees_amount">Adzo Fees (<?php echo ADZO_FEES_PERCENTAGE; ?>%)</label>
                            <input name="adzo_fees_amount" id="adzo_fees_amount" class="form-control" type="text" value="" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="total_amount">Total Amount</label>
                            <input name="total_amount" id="total_amount" class="form-control" type="text" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="button" class="form-control btn btn-info" value="Pay Now" id="pay_bonus_modal_button">
                                </div>
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>