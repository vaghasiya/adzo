<?php $this->load->view('elements/admin_header', array("title"=>"Batch Summary","active_menu"=>"manage","sub_menu"=>"manage_batches")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $("#list_table").DataTable();
        });
    </script>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>Batch Summary</h3>
                            <?php
                            $class = $this -> session -> flashdata('class');
                            $message = $this -> session -> flashdata('message');
                            if(empty($class)) $class = "danger";
                            if(!empty($message)){ ?>
                                <div class="alert alert-<?php echo $class; ?>">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <span class="message-text"><?php echo $message; ?></span>
                                </div>
                            <?php } ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered table-hover dt-responsive">
                                        <tbody>
                                            <tr>
                                                <th>Batch Name</th>
                                                <td><?php echo $batch_name; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Batch Description</th>
                                                <td><?php echo $batch_description; ?></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td class="text">
                                                    <?php if($assignments_submitted > 0){ ?>
                                                        <form action="<?php echo base_url(); ?>admin-manage-batches/download-batch-result" style="display: inline;" method="post">
                                                            <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                                                            <button class="btn btn-success btn-xs">Download Result</button>
                                                        </form>
                                                    <?php } ?>
                                                    <?php if($status == "in progress"){ ?>
                                                    <form action="<?php echo base_url(); ?>admin-manage-batches/cancel-batch" style="display: inline;" method="post">
                                                        <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                                                        <button class="btn red btn-xs">Cancel This Batch</button>
                                                    </form>
                                                    <?php } ?>
                                                    <form action="<?php echo base_url(); ?>admin-manage-batches/result" style="display: inline;" method="post">
                                                        <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                                                        <button class="btn blue btn-xs">View Result</button>
                                                    </form>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Project Name</th>
                                                <td><?php echo $project_name; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Title</th>
                                                <td><?php echo $title; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <td><?php echo $description; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Keywords</th>
                                                <td><?php echo $keywords_description; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Tasks expires on</th>
                                                <td>
                                                    <span id="task_expires_time_span"><?php echo date("d-m-Y H:i:s", strtotime($expire_date_time)); ?></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered table-hover dt-responsive">
                                        <tbody>
                                            <tr>
                                                <th>Total number of task</th>
                                                <td><?php echo $total_hits; ?></td>
                                                <th>Assignments Pending (In-Progress)</th>
                                                <td style="width: 100px;"><?php echo $assignments_pending; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Assignments per task</th>
                                                <td><?php echo $number_of_assignment; ?></td>
                                                <th>Assignments Submitted</th>
                                                <td style="width: 100px;"><?php echo $assignments_submitted; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Total number of assignments</th>
                                                <td><?php echo $total_assignments; ?></td>
                                                <th>Assignments Approved</th>
                                                <td style="width: 100px;"><?php echo $assignments_approved; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Reward per assignment</th>
                                                <td>$<?php echo $reward_per_assignment; ?></td>
                                                <th>Assignments Rejected</th>
                                                <td style="width: 100px;"><?php echo $assignments_rejected; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Total cost</th>
                                                <td>$<?php echo $total_amount; ?></td>
                                                <th>Assignments Pending for Review</th>
                                                <td style="width: 100px;"><?php echo $assignments_pending_review; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Batch Data File</th>
                                                <td>
                                                    <?php
                                                    if($category_type=="multiple"){
                                                        echo $csv_file; ?>
                                                        <form action="<?php echo base_url(); ?>admin-manage-batches/download-batch-data" method="post" target="_blank">
                                                            <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                                                            <button type="submit" class="btn blue btn-xs">Download</button>
                                                        </form>
                                                    <?php } else { echo "---"; } ?>
                                                </td>
                                                <th>Assignments Cancelled</th>
                                                <td style="width: 100px;"><?php echo $assignments_cancelled; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Assignments Remaining (Not Accepted)</th>
                                                <td><?php echo $total_assignments + $assignments_republished - $assignments_submitted - $assignments_pending - $assignments_cancelled; ?></td>
                                                <th>Assignments Republished</th>
                                                <td style="width: 100px;"><?php echo $assignments_republished; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Worker Summary</h3>
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="list_table" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Worker ID</th>
                                                <th class="text-center">Submitted</th>
                                                <th class="text-center">Approved</th>
                                                <th class="text-center">Rejected</th>
                                                <th class="text-center">Pending</th>
                                                <th class="text-center">Reassigned</th>
                                                <th class="text-center">Expired</th>
                                                <th class="text-center">Returned</th>
                                                <th class="text-center">Earnings</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($worker_summary_list as $row){ ?>
                                            <tr>
                                                <td><?php echo $row['unique_user_id']; ?></td>
                                                <td class="text-center"><?php echo $row['submitted']; ?></td>
                                                <td class="text-center"><?php echo $row['approved']; ?></td>
                                                <td class="text-center"><?php echo $row['rejected']; ?></td>
                                                <td class="text-center"><?php echo $row['pending']; ?></td>
                                                <td class="text-center"><?php echo $row['reassigned']; ?></td>
                                                <td class="text-center"><?php echo $row['expired']; ?></td>
                                                <td class="text-center"><?php echo $row['returned_task']; ?></td>
                                                <td class="text-right"><?php echo $row['earnings']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/admin_footer'); ?>