<?php $this->load->view('elements/publisher_header', array("title"=>"Manage Batches","active_menu"=>"manage","sub_menu"=>"batches")); ?>
<script type="application/javascript">
    $(document).ready(function() {
        $('#batches_in_progress_table').DataTable({
            responsive: true,
            autoWidth: false,
            order: [[ 0, "desc" ]],
            columnDefs: [
                { width: 60, targets: [0] },
                { width: 70, targets: [2,3] },
                { width: 30, targets: [4,5,6] },
                { width: 215, targets: [7] }
            ]
        });

        $('#batches_review_pending_table').DataTable({
            responsive: true,
            autoWidth: false,
            order: [[ 0, "desc" ]],
            columnDefs: [
                { width: 60, targets: [0] },
                { width: 70, targets: [2,3] },
                { width: 30, targets: [4,5,6,7] },
                { width: 105, targets: [8] }
            ]
        });

        $('#batches_reviewed_table').DataTable({
            responsive: true,
            autoWidth: false,
            order: [[ 0, "desc" ]],
            columnDefs: [
                { width: 60, targets: [0] },
                { width: 70, targets: [2,3] },
                { width: 30, targets: [4,5,6] },
                { width: 105, targets: [7] }
            ]
        });

        $(".tab-content").on("click",".batch_result",function(){
            var batch_id = $(this).data("id");
            $("#result_batch_id").val(batch_id);
            $("#result_form").submit();
        });

        $(".tab-content").on("click",".batch_cancel",function(){
            var batch_id = $(this).data("id");
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this process.!!!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, cancel it!'
            }).then(function () {
                $("#cancel_batch_id").val(batch_id);
                $("#cancel_form").submit();
            });
        });

        $(".tab-content").on("click",".batch_delete",function(){
            var batch_id = $(this).data("id");
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this process.!!!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                $("#delete_batch_id").val(batch_id);
                $("#delete_form").submit();
            });
        });

        $(".tab-content").on("click",".batch_summary",function(){
            var batch_id = $(this).data("id");
            $("#summary_batch_id").val(batch_id);
            $("#summary_form").submit();
        });
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Manage Batches</h3>
                    <?php
                    $class = $this -> session -> flashdata('class');
                    $message = $this -> session -> flashdata('message');
                    if(empty($class)) $class = "danger";
                    if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $class; ?>">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <span class="message-text"><?php echo $message; ?></span>
                        </div>
                    <?php } ?>
                    <ul class="nav-responsive nav nav-tabs">
                        <li class="dropdown pull-right tabdrop hide">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="glyph-icon icon-align-justify"></i>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>Export CSV</li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="#tab1" id="tab1_anchor" data-toggle="tab"><strong>Batches in Progress</strong></a>
                        </li>
                        <li>
                            <a href="#tab2" id="tab2_anchor" data-toggle="tab"><strong>Batches ready for review</strong></a>
                        </li>
                        <li>
                            <a href="#tab3" id="tab3_anchor" data-toggle="tab"><strong>Batches already reviewed</strong></a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="batches_in_progress_table">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Batch Name</th>
                                            <th>Total<br/>Assignment</th>
                                            <th>Submitted</th>
                                            <th><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true" style="color: #008000;" data-tooltip="true" title="Assignments Approved" ></i></th>
                                            <th><i class="fa fa-thumbs-o-down fa-2x" aria-hidden="true" style="color: #cf1a27;" data-tooltip="true" title="Assignments Rejected"></i></th>
                                            <th><i class="fa fa-hourglass-start fa-2x" aria-hidden="true" data-tooltip="true" title="Assignments Not Submitted"></i></th>
                                            <th style="text-align: center;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($batch_in_progress_list as $batch){ ?>
                                        <tr>
                                            <td><?php echo date("Y-m-d", strtotime($batch['created_date'])); ?></td>
                                            <td><?php echo $batch['batch_name']; ?></td>
                                            <td><?php echo $batch['total_assignments']; ?></td>
                                            <td><?php echo $batch['assignments_submitted']; ?></td>
                                            <td><?php echo $batch['assignments_approved']; ?></td>
                                            <td><?php echo $batch['assignments_rejected']; ?></td>
                                            <td><?php echo $batch['total_assignments'] + $batch['total_republished'] - $batch['assignments_submitted'] - $batch['assignments_cancelled']; ?></td>
                                            <th>
                                                <a class="text-info batch_result" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Result</a>&nbsp;|&nbsp;
                                                <a class="text-success batch_cancel" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Cancel Batch</a>&nbsp;|&nbsp;
                                                <a class="text-warning batch_summary" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Summary</a>
                                            </th>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover dt-responsive" id="batches_review_pending_table">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Batch Name</th>
                                        <th>Total<br/>Assignment</th>
                                        <th>Submitted</th>
                                        <th><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true" style="color: #008000;" data-tooltip="true" title="Assignments Approved" ></i></th>
                                        <th><i class="fa fa-thumbs-o-down fa-2x" aria-hidden="true" style="color: #cf1a27;" data-tooltip="true" title="Assignments Rejected"></i></th>
                                        <th><i class="fa fa-hourglass-start fa-2x" aria-hidden="true" data-tooltip="true" title="Assignments Not Reviewed"></i></th>
                                        <th><i class="fa fa-ban fa-2x" aria-hidden="true" data-tooltip="true" title="Assignments Cancelled" style="color: #ff0000;"></i></th>
                                        <th style="text-align: center;">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($batch_review_pending_list as $batch){ ?>
                                        <tr>
                                            <td><?php echo date("Y-m-d", strtotime($batch['created_date'])); ?></td>
                                            <td><?php echo $batch['batch_name']; ?></td>
                                            <td><?php echo $batch['total_assignments']; ?></td>
                                            <td><?php echo $batch['assignments_submitted']; ?></td>
                                            <td><?php echo $batch['assignments_approved']; ?></td>
                                            <td><?php echo $batch['assignments_rejected']; ?></td>
                                            <td><?php echo $batch['assignments_pending_review']; ?></td>
                                            <td><?php echo $batch['assignments_cancelled']; ?></td>
                                            <th>
                                                <a class="text-info batch_result" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Result</a>&nbsp;|&nbsp;
                                                <a class="text-danger batch_delete" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Delete Batch</a>&nbsp;|&nbsp;
                                                <a class="text-warning batch_summary" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Summary</a>
                                            </th>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="tab3" class="tab-pane fade">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover dt-responsive" id="batches_reviewed_table">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Batch Name</th>
                                        <th>Total<br/>Assignment</th>
                                        <th>Submitted</th>
                                        <th><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true" style="color: #008000;" data-tooltip="true" title="Assignments Approved" ></i></th>
                                        <th><i class="fa fa-thumbs-o-down fa-2x" aria-hidden="true" style="color: #cf1a27;" data-tooltip="true" title="Assignments Rejected"></i></th>
                                        <th><i class="fa fa-ban fa-2x" aria-hidden="true" data-tooltip="true" title="Assignments Cancelled" style="color: #ff0000;"></i></th>
                                        <th style="text-align: center;">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($batch_reviewed_list as $batch){ ?>
                                        <tr>
                                            <td><?php echo date("Y-m-d", strtotime($batch['created_date'])); ?></td>
                                            <td><?php echo $batch['batch_name']; ?></td>
                                            <td><?php echo $batch['total_assignments']; ?></td>
                                            <td><?php echo $batch['assignments_submitted']; ?></td>
                                            <td><?php echo $batch['assignments_approved']; ?></td>
                                            <td><?php echo $batch['assignments_rejected']; ?></td>
                                            <td><?php echo $batch['assignments_cancelled']; ?></td>
                                            <th>
                                                <a class="text-info batch_result" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Result</a>&nbsp;|&nbsp;
                                                <a class="text-danger batch_delete" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Delete Batch</a>&nbsp;|&nbsp;
                                                <a class="text-warning batch_summary" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Summary</a>
                                            </th>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>batch/result" method="post" id="result_form" target="_blank">
    <input type="hidden" id="result_batch_id" name="batch_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>batch/cancel-batch" method="post" id="cancel_form">
    <input type="hidden" id="cancel_batch_id" name="batch_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>batch/delete-batch" method="post" id="delete_form">
    <input type="hidden" id="delete_batch_id" name="batch_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>batch/summary" method="post" id="summary_form" target="_blank">
    <input type="hidden" id="summary_batch_id" name="batch_id" value=""/>
</form>
<?php $this->load->view('elements/publisher_footer'); ?>



