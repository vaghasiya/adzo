<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png">
    <title id="title">Workfreelance Digital Market</title>

    <link href="<?php echo base_url(); ?>frontend_assets/styles/master.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>frontend_assets/styles/custom.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/jquery-2.1.1.min.js"></script>
    <!--include jQuery Validation Plugin-->
    <script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/jquery.validate.min.js" type="text/javascript"></script>

    <!--Optional: include only if you are using the extra rules in additional-methods.js -->
    <script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/additional-methods.min.js" type="text/javascript"></script>
    <style>
        .social-icons li{
            display: inline;
            float: left;
            list-style: outside none none;
            margin-bottom: 5px;
            margin-right: 5px;
            text-indent: -9999px;
        }
        .social-icons li .facebook{
            background: rgba(0, 0, 0, 0) url("<?php echo base_url(); ?>assets/global/img/social/facebook.png") no-repeat scroll 0 0;
        }
        .social-icons li .twitter{
            background: rgba(0, 0, 0, 0) url("<?php echo base_url(); ?>assets/global/img/social/twitter.png") no-repeat scroll 0 0;
        }
        .social-icons li .googleplus{
            background: rgba(0, 0, 0, 0) url("<?php echo base_url(); ?>assets/global/img/social/googleplus.png") no-repeat scroll 0 0;
        }
        .social-icons li .linkedin{
            background: rgba(0, 0, 0, 0) url("<?php echo base_url(); ?>assets/global/img/social/linkedin.png") no-repeat scroll 0 0;
        }
        .social-icons li > a{
            background-position: 0 0;
            background-repeat: no-repeat;
            border-radius: 2px;
            display: block;
            height: 28px;
            transition: all 0.3s ease-in-out 0s;
            width: 28px;
        }
        .social-icon-color{
            background-position: 0 -38px !important;
        }
    </style>
</head>
<body>
<div  class="layout-theme animated-css">
    <!-- Loader Landing Page-->
    <?php
    //            $controller = $this->router->fetch_class();
    //            $method = $this->router->fetch_method();
    //            if ($controller == 'user' && $method == 'index') {
    ?>
    <div id="ip-container" class="ip-container">
        <!-- initial header-->
        <header class="ip-header">
            <div class="ip-loader">
                <div class="text-center">
                    <div class="ip-logo"> <a href="/"><img src="<?php echo base_url(); ?>frontend_assets/images/logo.png" alt="logo"></a> </div>
                </div>
                <svg width="60px" height="60px" viewBox="0 0 80 80" class="ip-inner">
                    <path d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,39.3,10z" class="ip-loader-circlebg"></path>
                    <path id="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z" class="ip-loader-circle"></path>
                </svg> </div>
        </header>
    </div>
    <?php // } ?>
    <div id="wrapper">
        <div class="main">
            <header class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header__logo"> <a href="<?php echo base_url(); ?>" class="logo"><img src="<?php echo base_url(); ?>frontend_assets/images/logo.png" alt="Logo" > </a>
                            </div>
                            <div class="navbar">
                                <div class="yamm">
                                    <div class="navbar-header hidden-md hidden-lg hidden-sm ">
                                        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </button>
                                        <a href="#" class="navbar-brand">Menu</a>
                                    </div>
                                    <nav id="navbar-collapse-1" class="navbar-collapse collapse ">
                                        <ul class="nav">
                                            <li class="dropdown"><a  href="<?php echo base_url(); ?>">Home</a></li>
                                            <li class="dropdown"><a  href="<?php echo base_url(); ?>publisher/dashboard">Publisher</a></li>
                                            <li><a href="<?php echo base_url(); ?>tasks/all-tasks">Worker</a></li>
                                            <li><a data-toggle="modal" href="#login_modal">Login</a> </li>
                                            <li><a data-toggle="modal" href="#signup_modal" class="register-btn">Register</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>