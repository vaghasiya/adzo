<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3 class="ui-title-inner">COMPANY</h3>
                <div class="border-color"></div>                       
                <ul class="list-mark">
                    <li class="footer-company"><a href="#" class="list-mark__link"><span class="icon"></span>About Us</a></li>
                    <li class="footer-company"><a href="#" class="list-mark__link"><span class="icon"></span>Contact Us</a></li>
                    <li class="footer-company"><a href="#" class="list-mark__link"><span class="icon"></span>Participation Agreement</a></li>                                
                    <li class="footer-company"><a href="#" class="list-mark__link"><span class="icon"></span>Privacy Policies</a></li>
                </ul>                                
            </div>
            <div class="col-md-4">
                <h3 class="ui-title-inner">QUICK LINKS</h3>
                <div class="border-color"></div>
                <ul class="list-mark text-capitalize">
                    <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>Publisher</a> </li>
                    <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>Worker</a> </li>
                    <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>FAQ-Worker</a> </li>
                    <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>Developers</a> </li>
                    <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>FAQ-Publisher</a> </li>
                </ul>
            </div>
            <div class="col-md-4">
                <h3 class="ui-title-inner">CONTACT US</h3>
                <div class="border-color"></div>
                <div class="footer__title-block">Office Address</div>
                <p>Route No 68, Opposite Whitehouse<br>
                    Washington DC -989934</p>
            </div>
        </div>
        <div class="row">
            <div class="copyright"><span>© Copyrights <?php echo date('Y').',';?><strong> Kaprat Consultancy Services Pvt Ltd.</strong></span><span> All Rights Reserved.</span></div>
        </div>
    </div>
</footer>


<div class="container">
    <div class="row">
        <div class="modal" id="login_modal">
            <div class="mymodal clearfix">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h3 style="margin-top: 0px; margin-bottom: 0px;">Sign in</h3>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <div class="col-md-12">
                        <div class="panel panel-login">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form id="user_login_form" action="<?php echo base_url(); ?>user/login-process" method="post" role="form" style="display: block;">
                                            <?php
                                            $modal = $this -> session -> flashdata("modal");
                                            $class = $this -> session -> flashdata('class');
                                            $message = $this -> session -> flashdata('message');
                                            if(empty($class)) $class = "danger";
                                            if((!empty($message)) && ($modal == "login_modal")){ ?>
                                                <div class="alert alert-<?php echo $class; ?>">
                                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                                    <span class="message-text"><?php echo $message; ?></span>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group">
                                                <input type="text" name="user_name" id="user_name" tabindex="1" class="form-control" placeholder="User Email" value="" required="required">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required="required">
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" tabindex="3" name="remember" id="remember">
                                                <label for="remember" style="font-weight: normal;"> Remember Me</label>
                                                <a data-toggle="modal" id="forget_password_link" href="#forget_password_modal" tabindex="6" class="forgot-password pull-right">Forgot Password?</a>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-5">
                                                        <input type="submit" id="login-submit-publisher" tabindex="4" class="form-control btn btn-login" value="Login Now">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2 text-center">
                                                    <a href="#">Participation Agreement</a>
                                                    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                                    <a href="#">Privacy Policy</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="signup_modal">
            <div class="mymodal clearfix">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h3 style="margin-top: 0px; margin-bottom: 0px;">Sign Up</h3>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <div class="col-md-12">
                        <div class="panel panel-login">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form action="<?php echo base_url(); ?>user/register-process" id="register_form" method="post" role="form" style="display: block;">
                                            <input type="hidden" name="user_type" id="register_user_type" value="">
                                            <?php
                                            $class = $this -> session -> flashdata('class');
                                            $message = $this -> session -> flashdata('message');
                                            if(empty($class)) $class = "danger";
                                            if((!empty($message)) && ($modal == "signup_modal")){ ?>
                                                <div class="alert alert-<?php echo $class; ?>">
                                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                                    <span class="message-text"><?php echo $message; ?></span>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group">
                                                <input type="email" name="email" tabindex="1" class="form-control" placeholder="Email Address" value="" required="required">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" tabindex="2" class="form-control" placeholder="Password" required="required">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-5">
                                                        <input type="submit" id="register-submit-publisher" tabindex="3" class="form-control btn btn-login" value="Register as Publisher">
                                                    </div>
                                                    <div class="col-sm-2">&nbsp;</div>
                                                    <div class="col-sm-5">
                                                        <input type="submit" id="register-submit-worker" tabindex="4" class="form-control btn btn-login pull-right" value="Register as Worker">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2 text-center">
                                                    <a href="#">Participation Agreement</a>
                                                    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                                    <a href="#">Privacy Policy</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="forget_password_modal">
            <div class="mymodal clearfix">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h3 style="margin-top: 0px; margin-bottom: 0px;">Give us your details, We will Send you Password</h3>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <div class="col-md-12">
                        <div class="panel panel-login" style="margin-bottom: 35px;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form action="<?php echo base_url(); ?>user/forget-password-process" method="post" role="form" style="display: block;">
                                            <?php
                                            $class = $this -> session -> flashdata('class');
                                            $message = $this -> session -> flashdata('message');
                                            if(empty($class)) $class = "danger";
                                            if((!empty($message)) && ($modal == "forget_password_modal")){ ?>
                                                <div class="alert alert-<?php echo $class; ?>">
                                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                                    <span class="message-text"><?php echo $message; ?></span>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group">
                                                <input type="text" name="user_name" tabindex="1" class="form-control" placeholder="User Name" value="" required="required">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" name="email" tabindex="2" class="form-control" placeholder="Email Address" required="required">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <input type="submit" tabindex="4" class="form-control btn btn-login" value="Send Now">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    </div> <!-- wrapper class closed from header-->
</div> <!-- layout-theme class closed from header-->

<script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/modernizr.custom.js"></script>
<!-- SCRIPTS-->
<script src="<?php echo base_url(); ?>frontend_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/plugins/owl-carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>-->
<script src="<?php echo base_url(); ?>frontend_assets/plugins/datetimepicker/jquery.datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/plugins/jelect/jquery.jelect.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/plugins/nouislider/jquery.nouislider.all.min.js"></script>
<!-- Loader-->
<script src="<?php echo base_url(); ?>frontend_assets/plugins/loader/js/classie.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/plugins/loader/js/pathLoader.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/plugins/loader/js/main.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/classie.js"></script>
<!-- THEME-->
<script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/cssua.min.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/wow.min.js"></script>
<script src="<?php echo base_url(); ?>frontend_assets/scripts/common.js"></script>
<script>
    $(document).ready(function() {
        $("#register-submit-publisher").on("click",function(){
            $("#register_user_type").val("publisher");
        });

        $("#register-submit-worker").on("click",function(){
            $("#register_user_type").val("worker");
        });

        $("#forget_password_link").on("click",function(){
            $("#login_modal").modal("hide");
        });
        <?php if($modal == "login_modal"){ ?>
        $("#login_modal").modal("show");
        <?php } else if($modal == "signup_modal"){ ?>
        $("#signup_modal").modal("show");
        <?php } else if($modal == "forget_password_modal"){ ?>
        $("#forget_password_modal").modal("show");
        <?php } ?>
    });
</script>
</body>
</html>