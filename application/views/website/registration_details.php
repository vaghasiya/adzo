<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png">
    <title id="title">Adzo Digital Market</title>
    
    <link href="<?php echo base_url(); ?>frontend_assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/global/plugins/sweetalert2/sweetalert2.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>frontend_assets/styles/master.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>frontend_assets/styles/custom.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/jquery-2.1.1.min.js"></script>
    <script src="<?php echo base_url(); ?>frontend_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/sweetalert2/sweetalert2.js"></script>
    <!--include jQuery Validation Plugin-->
    <script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/jquery.validate.min.js" type="text/javascript"></script>

    <!--Optional: include only if you are using the extra rules in additional-methods.js -->
    <script src="<?php echo base_url(); ?>frontend_assets/scripts/libs/additional-methods.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/scripts/global.js"></script>
    <script type="text/javascript">
        var india_state_list = <?php echo json_encode($india_state_list); ?>;
        var usa_state_list = <?php echo json_encode($usa_state_list); ?>;
        $(document).ready(function() {
            $("#country").on("change",function(){
                var country = $(this).val();
                if(country == "India"){
                    load_state_data(india_state_list);
                } else if (country == "United States of America"){
                    load_state_data(usa_state_list);
                } else {
                    $("#state").html('<option value="">None</option>');
                }
            });

            $("#registration_form").on("submit", function () {
                if(!$("#agree_checkbox").is(":checked")) {
                    swal("Oops...", "Please Check Agreement Checkbox.!!!", "error");
                    $("#agree_checkbox").focus();
                    return false;
                }
            });

            $("#profile_pic").on("change", function (event) {
                var id = $(this).attr("id");
                filename = event.target.files[0].name;
                file = filename.split(".");
                if(file[1] == "jpg" || file[1] == "JPG" || file[1] == "jpeg" || file[1] == "JPEG" || file[1] == "png" || file[1] == "PNG"){
                    console.log("valid file");
                } else {
                    swal("Oops Error Found","Sorry, Please select correct file only.!!!","error");
                    $(this).val("");
                }
            });
        });

        function load_state_data(state_list) {
            var option_html = "";
            $(state_list).each(function (index, value) {
                option_html += '<option value="' + value.state_name + '">' + value.state_name + '</option>';
            });
            $("#state").html(option_html);
        }
    </script>

    <!-- custom css form text boarder-->
    <style type="text/css">
         .form-control{
            border: 1px solid #ccc; 
        }
    </style>
    
</head>
<body>
<div class="layout-theme animated-css">
<div id="wrapper">
    <div class="main">
        <header class="header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="header__logo"> <a href="<?php echo base_url(); ?>" class="logo"><img src="<?php echo base_url(); ?>frontend_assets/images/logo.png" alt="Logo" > </a></div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding-top: 0px;">
                            <div id="home" class="tab-pane fade in active" style="margin-top: 0;">
                                <div class="row col-xs-12 col-md-12"><!-- Instructions -->
                                    <div class="panel panel-primary" style="width: 100%; border: 3px solid rgb(51, 122, 183);">
                                        <div class="panel-heading" style="background-color: #337AB7 !important; font-size: 18px;"><strong>Adzo  Participation agreement</strong></div>
                                        <div class="panel-body" style="height: 200px; width: 100%; overflow: auto;  ">
                                            <p>Find the company's name and email address from the information listed below. Do this by searching the business's website and the web (e.g. Google, Yelp, Facebook, Manta.com, Hunter.io, etc). Even if the email address is a general email address such as info@, enquiries@, note it down. If there is a specific person email address, note it down compulsorily.</p>

                                            <ul style="margin-left:2%;">
                                                <li> 1. A Task will be rewarded only if the name and email address are found.</li>
                                                <li> 2. Task will not be rewarded if the contact details provided are not the owner's contact details.</li>
                                                <li> 3. Do not submit junk emails such as xxx@qq.com, email@address.com.</li>
                                                <li> 4. Avoid taking career@, hr@, webmaster@ and privacy@ emails.</li>
                                                <li> 5. Do not submit online ‘contact us form links’ if email address is not found.</li>
                                            </ul>
                                            <br/>
                                            <p>Find the company's name and email address from the information listed below. Do this by searching the business's website and the web (e.g. Google, Yelp, Facebook, Manta.com, Hunter.io, etc). Even if the email address is a general email address such as info@, enquiries@, note it down. If there is a specific person email address, note it down compulsorily.</p>

                                            <ul style="margin-left:2%;">
                                                <li> 1. A Task will be rewarded only if the name and email address are found.</li>
                                                <li> 2. Task will not be rewarded if the contact details provided are not the owner's contact details.</li>
                                                <li> 3. Do not submit junk emails such as xxx@qq.com, email@address.com.</li>
                                                <li> 4. Avoid taking career@, hr@, webmaster@ and privacy@ emails.</li>
                                                <li> 5. Do not submit online ‘contact us form links’ if email address is not found.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-body" style="padding-top: 0px; padding-bottom: 0px;">
                                            <input type="checkbox" id="agree_checkbox"/>
                                            <label for="agree_checkbox">&nbsp;I agree to the following:</label>
                                            <br/>
                                            <ul style="list-style: inside none disc;">
                                                <li>I accept the User Agreement and Privacy Policy above.</li>
                                                <li>I am over 18 years of age.</li>
                                                <li>I will comply with all applicable laws, including without limitation tax and filing requirements.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php
                                    $class = $this -> session -> flashdata('class');
                                    $message = $this -> session -> flashdata('message');
                                    if(empty($class)) $class = "danger";
                                    if(!empty($message)){ ?>
                                        <div class="alert alert-<?php echo $class; ?>">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <span class="message-text"><?php echo $message; ?></span>
                                        </div>
                                    <?php } ?>
                                    <form class="form-horizontal" id="registration_form" method="POST" action="<?php echo base_url(); ?>user/complete-registration-process" enctype="multipart/form-data">
                                        <input type="hidden" name="user_id" value="<?php echo $user['user_id']; ?>"/>
                                        <input type="hidden" name="email" value="<?php echo $user['email']; ?>"/>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="first_name">* First Name:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="first_name" id="first_name" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="last_name">* Last Name:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="last_name" id="last_name" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="company_name">Company / Institute Name:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="company_name" id="company_name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="user_name">* User Name:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="user_name" id="user_name" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="country">* Country:</label>
                                                <div class="col-sm-8">
                                                    <select id="country" name="country" class="form-control" style="border: 1px solid #ccc;" required="required">
                                                        <option value="" style="display: none;">Select Here</option>
                                                        <option value="India">India</option>
                                                        <option value="United States of America">United States of America</option>
                                                        <option disabled>─────────────────</option>
                                                        <?php foreach ($country_list as $row) { ?>
                                                            <option value="<?php echo $row['country_nice_name']; ?>"><?php echo $row['country_nice_name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="state">State:</label>
                                                <div class="col-sm-8">
                                                    <select id="state" name="state" class="form-control" style="border: 1px solid #ccc;">
                                                        <option>None</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4">&nbsp;</label>
                                                <button type="submit" class="btn btn-success btn-md" id="registration_button" style="margin-left: 15px;">Submit Details</button>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="address1">Address 1:</label>
                                                <div class="col-sm-8">
                                                   <input type="text" class="form-control" name="address1" id="address1">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="address2">Address 2:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="address2" id="address2">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="address3">Address 3:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="address3" id="address3">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="city">City :</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="city" id="city" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="pin">Pin :</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="pin" id="pin">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="mobile_number">Mobile Number :</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="mobile_number" id="mobile_number">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="mobile_number">Profile Photo :</label>
                                                <div class="col-sm-8">
                                                    <input type="file" class="form-control" name="profile_pic" id="profile_pic">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3 class="ui-title-inner">COMPANY</h3>
                    <div class="border-color"></div>
                    <ul class="list-mark">
                        <li class="footer-company"><a href="#" class="list-mark__link"><span class="icon"></span>About Us</a></li>
                        <li class="footer-company"><a href="#" class="list-mark__link"><span class="icon"></span>Contact Us</a></li>
                        <li class="footer-company"><a href="#" class="list-mark__link"><span class="icon"></span>Participation Agreement</a></li>
                        <li class="footer-company"><a href="#" class="list-mark__link"><span class="icon"></span>Privacy Policies</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="ui-title-inner">QUICK LINKS</h3>
                    <div class="border-color"></div>
                    <ul class="list-mark text-capitalize">
                        <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>Publisher</a> </li>
                        <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>Worker</a> </li>
                        <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>FAQ-Worker</a> </li>
                        <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>Developers</a> </li>
                        <li class="list-mark__item"><a href="javascript:void(0);" class="list-mark__link"><span class="icon"></span>FAQ-Publisher</a> </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="ui-title-inner">CONTACT US</h3>
                    <div class="border-color"></div>
                    <div class="footer__title-block">Office Address</div>
                    <p>#94, PES College Road, 22nd Main, Behind Nagendra Block, Banishankari first stage <br>
                        Bangalore -560050</p>
                </div>
            </div>
            <div class="row">
                <div class="copyright"><span>© Copyrights <?php echo date('Y').',';?><strong> Crowdsourcing Online Services Pvt Ltd.</strong></span><span> All Rights Reserved.</span></div>
            </div>
        </div>
    </footer>
</div> <!-- wrapper class closed from header-->
</div> <!-- layout-theme class closed from header-->
