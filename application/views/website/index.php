<?php $this->load->view('website/header'); ?>
<div id="main-slider" data-pagination="false" data-single-item="true" data-auto-play="7000" data-transition-style="fadeUp" data-main-text-animation="true" data-after-init-delay="4000" data-after-move-delay="500" data-stop-on-hover="true" class="main-slider main-slider_mod-2 enable-owl-carousel owl-main-slider owl-carousel owl-theme">
    <div class="item">
        <div class="item-inner"> <img src="<?php echo base_url(); ?>frontend_assets/media/main-slider/foto-2.jpg" alt="img"> </div>
        <div class="item-wrap">
            <div class="container">
                <div style="visibility: hidden;" class="slide-title main-slider_slideInUp animated">We Provide You With<strong>A Smarter Way To Move</strong> </div>
                <p style="visibility: hidden;" class="slide-text main-slider_fadeInLeft animated">Abitant morbi tristique senectus eft netus eft malesuada fames ac turpis egestas enean non tell. Donec pede quam placerat ristique faucibus.</p>
                <a href="javascript:void(0);" style="visibility: hidden;" class="btn main-slider_zoomIn animated">READ MORE</a> </div>
        </div>
    </div>
    <div class="item">
        <div class="item-inner"> <img src="<?php echo base_url(); ?>frontend_assets/media/main-slider/foto-3.jpg" alt="img"> </div>
        <div class="item-wrap">
            <div class="container">
                <div style="visibility: hidden;" class="slide-title main-slider_slideInUp animated">We Provide You With<strong>A Smarter Way To Move</strong> </div>
                <p style="visibility: hidden;" class="slide-text main-slider_fadeInLeft animated">Abitant morbi tristique senectus eft netus eft malesuada fames ac turpis egestas enean non tell. Donec pede quam placerat ristique faucibus.</p>
                <a href="javascript:void(0);" style="visibility: hidden;" class="btn main-slider_zoomIn animated">READ MORE</a> </div>
        </div>
    </div>
    <div class="item">
        <div class="item-inner"> <img src="<?php echo base_url(); ?>frontend_assets/media/main-slider/foto-4.jpg" alt="img"> </div>
        <div class="item-wrap">
            <div class="container">
                <div style="visibility: hidden;" class="slide-title main-slider_slideInUp animated">We Provide You With<strong>A Smarter Way To Move</strong> </div>
                <p style="visibility: hidden;" class="slide-text main-slider_fadeInLeft animated">Abitant morbi tristique senectus eft netus eft malesuada fames ac turpis egestas enean non tell. Donec pede quam placerat ristique faucibus.</p>
                <a href="javascript:void(0);" style="visibility: hidden;" class="btn main-slider_zoomIn animated">READ MORE</a> </div>
        </div>
    </div>
</div>
<div data-wow-duration="2s" class="section-area wow bounceInUp">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="list-pennant">
                    <li class="list-pennant__item">
                        <div class="icon fa fa-laptop"></div>
                        <div class="list-pennant__title">Work</div>
                    </li>
                    <li class="list-pennant__item">
                        <div class="icon fa fa-money"></div>
                        <div class="list-pennant__title">Earn</div>
                    </li>
                    <li class="list-pennant__item">
                        <div class="icon fa fa-share-alt"></div>
                        <div class="list-pennant__title">Share</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div data-wow-duration="2s" data-wow-delay="1s" class="section-area section_mod-4 wow bounceInRight">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2 class="ui-subtitle-block">Workfreelance is a global market place for work.</h2>
                <div class="border-color"></div>
                <p>Workfreelance  is a global market place for work. We use the traits of advanced technology to complete the process where both the Publisher and the Worker meet. This process involves obtaining the services, ideas or content by seeking contributions from masses. These masses can be found online on our platform.
                    This innovative approach is ready to provide the job opportunities to millions of people around the world. The concept is where the crowd is outsourced to get the work done efficiently. Workfreelance has a lot of professionals to choose from, from a single platform.
                    Being away from traditional concept, Workfreelance provides fine online resources.</p>                            
            </div>
        </div>
    </div>
</div>

<section class="section-area section_slider-services " style="background-color: #26c9ff">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="ui-title-block">WHAT WE OFFER</h2>
                <div class="ui-subtitle-block"><strong>Solution offered</strong> </div>
                <div class="border-color"></div>
                <p class="description">The website provides a range of services. The platform is not limited to a number of services that are limited to the digital market. The main solution offered are as follows.</p>
                <ul class="slider-services slider-services_mod-2">
                    <li data-wow-duration="2s" data-wow-delay=".5s" class="slide wow bounceInUp"> <img src="<?php echo base_url(); ?>frontend_assets/media/370x250/4.png" alt="Foto" class="img-responsive">
                        <div class="slider-services__info">
                            <div class="slider-services__title text-center">Categorization</div>
                        </div>
                    </li>
                    <li data-wow-duration="2s" data-wow-delay="1s" class="slide wow bounceInUp"> <img src="<?php echo base_url(); ?>frontend_assets/media/370x250/5.png" alt="Foto"  class="img-responsive">
                        <div class="slider-services__info">
                            <div class="slider-services__title text-center">Social Media Marketing</div>
                        </div>
                    </li>
                    <li data-wow-duration="2s" data-wow-delay="1.5s" class="slide wow bounceInUp"> <img src="<?php echo base_url(); ?>frontend_assets/media/370x250/6.png" alt="Foto"  class="img-responsive">
                        <div class="slider-services__info">
                            <div class="slider-services__title text-center">Data Collection</div>
                        </div>
                    </li>
                    <li data-wow-duration="2s" data-wow-delay=".5s" class="slide wow bounceInUp"> <img src="<?php echo base_url(); ?>frontend_assets/media/370x250/7.png" alt="Foto"  class="img-responsive">
                        <div class="slider-services__info">
                            <div class="slider-services__title text-center">Advertisement</div>
                        </div>
                    </li>
                    <li data-wow-duration="2s" data-wow-delay="1s" class="slide wow bounceInUp"> <img src="<?php echo base_url(); ?>frontend_assets/media/370x250/8.png" alt="Foto"  class="img-responsive">
                        <div class="slider-services__info">
                            <div class="slider-services__title text-center">Survey</div>
                        </div>
                    </li>
                    <li data-wow-duration="2s" data-wow-delay="1.5s" class="slide wow bounceInUp"> <img src="<?php echo base_url(); ?>frontend_assets/media/370x250/9.png" alt="Foto"  class="img-responsive">
                        <div class="slider-services__info">
                            <div class="slider-services__title text-center">Transcription</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section data-wow-duration="2s" class="section-area section_list-scheme triagl triagl-top wow bounceInUp">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="ui-title-block">WANT TO WORK WITH US?</h2>
                <div class="ui-subtitle-block"><strong>HOW WE CAN MAKE IT HAPPEN</strong> </div>
                <div class="border-color border-color_center"></div>
            </div>
        </div>
        <div class="row">
            <ul class="list-scheme">
                <li class="list-scheme__item">
                    <a href="javascript:void(0);" class="list-scheme__link">
                        <img src="<?php echo base_url(); ?>frontend_assets/media/happen/create-tasks.png" alt="create" />
                        <span class="list-scheme__number">1</span>
                        <div class="list-scheme__title">CREATE TASKS</div>
                    </a> 
                    <span class="arrow"><i class="arrow__inner fa fa-angle-right"></i></span>
                </li>
                <li class="list-scheme__item">
                    <a href="javascript:void(0);" class="list-scheme__link">
                        <img src="<?php echo base_url(); ?>frontend_assets/media/happen/post-tasks.png" alt="tasks" />
                        <span class="list-scheme__number">2</span>
                        <div class="list-scheme__title">POSTS TASKS</div>
                    </a> <span
                        class="arrow"><i class="arrow__inner fa fa-angle-right"></i> </span> </li>
                <li class="list-scheme__item">
                    <a href="javascript:void(0);" class="list-scheme__link">
<!--                        <i class="icon flaticon-logistics3 helper"></i>-->
                        <img src="<?php echo base_url(); ?>frontend_assets/media/happen/get-results.png" alt="results" />
                        <span class="list-scheme__number">3</span>
                        <div class="list-scheme__title">GET RESULT</div>
                    </a> <span
                        class="arrow"><i class="arrow__inner fa fa-angle-right"></i> </span> </li>
                <li class="list-scheme__item">
                    <a href="javascript:void(0);" class="list-scheme__link">
                        <!--<i class="icon flaticon-logisticsdelivery helper"></i>-->
                        <img src="<?php echo base_url(); ?>frontend_assets/media/happen/be-happy.png" alt="happy" />
                        <span class="list-scheme__number">4</span>
                        <div class="list-scheme__title">BE HAPPY !!! </div>
                    </a> </li>
            </ul>
        </div>
    </div>
</section>

<section data-wow-duration="2s" data-wow-delay="1s" class="section-area section_mod-3 section_mod-3b wow bounceInLeft">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <script src="<?php echo base_url(); ?>frontend_assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
                <ul class="list-progress">
                    <div class="col-md-3">
                        <li class="list-progress__item"><i class="icon fa fa-folder-open"></i>
                            <div class="list-progress__info"><span data-percent="648" class="chart"><span class="percent"></span></span> </div>
                            <div class="clearfix"></div>
                            <i class="brand-decor"><img src="<?php echo base_url(); ?>frontend_assets/images/brand-decor_white.png" width="25%" height="6" alt="Decor"></i><span class="list-progress__label">PUBLISHERS</span> 
                        </li>
                    </div>
                    <div class="col-md-3">
                        <li class="list-progress__item"><i class="icon fa fa-users"></i>
                            <div class="list-progress__info"><span data-percent="1694" class="chart"><span class="percent"></span></span> </div>
                            <div class="clearfix"></div>
                            <i class="brand-decor"><img src="<?php echo base_url(); ?>frontend_assets/images/brand-decor_white.png" width="25%" height="6" alt="Decor"></i><span class="list-progress__label">WORKERS</span>
                        </li>
                    </div>
                    <div class="col-md-3">
                        <li class="list-progress__item"><i class="icon fa fa-home"></i>
                            <div class="list-progress__info"><span data-percent="56" class="chart"><span class="percent"></span></span> </div>
                            <div class="clearfix"></div>
                            <i class="brand-decor"><img src="<?php echo base_url(); ?>frontend_assets/images/brand-decor_white.png" width="75" height="6" alt="Decor"></i><span class="list-progress__label">COMPLETED TASKS</span>
                        </li>
                    </div>
                    <div class="col-md-3">
                        <li class="list-progress__item"><i class="icon fa fa-comments-o"></i>
                            <div class="list-progress__info"><span data-percent="2543" class="chart"><span class="percent"></span></span> </div>
                            <div class="clearfix"></div>
                            <i class="brand-decor"><img src="<?php echo base_url(); ?>frontend_assets/images/brand-decor_white.png" width="75" height="6" alt="Decor"></i><span class="list-progress__label">TASKS IN PROGRESS </span>
                        </li>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</section>

<div data-wow-duration="2s" class="section-area section_block-contacts section_block-contacts_mod-b triagl triagl_mod-c triagl-top wow bounceInUp">
    <div class="container">
        <div class="block-contacts">
            <div class="row">

                <div class="col-lg-6 col-sm-4 col-xs-12">
                    <ul class="social-links">
                        <li class="social-links__item"><a class="social-links__link"><i class="icon fa fa-twitter"></i></a> </li>
                        <li class="social-links__item"><a class="social-links__link"><i class="icon fa fa-facebook"></i></a> </li>
                        <li class="social-links__item"><a class="social-links__link"><i class="icon fa fa-google-plus"></i></a> </li>
                        <li class="social-links__item"><a class="social-links__link"><i class="icon fa fa-linkedin"></i></a> </li>
                        <li class="social-links__item"><a class="social-links__link"><i class="icon fa fa-pinterest-p"></i></a> </li>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-4 col-xs-12 text-right">
                    <div class="block-contacts__phone">+91 9108167660</div>
                    <div class="block-contacts__phone text-info"><a href="" class="text-info">Hello@kaprat.com</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('website/footer'); ?>
