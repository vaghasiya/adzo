                        <div class="page-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="page-footer-inner hidden"> &copy; Copyrights 2017, Crowdsourcing Online Services Private Limited. All Right Reserved</div>
                                    <div class="scroll-to-top">
                                        <i class="icon-arrow-up"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div> <!-- page-content class closed from header-->
                </div> <!-- page-content-wrapper class closed from header-->
            </div> <!-- page-container class closed from header-->
        </div> <!-- page-wrapper class closed from header-->
        <div id="login_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Sign in</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <form id="user_login_form" action="<?php echo base_url(); ?>user/login-process" method="post" role="form" style="display: block;">
                                    <input type="hidden" name="redirect_to" value="publisher/dashboard"/>
                                    <?php
                                    $modal = $this -> session -> flashdata("modal");
                                    $class = $this -> session -> flashdata('class');
                                    $message = $this -> session -> flashdata('message');
                                    if(empty($class)) $class = "danger";
                                    if((!empty($message)) && ($modal == "login_modal")){ ?>
                                        <div class="alert alert-<?php echo $class; ?>">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <span class="message-text"><?php echo $message; ?></span>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <input type="text" name="user_name" id="user_name" tabindex="1" class="form-control" placeholder="Username" value="" required="required">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required="required">
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" tabindex="3" name="remember" id="remember">
                                        <label for="remember" style="font-weight: normal;"> Remember Me</label>
                                        <a data-toggle="modal" id="forget_password_link" href="#forget_password_modal" tabindex="6" class="forgot-password pull-right">Forgot Password?</a>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <input type="submit" id="login-submit-publisher" tabindex="4" class="form-control btn btn-info" value="Login Now">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-7 col-sm-offset-2 text-center">
                                            <a href="#">Participation Agreement</a>
                                            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                            <a href="#">Privacy Policy</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="forget_password_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Give us your details, We will Send you Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="<?php echo base_url(); ?>user/forget-password-process" method="post" role="form" style="display: block;">
                                    <input type="hidden" name="redirect_to" value="publisher/dashboard"/>
                                    <?php
                                    $class = $this -> session -> flashdata('class');
                                    $message = $this -> session -> flashdata('message');
                                    if(empty($class)) $class = "danger";
                                    if((!empty($message)) && ($modal == "forget_password_modal")){ ?>
                                        <div class="alert alert-<?php echo $class; ?>">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <span class="message-text"><?php echo $message; ?></span>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <input type="text" name="user_name" tabindex="1" class="form-control" placeholder="User Name" value="" required="required">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" tabindex="2" class="form-control" placeholder="Email Address" required="required">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <input type="submit" tabindex="4" class="form-control btn btn-info" value="Send Now">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="application/javascript">
            $(document).ready(function () {

                $('#example1').DataTable({
                    responsive: true,
                    columnDefs: [
                        {responsivePriority: 1, targets: 0},
                        {responsivePriority: 2, targets: -2}
                    ]
                });

                $('#example2').DataTable({
                    responsive: true,
                    columnDefs: [
                        {responsivePriority: 1, targets: 0},
                        {responsivePriority: 2, targets: -2}
                    ]
                });

                $('#example3').DataTable({
                    responsive: true,
                    columnDefs: [
                        {responsivePriority: 1, targets: 0},
                        {responsivePriority: 2, targets: -2}
                    ]
                });

                $("#forget_password_link").on("click",function(){
                    $("#login_modal").modal("hide");
                });
                <?php if($modal == "login_modal"){ ?>
                $("#login_modal").modal("show");
                <?php } else if($modal == "forget_password_modal"){ ?>
                $("#forget_password_modal").modal("show");
                <?php } ?>

                <?php $user_id = $this -> session -> userdata('user_id');
                if(empty($user_id)){ ?>
                $("a").on("click",function(){
                    $("#login_modal").modal("show");
                    return false;
                });
                <?php } ?>
            });
        </script>
    </body>
</html>