<html>
<head>
    <title>Adzo Digital Market</title>
</head>
<body>
<div style="width:500px; height: 300px; margin: 0 auto; background-color: #ffffff; border: 1px solid #ccc;  border-radius: 5px 5px 5px 5px; padding: 20px;">
    <div style="float: right; width: 150px;">
        <div style=" margin-bottom: 5px;">
            <a href="<?php echo base_url(); ?>">
                <img title="Adzo Digital Market" alt="Adzo Logo" src="http://oneodia.com/adzo/frontend_assets/images/logo.png">
            </a>
        </div>
    </div>
    <div style="float:left; width: 350px;">
        <p style=" font-family:'Helvetica'; font-size: 13px; line-height: 22px;margin-bottom:20px;">
            Hello <?php echo $user['first_name'].' '.$user['last_name']; ?>,<br/>
        </p>
        <p style="font-family:'Helvetica'; font-size: 13px; line-height: 22px;margin-bottom:20px;">
            You have requested to provide your forgotten password. <br/><br/>
            Your Login Details are as follows: <br/>
            User Name: <?php echo $user['user_name']; ?> <br/>
            Password: <?php echo $user['password']; ?>
        </p>
        <br/>
        <a style=" background: none repeat scroll 0 0 #018AD8; border: 1px solid #00507E; border-radius: 5px 5px 5px 5px; box-shadow: 0 0 1px 1px rgba(255, 255, 255, 0.3) inset;
    color: #FFFFFF; cursor: pointer;font-size: 18px; padding: 7px 15px 6px; text-align: center;text-decoration: none;font-family:'Helvetica';vertical-align:bottom;" href="<?php echo base_url(); ?>" target="_blank"">Click Here for Login</a>
        <p style=" font-family:'Helvetica'; font-size: 13px; line-height: 22px;margin-top:30px;">
            Regards,<br/>
            Adzo Digital Market Team
        </p>
    </div>
</div>
</body>
</html>