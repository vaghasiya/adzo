<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php echo $title; ?> | Adzo</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
<!--    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />-->
    <link href="<?php echo base_url(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo base_url(); ?>assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <!--message-->
    <link href="<?php echo base_url(); ?>assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/sweetalert2/sweetalert2.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/global/plugins/countdown/css/jquery.countdown.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/css/global.css" rel="stylesheet" type="text/css" />
    <!--<body class="page-container-bg-solid page-header-top-fixed">-->

    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/excanvas.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/apps/scripts/inbox.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/sweetalert2/sweetalert2.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/countdown/js/jquery.plugin.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/countdown/js/jquery.countdown.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/scripts/global.js"></script>

</head>
<div class="page-wrapper-row">
    <div class="page-wrapper-top">
        <div class="page-header">
            <div class="page-header-top">
                <div class="container">
                    <div class="page-logo">
                        <a href="<?php echo base_url(); ?>worker/dashboard">
                            <img src="<?php echo base_url(); ?>frontend_assets/images/logo.png" alt="logo" class="img-responsive" style="padding-top: 10px;">
                        </a>
                    </div>
                    <a href="javascript:;" class="menu-toggler"></a>
                    <div class="top-menu">
                        <?php
                        $user_id = $this -> session -> userdata('user_id');
                        if(empty($user_id)){ ?>
                            <ul class="nav navbar-nav pull-right">
                                <li><a data-toggle="modal" href="#login_modal" class="btn btn-info" style="border-radius: 15% ! important;">Login</a> </li>
                            </ul>
                        <?php } else { ?>
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-extended dropdown-notification dropdown-dark hidden" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default">7</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>You have
                                            <strong>12 pending</strong> tasks</h3>
                                        <a href="app_todo.html">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">just now</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span> New user registered.
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark hidden" id="header_task_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-calendar"></i>
                                    <span class="badge badge-default">3</span>
                                </a>
                                <ul class="dropdown-menu extended tasks">
                                    <li class="external">
                                        <h3>You have
                                            <strong>12 pending</strong> tasks</h3>
                                        <a href="app_todo_2.html">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">New release v1.2 </span>
                                                        <span class="percent">30%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>                                                                                                                
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="droddown dropdown-separator hidden">
                                <span class="separator"></span>
                            </li>
                            <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark hidden" id="header_inbox_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="circle">3</span>
                                    <span class="corner"></span>
                                </a>                                            
                            </li>
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="<?php echo $this -> session -> userdata('profile_pic'); ?>">
                                    <span class="username username-hide-mobile"><?php echo $this -> session -> userdata('first_name').' '.$this -> session -> userdata('last_name'); ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<?php echo base_url(); ?>worker/my-profile">
                                            <i class="icon-user"></i> My Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>worker/change-password">
                                            <i class="icon-user"></i> Change Password
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>user/logout">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown quick-sidebar-toggler">
                                <a href="<?php echo base_url(); ?>user/logout" class="dropdown-toggle" style="padding-top: 0px;">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
                        </ul>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="page-header-menu">
                <div class="container">                                
                    <div class="hor-menu  ">
                        <ul class="nav navbar-nav">
                            <li class="menu-dropdown classic-menu-dropdown <?php if($active_menu == "dashboard"){ echo "active"; } ?>">
                                <a href="<?php echo base_url(); ?>worker/dashboard"> Dashboard
                                    <span class="arrow"></span>
                                </a>                                            
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown  <?php if($active_menu == "all_project"){ echo "active"; } ?>">
                                <a href="javascript:;"> All Projects
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="<?php if($sub_menu == "all_task"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>tasks/all-tasks" class="nav-link">
                                            <i class="icon-bar-chart"></i> All Tasks
                                            <span class="badge badge-success hidden">1</span>
                                        </a>
                                    </li>
                                    <li class="<?php if($sub_menu == "individual_task"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>worker/individual" class="nav-link  ">
                                            <i class="icon-bulb"></i> Individual Task
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown <?php if($active_menu == "my_projects"){ echo "active"; } ?>">
                                <a href="javascript:;">Projects Qualified to me
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="">
                                        <a href="index.html" class="nav-link">
                                            <i class="icon-bar-chart"></i> All Tasks
                                            <span class="badge badge-success">1</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>worker/individual" class="nav-link  ">
                                            <i class="icon-bulb"></i> Individual Task
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown <?php if($active_menu == "my_account"){ echo "active"; } ?>">
                                <a href="<?php echo base_url(); ?>worker/my-account">My Cart
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="<?php if($sub_menu == "favourite_publishers"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>worker/favourite-publishers" class="nav-link">
                                            <i class="icon-bulb"></i>Favourite Publishers
                                        </a>
                                    </li>
                                    <li class="<?php if($sub_menu == "my_tasks"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>worker/my-tasks" class="nav-link">
                                            <i class="icon-badge"></i> Tasks in Queue
                                        </a>
                                    </li>
                                    <li class="<?php if($sub_menu == "reassigned_tasks"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>worker/reassigned-tasks" class="nav-link">
                                            <i class="icon-user-unfollow"></i> Reassigned Tasks
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown <?php if($active_menu == "summary"){ echo "active"; } ?>">
                                <a href="javascript:;">Summary
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="<?php if($sub_menu == "task_summary"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>worker/task-summary" class="nav-link">
                                            <i class="icon-bulb"></i> Task
                                        </a>
                                    </li>
                                    <li class="<?php if($sub_menu == "publisher"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>worker/publisher-summary" class="nav-link">
                                            <i class="icon-badge"></i> Publisher
                                        </a>
                                    </li>
                                    <li class="<?php if($sub_menu == "transaction"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>worker/transaction-summary" class="nav-link">
                                            <i class="icon-user-unfollow"></i> Transactions
                                        </a>
                                    </li>
                                    <li class="<?php if($sub_menu == "withdrawal"){ echo "active"; } ?>">
                                        <a href="<?php echo base_url(); ?>worker/withdrawal-summary" class="nav-link">
                                            <i class="icon-user-unfollow"></i> Withdrawals
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?php if($active_menu == "message"){ echo "active"; } ?>">
                                <a href="<?php echo base_url(); ?>worker/message" class="nav-link" style="padding-top: 15px;">
                                    <i class="icon-bar-chart"></i> Message
                                    <span class="badge badge-success">1</span>
                                </a>
                            </li>
                            <li class="<?php if($active_menu == "rate_publisher"){ echo "active"; } ?>">
                                <a href="javascript:;" class="nav-link">
                                    <i class="icon-bulb"></i>Rate Publisher
                                </a>
                            </li>
                            <li class="<?php if($active_menu == "create_task"){ echo "active"; } ?>">
                                <a href="javascript:;" class="nav-link">
                                    <i class="icon-bulb"></i>Create Tasks
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>