<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php echo $title; ?> | Adzo Admin </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
<!--    <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />-->
<!--    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />-->
    <link href="<?php echo base_url(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/layouts/layout5/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/layouts/layout5/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link href="<?php echo base_url(); ?>assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/global/plugins/sweetalert2/sweetalert2.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/global/css/global.css" rel="stylesheet" type="text/css" />

    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/excanvas.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/horizontal-timeline/horozontal-timeline.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>


    <script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/pages/scripts/login.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/sweetalert2/sweetalert2.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-ui/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/scripts/global.js"></script>
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo">
<div class="wrapper">
<header class="page-header">
    <nav class="navbar mega-menu" role="navigation">
        <div class="container-fluid">
            <div class="clearfix navbar-fixed-top">
                <a id="index" class="page-logo" href="index.html">
                    <img src="<?php echo base_url(); ?>assets/logo_new.png" alt="Logo"> </a>
                <div class="topbar-actions">                               
                    <div class="btn-group-img btn-group">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="color: white; text-decoration: none;">
                            <img alt="" class="img-circle" src="<?php echo $this -> session -> userdata('profile_pic'); ?>" style="height: 35px;"/>
                            <span class="username username-hide-on-mobile"><?php echo $this -> session -> userdata('first_name').' '.$this -> session -> userdata('last_name'); ?></span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu-v2" role="menu" style="margin-top: 10px;">
                            <li>
                                <a href="page_user_profile_1.html">
                                    <i class="icon-user"></i> My Profile
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>admin/change-password">
                                    <i class="icon-user"></i> Change Password
                                </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="<?php echo base_url(); ?>user/logout">
                                    <i class="icon-key"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </div>
                    <a class="btn quick-sidebar-toggler md-skip" href="<?php echo base_url(); ?>user/logout" style="top: 0;">
                        <i class="icon-logout"></i>
                    </a>
                </div>
            </div>
            <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-fw dropdown-fw-disabled <?php if($active_menu == "dashboard"){ echo "open selected"; } ?>">
                        <a href="<?php echo base_url(); ?>admin/dashboard" class="text-uppercase">
                            <i class="icon-home"></i> Dashboard </a>
                        <ul class="dropdown-menu dropdown-menu-fw hidden">
                            <li class="active">
                                <a href="<?php echo base_url(); ?>admin/dashboard">
                                    <i class="icon-bar-chart"></i> Default
                                </a>
                            </li>
                        </ul>    
                    </li>
                    <li class="dropdown dropdown-fw dropdown-fw-disabled <?php if($active_menu == "manage"){ echo "open selected"; } ?>">
                        <a href="javascript:;" class="text-uppercase">
                            <i class="icon-puzzle"></i> Manage </a>
                        <ul class="dropdown-menu dropdown-menu-fw">
                            <li class="<?php if($sub_menu == "manage_workers"){ echo "active"; } ?>">
                                <a href="<?php echo base_url(); ?>admin-manage-workers">
                                    <i class="icon-bar-chart"></i> Workers
                                </a>
                            </li>
                            <li class="<?php if($sub_menu == "manage_publishers"){ echo "active"; } ?>">
                                <a href="<?php echo base_url(); ?>admin-manage-publishers">
                                    <i class="icon-bulb"></i> Publishers
                                </a>
                            </li>
                            <li class="<?php if($sub_menu == "manage_batches"){ echo "active"; } ?>">
                                <a href="<?php echo base_url(); ?>admin-manage-batches">
                                    <i class="icon-graph"></i> Batches
                                </a>
                            </li>
                            <li class="<?php if($sub_menu == "manage_transactions"){ echo "active"; } ?>">
                                <a href="<?php echo base_url(); ?>admin-manage-transactions">
                                    <i class="icon-envelope-open"></i> Transactions
                                </a>
                            </li>
                            <li class="<?php if($sub_menu == "saved_projects"){ echo "active"; } ?>">
                                <a href="<?php echo base_url(); ?>admin/saved-project">
                                    <i class="icon-envelope-open"></i> Saved Projects
                                </a>
                            </li>
                            <li class="<?php if($sub_menu == "rating"){ echo "active"; } ?>">
                                <a href="javascript:;">
                                    <i class="icon-star"></i> Rating
                                </a>
                            </li>
                        </ul> 
                    </li>                                        
                    <li class="dropdown dropdown-fw dropdown-fw-disabled <?php if($active_menu == "qualification"){ echo "open selected"; } ?>">
                        <a href="<?php echo base_url(); ?>admin-qualification/manage" class="text-uppercase">
                            <i class="icon-key"></i> Qualification
                        </a>
                    </li>
                    <li class="dropdown dropdown-fw dropdown-fw-disabled <?php if($active_menu == "message"){ echo "open selected"; } ?>">
                        <a href="javascript:;" class="text-uppercase">
                            <i class="icon-envelope-open"></i> Messages
                        </a>
                        <ul class="dropdown-menu dropdown-menu-fw hidden">
                            <li class="active">
                                <a href="<?php echo base_url(); ?>admin/dashboard">
                                    <i class="icon-bar-chart"></i> Default
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-fw dropdown-fw-disabled <?php if($active_menu == "my_account"){ echo "open selected"; } ?>">
                        <a href="javascript:;" class="text-uppercase">
                            <i class="icon-user"></i> My Account
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>