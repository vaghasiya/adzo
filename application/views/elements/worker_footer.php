<div class="page-wrapper-row">
    <div class="page-wrapper-bottom">
        <div class="page-prefooter">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-6 col-xs-12 footer-block">
                        <ul style="padding-left: 0px; font-size: large;">
                            <li style="display: inline; float:left;">
                                <a href="javascript:;">Participation Agreement</a>
                            </li>
                            <li style="display: inline; float:left; margin-left: 30px;">
                                <a href="javascript:;">Privacy Policy</a>
                            </li>
                            <li style="display: inline; float:left; margin-left: 30px;">
                                <a href="javascript:;">FAQ</a>
                            </li>
                            <li style="display: inline; float:left; margin-left: 30px;">
                                <a href="javascript:;">Documentation</a>
                            </li>
                            <li style="display: inline; float:left; margin-left: 30px;">
                                <a href="javascript:;">Contact US</a>
                            </li>
                        </ul>
                        <h4 style="padding-top: 20px; clear: both;">&copy; Copyrights 2017, Crowdsourcing Online Services Private Limited. All Right Reserved</h4>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                        <h2>Follow Us On</h2>
                        <ul class="social-icons">
                            <li>
                                <a href="javascript:;" data-original-title="rss" class="rss"></a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="googleplus" class="googleplus"></a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="youtube" class="youtube"></a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="vimeo" class="vimeo"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-footer hidden">
            <div class="container"> 2016 &copy; Metronic Theme By
                <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
</div>
<div id="login_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sign in</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form id="user_login_form" action="<?php echo base_url(); ?>user/login-process" method="post" role="form" style="display: block;">
                            <input type="hidden" name="redirect_to" value="tasks/all-tasks"/>
                            <?php
                            $modal = $this -> session -> flashdata("modal");
                            $class = $this -> session -> flashdata('class');
                            $message = $this -> session -> flashdata('message');
                            if(empty($class)) $class = "danger";
                            if((!empty($message)) && ($modal == "login_modal")){ ?>
                                <div class="alert alert-<?php echo $class; ?>">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <span class="message-text"><?php echo $message; ?></span>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <input type="text" name="user_name" id="user_name" tabindex="1" class="form-control" placeholder="Username" value="" required="required">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required="required">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" tabindex="3" name="remember" id="remember">
                                <label for="remember" style="font-weight: normal;"> Remember Me</label>
                                <a data-toggle="modal" id="forget_password_link" href="#forget_password_modal" tabindex="6" class="forgot-password pull-right">Forgot Password?</a>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <input type="submit" id="login-submit-publisher" tabindex="4" class="form-control btn btn-info" value="Login Now">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7 col-sm-offset-2 text-center">
                                    <a href="#">Participation Agreement</a>
                                    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                    <a href="#">Privacy Policy</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="forget_password_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Give us your details, We will Send you Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="<?php echo base_url(); ?>user/forget-password-process" method="post" role="form" style="display: block;">
                            <input type="hidden" name="redirect_to" value="tasks/all-tasks"/>
                            <?php
                            $class = $this -> session -> flashdata('class');
                            $message = $this -> session -> flashdata('message');
                            if(empty($class)) $class = "danger";
                            if((!empty($message)) && ($modal == "forget_password_modal")){ ?>
                                <div class="alert alert-<?php echo $class; ?>">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <span class="message-text"><?php echo $message; ?></span>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <input type="text" name="user_name" tabindex="1" class="form-control" placeholder="User Name" value="" required="required">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" tabindex="2" class="form-control" placeholder="Email Address" required="required">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="submit" tabindex="4" class="form-control btn btn-info" value="Send Now">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true,
            columnDefs: [
                {responsivePriority: 1, targets: 0},
                {responsivePriority: 2, targets: -2}
            ]
        });

        $("#forget_password_link").on("click",function(){
            $("#login_modal").modal("hide");
        });
        <?php if($modal == "login_modal"){ ?>
        $("#login_modal").modal("show");
        <?php } else if($modal == "forget_password_modal"){ ?>
        $("#forget_password_modal").modal("show");
        <?php } ?>

        <?php $user_id = $this -> session -> userdata('user_id');
        if(empty($user_id)){ ?>
        $("a").on("click",function(){
            $("#login_modal").modal("show");
            return false;
        });
        <?php } ?>
    });
</script>
</html>
