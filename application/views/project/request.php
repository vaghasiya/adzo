<?php $this->load->view('elements/publisher_header'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Qualification</h3>
                    <p></p>
                    <p></p>
                    <ul class="nav-responsive nav nav-tabs">
                        <li class="dropdown pull-right tabdrop hide">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="glyph-icon icon-align-justify"></i>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu"></ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>project/qualification"><strong> Qualification </strong></a>
                        </li>
                        <li  class="active">
                            <a href="#" ><strong>Request</strong></a>
                        </li>
                    </ul>
                    <div class="portlet-body">
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="example" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="all">Worker ID</th>
                                        <th class="min-phone-l">Requested qualification</th>
                                        <th class="min-tablet">Qualification ID</th>
                                        <th class="">Score</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>DAKFJAKDFDAKLF</td>
                                        <td>Lijo qualification</td>
                                        <td>ADAFKJADKFJADK</td>
                                        <td>1-100 drop</td>
                                        <th class="text-center">
                                            <a href="#" class="text-success">Approve</a> |
                                            <a href="#" class="text-warning">Remove</a>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>DAKFJAKDFDAKLF</td>
                                        <td>Lijo qualification</td>
                                        <td>ADAFKJADKFJADK</td>
                                        <td>1-100 drop</td>
                                        <th class="text-center">
                                            <a href="#" class="text-success">Approve</a> |
                                            <a href="#" class="text-warning">Remove</a>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>DAKFJAKDFDAKLF</td>
                                        <td>Lijo qualification</td>
                                        <td>ADAFKJADKFJADK</td>
                                        <td>1-100 drop</td>
                                        <th class="text-center">
                                            <a href="#" class="text-success">Approve</a> |
                                            <a href="#" class="text-warning">Remove</a>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>DAKFJAKDFDAKLF</td>
                                        <td>Lijo qualification</td>
                                        <td>ADAFKJADKFJADK</td>
                                        <td>1-100 drop</td>
                                        <th class="text-center">
                                            <a href="#" class="text-success">Approve</a> |
                                            <a href="#" class="text-warning">Remove</a>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>DAKFJAKDFDAKLF</td>
                                        <td>Lijo qualification</td>
                                        <td>ADAFKJADKFJADK</td>
                                        <td>1-100 drop</td>
                                        <th class="text-center">
                                            <a href="#" class="text-success">Approve</a> |
                                            <a href="#" class="text-warning">Remove</a>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>