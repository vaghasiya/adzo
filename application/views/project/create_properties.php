<?php $this->load->view('elements/publisher_header'); ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="#">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Dashboard</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>&nbsp;
            <span class="thin uppercase hidden-xs"></span>&nbsp;
            <i class="fa fa-angle-down"></i>
        </div>
    </div>
</div>
<div class="content-box">
    <!-- Enter Properties -->
    <div class="example-box-wrapper panel-body">
        <form id="datacollection-step1" name="datacollection-step1"  class="form form-group form-horizontal datacollection-step1 bordered-row" method="POST">
            <div class="form-group">
                <label class="col-sm-3 control-label">Project Name</label>
                <div class="col-sm-6">
                    <input type="text" name="project_name" value="" id="project_name" class="form-control projName required">
                </div>
                <span class="col-sm-3" style="font-size: 10px;">This name is not displayed to workers.</span>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-6">
                    <input name="title" id="title" type="text" value="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-6">
                    <input name="description" id="description" type="text" value="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Keywords</label>
                <div class="col-sm-6">
                    <input name="keywords_description" type="text" value="" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox checkbox-success">
                        <label>
                            <input id="is_adult" type="checkbox" value="1" class="custom-checkbox" checked="checked" name="nudity">
                            This project may contain potentially explicit or offensive content, for eg, murder.
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <div class="input-group ">
                        <label class="control-label" style="text-align:left !important;">
                            Reward per assignment
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="number" name="reward_per_assignment" id="reward_per_assignment"  min="0.01"  step="0.01" max="999.00"  value="" class="form-control col-md-2 reward_per_assignment" onkeypress="return numberWithDecimal(event);"/>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group ">
                        <label class="control-label" style="text-align:left !important;">
                            Number of assignment per Task
                            <div class="input-group">
                                <input style="border-radius:3px;"  name="number_per_assignment" type="number" step="1" min="1" value="" class="form-control col-md-2" onkeypress="return numbersonly(this, event);">
                            </div>
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <div class="input-group ">
                        <label class="control-label" style="text-align:left !important;">
                            Time allotted per assignment 
                            <div class="input-group">
                                <input style="width:40%;" name="time_per_assignment" type="number" step="1" min="1" value="" class="form-control col-md-2 time_per_assignment" onkeypress="return numbersonly(this, event);">

                                <select class="form-control noofsecs" name="noofsecs" style="width:60%; float:right;">
                                    <option value="Days">Days</option>
                                    <option value="Hours">Hours</option>
                                    <option value="Minutes">Minutes</option>
                                </select>

                            </div>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group ">
                        <label class="control-label" style="text-align:left !important;">
                            Task expires in
                            <div class="input-group">
                                <input style="width:40%;" name="hit_expire" type="number" step="1" min="1" value="" class="form-control col-md-2 hit_expire" onkeypress="return numbersonly(this, event);">

                                <select class="form-control expire_noofsec" name="expire_noofsec" style="width:60%; float:right;">
                                    <option value="Days">Days</option>
                                    <option value="Hours">Hours</option>
                                    <option value="Minutes">Minutes</option>
                                </select>

                            </div>
                        </label>
                    </div>
                </div>
            </div>


            <div class="form-group">

                <div class="col-sm-12">
                    <label class="control-label col-sm-12" style="text-align:left !important;" >System Qualification</label>
                </div>

                <div class="col-sm-12">&nbsp;</div>

                <div class="col-sm-12">

                    <div class="form-group sysCommonQualificationNew systemQuailificationDiv">
                        <div class="col-md-12">
                            <div class="col-md-2 no-padding-right">
                                <select name="systemQuailification[]" class="form-control systemQuailification">

                                    <option value="location">Location</option>
                                    <option value="hit_approval">Task Approval Rate (%)</option>
                                    <option value="hit_reject">Task Rejection Rate (%)</option>
                                    <option value="hits_approved">Number of HITs Approved</option>
                                </select>
                            </div>
                            <!-- Location Drop Downs -->
                            <div class="col-md-2 location1div">
                                <select name="location1[]" class="form-control location1"  >

                                    <option  value="is">is</option>
                                    <option value="isoneof">is one of</option>
                                    <option value="isnot">is not</option>
                                    <option value="isnotoneof">is not one of</option>
                                </select>
                            </div>
                            <div class="col-md-3 location2div">
                                <select class="form-control location2" name="location2[]"  >
                                    <option value="all">All</option>
                                    <option value="IN">India</option>
                                    <option value="US">United States</option>
                                </select>
                            </div>
                            <div class="col-md-3 india_statesdiv">
                                <select name="india_states[]" class="form-control india_states">
                                    <option selected value="ALL">All</option>
                                </select>
                            </div>

                            <div class="col-md-3 us_statesdiv">
                                <select name="us_states[]" class="form-control us_states"  >
                                    <option selected value="ALL">All</option>
                                </select>
                            </div>

                            <!-- End of the location drop down -->

                            <!-- Hit Approval Rate -->
                            <div class="col-md-2 hitapprove1div">
                                <select class="form-control hitapprove1" name="hitapprove1[]"  >

                                    <option  value="gt">Greater than</option>
                                    <option value="gte">Greater than or equal to</option>
                                    <option value="lt">Less than</option>
                                    <option value="ngt">Less than or equal to</option>

                                </select>
                            </div>

                            <div class="col-md-2 hitapprove2div">
                                <select class="form-control hitapprove2" name="hitapprove2[]"  >
                                    <option value=""></option>
                                </select>
                            </div>
                            <!-- End of the Hit approval Rate -->

                            <!-- Hit Reject Rate -->
                            <div class="col-md-2 rejectrate2div" style="display:none;">
                                <select class="form-control rejectrate2" name="rejectrate2[]"  >
                                    <option value=""></option>
                                </select>
                            </div>
                            <!-- End of the Hit Reject Rate -->

                            <!-- Number of HITS Approved -->
                            <div class="col-md-2 noofhitsdiv" style="display:none;">
                                <select class="form-control noofhits" name="noofhits[]"  >
                                    <option value="0">0</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="2500">2500</option>
                                    <option value="5000">5000</option>
                                    <option value="10000">10000</option>
                                </select>
                            </div>
                            <!-- End of the HITS Approved -->

                            <!-- Remove Button -->
                            <div class="col-md-2" style="float:right;">
                                <button type="button" class="btn btn-info removeSysQualification">Remove</button>
                            </div>

                            <small class="help_small col-lg-12" style="padding-left:.6em; "><!--Maximum time your HIT will be available to Workers on Mechanical Turk.--></small>


                        </div>

                    </div>
                    <!-- Qualification drop downs for new Hit -->
                    <div class="form-group sysCommonQualificationNew systemQuailificationDiv1">
                        <div class="col-md-12">

                            <div class="col-md-2 no-padding-right">
                                <select name="systemQuailification[]" class="form-control systemQuailification">

                                    <option value="location">Location</option>
                                    <option value="hit_approval">Task Approval Rate (%)</option>
                                    <option value="hit_reject">Task Rejection Rate (%)</option>
                                    <option value="hits_approved">Number of HITs Approved</option>
                                </select>
                            </div>
                            <!-- Location Drop Downs -->
                            <div class="col-md-2 location1div" style="display:none;">
                                <select name="location1[]" class="form-control location1"  >

                                    <option value="is">is</option>
                                    <option value="isoneof">is one of</option>
                                    <option value="isnot">is not</option>
                                    <option value="isnotoneof">is not one of</option>
                                </select>
                            </div>
                            <div class="col-md-3 location2div" style="display:none;">
                                <select class="form-control location2" name="location2[]"  >

                                    <option value="all">All</option>
                                    <option value="IN">India</option>
                                    <option value="US">United States</option>

                                    <option value=""></option>

                                </select>
                            </div>
                            <div class="col-md-3 india_statesdiv" style="display:none;">
                                <select name="india_states[]" class="form-control india_states">

                                    <option value="ALL">All</option>
                                    <option value=""></option>

                                </select>
                            </div>

                            <div class="col-md-3 us_statesdiv" style="display:none;">
                                <select name="us_states[]" class="form-control us_states"  >

                                    <option value="ALL">All</option>

                                    <option value=""></option>
                                </select>
                            </div>

                            <!-- End of the location drop down -->

                            <!-- Hit Approval Rate -->
                            <div class="col-md-2 hitapprove1div" style="display:none;">
                                <select class="form-control hitapprove1" name="hitapprove1[]"  >
                                    <!--<option>Select</option>
                                            <option>equal to</option>
                                            <option>is one of</option>
                                            <option>less than</option>
                                            <option>less than or equal to</option>
                                            <option>not equal to</option>
                                            <option>is not one of</option>
                                    -->
                                    <option value="gt">Greater than</option>
                                    <option value="gte">Greater than or equal to</option>
                                    <option value="lt">Less than</option>
                                    <option value="ngt">Less than or equal to</option>

                                </select>
                            </div>

                            <div class="col-md-2 hitapprove2div" style="display:none;">
                                <select class="form-control hitapprove2" name="hitapprove2[]"  >
                                    <!--<option>Select</option>-->
                                    <option value=""></option>
                                </select>
                            </div>

                            <!-- End of the Hit approval Rate -->

                            <!-- Hit Reject Rate -->

                            <div class="col-md-2 rejectrate2div" style="display:none;">
                                <select class="form-control rejectrate2" name="rejectrate2[]"  >
                                    <!--<option>Select</option>-->
                                    <option value=""></option>
                                </select>
                            </div>
                            <!-- End of the Hit Reject Rate -->

                            <!-- Number of HITS Approved -->
                            <div class="col-md-2 noofhitsdiv" style="display:none;">
                                <select class="form-control noofhits" name="noofhits[]"  >
                                    <option value="0">0</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="2500">2500</option>
                                    <option value="5000">5000</option>
                                    <option value="10000">10000</option>
                                </select>
                            </div>
                            <!-- End of the HITS Approved -->

                            <!-- Remove Button -->
                            <div class="col-md-2" style="float:right;"><button type="button" class="btn btn-info removeSysQualification">Remove</button></div>

                            <small class="help_small col-lg-12" style="padding-left:.6em; "><!--Maximum time your HIT will be available to Workers on Mechanical Turk.--></small>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <button type="button" class="btn btn-info addQualificationCriteria">(+) Add another criterion</button>
                </div>

            </div>	

            <div class="form-group">
                <label class="col-md-3 control-label">Adzo Qualification</label>
                <div class="col-md-7">
                    <div class="col-md-4 input-group">
                        <select class="form-control adzo_qualification" name="adzo_qualification" >
                            <option>Nil</option>
                            <option value=""></option>
                        </select>
                    </div>
                    <small class="help_small" style="padding-top:2em"></small>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Qualification Type You Have Created</label>
                <div class="col-md-7">
                    <div class="col-md-4 input-group">
                        <select class="form-control your_qualification" name="your_qualification">
                            <option>Nil</option>
                            <option value=""></option>
                        </select>
                    </div>
                    <small class="help_small" style="padding-top:2em"></small>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Task Visibility</label>
                <div class="col-md-7">
                    <div class="col-md-12 input-group">

                        <input id="hit_validity" type="checkbox" value="1" class="custom-checkbox" name="hit_validity">
                        Only workers who accept can view my task.
                    </div>
                    <small class="help_small" style="padding-top:2em"></small>
                </div>
            </div>	

            <div class="form-group panel-body">
                <div class="example-box-wrapper" style="float:right;">
                    <input type="submit" value="Next" class="btn btn-primary default save-step1 ">
                    <input type="submit" value="Display" class="btn btn-primary default save-step1 ">
                </div>
            </div>
        </form>
    </div>
    <!-- End of the properties -->
</div>
<?php $this->load->view('elements/publisher_footer'); ?>