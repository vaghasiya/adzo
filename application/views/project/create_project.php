<?php $this->load->view('elements/publisher_header', array("title"=>"Create Project")); ?>
    <style>
        .error_control {
            border-color: #FF0000;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            background-color: #59c48e !important;
            color: white;
            font-weight: bold;
            border-top: 1px solid #ddd;
            border-left: 1px solid #ddd;
            border-right: 1px solid #ddd;
        }
    </style>
    <script type="application/javascript">
    var system_qualification_row_html = [
        '<tr>',
        '<td style="width: 200px;">',
        '<select class="form-control system_qualification">',
        '<option value="location">Location</option>',
        '<option value="hit_approval_rate">Task Approval Rate (%)</option>',
        '<option value="hit_rejection_rate">Task Rejection Rate (%)</option>',
        '<option value="total_hits_approved">Number of HITs Approved</option>',
        '</select>',
        '</td>',
        '<td style="width: 200px;">',
        '<div class="location_row">',
        '<select class="form-control location_key">',
        '<option value="is">is</option>',
        '<option value="is_one_of">is one of</option>',
        '<option value="is_not">is not</option>',
        '<option value="is_not_one_of">is not one of</option>',
        '</select>',
        '</div>',
        '<div class="compare_row" style="display: none;">',
        '<select class="form-control compare_key">',
        '<option value="greater_than">Greater than</option>',
        '<option value="greater_than_equal_to">Greater than or equal to</option>',
        '<option value="less_than">Less than</option>',
        '<option value="less_than_equal_to">Less than or equal to</option>',
        '</select>',
        '</div>',
        '</td>',
        '<td>',
        '<div class="location_row" style="display: inline;">',
        '<select class="location_value">',
        '<option value="All">All</option>',
        '<option value="India">India</option>',
        '<option value="United States of America">United States of America</option>',
        <?php foreach ($country_list as $row) {
            if($row['country_nice_name'] == "India" || $row['country_nice_name'] == "United States of America"){ continue; }
        ?>
        '<option value="<?php echo $row['country_nice_name']; ?>"><?php echo $row['country_nice_name']; ?></option>',
        <?php } ?>
        '</select>',
        '</div>',
        '<div class="india_state_div" style="display: none; margin-left: 15px;">',
        '<select class="india_state_value" multiple="multiple">',
        '<option value="All">All</option>',
        <?php foreach ($india_state_list as $row) { ?>
        '<option value="<?php echo $row['state_name']; ?>"><?php echo $row['state_name']; ?></option>',
        <?php } ?>
        '</select>',
        '</div>',
        '<div class="usa_state_div" style="display: none; margin-left: 15px;">',
        '<select class="usa_state_value" multiple="multiple">',
        '<option value="All">All</option>',
        <?php foreach ($usa_state_list as $row) { ?>
        '<option value="<?php echo $row['state_name']; ?>"><?php echo $row['state_name']; ?></option>',
        <?php } ?>
        '</select>',
        '</div>',
        '<div class="compare_row" style="display: none;">',
        '<select class="form-control compare_value">',
        <?php for($count=1; $count<101; $count++){ ?>
        '<option value="<?php echo $count; ?>"><?php echo $count; ?></option>',
        <?php } ?>
        '</select>',
        '</div>',
        '</td>',
        '<td style="width: 100px;">',
        '<input type="hidden" name="system_qualification[]" value=\'{"sq_type":"location","sq_friendly_name":"Location","sq_key":"is","sq_friendly_key":"is","sq_value":"All","sq_state_value":"All"}\' class="system_qualification_json"/>',
        '<button type="button" class="btn btn-info remove_system_qualification_button">Remove</button>',
        '</td>',
        '</tr>'
    ].join('');

    var availableTags = [
        "survey",
        "data collection",
        "social media",
        "categorization",
        "image",
        "video",
        "transcription",
        "data entry",
        "marketing",
        "research",
        "tagging",
        "sentiment",
        "moderation",
        "facebook",
        "twitter",
        "social media",
        "study"
    ];

    var project_name_status = true;

    $(document).ready(function() {
        $( "#keywords_description" )
            // don't navigate away from the field on tab when selecting an item
            .on( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).autocomplete( "instance" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                minLength: 0,
                source: function( request, response ) {
                    // delegate back to autocomplete, but extract the last term
                    response( $.ui.autocomplete.filter(
                        availableTags, extractLast( request.term ) ) );
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( ", " );
                    return false;
                }
            });

        $("#nudity").on("change",function(){
            if($(this).is(":checked")){
                $("#hit_visibility").prop("checked",true);
            } else {
                $("#hit_visibility").prop("checked",false);
            }
        });

        $("#hit_visibility").on("click",function(){
            if($("#nudity").is(":checked")){
                swal("Oops You have error","Sorry, This project may contain potentially explicit or offensive content..!!!","error");
                return false;
            }
        });

        $("#adzo_qualification_id").on("change",function(){
            var id = $(this).val();
            if(id == ""){
                $("#adzo_qualification_key").hide();
                $("#adzo_qualification_value").hide();
                $("#adzo_qualification_key").prop("selectedIndex", 0);
                $("#adzo_qualification_value").prop("selectedIndex", 0);
            } else {
                $("#adzo_qualification_key").show();
                $("#adzo_qualification_value").show();
            }
        });

        $("#publisher_qualification_id").on("change",function(){
            var id = $(this).val();
            if(id == ""){
                $("#publisher_qualification_key").hide();
                $("#publisher_qualification_value").hide();
                $("#publisher_qualification_key").prop("selectedIndex", 0);
                $("#publisher_qualification_value").prop("selectedIndex", 0);
            } else {
                $("#publisher_qualification_key").show();
                $("#publisher_qualification_value").show();
            }
        });

        $("#add_system_qualification_button").on("click",function(){
            var count = $('#system_qualification_table > tbody > tr').length;
            if(count >= 3){
                swal("Oops Limit Over","Sorry, you can add maximum of 3 qualification.!!!","error");
                return false;
            }
            $("#system_qualification_table").append(system_qualification_row_html);
            var last_row = $("#system_qualification_table > tbody > tr:last");
            $(".location_value", last_row).multiselect({
                buttonWidth: 200,
                maxHeight: 200,
                numberDisplayed: 1,
                allSelectedText: 'All Country Selected',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Search Country'
            });
            $(".india_state_value", last_row).multiselect({
                buttonWidth: 200,
                maxHeight: 200,
                numberDisplayed: 1,
                allSelectedText: 'All States Selected',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Search State'
            });
            $(".usa_state_value", last_row).multiselect({
                buttonWidth: 200,
                maxHeight: 200,
                numberDisplayed: 1,
                allSelectedText: 'All States Selected',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Search State'
            });
            var json = JSON.parse(last_row.find(".system_qualification_json").val());
            json.sq_value = $(".location_value", last_row).val();
            json.sq_state_value = "";
            last_row.find(".system_qualification_json").val(JSON.stringify(json));
        });

        $("#system_qualification_table").on("change", ".system_qualification", function(){
            var row = $(this).closest("tr");
            var json = JSON.parse(row.find(".system_qualification_json").val());
            var system_qualification_friendly_name = $(this).find(":selected").text();
            var system_qualification_type = $(this).val();
            var system_qualification_key = "";
            var system_qualification_friendly_key = "";
            var system_qualification_value = "";
            if(system_qualification_type == "location"){
                row.find(".location_key").prop("selectedIndex", 0);
                system_qualification_key = row.find(".location_key").val();
                system_qualification_friendly_key = row.find(".location_key").find(":selected").text();
                row.find(".location_value").removeAttr("multiple");
                row.find(".location_value").prop("selectedIndex", 0);
                system_qualification_value = row.find(".location_value").val();
                $(".location_value", row).multiselect('rebuild');
                row.find(".location_row").show();
                row.find(".location_row").css("display","inline");
                row.find(".compare_row").hide();
            } else {
                row.find(".compare_key").prop("selectedIndex", 0);
                row.find(".compare_value").prop("selectedIndex", 0);
                system_qualification_key = row.find(".compare_key").val();
                system_qualification_friendly_key = row.find(".compare_key").find(":selected").text();
                system_qualification_value = row.find(".compare_value").val();
                row.find(".location_row").hide();
                row.find(".compare_row").show();
            }
            row.find(".india_state_div").hide();
            row.find(".usa_state_div").hide();
            json.sq_friendly_name = system_qualification_friendly_name;
            json.sq_type = system_qualification_type;
            json.sq_key = system_qualification_key;
            json.sq_friendly_key = system_qualification_friendly_key;
            json.sq_value = system_qualification_value;
            json.sq_state_value = "";
            row.find(".system_qualification_json").val(JSON.stringify(json));
        });

        $("#system_qualification_table").on("change", ".location_key", function(){
            var row = $(this).closest("tr");
            var json = JSON.parse(row.find(".system_qualification_json").val());
            var system_qualification_key = $(this).val();
            var system_qualification_friendly_key = $(this).find(":selected").text();
            var system_qualification_value = [];
            if(system_qualification_key == "is" || system_qualification_key == "is_not"){
                row.find(".location_value").removeAttr("multiple");
            } else {
                row.find(".location_value").attr("multiple","multiple");
            }
            row.find(".location_value").prop("selectedIndex", 0);
            row.find(".india_state_div").hide();
            row.find(".usa_state_div").hide();
            $('.location_value option:selected', row).each(function() {
                system_qualification_value.push($(this).val());
            });
            system_qualification_value = system_qualification_value.join();
            $(".location_value", row).multiselect('rebuild');
            json.sq_key = system_qualification_key;
            json.sq_friendly_key = system_qualification_friendly_key;
            json.sq_value = system_qualification_value;
            json.sq_state_value = "";
            row.find(".system_qualification_json").val(JSON.stringify(json));
        });

        $("#system_qualification_table").on("change", ".location_value", function(){
            var row = $(this).closest("tr");
            var json = JSON.parse(row.find(".system_qualification_json").val());
            var system_qualification_value = [];
            var system_qualification_state_value = [];
            $('.location_value option:selected',row).each(function() {
                system_qualification_value.push($(this).val());
            });
            if(system_qualification_value[0] == "India" && system_qualification_value.length == 1){
                row.find(".india_state_div").css("display","inline");
                row.find(".india_state_value").prop("selectedIndex", 0);
                row.find(".usa_state_div").hide();
                $('.india_state_value option:selected',row).each(function() {
                    system_qualification_state_value.push($(this).val());
                });
                system_qualification_state_value = system_qualification_state_value.join();
                $(".india_state_value", row).multiselect('rebuild');
            } else if(system_qualification_value[0] == "United States of America" && system_qualification_value.length == 1){
                row.find(".india_state_div").hide();
                row.find(".usa_state_div").css("display","inline");
                row.find(".usa_state_value").prop("selectedIndex", 0);
                $('.usa_state_value option:selected',row).each(function() {
                    system_qualification_state_value.push($(this).val());
                });
                system_qualification_state_value = system_qualification_state_value.join();
                $(".usa_state_value", row).multiselect('rebuild');
            } else {
                row.find(".india_state_div").hide();
                row.find(".usa_state_div").hide();
                system_qualification_state_value = "";
            }
            system_qualification_value = system_qualification_value.join();
            json.sq_value = system_qualification_value;
            json.sq_state_value = system_qualification_state_value;
            row.find(".system_qualification_json").val(JSON.stringify(json));
        });

        $("#system_qualification_table").on("change", ".usa_state_value", function(){
            var row = $(this).closest("tr");
            var json = JSON.parse(row.find(".system_qualification_json").val());
            var system_qualification_state_value = [];
            $('.usa_state_value option:selected',row).each(function() {
                system_qualification_state_value.push($(this).val());
            });
            json.sq_state_value = system_qualification_state_value;
            row.find(".system_qualification_json").val(JSON.stringify(json));
        });

        $("#system_qualification_table").on("change", ".india_state_value", function(){
            var row = $(this).closest("tr");
            var json = JSON.parse(row.find(".system_qualification_json").val());
            var system_qualification_state_value = [];
            $('.india_state_value option:selected',row).each(function() {
                system_qualification_state_value.push($(this).val());
            });
            json.sq_state_value = system_qualification_state_value;
            row.find(".system_qualification_json").val(JSON.stringify(json));
        });

        $("#system_qualification_table").on("change", ".compare_key", function(){
            var row = $(this).closest("tr");
            var json = JSON.parse(row.find(".system_qualification_json").val());
            json.sq_key = $(this).val();
            json.sq_friendly_key = $(this).find(":selected").text();
            row.find(".system_qualification_json").val(JSON.stringify(json));
        });

        $("#system_qualification_table").on("change", ".compare_value", function(){
            var row = $(this).closest("tr");
            var json = JSON.parse(row.find(".system_qualification_json").val());
            json.sq_value = $(this).val();
            row.find(".system_qualification_json").val(JSON.stringify(json));
        });

        $(".Max999").on("change", function(){
            if($(this).val() > 999){
                $(this).val("999");
            }
        });

        $("#project_name").on("change", function(){
            var project_name = $(this).val();
            if(project_name=='') {
                swal({
                    text: "Please enter project name.! ! !",
                    type: 'warning'
                });
                $(".project_name").val("");
                $("#project_name").focus();
                project_name_status = false;
            } else {
                var post_data = {"project_name": project_name};
                ajax_request("<?php echo base_url(); ?>project/ajax-check-project-name", post_data, response_check_project_name, undefined);
                $(".project_name").val($(this).val());
            }
        });

        $("#system_qualification_table").on("click", ".remove_system_qualification_button", function(){
            var row = $(this).closest("tr");
            var json = JSON.parse(row.find(".system_qualification_json").val());
//            console.log(json);
            row.remove();
            var count = $('#system_qualification_table > tbody > tr').length;
            if(count <= 0){
                $("#add_system_qualification_button").trigger("click");
            }
        });

        $("#preview_finish_li").on("click",function(){
            var preview_project_title = $("#title").val();
            var preview_reward_per_assignment = $("#reward_per_assignment").val();
            var preview_time_per_assignment = $("#time_per_assignment").val() + " " + $("#time_per_assignment_unit").val();
            var preview_system_qualifications = "None";
            var count = $('#system_qualification_table > tbody > tr').length;
            var json = "";
            if(count >= 1){
                preview_system_qualifications = "";
                $('#system_qualification_table .system_qualification_json').each(function(index) {
                    json = JSON.parse($(this).val());
                    if(count > 1) {
                        preview_system_qualifications += "<br/>-&nbsp;";
                    }
                    preview_system_qualifications += json.sq_friendly_name
                    + " " + json.sq_friendly_key
                    + " " + json.sq_value;
                });
            }
            $("#preview_project_title").html(preview_project_title);
            $("#preview_reward_per_assignment").html(preview_reward_per_assignment);
            $("#preview_time_per_assignment").html(preview_time_per_assignment);
            $("#preview_system_qualifications").html(preview_system_qualifications);

            var design_layout_html = CKEDITOR.instances.design_layout.getData();
            $("#preview_frame").attr("srcdoc",design_layout_html);
        });

        $("#enter_property_next_button").on("click",function(){
            $("#design_layout_tab_anchor").trigger("click");
        });

        $("#design_layout_next_button").on("click",function(){
            $("#preview_finish_anchor").trigger("click");
        });

        $("#add_system_qualification_button").trigger("click");

        $("#finish_button").on("click", function () {
            if(check_validations()){
                swal("Great","Project is Creating Now.","success");
                setTimeout(function() { $("#create-project-form").submit(); }, 2500);
            } else {
                swal("Opps, You have some errors","Please correct all red-marked errors.! ! !","error");
            }
        });
    });

    function response_check_project_name(result, element){
        if(result != undefined && result != NaN && result != ""){
            result = JSON.parse(result);
            if(result.status == false){
                $("#project_name").val("");
                $("#project_name").focus();
                project_name_status = false;
                swal("Opps, Duplicate Error","Please Change Project Name.! ! !","error");
            } else {
                project_name_status = true;
            }
        }
    }

    function split( val ) {
        return val.split( /,\s*/ );
    }

    function extractLast( term ) {
        return split( term ).pop();
    }

    function get_variable_names() {
        var words = CKEDITOR.instances.design_layout.getData().split("${");
        var variables = [];
        var variable_index = 0;
        for(var i = 1; i < words.length; i++) {
            variable_index = words[i].indexOf("}");
            if(variable_index > 0) {
                variables.push(words[i].substring(0,variable_index).trim());
            }
        }
        return variables;
    }

    function is_array_values_alpha_numeric(variables) {
        var alpha_numeric_pattern = /^[a-z0-9\s]+$/i;
        var status = true;
        for(var i = 0; i < variables.length; i++) {
            if(alpha_numeric_pattern.test(variables[i]) == false){
                status = false;
            }
        }
        return status;
    }

    function design_layout_validations() {
        var status = true;
        var errors = "";
        if($("#preview_frame").contents().find("form").prop("tagName") == "FORM"){
            errors += '<li>Remove internal form tag (&lt;form&gt;)</li>';
            status = false;
        }
        var name_missing_counter = 0;
        var total_controls = 0;
        var total_buttons = 0;
        var name = "";
        $("#preview_frame").contents().find("input").each(function () {
            total_controls++;
            name = $(this).attr('name');
            if (typeof name == typeof undefined || name == false || name == "") {
                name_missing_counter++;
            }

            name = $(this).attr('type');
            if (name == "submit" || name == "button") {
                total_buttons++;
            }
        });

        $("#preview_frame").contents().find("button").each(function () {
            total_buttons++;
        });

        if(name_missing_counter > 0){
            errors += '<li>You have '+name_missing_counter+' controls witout name property. Please give name to each control.</li>';
            status = false;
        }

        if(total_buttons > 0){
            errors += '<li>You have created '+total_buttons+' buttons. Please remove all from layout.</li>';
            status = false;
        }

        if(total_controls < 1){
            errors += '<li>You did not create any input control for worker. Please create at least one.</li>';
            status = false;
        }

        if($("#category_type").val() == "multiple") {
            var variables = get_variable_names();
            if (variables.length < 1) {
                errors += '<li>You did not defined any variable. Please define at least one.</li>';
                status = false;
                $("#variables").val("");
            } else {
                if (is_array_values_alpha_numeric(variables) == true) {
                    $("#variables").val(variables.join());
                } else {
                    errors += '<li>Variable should be alpha-numeric only. Please remove special characters from variables.</li>';
                    status = false;
                    $("#variables").val("");
                }
            }
        }
        $("#design_errors_ul").html(errors);
        return status;
    }

    function enter_properties_validations() {
        var status = true;
        $('#enter_properties_tab input').filter('[required]').each(function(i, requiredField){
            if($(requiredField).val().trim()=='') {
                status = false;
                $(this).addClass("error_control");
            } else {
                $(this).removeClass("error_control");
            }
        });

        if(project_name_status == false){
            $("#project_name").addClass("error_control");
        } else {
            $("#project_name").removeClass("error_control");
        }

        return status;
    }

    function check_validations() {
        var status = true;

        if(enter_properties_validations() == false){
            $("#enter_properties_anchor").css("color","red");
            status = false;
        } else {
            $("#enter_properties_anchor").css("color","#337ab7");
        }

        if(design_layout_validations() == false){
            $("#design_layout_tab_anchor").css("color","red");
            $("#design_errors_div").show();
            status = false;
        } else {
            $("#design_layout_tab_anchor").css("color","#337ab7");
            $("#design_errors_div").hide();
        }
        return status;
    }
    </script>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Create New Project - <?php echo $category_name; ?></strong></div>
                <div class="panel-body">
                    <!--<div class="container">-->
                    <ul class="nav nav-tabs">
                        <li><a data-toggle="tab" href="#example_tab">Example</a></li>
                        <li class="active"><a data-toggle="tab" href="#enter_properties_tab" id="enter_properties_anchor">Enter Properties</a></li>
                        <li><a data-toggle="tab" href="#design_layout_tab" id="design_layout_tab_anchor">Design Layout</a></li>
                        <li id="preview_finish_li"><a data-toggle="tab" href="#preview_finish_tab" id="preview_finish_anchor">Preview and Finish</a></li>
                    </ul>
                    <form id="create-project-form" action="<?php echo base_url(); ?>project/save-project" class="form form-group form-horizontal datacollection-step1 bordered-row" method="POST">
                        <input name="category_id" value="<?php echo $category_id; ?>" type="hidden">
                        <input name="category_type" id="category_type" value="<?php echo $type; ?>" type="hidden">
                        <input name="csv_header" id="variables" value="<?php echo $variables; ?>" type="hidden">
                    <div class="tab-content">
                        <div id="example_tab" class="tab-pane fade">
                            <div class="row"><!-- Instructions -->
                                <div class="col-sm-12 col-md-12">
                                    <?php echo $category_html; ?>
                                </div>
                            </div>
                        </div>
                        <div id="enter_properties_tab" class="tab-pane fade in active">
                            <div class="content-box">
                                <div class="example-box-wrapper panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Project Name</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $project_name; ?>" name="project_name" id="project_name" class="form-control" type="text" maxlength="30" required="required">
                                        </div>
                                        <span class="col-sm-3" style="font-size: 10px;">This name is not displayed to workers.</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Title</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $title; ?>" name="title" id="title" class="form-control" type="text" maxlength="60" required="required">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Description</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $description; ?>" name="description" id="description" class="form-control" type="text" maxlength="250" required="required">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Keywords</label>
                                        <div class="col-sm-6">
                                            <input value="<?php echo $keywords; ?>" name="keywords_description" id="keywords_description" class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                            <div class="checkbox checkbox-success">
                                                <label>
                                                    <div class="checker" id="uniform-is_adult">
                                                        <input id="nudity" value="1" class="custom-checkbox" name="nudity" type="checkbox">
                                                    </div>
                                                    This project may contain potentially explicit or offensive content, for eg, murder.
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <div class="input-group ">
                                                <label class="control-label" style="text-align:left !important;">
                                                    Reward per assignment
                                                    <div class="input-group">
                                                        <span class="input-group-addon">$</span>
                                                        <input name="reward_per_assignment" id="reward_per_assignment" min="0.01" step="0.01" max="999.00" value="" class="form-control col-md-2 DecimalOnly" type="number" required="required">
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-group ">
                                                <label class="control-label" style="text-align:left !important;">
                                                    Number of assignment per Task
                                                    <div class="input-group">
                                                        <input name="number_of_assignment" id="number_of_assignment" step="1" min="1" value="1" class="form-control col-md-2 IntegerOnly" type="number" required="required">
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <div class="input-group ">
                                                <label class="control-label" style="text-align:left !important;">
                                                    Time allotted per assignment
                                                    <div class="input-group">
                                                        <input style="width:40%;" id="time_per_assignment" name="time_per_assignment" step="1" min="1" max="999" value="3" class="form-control col-md-2 IntegerOnly Max999" type="number" required="required">
                                                        <select class="form-control" name="time_per_assignment_unit" id="time_per_assignment_unit" style="width:60%; float:right;">
                                                            <option value="Days">Days</option>
                                                            <option selected="selected" value="Hours">Hours</option>
                                                            <option value="Minutes">Minutes</option>
                                                        </select>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-group ">
                                                <label class="control-label" style="text-align:left !important;">
                                                    Task expires in
                                                    <div class="input-group">
                                                        <input style="width:40%;" name="hit_expire" step="1" min="1" value="7" max="999" class="form-control col-md-2 IntegerOnly Max999" type="number" required="required">
                                                        <select class="form-control" name="hit_expire_unit" style="width:60%; float:right;">
                                                            <option value="Days">Days</option>
                                                            <option selected="selected" value="Hours">Hours</option>
                                                            <option value="Minutes">Minutes</option>
                                                        </select>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-12" style="text-align:left !important;">System Qualification</label>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 0px;">
                                        <div class="col-md-12">
                                            <table class="table" id="system_qualification_table" style="margin-bottom: 0px;">
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-info" id="add_system_qualification_button">(+) Add Qualification Criteria</button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Adzo Qualification</label>
                                        <div class="col-md-3">
                                            <select class="form-control" id="adzo_qualification_id" name="adzo_qualification_id">
                                                <option value="">None</option>
                                                <?php foreach ($admin_qualifications_list as $qualification) { ?>
                                                    <option value="<?php echo $qualification['qualification_id']; ?>"><?php echo $qualification['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control" id="adzo_qualification_key" name="adzo_qualification_key" style="display: none;">
                                                <option value="greater_than">Greater than</option>
                                                <option value="greater_than_equal_to">Greater than or equal to</option>
                                                <option value="less_than">Less than</option>
                                                <option value="less_than_equal_to">Less than or equal to</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control" id="adzo_qualification_value" name="adzo_qualification_value" style="display: none;">
                                                <?php for($count=0; $count<101; $count++){ ?>
                                                    <option value="<?php echo $count; ?>"><?php echo $count; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Qualification Type You Have Created</label>
                                        <div class="col-md-3">
                                            <select class="form-control your_qualification" id="publisher_qualification_id" name="publisher_qualification_id">
                                                <option value="">None</option>
                                                <?php foreach ($publisher_qualifications_list as $qualification) { ?>
                                                    <option value="<?php echo $qualification['qualification_id']; ?>"><?php echo $qualification['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control" id="publisher_qualification_key" name="publisher_qualification_key" style="display: none;">
                                                <option value="greater_than">Greater than</option>
                                                <option value="greater_than_equal_to">Greater than or equal to</option>
                                                <option value="less_than">Less than</option>
                                                <option value="less_than_equal_to">Less than or equal to</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control" id="publisher_qualification_value" name="publisher_qualification_value" style="display: none;">
                                                <?php for($count=0; $count<101; $count++){ ?>
                                                    <option value="<?php echo $count; ?>"><?php echo $count; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Task Visibility</label>
                                        <div class="col-md-7">
                                            <div class="col-md-12 input-group">
                                                <label class="control-label" style="text-align:left !important;">
                                                    <div class="checker"><input id="hit_visibility" value="true" class="custom-checkbox" name="hit_visibility" type="checkbox"></div>
                                                    Only workers who accept can view my task.
                                                </label>
                                            </div>
                                            <small class="help_small" style="padding-top:2em"></small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="example-box-wrapper" style="float:right;">
                                            <input class="btn btn-primary" type="button" id="enter_property_next_button" value="Next">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="design_layout_tab" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet box green">
                                        <div class="portlet-body form">
                                            <div class="form-group" style="border: none; margin-bottom: 0px;">
                                                <label class="col-md-2 control-label"><strong>Project Name</strong></label>
                                                <div class="col-md-6" style="border: none">
                                                    <input class="form-control project_name" type="text" value="<?php echo $project_name; ?>" readonly="readonly">
                                                </div>
                                                <div class="col-md-4" style="border: none">
                                                    <span>This name is not displayed to workers.</span>
                                                </div>
                                            </div>
                                            <div class="form-body">
                                                <div class="panel" style="margin-bottom: 0px; display: none;" id="design_errors_div">
                                                    <div class="panel panel-danger" style="margin-bottom: 10px;">
                                                        <div class="panel-heading">
                                                            <strong>You have following errors in design template code</strong>
                                                        </div>
                                                        <div class="panel-body">
                                                            <ul style="margin-bottom: 0px;" id="design_errors_ul">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <div class="col-md-12">
                                                        <textarea class="form-control ckeditor" name="design_layout" rows="6" id="design_layout">
                                                            <?php echo $design_layout; ?>
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="example-box-wrapper" style="float:right;">
                                                        <input class="btn btn-primary" type="button" id="design_layout_next_button" value="Next">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="preview_finish_tab" class="tab-pane fade">
                            <div class="row">
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-sm-12 padding-30">
                                    <div class="form-group" style="border: none">
                                        <label class="col-md-2 control-label"><strong>Project Name</strong></label>
                                        <div class="col-md-6" style="border: none">
                                            <input class="form-control project_name" type="text" value="<?php echo $project_name; ?>" readonly="readonly">
                                        </div>
                                        <div class="col-md-4" style="border: none">
                                            <span>This name is not displayed to workers.</span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <strong id="preview_project_title"></strong>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <span class="h5"><strong>Requester:</strong> <?php echo $this -> session -> userdata('first_name').' '.$this -> session -> userdata('last_name'); ?></span>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <span class="h5">
                                                            <strong>Rewards:&nbsp;&nbsp;</strong>
                                                            <span id="preview_reward_per_assignment"></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <span class="h5">
                                                            <strong>Hits Available:&nbsp;&nbsp;</strong>0</span>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <span class="h5">
                                                            <strong>Time Allotted:&nbsp;&nbsp;</strong>
                                                            <span id="preview_time_per_assignment"></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-12" style="margin-top: 15px;">
                                                        <span class="h5">
                                                            <strong>Qualifications Required:&nbsp;&nbsp;</strong>
                                                            <span id="preview_system_qualifications">None</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="col-md-12">
                                    <iframe frameborder="1" id="preview_frame" style="width: 100%; height: 450px;"></iframe>
                                    <br/>
                                </div>
                                <div class="row">
                                    <div class="col-md-10"></div>
                                    <div class="col-md-2">
                                        <input type="button" class="btn btn-danger" id="finish_button" value="Finish"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>