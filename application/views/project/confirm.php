<?php $this->load->view('elements/publisher_header'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Confirm</h3>
                    <p></p>
                    <p></p>
                    <div class="row">
                        <div class="col-md-7">
                            <a class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">Project Details</a>
                        </div>
                        <div class="col-md-2">
                            <span class="h5"><strong>Reward:</strong> $0.05 </span>
                        </div>
                        <div class="col-md-3">
                            <span class="h5"><strong>Time Allotted:</strong> 00:30:00 </span>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Project Details</h4>
                                </div>
                                <div class="modal-body">
                                    <div class=" row">
                                        <div class="modal-body project-details no-footer">
                                            <div class="row">
                                                <div class="col-xs-12" >
                                                    <h4><strong>Publisher</strong></h4>
                                                    <span>GoldenAgeTranscription</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <h4><strong>Project Title</strong></h4>
                                                    <span>Transcribe Short Audio Clips</span>
                                                </div>
                                            </div>
                                            <div class="row" >
                                                <div class="col-xs-12">
                                                    <h4><strong>Description</strong></h4>
                                                    <span>Transcribe short audio clips. Note: verbatim transcription required.</span></div>
                                            </div>
                                            <div class="row project-metadata m-b-0" >
                                                <div class="col-xs-6 field" >
                                                    <h4 ><strong>Tasks</strong></h4><span >14010</span>
                                                </div>
                                                <div class="col-xs-6 field">
                                                    <h4><strong>Created </strong></h4>
                                                    <span title="1/11/2017 06:57am" >1/11/2017</span>
                                                </div>
                                                <div class="col-xs-6 field">
                                                    <h5><strong>Time Allotted</strong></h5><span >30 Min</span>
                                                </div>
                                                <div class="col-xs-6 field">
                                                    <h4 ><strong>Expires </strong></h4>
                                                    <span title="3/12/2017 06:57am" >in 20d</span>
                                                </div>
                                                <div class="col-xs-6">
                                                    <h4 ><strong> Reward </strong></h4>
                                                    <span >$0.05</span>
                                                </div></div>
                                            <div class="row m-t-lg m-b-0" >
                                                <div class="col-xs-12" >
                                                    <a target="_blank" href="#">Contact This Requester</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>