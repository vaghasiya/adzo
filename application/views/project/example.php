<?php $this->load->view('elements/publisher_header'); ?>
<div class="portfolio-content">
    <div class="cbp-l-project-title">Instruction</div>
    <div class="cbp-l-project-container">
        <div class="cbp-l-project-desc">
            <div class="cbp-l-project-desc-text">
                <p>Find the company's name and email address from the information listed below. Do this by searching the business's website and the web (e.g. Google, Yelp, Facebook, Manta.com, Hunter.io, etc). Even if the email address is a general email address such as info@, enquiries@, note it down. If there is a specific person email address, note it down compulsorily. </p>
                <p>A Task will be rewarded only if the name and email address are found.</p>
                <p>Task will not be rewarded if the contact details provided are not the owner's contact details.</p>
                <p>Do not submit junk emails such as xxx@qq.com, email@address.com.</p>
                <p>Avoid taking career@, hr@, webmaster@ and privacy@ emails.</p>
                <p>Do not submit online ‘contact us form links’ if email address is not found.</p>
                <p>Company Name : Crowdsourcing Online Services Private Limited</p>
                <p>Location : Bangalore</p>
                <p>Website : www.adzocrowd.com</p>
                <p>Is the company's website active? : <input type="radio"/> Yes, it is active  <input type="radio"/> No, it is broken</p>
                <p>Email address 1 : <input type="text" /></p>
                <p>Email address 2 : <input type="text" /></p>
                <p>Email address 3 : <input type="text" /></p>
                <p>URL where your found the email address : </p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>
