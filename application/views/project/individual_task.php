<?php $this->load->view('elements/publisher_header'); ?>
<div class="portlet-title">
    <div class="caption">
        <i class="icon-bar-chart font-green-sharp"></i>
        <span class="caption-subject font-yellow-gold bold uppercase">Create</span>
    </div>
    <div class="tools">
        <button type="button" onclick="history.go(-1);" class="btn btn-default btn-circle"><i class="fa fa-reply"></i> Back</button>
        <a href="javascript:;" class="fullscreen" data-original-title="" title=""></a>
    </div>
</div>
<div class="portlet-body">
    <div class="tabbable-line">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#overview_1" data-toggle="tab">New Hit </a>
            </li>
        </ul>
    </div>
    <div style="margin-top:20px;" class="row">
        <form id="hit_create_form" accept-charset="utf-8" class="form" role="form" novalidate="novalidate">
            <div class="col-lg-11 col-md-6 col-sm-8 col-xs-9 bhoechie-tab-container">
                <div class="portlet box blue" style="margin-bottom: 3px;">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-edit"></i>Start a New Hit
                        </div>
                    </div>
                </div>

                <div class="col-md-12 padding-30">
                    <div class="portlet red-pink box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Describe Your HIT
                            </div>
                        </div>
                        <div class="portlet-body margin-top-10 form-horizontal">

                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label">Title:</label>
                                <div class="col-md-7">
                                    <div class="input-group col-md-6"><input name="title" placeholder="" value="" class="form-control hittitle" type="text">
                                    </div>
                                    <small class="help_small" style="padding-left:.6em">What do you want to call your HIT?</small>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Description:</label>
                                <div class="col-md-7">
                                    <div class="col-md-6 input-group">
                                        <textarea name="description" class="form-control description"></textarea>
                                    </div>
                                    <small class="help_small" style="padding-left:.6em">What is your HIT about? Enter a short description of the task you want completed.</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Keywords:</label>
                                <div class="col-md-7">
                                    <div class="col-md-6 input-group">
                                        <input name="hitkeywords" placeholder="" value="" class="form-control hitkeywords" type="text">
                                    </div>
                                    <small class="help_small" style="padding-left:.6em">What keywords do you want to associate with your HIT? Enter terms that will help workers find your HIT.</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Expires in:</label>
                                <div class="col-md-7">
                                    <div class="col-md-2 no-padding-right">
                                        <!--<input name="hitexpires" type="text" placeholder="" value="" class="form-control hitexpires">-->

                                        <input name="hitexpires" step="1" min="1" placeholder="" value="" class="form-control" onkeypress="return numbersonly(this, event);" type="number">
                                    </div>
                                    <div class="col-md-4">
                                        <select name="hitexpiretime" class="form-control hitexpiretime">
                                            <option value="Days">Days</option>
                                            <option selected="selected" value="Hours">Hours</option>
                                            <option value="Minutes">Minutes</option>
                                        </select>
                                    </div>
                                    <small class="help_small col-lg-12" style="padding-left:.6em">Maximum time your HIT will be available to Workers on Mechanical Turk.</small>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-12 padding-30">
                    <div class="portlet red-pink box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Provide Detailed Instructions
                            </div>
                        </div>
                        <div class="portlet-body margin-top-10 form-horizontal">

                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <div class="col-md-10 input-group">
                                        What is your HIT about? Enter a short description of the task you want completed.
                                    </div>

                                    <div class="col-md-10 input-group">
                                        <br>
                                        <textarea name="instructions" class="col-md-6 instructions"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Workers will have:</label>
                                <div class="col-md-8">
                                    <div class="col-md-2 no-padding-right">
                                        <!--<input name="timeallocated" type="text" placeholder="" value="" class="form-control timeallocated">-->
                                        <input name="timeallocated" step="1" min="1" max="360" placeholder="" value="" class="form-control timeallocated" onkeypress="return numbersonly(this, event);" type="number">
                                    </div>
                                    <div class="col-md-4">
                                        <select name="timeallottime" class="form-control timeallottime">
                                            <option value="Days">Days</option>
                                            <option selected="selected" value="Hours">Hours</option>
                                            <option value="Minutes">Minutes</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <small class="help_small col-lg-12" style="padding-left:.6em"> to complete this HIT.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 padding-30">
                    <div class="portlet red-pink box ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Select an Answer Format
                            </div>
                        </div>
                        <div class="portlet-body margin-top-10 form-horizontal">

                            <!-- Name input-->

                            <div class="form-group">

                                <div class="col-md-12 group_list">
                                    <a>
                                        <div class="col-md-2 hit_ans_type padding-right" hit-type="textanswer">
                                            <img src="<?php echo base_url(); ?>assets/images/answer-gallery-text.gif">
                                            <small class="help_small col-lg-8" style="padding-left:.6em">Free Text Answer</small>
                                        </div>
                                    </a>
                                    <a>
                                        <div class="col-md-2 hit_ans_type padding-right" hit-type="multichoicetext">
                                            <img src="<?php echo base_url(); ?>assets/images/answer-gallery-checkboxes-text.gif">
                                            <small class="help_small col-lg-8" style="padding-left:.6em">Multiple choice - text</small>
                                        </div>
                                    </a>
                                    <a>
                                        <div class="col-md-2 hit_ans_type padding-right" hit-type="multichoicepic">
                                            <img src="<?php echo base_url(); ?>assets/images/answer-gallery-checkboxes-images.gif">
                                            <small class="help_small col-lg-8" style="padding-left:.6em">Multiple choice - pictures</small>
                                        </div>
                                    </a>
                                    <a>
                                        <div class="col-md-2 hit_ans_type padding-right" hit-type="singlechoicetext">
                                            <img src="<?php echo base_url(); ?>assets/images/answer-gallery-radio-text.gif">
                                            <small class="help_small col-lg-8" style="padding-left:.6em">Single choice - text</small>
                                        </div>
                                    </a>
                                    <a>
                                        <div class="col-md-2 hit_ans_type padding-right" hit-type="singlechoicepic">
                                            <img src="<?php echo base_url(); ?>assets/images/answer-gallery-radio-images.gif">
                                            <small class="help_small col-lg-8" style="padding-left:.6em">Single choice - pictures</small>
                                        </div>
                                    </a>
                                    <a>
                                        <div class="col-md-2 hit_ans_type padding-right" hit-type="fileupload">
                                            <img src="<?php echo base_url(); ?>assets/images/answer-gallery-file-upload.gif">
                                            <small class="help_small col-lg-8" style="padding-left:.6em">File Upload</small>
                                        </div>
                                    </a>
                                </div>


                                <div class="gotoanstype" style="display:none; margin-left:10px;"><a href="javascript:void(0);">« Choose a different answer format</a></div>

                                <br>

                                <div class="col-md-10 padding-right ans_type_text" style="display:none;">
                                    Answer Format:  The worker will be able to enter text in this space.
                                    <div>
                                        <textarea readonly=""></textarea>

                                    </div>
                                </div>

                                <div class="col-md-10 padding-right multi_choice_text" style="display:none;">
                                    Answer Format:  The worker will be able to select any of the following choices.
                                    <ul class="multicheck">
                                        <li><div class="checker disabled"><span><input disabled="" type="checkbox"></span></div><input name="multi_check0" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                        <li><div class="checker disabled"><span><input disabled="" type="checkbox"></span></div><input name="multi_check1" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                        <li><div class="checker disabled"><span><input disabled="" type="checkbox"></span></div><input name="multi_check2" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                    </ul>

                                </div>

                                <div class="col-md-10 padding-right free_text multi_choice_pic" style="display:none;">
                                    Answer Format:  The worker will be able to select any of the following pictures.
                                    <ul class="multipiccheck">
                                        <li><div class="checker disabled"><span><input disabled="" type="checkbox"></span></div>Picture's URL<input name="multi_pic_check0" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                        <li><div class="checker disabled"><span><input disabled="" type="checkbox"></span></div>Picture's URL<input name="multi_pic_check1" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                        <li><div class="checker disabled"><span><input disabled="" type="checkbox"></span></div>Picture's URL<input name="multi_pic_check2" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                    </ul>
                                </div>

                                <div class="col-md-10 padding-right free_text single_choice_text" style="display:none;">
                                    Answer Format:  The worker will be able to enter text in this space.
                                    <ul class="singlechoise">
                                        <li><div class="radio disabled"><span><input disabled="" type="radio"></span></div> <input name="single_check0" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                        <li><div class="radio disabled"><span><input disabled="" type="radio"></span></div> <input name="single_check1" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                        <li><div class="radio disabled"><span><input disabled="" type="radio"></span></div> <input name="single_check2" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>

                                    </ul>
                                </div>

                                <div class="col-md-10 padding-right free_text single_choice_pic" style="display:none;">
                                    Answer Format:  The worker will be able to select one of the following choices.dd
                                    <ul class="singlechoisepic">
                                        <li><div class="radio disabled"><span><input disabled="" type="radio"></span></div>Picture's URL<input name="single_pic_check0" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                        <li><div class="radio disabled"><span><input disabled="" type="radio"></span></div>Picture's URL<input name="single_pic_check1" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                        <li><div class="radio disabled"><span><input disabled="" type="radio"></span></div>Picture's URL<input name="single_pic_check2" type="text"><a class="remove" href="javascript:void(0)">remove</a></li>
                                    </ul>
                                </div>

                                <div class="col-md-10 padding-right free_text hitfileupload" style="display:none;">
                                    Answer Format:  The worker will be able to upload a file (up to 100 MB) using his or her web browser's standard upload field as shown here. You can review and download the file when you review the work.
                                    <input disabled="" name="disabledUpload" type="file">
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-12 padding-30">
                    <div class="portlet red-pink box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Pay For Your HIT
                            </div>
                        </div>
                        <div class="portlet-body margin-top-10 form-horizontal">

                            <!-- Name input-->

                            <div class="form-group">
                                <label class="col-md-3 control-label">Assignments:</label>
                                <div class="col-md-7">
                                    <div class="col-md-2 no-padding-right">
                                        <input name="assignment" placeholder="" value="" class="form-control assignment" onkeypress="return numbersonly(this, event);" type="text">
                                    </div>
                                    <div class="col-md-3 no-padding-right">
                                        <label class="col-md-3 control-label">each&nbsp;paying&nbsp;$:</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input name="assignment_pay" placeholder="" value="" class="form-control assignment_pay" onkeypress="return numberWithDecimal(event);" type="text">
                                    </div>
                                    <small class="help_small col-lg-12" style="padding-left:.6em"> How many workers do you want to complete your HIT, and how much do you want to pay for each result?.</small>
                                    <small class="help_small col-lg-12" style="padding-left:.6em"><a data-toggle="modal" href="#what_is_it">What is this?</a></small>
                                </div>
                            </div>


                        </div>
                        <div class="col-lg-12 pulls">
                            <input name="anstype" class="ans_type" value="" type="hidden">
                            <input name="ans_content" class="ans_content" type="hidden">
                            <a data-toggle="modal" class="btn btn-lg default hit_form_clear">Clear  
                            </a>
                                                                                                <!--<input class="btn btn-lg blue" data-toggle="modal" id="hit_preview" data-target="#hit_preview" type="submit" value="Preview">-->

                            <a id="hit_preview" href="#hit_preview" data-toggle="modal" class="btn btn-lg default hit_form_clear">Preview  
                            </a>

                            <input class="btn btn-lg green" value="Save" type="submit">
<!--<a href="#hit_preview"  data-toggle="modal" class="btn btn-lg green hit_preview">Preview  <i class="fa fa-edit"></i>
</a>-->
                        </div>
                    </div>
                </div>

            </div>
        </form>
        <div class="clearfix"></div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>