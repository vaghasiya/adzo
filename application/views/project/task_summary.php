<?php $this->load->view('elements/publisher_header'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Results </h3>
                    <p></p>
                    <p></p>
                    <ul class="nav-responsive nav nav-tabs">
                        <li class="dropdown pull-right tabdrop hide">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="glyph-icon icon-align-justify"></i>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu"></ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>project/result"><strong> Submitted </strong></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>project/approved" ><strong>Approved</strong></a>
                        </li>
                        <li >
                            <a href="<?php echo base_url(); ?>project/rejected"><strong>Rejected</strong></a>
                        </li>
                        <li class="active">
                            <a href="#"><strong>Task Summary</strong></a>
                        </li>
                    </ul>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover dt-responsive">
                                    <tbody>
                                        <tr>
                                            <th>Batch Details</th>
                                            <td>Data Collection in the email</td>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <td class="text">
                                                <a href="#" class="btn red btn-xs">Cancel Batches</a>
                                                <a href="#" class="btn blue btn-xs">Result</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Project Name :</th>
                                            <td>Data Collection in the email</td>
                                        </tr>
                                        <tr>
                                            <th>Title :</th>
                                            <td>Collect data</td>
                                        </tr>
                                        <tr>
                                            <th>Description :</th>
                                            <td>Collect data</td>
                                        </tr>
                                        <tr>
                                            <th>Keywords :</th>
                                            <td>Collect data</td>
                                        </tr>
                                        <tr>
                                            <th>Keywords :</th>
                                            <td>Collect data</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover dt-responsive">
                                    <tbody>
                                        <tr>
                                            <th> Total assignments :</th>
                                            <td> 6545 </td>
                                            <th> Tasks expires in : </th>
                                            <td> 10 February, 2017 <span class="btn red btn-xs">Extend</span> </td>
                                        </tr>
                                        <tr>
                                            <th>Reward per Tasks :</th>
                                            <td> $0.03 </td>
                                            <th>Total cost :</th>
                                            <td> $100.00 </td>
                                        </tr>
                                        <tr>
                                            <th>Tasks submitted :</th>
                                            <td> 41 </td>
                                            <th> Tasks Approved :</th>
                                            <td> 10 </td>
                                        </tr>
                                        <tr>
                                            <th>Task Pending :</th>
                                            <td> 14 </td>
                                            <th>File :</th>
                                            <td> file.zip </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Worker Summary</h3>

                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="example3" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="all">Worker ID</th>
                                            <th class="min-phone-l">No of Tasks submit</th>
                                            <th class="min-tablet">Approved</th>
                                            <th class="">Rejected</th>
                                            <th class="">Pending</th>
                                            <th class="">Total Payment </th>                                           
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1-1-2017</td>
                                            <td>Collect email address </td>
                                            <td>854</td>
                                            <td>50</td>
                                            <td>804</td>
                                            <td>804</td>
                                            <th class="text"><a href="#" class="text-info">Result</a>|
                                                <a href="#" class="text-success">Delete</a>|
                                                <a href="<?php echo base_url(); ?>project/summary" class="text-warning">Summary</a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>1-1-2017</td>
                                            <td>Collect email address </td>
                                            <td>854</td>
                                            <td>50</td>
                                            <td>804</td>
                                            <td>804</td>
                                            <th class="text"><a href="#" class="text-info">Result</a>|
                                                <a href="#" class="text-success">Delete</a>|
                                                <a href="<?php echo base_url(); ?>project/summary" class="text-warning">Summary</a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>1-1-2017</td>
                                            <td>Collect email address</td>
                                            <td>854</td>
                                            <td>50</td>
                                            <td>804</td>
                                            <td>804</td>
                                            <th class="text"><a href="#" class="text-info">Result</a>|
                                                <a href="#" class="text-success">Delete</a>|
                                                <a href="<?php echo base_url(); ?>project/summary" class="text-warning">Summary</a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>1-1-2017</td>
                                            <td>Collect email address</td>
                                            <td>854</td>
                                            <td>50</td>
                                            <td>804</td>
                                            <td>804</td>
                                            <th class="text"><a href="#" class="text-info">Result</a>|
                                                <a href="#" class="text-success">Delete</a>|
                                                <a href="<?php echo base_url(); ?>project/summary" class="text-warning">Summary</a>
                                            </th>
                                        </tr>                                        
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>