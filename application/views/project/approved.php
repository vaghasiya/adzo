<?php $this->load->view('elements/publisher_header'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Results </h3>
                    <p></p>
                    <p></p>
                    <ul class="nav-responsive nav nav-tabs">
                        <li class="dropdown pull-right tabdrop hide">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="glyph-icon icon-align-justify"></i>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu"></ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>project/result"><strong> Submitted </strong></a>
                        </li>
                        <li class="active">
                            <a href="#" ><strong>Approved</strong></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>project/rejected"><strong>Rejected</strong></a>
                        </li>
                        <li >
                            <a href="<?php echo base_url(); ?>project/task_summary"><strong>Task Summary</strong></a>
                        </li>
                    </ul>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="example1" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="all" style="width: 200px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Title &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th class="min-phone-l"> Task ID </th>
                                    <th class="min-tablet">Assignment id</th>
                                    <th class="">Reward</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Collect email address from various</td>
                                    <td> TYLKIUDH </td>
                                    <td>AOLDAFJKDAJKK</td>
                                    <td>$10.03</td>
                                    <th class="text-center">
                                        <a href="#" class="text-info">Pay Bonus</a>|
                                        <a href="#" class="text-success">View Result</a>
                                    </th>
                                </tr> 
                                <tr>
                                    <td>Collect email address from various</td>
                                    <td> TYLKIUDH </td>
                                    <td>AOLDAFJKDAJKK</td>
                                    <td>$10.03</td>
                                    <th class="text-center">
                                        <a href="#" class="text-info">Pay Bonus</a>|
                                        <a href="#" class="text-success">View Result</a>
                                    </th>
                                </tr>
                                <tr>
                                    <td>Collect email address from various</td>
                                    <td> TYLKIUDH </td>
                                    <td>AOLDAFJKDAJKK</td>
                                    <td>$10.03</td>
                                    <th class="text-center">
                                        <a href="#" class="text-info">Pay Bonus</a>|
                                        <a href="#" class="text-success">View Result</a>
                                    </th>
                                </tr>
                                <tr>
                                    <td>Collect email address from various</td>
                                    <td> TYLKIUDH </td>
                                    <td>AOLDAFJKDAJKK</td>
                                    <td>$10.03</td>
                                    <th class="text-center">
                                        <a href="#" class="text-info">Pay Bonus</a>|
                                        <a href="#" class="text-success">View Result</a>
                                    </th>
                                </tr>
                                <tr>
                                    <td>Collect email address from various</td>
                                    <td> TYLKIUDH </td>
                                    <td>AOLDAFJKDAJKK</td>
                                    <td>$10.03</td>
                                    <th class="text-center">
                                        <a href="#" class="text-info">Pay Bonus</a>|
                                        <a href="#" class="text-success">View Result</a>
                                    </th>
                                </tr>                                        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>