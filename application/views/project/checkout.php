<?php $this->load->view('elements/publisher_header'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 class="alert alert-warning"><strong>Confirm and Publish Batch</strong></h3>
                    <p></p>
                    <p></p>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover dt-responsive">
                                <tbody>
                                    <tr>
                                        <th>Batch Details</th>
                                        <td>Data Collection in the email</td>
                                    </tr>
                                    <tr>
                                        <th>Project Name :</th>
                                        <td>Data Collection in the email</td>
                                    </tr>
                                    <tr>
                                        <th>Title :</th>
                                        <td>Collect data</td>
                                    </tr>
                                    <tr>
                                        <th>Description :</th>
                                        <td>Collect data</td>
                                    </tr>
                                    <tr>
                                        <th>Keywords :</th>
                                        <td>Collect data</td>
                                    </tr>
                                    <tr>
                                        <th>Keywords :</th>
                                        <td>Collect data</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover dt-responsive">
                                <tbody>
                                    <tr>
                                        <th> Total assignments :</th>
                                        <td> 6545 </td>
                                        <th> Tasks expires in : </th>
                                        <td> 10 February, 2017 </td>
                                    </tr>
                                    <tr >
                                        <th>Reward per Tasks :</th>
                                        <td> $0.03 </td>
                                        <th class="alert alert-info" style="font-size: 20px">Total cost :</th>
                                        <td class="alert alert-info" style="font-size: 20px"> $100.00 </td>
                                    </tr>                                    
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    <input type="button" class="btn btn-info pull-right" value="Checkout" />
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>