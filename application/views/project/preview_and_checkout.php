<?php $this->load->view('elements/publisher_header', array("title"=>"Publish Batch")); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li id="preview_task_li"><a data-toggle="tab" id="preview_task_anchor" href="#preview_hits_tab">Preview Tasks</a></li>
                        <li class="active" id="finish_checkout_li"><a data-toggle="tab" id="finish_checkout_anchor" href="#finish_checkout_tab">Finish & Checkout</a></li>
                    </ul>
                    <form id="publish-project-form" action="<?php echo base_url(); ?>project/checkout" class="form form-group form-horizontal" method="POST">
                        <input name="project_id" value="<?php echo $project_id; ?>" type="hidden">
                        <input name="use_wallet" id="use_wallet" value="false" type="hidden">
                        <input id="design_layout" value="<?php echo htmlentities($design_layout, ENT_QUOTES, 'utf-8'); ?>" type="hidden">
                        <input type="hidden" id="adzo_balance" value="<?php echo $user['balance']; ?>"/>
                        <input type="submit" class="hidden" id="publish-project-form-submit"/>
                        <div class="tab-content">
                            <div id="preview_hits_tab" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel" style="margin-bottom: 10px;">
                                            <div class="panel panel-primary" style="margin-bottom: 10px;">
                                                <div class="panel-heading">
                                                    <strong><?php echo $title; ?></strong>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <span class="h5"><strong>Requester:</strong> <?php echo $this -> session -> userdata('first_name').' '.$this -> session -> userdata('last_name'); ?></span>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <span class="h5">
                                                                <strong>Rewards:&nbsp;&nbsp;</strong>
                                                                <span><?php echo $reward_per_assignment; ?></span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <span class="h5">
                                                                <strong>Hits Available:&nbsp;&nbsp;</strong><?php echo $total_hits; ?>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <span class="h5">
                                                                <strong>Time Allotted:&nbsp;&nbsp;</strong>
                                                                <span><?php echo $time_per_assignment.' '.$time_per_assignment_unit; ?></span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top: 15px;">
                                                            <span class="h5">
                                                                <strong>Qualifications Required:&nbsp;&nbsp;</strong>
                                                                <span>
                                                                    <?php
                                                                    foreach ($system_qualifications as $system_qualification) {
                                                                        if(count($system_qualifications) > 1){ echo "<br>"; }
                                                                        echo "{$system_qualification['sq_friendly_name']} {$system_qualification['sq_friendly_key']} {$system_qualification['sq_value']}";
                                                                        if(!empty($system_qualification['sq_state_value'])){
                                                                            echo " [{$system_qualification['sq_state_value']}]";
                                                                        }
                                                                    } ?>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="col-md-12">
                                        <div class="panel" style="margin-bottom: 10px;">
                                            <div class="panel panel-primary" style="margin-bottom: 10px;">
                                                <div class="panel-heading text-center" style="background-color: #eaf3fe; color: #2a609d;">
                                                    <strong>Task Preview</strong>
                                                </div>
                                                <div class="panel-body">
                                                    <?php if($category_type == "multiple") {
                                                        $design_layout = str_replace($csv_header, $csv_data[0], $design_layout);
                                                    } ?>
                                                    <iframe frameborder="0" id="preview_frame" style="width: 100%; height: 450px; max-height: 900px;" srcdoc='<?php echo htmlentities($design_layout, ENT_QUOTES, 'utf-8'); ?>'></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($category_type == "multiple"){ ?>
                                    <div class="col-md-12">
                                        <div class="col-md-5">
                                            <input type="button" class="btn btn-info pull-right" id="previous_task_button" value="Previous Task" style="display: none;"/>
                                        </div>
                                        <div class="col-md-2 text-center" style="padding-top: 5px;">
                                            Showing Task <span id="current_hit_number">1</span> of <?php echo $total_hits; ?>
                                        </div>
                                        <div class="col-md-5">
                                            <?php if($total_hits > 1){ ?>
                                                <input type="button" class="btn btn-info" id="next_task_button" value="Next Task"/>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="col-md-12">
                                        <input type="button" id="next_tab_button" class="btn btn-success pull-right" value="Go to Finish Step"/>
                                    </div>
                                </div>
                            </div>
                            <div id="finish_checkout_tab" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-striped table-bordered table-hover dt-responsive">
                                            <tbody>
                                            <tr>
                                                <th>Batch Details</th>
                                                <td>
                                                    <label for="batch_name">Batch Name:&nbsp;&nbsp;</label>
                                                    <input id="batch_name" type="text" class="form-control" value="<?php echo $batch_name; ?>" name="batch_name" maxlength="60" style="display: inline; width: 300px;" required="required"/>
                                                    <label for="batch_description" style="margin-left: 30px;">Batch Description: </label>&nbsp;&nbsp;
                                                    <input id="batch_description" type="text" class="form-control" value="<?php echo $description; ?>" name="batch_description" maxlength="250" style="display: inline; width: 300px;" required="required"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Project Name</th>
                                                <td><?php echo $project_name; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Title</th>
                                                <td><?php echo $title; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <td><?php echo $description; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Keywords</th>
                                                <td><?php echo $keywords_description; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Batch Expires in</th>
                                                <td><?php echo $hit_expire.' '.$hit_expire_unit; ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table table-striped table-bordered table-hover dt-responsive">
                                            <thead>
                                            <th colspan="2" class="text-center">Cost Summary</th>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th>Reward per assignment</th>
                                                <td><span style="padding-left: 10px;">$<?php echo $reward_per_assignment; ?></span></td>
                                            </tr>
                                            <tr>
                                                <th>Total Assignments</th>
                                                <td><span style="padding-left: 10px;">&nbsp;&nbsp;x&nbsp;<?php echo $total_assignments; ?></span></td>
                                            </tr>
                                            <tr>
                                                <th>Estimated Total Rewards</th>
                                                <td style="border-top: 2px solid #000000;"><span style="padding-left: 10px;">$<?php echo $total_rewards_amount; ?></span></td>
                                            </tr>
                                            <tr>
                                                <th>Adzo Fees (<?php echo intval($adzo_fees_percentage)."% of $$total_rewards_amount"; ?>)</th>
                                                <td><span style="padding-left: 10px;">$<?php echo $adzo_fees_amount; ?></span></td>
                                            </tr>
                                            <tr>
                                                <th class="alert alert-info" style="font-size: 20px;">Total cost</th>
                                                <td class="alert alert-info" style="font-size: 20px; padding-left: 10px;">$<span id="total_amount_span"><?php echo $total_amount; ?></span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table table-striped table-bordered table-hover dt-responsive">
                                            <thead>
                                                <th colspan="2" class="text-center">Task Summary</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th> Total tasks in csv file</th>
                                                    <td><span style="padding-left: 10px;"><?php echo $total_hits; ?></span></td>
                                                </tr>
                                                <tr>
                                                    <th> Number of assignments per task</th>
                                                    <td>x&nbsp;<?php echo $number_of_assignment; ?></td>
                                                </tr>
                                                <tr>
                                                    <th> Total Assignments</th>
                                                    <td style="border-top: 2px solid #000000;"><span style="padding-left: 10px;"><?php echo $total_assignments; ?></span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="alert alert-info"><input type="button" id="checkout_by_wallet_button" class="btn btn-success" value="Deduct from My Adzo Wallet" /></td>
                                                    <td class="alert alert-info"><input type="button" id="checkout_button" class="btn btn-info pull-right" value="Checkout" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12">
                                        <input type="button" id="previous_tab_button" class="btn btn-success" value="Go Back to Preview Task"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>
<script>
    var batch_name_status = true;
    var csv_header = <?php echo json_encode($csv_header); ?>;
    var csv_data = <?php echo json_encode($csv_data); ?>;
    $(document).ready(function() {
        $("#batch_name").on("change", function(){
            var batch_name = $(this).val();
            if(batch_name=='') {
                $("#batch_name").focus();
                batch_name_status = false;
                swal({
                    text: "Please enter batch name.! ! !",
                    type: 'warning'
                });
            } else {
                var post_data = {"batch_name": batch_name};
                ajax_request("<?php echo base_url(); ?>project/ajax-check-batch-name", post_data, function (result, element) {
                    if(result != undefined && result != NaN && result != ""){
                        result = JSON.parse(result);
                        if(result.status == false){
                            $("#batch_name").val("");
                            $("#batch_name").focus();
                            batch_name_status = false;
                            swal("Opps, Duplicate Error","Please Enter Unique Batch Name.! ! !","error");
                        } else {
                            batch_name_status = true;
                        }
                    }
                }, undefined);
            }
        });

        $("#next_task_button").on("click",function(){
            var total_hits = csv_data.length;
            var current_hit_index = parseInt($("#current_hit_number").html()) - 1;
            var next_hit_index = current_hit_index + 1;
            var design_layout_html = $("#design_layout").val();
            design_layout_html = $("<div />").html(design_layout_html).html();
            var replace_value = csv_data[next_hit_index];
//            design_layout_html = "This is sample data for ${company name} - ${location} - ${website} for testing ${location} data";
            $(csv_header).each(function (index) {
                design_layout_html = design_layout_html.replace(new RegExp("\\"+csv_header[index], "gi"), replace_value[index]);
            });
            $("#preview_frame").attr("srcdoc",design_layout_html);
            $("#current_hit_number").html(next_hit_index+1);
            if($("#current_hit_number").html() == "2"){
                $("#previous_task_button").show();
            }
            if($("#current_hit_number").html() == total_hits){
                $("#next_task_button").hide();
            }
        });

        $("#previous_task_button").on("click",function(){
            var total_hits = csv_data.length;
            var current_hit_index = parseInt($("#current_hit_number").html()) - 1;
            var previous_hit_index = current_hit_index - 1;
            var design_layout_html = $("#design_layout").val();
            var replace_value = csv_data[previous_hit_index];
//            design_layout_html = "This is sample data for ${company name} - ${location} - ${website} for testing ${location} data";
            $(csv_header).each(function (index) {
                design_layout_html = design_layout_html.replace(new RegExp("\\"+csv_header[index], "gi"), replace_value[index]);
            });
            $("#preview_frame").attr("srcdoc",design_layout_html);
            $("#current_hit_number").html(previous_hit_index+1);
            if($("#current_hit_number").html() == "1"){
                $("#previous_task_button").hide();
            }
            if($("#current_hit_number").html() == total_hits-1){
                $("#next_task_button").show();
            }
        });

        $("#previous_tab_button").on("click",function(){
            $("#preview_task_anchor").trigger("click");
        });

        $("#next_tab_button").on("click",function(){
            $("#finish_checkout_anchor").trigger("click");
        });

        $("#checkout_button").on("click",function(){
            if(batch_name_status == false){
                $("#batch_name").focus();
                swal({
                    text: "Please enter valid batch name.! ! !",
                    type: 'warning'
                });
            } else {
                $("#use_wallet").val("false");
                $("#publish-project-form-submit").trigger("click");
            }
        });

        $("#checkout_by_wallet_button").on("click",function(){
            if(batch_name_status == false){
                $("#batch_name").focus();
                swal({
                    text: "Please enter valid batch name.! ! !",
                    type: 'warning'
                });
            } else {
                var adzo_balance = parseFloat($("#adzo_balance").val());
                var total_amount = parseFloat($("#total_amount_span").html());
                if(total_amount <= adzo_balance){
                    $("#use_wallet").val("true");
                    $("#publish-project-form-submit").trigger("click");
                } else {
                    $("#use_wallet").val("false");
                    swal("Opps, Error Found","You do not have enough balance for checkout","error");
                }
            }
        });
    });
</script>