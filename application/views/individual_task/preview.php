<?php $this->load->view('elements/publisher_header'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="portlet box purple" style="margin-bottom: 3px;">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-yelp fa-4x" style="font-size: 20px"></i>
                                Preview Tasks
                            </div>
                        </div>
                    </div>
                    <p></p>
                    <hr>
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" cellspacing="0" width="100%">                            
                            <tbody>
                                <tr>
                                    <td><strong><a data-toggle="modal" data-target="#myModal">Project Details</a></strong></td>                                    
                                    <td><strong>Task Available:</strong></td>
                                    <td>1000</td>
                                    <td><strong>Duration:</strong></td>
                                    <td>1000</td> 
                                    <td><strong>Reward:</strong></td>
                                    <td>USD100</td> 
                                </tr>                                                                         
                            </tbody>
                        </table>                            
                    </div>
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Project Details</h4>
                                </div>
                                <div class="modal-body">
                                    <div class=" row">
                                        <div class="modal-body project-details no-footer">
                                            <div class="row">
                                                <div class="col-xs-12" >
                                                    <h4><strong>Publisher</strong></h4>
                                                    <span>GoldenAgeTranscription</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <h4><strong>Project Title</strong></h4>
                                                    <span>Transcribe Short Audio Clips</span>
                                                </div>
                                            </div>
                                            <div class="row" >
                                                <div class="col-xs-12">
                                                    <h4><strong>Description</strong></h4>
                                                    <span>Transcribe short audio clips. Note: verbatim transcription required.</span></div>
                                            </div>
                                            <div class="row project-metadata m-b-0" >
                                                <div class="col-xs-6 field" >
                                                    <h4 ><strong>Tasks</strong></h4><span >14010</span>
                                                </div>
                                                <div class="col-xs-6 field">
                                                    <h4><strong>Created </strong></h4>
                                                    <span title="1/11/2017 06:57am" >1/11/2017</span>
                                                </div>
                                                <div class="col-xs-6 field">
                                                    <h5><strong>Time Allotted</strong></h5><span >30 Min</span>
                                                </div>
                                                <div class="col-xs-6 field">
                                                    <h4 ><strong>Expires </strong></h4>
                                                    <span title="3/12/2017 06:57am" >in 20d</span>
                                                </div>
                                                <div class="col-xs-6">
                                                    <h4 ><strong> Reward </strong></h4>
                                                    <span >$0.05</span>
                                                </div></div>
                                            <div class="row m-t-lg m-b-0" >
                                                <div class="col-xs-12" >
                                                    <a target="_blank" href="#">Contact This Requester</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="text-center">Instructions</h3>
                    The goal is to categorize these profile pictures that are typically got from Facebook.

                    <li>Choose appropriate category.</li>
                    <li>Any image other than male & female consider as N/A.</li>
                    <li>Profile is picture is not clear choose N/A</li>

                    <li>Please note that not following the above rules may disqualify you from performing subsequent categorization tasks.</li>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/03.jpg"  class="img-responsive" alt="text"  width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>                                                    
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/04.jpg"  class="img-responsive" alt="hghg"  width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/9.jpg" class="img-responsive" alt="htyedr" width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/01.jpg"  class="img-responsive" alt="text"  width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>                                                    
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/05.jpg"  class="img-responsive" alt="hghg"  width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label> 
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/08.jpg" class="img-responsive" alt="htyedr" width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/1.jpg"  class="img-responsive" alt="text"  width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>                                                    
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/03.jpg"  class="img-responsive" alt="hghg"  width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label><br>
                            <input type="button" class="btn btn-danger" value="Submit" />
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/global/img/portfolio/600x600/09.jpg" class="img-responsive" alt="htyedr" width="80%" />
                            <label class="mt-radio"> Male
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> Female
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label>
                            <label class="mt-radio"> None
                                <input value="1" name="test" type="radio">
                                <span></span>
                            </label> 
                        </div>
                    </div>
                </div>
                <label class="mt-btm-transform"> Showing 1 out of 100
                <input type="button" class="btn btn-success" value="Next" />
                                <span></span>
                </label> 
                <label class="mt-btm-transform" style="float: right">
                <input type="button" class="btn btn-warning" value="Cancel" />
                                <span></span>
                </label>
                <label class="mt-btm-transform" style="float: right">
                <input type="button" class="btn btn-info" value="Next" />
                                <span></span>
                </label>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>