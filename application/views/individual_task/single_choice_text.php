<?php $this->load->view('elements/publisher_header'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="portlet box red" style="margin-bottom: 3px;">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-edit fa-4x" style="font-size: 20px"></i>
                                Single Choice Text
                            </div>
                        </div>
                    </div>
                    <p></p>
                    <p></p>
                    <hr>
                    <form>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="title"><b>Title :</b></label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="title" placeholder="Enter Title">
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>   
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="description"><b>Description :</b></label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="description" placeholder="Enter Description">
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="keywords"><b>Keywords :</b></label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="keywords" placeholder="Enter Keywords">
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="expires"><b>Expires in :</b></label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="expires" placeholder="Enter Expires in">
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="keywords"><b>Instruction :</b></label>
                                </div>
                                <div class="col-md-6">
                                    <textarea class="form-control" ></textarea>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="keywords"><b>Task expires in :</b></label>
                                </div>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" id="keywords">
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control">
                                        <option>mints</option>
                                        <option>Hours</option>
                                    </select>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="keywords"><b>Answer Format :</b></label>
                                </div>
                                <div class="col-md-1">
                                    <div class="md-checkbox-inline">
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox6" class="md-check">
                                            <label for="checkbox6">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="keywords">
                                </div>
                                <div class="col-md-6">
                                     <button type="submit" class="btn btn-info">Remove</button>
                                </div>                                
                            </div>
                            <br><br>
                            <div class="form-group">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-1">
                                    <div class="md-checkbox-inline">
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox34" class="md-check">
                                            <label for="checkbox34">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="keywords">
                                </div>
                                <div class="col-md-6">
                                     <button type="submit" class="btn btn-info">Remove</button>
                                </div>                                
                            </div>
                            <br><br>
                            <div class="form-group">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-1">
                                    <div class="md-checkbox-inline">
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox35" class="md-check">
                                            <label for="checkbox35">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="keywords">
                                </div>
                                <div class="col-md-6">
                                     <button type="submit" class="btn btn-info">Remove</button>
                                </div>                                
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="keywords"><b>Reward per assignment:</b></label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="keywords" placeholder="Reward per assignment">
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="keywords"><b>Number of assignment :</b></label>
                                </div>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" placeholder="Number of assignment">
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-9">
                            </div>                            
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-danger">Preview</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>