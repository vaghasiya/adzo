<?php $this->load->view('elements/publisher_header', array("title"=>"Transaction Summary","active_menu"=>"transaction_summary")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $('#table_list').DataTable({
                responsive: true
            });
        });
    </script>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Transaction Summary</h3>
                        <?php
                        $class = $this -> session -> flashdata('class');
                        $message = $this -> session -> flashdata('message');
                        if(empty($class)) $class = "danger";
                        if(!empty($message)){ ?>
                            <div class="alert alert-<?php echo $class; ?>">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <span class="message-text"><?php echo $message; ?></span>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="table_list" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Date-Time</th>
                                        <th class="text-center">Transaction ID</th>
                                        <th>From<br>To</th>
                                        <th>Party Name</th>
                                        <th class="text-center">Details</th>
                                        <th class="text-right">Debit</th>
                                        <th class="text-right">Credit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($transactions_list as $row) { ?>
                                        <tr>
                                            <td><?php echo date("Y-m-d", strtotime($row['created_date']))."<br>".date("H:i:s", strtotime($row['created_date'])); ?></td>
                                            <td><?php echo $row['transaction_reference_no']; ?></td>
                                            <td><?php if($row['type'] == "debit"){ echo "To"; } else if($row['type'] == "credit") { echo "From"; } ?></td>
                                            <td><?php echo $row['first_name']." ".$row['last_name']; ?></td>
                                            <td><?php echo $row['details']; ?></td>
                                            <td class="text-right"><?php if($row['type'] == "debit"){ echo $row['transaction_amount']; } else { echo "&nbsp;"; } ?></td>
                                            <td class="text-right"><?php if($row['type'] == "credit"){ echo $row['transaction_amount']; } else { echo "&nbsp;"; } ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('elements/publisher_footer'); ?>