<?php $this->load->view('elements/publisher_header', array("title"=>"Worker Summary","active_menu"=>"worker_summary")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $('#days_table').DataTable();
            $('#months_table').DataTable();
            $('#lifetime_table').DataTable();
        });
    </script>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Worker Summary</h3>
                        <?php
                        $class = $this -> session -> flashdata('class');
                        $message = $this -> session -> flashdata('message');
                        if(empty($class)) $class = "danger";
                        if(!empty($message)){ ?>
                            <div class="alert alert-<?php echo $class; ?>">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <span class="message-text"><?php echo $message; ?></span>
                            </div>
                        <?php } ?>
                        <ul class="nav nav-tabs">
                            <li class="active bold alert-info"><a href="#days_tab" data-toggle="tab">7 Days</a></li>
                            <li class="bold alert-danger"><a href="#months_tab" data-toggle="tab">30 Days</a></li>
                            <li class="bold alert-warning"><a href="#lifetime_tab" data-toggle="tab">Life Time</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="days_tab" class="tab-pane fade in active">
                                <table class="table table-striped table-bordered table-hover" width="100%" id="days_table" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Worker ID</th>
                                        <th class="text-center">Submitted</th>
                                        <th class="text-center">Approved</th>
                                        <th class="text-center">Rejected</th>
                                        <th class="text-center">Review<br>Pending</th>
                                        <th class="text-center">Task<br>In Progress</th>
                                        <th class="text-center">Payments</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($week_list as $row) { ?>
                                        <tr>
                                            <td class=""><?php echo $row['unique_user_id']; ?></td>
                                            <td class="text-center"><?php echo $row['submitted']; ?></td>
                                            <td class="text-center"><?php echo $row['approved']; ?></td>
                                            <td class="text-center"><?php echo $row['rejected']; ?></td>
                                            <td class="text-center"><?php echo $row['review_pending']; ?></td>
                                            <td class="text-center"><?php echo $row['in_progress']; ?></td>
                                            <td class="text-right">$&nbsp;<?php if(empty($row['payment'])){ echo "0"; } else { echo $row['payment']; } ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="months_tab" class="tab-pane fade">
                                <table class="table table-striped table-bordered table-hover" width="100%" id="months_table" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Worker ID</th>
                                        <th class="text-center">Submitted</th>
                                        <th class="text-center">Approved</th>
                                        <th class="text-center">Rejected</th>
                                        <th class="text-center">Review<br>Pending</th>
                                        <th class="text-center">Task<br>In Progress</th>
                                        <th class="text-center">Payments</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($month_list as $row) { ?>
                                        <tr>
                                            <td class=""><?php echo $row['unique_user_id']; ?></td>
                                            <td class="text-center"><?php echo $row['submitted']; ?></td>
                                            <td class="text-center"><?php echo $row['approved']; ?></td>
                                            <td class="text-center"><?php echo $row['rejected']; ?></td>
                                            <td class="text-center"><?php echo $row['review_pending']; ?></td>
                                            <td class="text-center"><?php echo $row['in_progress']; ?></td>
                                            <td class="text-right">$&nbsp;<?php if(empty($row['payment'])){ echo "0"; } else { echo $row['payment']; } ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="lifetime_tab" class="tab-pane fade">
                                <table class="table table-striped table-bordered table-hover" width="100%" id="lifetime_table" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Worker ID</th>
                                        <th class="text-center">Submitted</th>
                                        <th class="text-center">Approved</th>
                                        <th class="text-center">Rejected</th>
                                        <th class="text-center">Review<br>Pending</th>
                                        <th class="text-center">Task<br>In Progress</th>
                                        <th class="text-center">Payments</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($life_time_list as $row) { ?>
                                        <tr>
                                            <td class=""><?php echo $row['unique_user_id']; ?></td>
                                            <td class="text-center"><?php echo $row['submitted']; ?></td>
                                            <td class="text-center"><?php echo $row['approved']; ?></td>
                                            <td class="text-center"><?php echo $row['rejected']; ?></td>
                                            <td class="text-center"><?php echo $row['review_pending']; ?></td>
                                            <td class="text-center"><?php echo $row['in_progress']; ?></td>
                                            <td class="text-right">$&nbsp;<?php if(empty($row['payment'])){ echo "0"; } else { echo $row['payment']; } ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('elements/publisher_footer'); ?>