<?php $this->load->view('elements/publisher_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $('#projects_list').DataTable({
                "order": [[ 2, "desc" ]]
            });

            $("#csv_file").on("change",function(event){
                var filename = event.target.files[0].name;
                var file = filename.split(".");
                if(file[1] != "csv"){
                    $(this).val("");
                    swal("Opps, You have Error","Please Select CSV File Only.","error");
                }
            });

            $("#verify_csv_button").on("click",function(){
                if($("#csv_file").val() != "") {
                    var formData = new FormData($("#publish_project_form")[0]);
                    formData.append('csv_file', $('#csv_file')[0].files[0]);
                    var action = $("#publish_project_form").attr("action");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>project/ajax-upload-csv",
                        data: formData,
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function(result){
                            result = JSON.parse(result);
                            if(result.status == "success"){
                                $('#csv_file').val("");
                                $("#publish_project_form").submit();
                            } else {
                                $("#errors_list_div").show();
                                var li_html = "";
                                $(result.errors).each(function (index) {
                                    li_html += '<li>' + result.errors[index] +'</li>';
                                });
                                $("#errors_list_ul").html(li_html);
                                $("#error_progress_bar").css("width","100%");
                            }
                        },
                        error: function() {
                            console.log('Data Request Failed.');
                        }
                    });
                } else {
                    swal("Opps, You have Error","Please Select CSV File.","error");
                }
            });

            $("#download_latest_csv_link").on("click",function(){
                $("#download_sample_project_id").val($("#publish_project_id").val());
                $("#download_latest_csv_file").val("true");
                $("#download_sample_data_form").submit();
            });

            $("#download_sample_csv_button").on("click",function(){
                $("#download_sample_project_id").val($("#publish_project_id").val());
                $("#download_latest_csv_file").val("false");
                $("#download_sample_data_form").submit();
            });

            $("#projects_list").on("click",".project_publish_button",function(){
                var project_id = $(this).data("id");
                var category_type = $(this).data("category-type");
                $("#publish_project_id").val(project_id);
                if(category_type == "multiple") {
                    var csv_file = $(this).data("csv-file");
                    if (csv_file != "") {
                        $("#download_latest_csv_div").show();
                    } else {
                        $("#download_latest_csv_file").val("false");
                        $("#download_latest_csv_div").hide();
                    }
                    $("#publish_project_modal").modal("show");
                } else {
                    $("#publish_project_form").submit();
                }
            });

            $("#projects_list").on("click",".project_edit_button",function(){
                var project_id = $(this).data("id");
                $("#edit_project_id").val(project_id);
                $("#edit_project_form").submit();
            });

            $("#tab2").on("click",".project_copy_button",function(){
                var project_id = $(this).data("id");
                $("#copy_project_id").val(project_id);
                $("#copy_project_modal").modal("show");
            });

            $("#copy_project_button").on("click",function(){
                var copy_project_name = $("#copy_project_name").val();
                if(copy_project_name != "") {
                    var post_data = {"project_name": copy_project_name};
                    ajax_request("<?php echo base_url(); ?>project/ajax-check-project-name", post_data, function (result, element) {
                        result = JSON.parse(result);
                        if(result.status == false){
                            $("#copy_project_name").val("");
                            $("#copy_project_name").focus();
                            swal("Opps, Duplicate Error","Please Change Project Name.! ! !","error");
                        } else {
                            $("#copy_project_form").submit();
                        }
                    }, undefined);
                } else {
                    swal("Opps, You have Error","Please enter project name.","error");
                }
            });

            $("#projects_list").on("click",".project_delete_button",function(){
                var project_id = $(this).data("id");
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this process.!!!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    $("#delete_project_id").val(project_id);
                    $("#delete_project_form").submit();
                });
            });

            $(".category_name").on("click",function(){
                <?php $user_id = $this -> session -> userdata('user_id');
                if(empty($user_id)) { ?>
                $("#login_modal").modal("show");
                <?php } else { ?>
                var category_id = $(this).data("category-id");
                $("#category_id").val(category_id);
                $("#create_project_form").submit();
                <?php } ?>
            });
        });
    </script>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php
                    $class = $this -> session -> flashdata('class');
                    $message = $this -> session -> flashdata('message');
                    if(empty($class)) $class = "danger";
                    if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $class; ?>">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <span class="message-text"><?php echo $message; ?></span>
                        </div>
                    <?php } ?>
                    <h3>Create Project</h3>
                    <p></p>
                    <ul class="nav-responsive nav nav-tabs">
                        <li class="dropdown pull-right tabdrop hide">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="glyph-icon icon-align-justify"></i>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu"></ul>
                        </li>
                        <li class="active">
                            <a href="#tab1" data-toggle="tab"><strong>NEW PROJECT</strong></a>
                        </li>
                        <li>
                            <a href="#tab2" class="<?php $user_id = $this -> session -> userdata('user_id'); if(empty($user_id)){ echo "hidden"; } ?>" data-toggle="tab"><strong>SAVED PROJECTS</strong></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <form action="<?php echo base_url(); ?>project/create-project" id="create_project_form" method="post">
                                <input type="hidden" id="category_id" name="category_id" value="" required="required"/>
                            </form>
                            <div class="panel">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 class="title-hero">
                                            Multiple Tasks
                                        </h3>
                                        <div class="example-box-wrapper">
                                            <div class="row">
                                                <?php foreach ($multiple_categories_list as $category) { ?>
                                                    <div class="col-md-4">
                                                        <div class="content-box">
                                                            <h3 class="content-box-header bg-red category_name" style="cursor: pointer; color: #fff; padding: 4px;" data-category-id="<?php echo $category['category_id']; ?>">
                                                                <?php echo $category['category_name']; ?>
                                                            </h3>
                                                            <div class="content-box-wrapper">
                                                                <img src="<?php echo base_url(); ?>assets/global/img/tasks/<?php echo $category['img_link']; ?>" width="95%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 class="title-hero">
                                            Individual Tasks
                                        </h3>
                                        <div class="example-box-wrapper">
                                            <div class="row">
                                                <?php foreach ($individual_categories_list as $category) { ?>
                                                    <div class="col-md-4">
                                                        <div class="content-box">
                                                            <h3 class="content-box-header bg-red category_name" style="cursor: pointer; color: #fff; padding: 4px;" data-category-id="<?php echo $category['category_id']; ?>">
                                                                <?php echo $category['category_name']; ?>
                                                            </h3>
                                                            <div class="content-box-wrapper">
                                                                <img src="<?php echo base_url(); ?>assets/global/img/tasks/<?php echo $category['img_link']; ?>" width="95%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-globe"></i>SAVED PROJECTS</div>
                                            <div class="tools"> </div>
                                        </div>
                                        <div class="portlet-body">
                                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="projects_list" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Project Name</th>
                                                        <th>Title</th>
                                                        <th style="width: 90px;">Created/Edited</th>
                                                        <th class="text-center" style="width: 200px !important;">Actions &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($projects_list as $project) { ?>
                                                    <tr>
                                                        <td><?php echo $project['project_name']; ?></td>
                                                        <td><?php echo $project['title']; ?></td>
                                                        <td><?php echo date("M d, Y H:i", strtotime($project['updated_date'])); ?></td>
                                                        <td class="text">
                                                            <button type="button" data-id="<?php echo $project['project_id']; ?>" data-csv-file="<?php echo $project['csv_file']; ?>" data-category-type="<?php echo $project['category_type']; ?>" class="btn red btn-xs project_publish_button">Publish</button>
                                                            <button type="button" data-id="<?php echo $project['project_id']; ?>" class="btn blue btn-xs project_edit_button">Edit</button>
                                                            <button type="button" data-id="<?php echo $project['project_id']; ?>" class="btn green btn-xs project_copy_button">Copy</button>
                                                            <button type="button" data-id="<?php echo $project['project_id']; ?>" class="btn yellow btn-xs project_delete_button">Delete</button>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>
                    </div>
                </div>                
            </div>
        </div>           
    </div>
</div>
<div id="publish_project_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Publish Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form id="publish_project_form" action="<?php echo base_url(); ?>project/publish-project" method="post" role="form" style="display: block;" enctype="multipart/form-data">
                            <input type="hidden" name="project_id" id="publish_project_id" value=""/>
                            <input type="hidden" id="upload_confirmed" value="0"/>
                            <div class="form-group">
                                Choose a .csv file with the variables you specified in your project.
                            </div>
                            <div class="form-group">
                                <input type="file" id="csv_file" name="csv_file" class="form-control" accept="text/csv"/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="button" class="form-control btn btn-info" id="verify_csv_button" value="Verify & Upload">
                                    </div>
                                    <div class="col-sm-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="button" class="form-control btn btn-warning" id="download_sample_csv_button" value="Download Sample Data">
                                    </div>
                                    <div class="col-sm-12" id="download_latest_csv_div" style="display: none; margin-top: 20px;">
                                        <label><a href="javascript:;" id="download_latest_csv_link">Click Here</a> to download, your last uploaded csv file.</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-12" id="errors_list_div" style="display: none;">
                        <div class="progress-bar progress-bar-danger" id="error_progress_bar" style="width: 0%; font-size: larger; padding-top: 2px; height: 25px; margin-bottom: 10px;">File validation completed with errors</div>
                        Your CSV file has the following errors. <br/>Please correct the following errors or check your file with sample data.
                        <div style="border: 1px solid #989898; margin-top: 10px; padding: 10px 10px;">
                            <strong>Errors:</strong>
                            <div id="csv_errors_div">
                                <ul id="errors_list_ul">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="copy_project_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #EAF3FE;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Copy Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form id="copy_project_form" action="<?php echo base_url(); ?>project/copy-project" method="post" role="form" style="display: block;">
                            <input type="hidden" name="project_id" id="copy_project_id" value=""/>
                            <div class="form-group">
                                Enter Project Name
                            </div>
                            <div class="form-group">
                                <input type="text" id="copy_project_name" name="project_name" class="form-control" maxlength="60"/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="button" class="form-control btn btn-info" id="copy_project_button" value="Copy Project">
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-4">
                                        <button type="button" class="close" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>project/edit-project" method="post" id="edit_project_form">
    <input type="hidden" id="edit_project_id" name="project_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>project/delete-project" method="post" id="delete_project_form">
    <input type="hidden" id="delete_project_id" name="project_id" value=""/>
</form>
<form action="<?php echo base_url(); ?>project/download-sample-data" method="post" target="_blank" id="download_sample_data_form">
    <input type="hidden" id="download_sample_project_id" name="project_id" value=""/>
    <input type="hidden" id="download_latest_csv_file" name="download_latest_csv_file" value="false"/>
</form>
<?php $this->load->view('elements/publisher_footer'); ?>