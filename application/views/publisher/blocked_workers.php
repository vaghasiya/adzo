<?php $this->load->view('elements/publisher_header', array("title"=>"Blocked Workers","active_menu"=>"manage","sub_menu"=>"blocked_workers")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $('#table_list').DataTable({
                responsive: true
            });

            $(".unblock_button").on("click",function(){
                var id = $(this).data("id");
                $("#block_id").val(id);
                $("#unblock_form").submit();
            });
        });
    </script>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Blocked Workers</h3>
                    <?php
                    $class = $this -> session -> flashdata('class');
                    $message = $this -> session -> flashdata('message');
                    if(empty($class)) $class = "danger";
                    if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $class; ?>">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <span class="message-text"><?php echo $message; ?></span>
                        </div>
                    <?php } ?>
                    <form action="<?php echo base_url(); ?>publisher/unblock-worker" id="unblock_form" method="post">
                        <input type="hidden" id="block_id" name="block_id" value="" />
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="table_list" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Worker ID</th>
                                    <th>Blocked Date-Time</th>
                                    <th class="text-center" style="width: 180px !important;">Actions &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($blocked_workers_list as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['unique_user_id']; ?></td>
                                        <td><?php echo date("d-m-Y H:i:s", strtotime($row['created_date'])); ?></td>
                                        <td class="text-center">
                                            <button type="button" data-id="<?php echo $row['block_id']; ?>" class="btn red btn-xs unblock_button">Unblock</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_footer'); ?>