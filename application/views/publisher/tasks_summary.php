<?php $this->load->view('elements/publisher_header', array("title"=>"Task Summary","active_menu"=>"task_summary")); ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $('#days_table').DataTable();
            $('#months_table').DataTable();
        });
    </script>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Task Summary</h3>
                        <?php
                        $class = $this -> session -> flashdata('class');
                        $message = $this -> session -> flashdata('message');
                        if(empty($class)) $class = "danger";
                        if(!empty($message)){ ?>
                            <div class="alert alert-<?php echo $class; ?>">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <span class="message-text"><?php echo $message; ?></span>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <span class="dashboard-stat dashboard-stat-v2 yellow">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $task_statistics['published']; ?>"><?php echo $task_statistics['submitted']; ?></span>
                                        </div>
                                        <div class="desc"> Published</div>
                                    </div>
                                </span>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <span class="dashboard-stat dashboard-stat-v2 red">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $task_statistics['approved']; ?>"><?php echo $task_statistics['approved']; ?></span>
                                        </div>
                                        <div class="desc"> Approved</div>
                                    </div>
                                </span>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <span class="dashboard-stat dashboard-stat-v2 green">
                                    <div class="visual">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $task_statistics['rejected']; ?>"><?php echo $task_statistics['rejected']; ?></span>
                                        </div>
                                        <div class="desc"> Rejected</div>
                                    </div>
                                </span>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <span class="dashboard-stat dashboard-stat-v2 purple">
                                    <div class="visual">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $task_statistics['review_pending']; ?>"><?php echo $task_statistics['review_pending']; ?></span>
                                        </div>
                                        <div class="desc">Review Pending</div>
                                    </div>
                                </span>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <span class="dashboard-stat dashboard-stat-v2 green">
                                    <div class="visual">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $task_statistics['in_progress']; ?>"><?php echo $task_statistics['in_progress']; ?></span>
                                        </div>
                                        <div class="desc">In Progress</div>
                                    </div>
                                </span>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <span class="dashboard-stat dashboard-stat-v2 blue">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <?php if (empty($task_statistics['payment'])) {
                                                $task_statistics['payment'] = 0;
                                            } ?>
                                            <span data-counter="counterup" data-value="$<?php echo $task_statistics['payment'] ?>">
                                                $<?php echo $task_statistics['payment']; ?>
                                            </span>
                                        </div>
                                        <div class="desc">Total Payment</div>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <br/>
                        <ul class="nav nav-tabs">
                            <li class="active bold alert-danger"><a href="#days_tab" data-toggle="tab">30 Days</a></li>
                            <li class="bold alert-warning"><a href="#months_tab" data-toggle="tab">Monthly</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="days_tab" class="tab-pane fade in active">
                                <table class="table table-striped table-bordered table-hover" width="100%" id="days_table" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Submitted</th>
                                        <th class="text-center">Approved</th>
                                        <th class="text-center">Rejected</th>
                                        <th class="text-center">Review<br>Pending</th>
                                        <th class="text-center">Task<br>In Progress</th>
                                        <th class="text-center">Payments</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($days_list as $row) { ?>
                                        <tr>
                                            <td class="text-center"><?php echo $row['start_date']; ?></td>
                                            <td class="text-center"><?php echo $row['submitted']; ?></td>
                                            <td class="text-center"><?php echo $row['approved']; ?></td>
                                            <td class="text-center"><?php echo $row['rejected']; ?></td>
                                            <td class="text-center"><?php echo $row['review_pending']; ?></td>
                                            <td class="text-center"><?php echo $row['in_progress']; ?></td>
                                            <td class="text-right">$&nbsp;<?php if(empty($row['payment'])){ echo "0"; } else { echo $row['payment']; } ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="months_tab" class="tab-pane fade">
                                <table class="table table-striped table-bordered table-hover" width="100%" id="months_table" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Submitted</th>
                                        <th class="text-center">Approved</th>
                                        <th class="text-center">Rejected</th>
                                        <th class="text-center">Review<br>Pending</th>
                                        <th class="text-center">Task<br>In Progress</th>
                                        <th class="text-center">Payments</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($monthly_list as $row) { ?>
                                        <tr>
                                            <td class="text-center"><?php echo $row['start_date']; ?></td>
                                            <td class="text-center"><?php echo $row['submitted']; ?></td>
                                            <td class="text-center"><?php echo $row['approved']; ?></td>
                                            <td class="text-center"><?php echo $row['rejected']; ?></td>
                                            <td class="text-center"><?php echo $row['review_pending']; ?></td>
                                            <td class="text-center"><?php echo $row['in_progress']; ?></td>
                                            <td class="text-right">$&nbsp;<?php if(empty($row['payment'])){ echo "0"; } else { echo $row['payment']; } ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('elements/publisher_footer'); ?>