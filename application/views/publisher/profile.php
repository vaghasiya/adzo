<?php $this->load->view('elements/publisher_login_header'); ?>
<h1 class="page-title"> User Profile
    <small></small>
</h1>
<div class="row">
    <div class="col-md-3">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<?php echo base_url(); ?>assets/pages/media/profile/profile_user.jpg" class="img-responsive" alt=""> </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> Marcus Doe </div>
                    <div class="profile-usertitle-job"> Developer </div>
                </div>
                <div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                    <button type="button" class="btn btn-circle red btn-sm">Message</button>
                </div>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="#"> <i class="icon-home"></i> Overview </a>
                        </li>                        
                        <li>
                            <a href="#"> <i class="icon-settings"></i> Status </a>
                            <div class="row" style="padding-left: 30px">
                                <strong><a href="">Tasks</a>&nbsp;<a>Workers</a>&nbsp;<a>Transaction</a> </strong>
                            </div>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-adjust"></i>Change Password</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-arrow"></i>Change Address</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-wallet"></i> Change Mobile </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-feed"></i> Change name </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <table class="table table-striped table-bordered table-hover dt-responsive" width="40%" cellspacing="0">            
            <tbody>
                <tr>
                    <td style="width: 20%"><strong>Name</strong></td>
                    <td >Adzo India</td>
                </tr>                
                <tr>
                    <td><strong>Publisher ID</strong></td>
                    <td>ADAJFDJAKLFJD</td> 
                </tr>                
                <tr>
                    <td><strong>Email address </strong></td>
                    <td>lijojohnrbs@gmail.com</td>                                       
                </tr>
                <tr>
                    <td><strong>Address </strong></td>
                    <td>#94 PES College road, 22nd main</td>                                       
                </tr>
                <tr>
                    <td><strong>Country </strong></td>
                    <td>India</td>                                       
                </tr>                
                <tr>
                    <td><strong>Mobile Number:</strong></td>
                    <td>+91 - 91081676670</td>                                       
                </tr>
            </tbody>
        </table>

        
        <div class="portlet-body">  
            <h5 class="text-center"><strong>Task – Summary</strong></h5>
            <div class="table-scrollable table-scrollable-borderless">
                <table class="table table-hover table-light">
                    <thead>
                        <tr class="uppercase">
                            <th> Date </th>
                            <th> Project  Name </th>
                            <th> Submitted </th>
                            <th> Approved </th>
                            <th> Rejected </th>
                            <th> Pending </th>
                            <th> Payment </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Data collection</td>
                            <td> Adzo Lod </td>
                            <td> 45 </td>
                            <td> 124 </td>
                            <td> 448 </td>
                            <td> 124 </td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Data collection</td>
                            <td> Adzo Lod </td>
                            <td> 45 </td>
                            <td> 124 </td>
                            <td> 448 </td>
                            <td> 124 </td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Data collection</td>
                            <td> Adzo Lod </td>
                            <td> 45 </td>
                            <td> 124 </td>
                            <td> 448 </td>
                            <td> 124 </td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Data collection</td>
                            <td> Adzo Lod </td>
                            <td> 45 </td>
                            <td> 124 </td>
                            <td> 448 </td>
                            <td> 124 </td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>                       
                    </tbody>                   
                </table>
                <span class="pull-right"><a>View More</a></span>
            </div>
        </div>
        
        <div class="portlet-body">  
            <h5 class="text-center"><strong>Worker – Summary</strong></h5>
            <div class="table-scrollable table-scrollable-borderless">
                <table class="table table-hover table-light">
                    <thead>
                        <tr class="uppercase">
                            <th> Worker ID </th>
                            <th> Submitted </th>
                            <th> Approved </th>
                            <th> Rejected </th>
                            <th> Pending </th>
                            <th> Payment </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> ADFAKDFJAJK</td>
                            <td> 45 </td>
                            <td> 124 </td>
                            <td> 448 </td>
                            <td> 124 </td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td> ADFAKDFJAJK </td>
                            <td> 45 </td>
                            <td> 124 </td>
                            <td> 448 </td>
                            <td> 124 </td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td> ADFAKDFJAJK </td>
                            <td> 45 </td>
                            <td> 124 </td>
                            <td> 448 </td>
                            <td> 124 </td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td> ADFAKDFJAJK </td>
                            <td> 45 </td>
                            <td> 124 </td>
                            <td> 448 </td>
                            <td> 124 </td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>                       
                    </tbody>
                </table>
                <span class="pull-right"><a>View More</a></span>
            </div>
        </div>
        <div class="portlet-body">  
            <h5 class="text-center"><strong>Transaction – Summary</strong></h5>
            <div class="table-scrollable table-scrollable-borderless">
                <table class="table table-hover table-light">
                    <thead>
                        <tr class="uppercase">
                            <th>Date</th>
                            <th>Description</th>
                            <th>Transaction ID</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1-1-2014</td>
                            <td>Batch 124</td>
                            <td>DAFDFADFD</td>
                            <td>448</td>
                            <td>124</td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td>1-1-2014</td>
                            <td>Batch 124</td>
                            <td>DAFDFADFD</td>
                            <td>448</td>
                            <td>124</td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td>1-1-2014</td>
                            <td>Batch 124</td>
                            <td>DAFDFADFD</td>
                            <td>448</td>
                            <td>124</td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td>1-1-2014</td>
                            <td>Batch 124</td>
                            <td>DAFDFADFD</td>
                            <td>448</td>
                            <td>124</td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                        <tr>
                            <td>1-1-2014</td>
                            <td>Batch 124</td>
                            <td>DAFDFADFD</td>
                            <td>448</td>
                            <td>124</td>
                            <td>
                                <span class="bold theme-font">$1000</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <span class="pull-right"><a>View More</a></span>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/publisher_login_footer'); ?>