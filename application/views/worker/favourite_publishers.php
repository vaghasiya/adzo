<?php $this->load->view('elements/worker_header', array("title"=>"Favourite Publishers","active_menu"=>"more","sub_menu"=>"favourite_publishers")); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-badge font-yellow-gold"></i>
                    <span class="caption-subject bold uppercase text-info">Favourite Publishers</span>
                </div>
            </div>
            <div class="portlet-body">
                <?php
                $class = $this -> session -> flashdata('class');
                $message = $this -> session -> flashdata('message');
                if(empty($class)) $class = "danger";
                if(!empty($message)){ ?>
                    <div class="alert alert-<?php echo $class; ?>">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span class="message-text"><?php echo $message; ?></span>
                    </div>
                <?php } ?>
                <table id="list_table" class="table display nowrap table-striped table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Publisher ID</th>
                        <th>Created Date-Time</th>
                        <th class="text-center" style="width: 180px !important;">Actions &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($favourite_publishers_list as $row) { ?>
                        <tr>
                            <td><?php echo $row['unique_user_id']; ?></td>
                            <td><?php echo date("d-m-Y H:i:s", strtotime($row['created_date'])); ?></td>
                            <td class="text-center">
                                <button type="button" data-id="<?php echo $row['favourite_id']; ?>" class="btn red btn-xs remove_favourite_button">Remove as Favourite</button>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>worker/remove-favourite-publisher" id="remove_favourite_form" method="post">
    <input type="hidden" id="favourite_id" name="favourite_id" value="" />
</form>
<?php $this->load->view('elements/worker_footer'); ?>
<script>
    $(document).ready(function() {
        $('#list_table').DataTable({
            responsive: true
        });

        $(".remove_favourite_button").on("click",function(){
            var id = $(this).data("id");
            $("#favourite_id").val(id);
            $("#remove_favourite_form").submit();
        });
    });
</script>