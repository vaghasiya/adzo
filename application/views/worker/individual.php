<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Basic</span>
                </div>
                <div class="tools"> </div>
            </div>
                <div class="portlet-body">
                    <table id="example" class="table display nowrap table-striped table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="all">Publisher</th>
                                <th class="desktop">Project</th>
                                <th class="min-tablet">Tasks</th>
                                <th class="">Reward</th>
                                <th class="">Created</th>
                                <th class="">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th></th>
                                <td>Tiger</td>
                                <td>SystemArchitectSystemArchitectSystemArchitect</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td class="text-center">61</td>
                                <td>2011/04/25</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>Garrett</td>
                                <td>Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td class="text-center">63</td>
                                <td>2011/07/25</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>Garrett</td>
                                <td>Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td class="text-center">63</td>
                                <td>2011/07/25</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>Garrett</td>
                                <td>Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td class="text-center">63</td>
                                <td>2011/07/25</td>
                            </tr> 
                            <tr>
                                <th></th>
                                <td>Garrett</td>
                                <td>Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td class="text-center">63</td>
                                <td>2011/07/25</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>Garrett</td>
                                <td>Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td class="text-center">63</td>
                                <td>2011/07/25</td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('elements/worker_footer'); ?>