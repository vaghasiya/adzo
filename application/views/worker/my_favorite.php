<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase text-danger">My Cart</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table id="example" class="table display nowrap table-striped table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="all">Publisher Rating</th>
                            <th class="desktop">Project</th>
                            <th class="text-center min-tablet">Reward</th>
                            <th class="text-center">Accept task in</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th></th>
                            <td>Adzo Ltd <span class="text-success bold">3.4 (100 review)</span></td>
                            <td>Please verify the following details</td>
                            <td class="text-center">$448.00</td>
                            <td class="text-center">1 day</td>
                            <th class="text-center">
                                <a href="#"class="text-danger">Preview</a> | 
                                <a class="text-warning" >Accept</a>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <td>Adzo Ltd <span class="text-success bold">3.4 (100 review)</span></td>
                            <td>Please verify the following details</td>
                            <td class="text-center">$448.00</td>
                            <td class="text-center">1 day</td>
                            <th class="text-center">
                                <a href="#"class="text-danger">Preview</a> | 
                                <a class="text-warning" >Accept</a>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <td>Adzo Ltd <span class="text-success bold">3.4 (100 review)</span></td>
                            <td>Please verify the following details</td>
                            <td class="text-center">$448.00</td>
                            <td class="text-center">1 day</td>
                            <th class="text-center">
                                <a href="#"class="text-danger">Preview</a> | 
                                <a class="text-warning" >Accept</a>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <td>Adzo Ltd <span class="text-success bold">3.8 (100 review)</span></td>
                            <td>Please verify the following details</td>
                            <td class="text-center">$585.00</td>
                            <td class="text-center">1 day</td>
                            <th class="text-center">
                                <a href="#"class="text-danger">Preview</a> | 
                                <a class="text-warning" >Accept</a>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <td>Adzo Ltd <span class="text-success bold">3.1 (100 review)</span></td>
                            <td>Please verify the following details</td>
                            <td class="text-center">$448.00</td>
                            <td class="text-center">1 day</td>
                            <th class="text-center">
                                <a href="#"class="text-danger">Preview</a> | 
                                <a class="text-warning" >Accept</a>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>