<?php $this->load->view('elements/worker_header', array("title"=>"Reassigned Tasks","active_menu"=>"my_account","sub_menu"=>"reassigned_tasks")); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-badge font-yellow-gold"></i>
                    <span class="caption-subject bold uppercase text-info">Tasks Reassigned to Me</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <?php
                $class = $this -> session -> flashdata('class');
                $message = $this -> session -> flashdata('message');
                if(empty($class)) $class = "danger";
                if(!empty($message)){ ?>
                    <div class="alert alert-<?php echo $class; ?>">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span class="message-text"><?php echo $message; ?></span>
                    </div>
                <?php } ?>
                <table id="list_table" class="table display nowrap table-striped table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="text-center column-size-80">Start Date</th>
                        <th>Publisher Rating</th>
                        <th>Project</th>
                        <th class="text-center column-size-80">Time Allotted</th>
                        <th class="text-center column-size-80">Expire Time</th>
                        <th class="text-center column-size-80">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($records_list as $row) { ?>
                        <tr>
                            <td class="text-center"><?php echo date("d-m-Y H:i:s", strtotime($row['start_date_time'])); ?></td>
                            <td><?php echo $row['first_name']." ".$row['last_name']; ?>&nbsp;<span class="text-primary bold"> - 5.0 (100 review)</span></td>
                            <td><?php echo $row['title']; ?></td>
                            <td><?php echo $row['time_per_assignment'] . " " . $row['time_per_assignment_unit']; ?></td>
                            <td class="text-center"><?php echo date("d-m-Y H:i:s", strtotime($row['expire_date_time'])); ?></td>
                            <th class="text-center">
                                <a href="javascript:;" class="text-danger return_button" data-batch-id="<?php echo $row['batch_id']; ?>" data-id="<?php echo $row['batch_task_id']; ?>">Return</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <a href="javascript:;" class="text-warning continue_button" data-batch-id="<?php echo $row['batch_id']; ?>" data-id="<?php echo $row['batch_task_id']; ?>">Continue</a>
                            </th>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>tasks/work-details" method="post" id="task_continue_form" target="_blank">
    <input type="hidden" id="details_batch_id" name="batch_id" value=""/>
    <input type="hidden" id="details_batch_task_id" name="batch_task_id" value="">
</form>
<form action="<?php echo base_url(); ?>tasks/return-reassigned-task" method="post" id="task_return_form">
    <input type="hidden" id="return_batch_id" name="batch_id" value=""/>
    <input type="hidden" id="return_batch_task_id" name="batch_task_id" value="">
</form>
<?php $this->load->view('elements/worker_footer'); ?>
<script>
    $(document).ready(function() {
        $('#list_table').DataTable({
            responsive: true
        });

        $(".continue_button").on("click",function(){
            var batch_id = $(this).data("batch-id");
            var batch_task_id = $(this).data("id");
            $("#details_batch_id").val(batch_id);
            $("#details_batch_task_id").val(batch_task_id);
            $("#task_continue_form").submit();
        });

        $(".return_button").on("click",function(){
            var batch_id = $(this).data("batch-id");
            var batch_task_id = $(this).data("id");
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this process.!!!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, return it!'
            }).then(function () {
                $("#return_batch_id").val(batch_id);
                $("#return_batch_task_id").val(batch_task_id);
                $("#task_return_form").submit();
            });
        });
    });
</script>