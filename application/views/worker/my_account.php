<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="container">
    <h1 class="page-title">
        <small>User Profile</small>
    </h1>
    <div class="row">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <div class="portlet light profile-sidebar-portlet ">
                    <div class="profile-userpic">
                        <img src="<?php echo $this -> session -> userdata('profile_pic'); ?>" class="img-responsive" alt=""> </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name text-center bold">  <?php echo $this -> session -> userdata('first_name').' '.$this -> session -> userdata('last_name'); ?> </div>
                        <div class="profile-usertitle-job text-center"> <span class="bold">Worker ID:</span> <?php echo $this -> session -> userdata('unique_user_id'); ?> </div>
                    </div>
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active">
                                <a href="#"> <i class="icon-home"></i> Overview </a>
                            </li>
                            <li>
                                <a href="#"> <i class="icon-settings"></i> Status </a>
                                <div class="row" style="padding-left: 30px">
                                    <strong><a href="<?php echo base_url(); ?>worker/tasks">Tasks</a>|<a href="<?php echo base_url(); ?>worker/publisher">Publishers</a>|
                                        <a href="<?php echo base_url(); ?>worker/transaction">Transaction</a>
                                        <a href="<?php echo base_url(); ?>worker/withdrawal/">Withdrawals</a></strong>
                                </div>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>worker/changename/">
                                    <i class="icon-feed"></i>&nbsp; Change name </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>worker/changepswd/">
                                    <i class="fa fa-expeditedssl"></i>&nbsp;Change Password</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>worker/changeaddr/">
                                    <i class="fa fa-black-tie"></i>&nbsp;Change Address</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>worker/changemobi/">
                                    <i class="icon-wallet"></i>&nbsp; Change Mobile </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>worker/vrifypage">
                                    <i class="fa fa-cc-diners-club"></i>&nbsp; Verify my account 
                                </a>
                                <div class="row" style="padding-left: 30px">
                                    <strong><a href="<?php echo base_url(); ?>worker/paypal">PayPal</a>|<a href="<?php echo base_url(); ?>worker/upi">UPI</a></strong>
                                </div>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>worker/bank_account">
                                    <i class="fa fa-sticky-note"></i>&nbsp;Bank account</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>worker/transfer_earning"><i class="fa fa-bug"></i>&nbsp;Transfer my earnings</a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-cubes"></i>&nbsp; FAQ</a>                                
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-external-link"></i>&nbsp; Participation Agreement</a>                                
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="40%" cellspacing="0">            
                <tbody>
                    <tr>
                        <td style="width: 20%"><strong>Full Name</strong></td>
                        <td >Adzo India</td>
                    </tr>                
                    <tr>
                        <td><strong>Worker ID</strong></td>
                        <td>KQI890GYHY</td>
                    </tr>                
                    <tr>
                        <td><strong>Email address </strong></td>
                        <td>lijojohnrbs@gmail.com</td>                                       
                    </tr>
                    <tr>
                        <td><strong>Address </strong></td>
                        <td>#94 PES College road, 22nd main</td>                                       
                    </tr>
                    <tr>
                        <td><strong>Country </strong></td>
                        <td>India</td>                                       
                    </tr>                
                    <tr>
                        <td><strong>Mobile Number:</strong></td>
                        <td>+91 - 91081676670</td>                                       
                    </tr>
                    <tr>
                        <td><strong>Payment mode:</strong></td>
                        <td>Online HDFC</td>                                       
                    </tr>
                </tbody>
            </table>


            <div class="portlet-body">  
                <h5 class="text-center"><strong>Task – Summary</strong></h5>
                <div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr class="uppercase">
                                <th> Date </th>
                                <th> Submitted </th>
                                <th> Approved </th>
                                <th> Rejected </th>
                                <th> Pending </th>
                                <th> Earnings</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Data collection</td>
                                <td> Adzo Lod </td>
                                <td> 124 </td>
                                <td> 448 </td>
                                <td> 124 </td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Data collection</td>
                                <td> Adzo Lod </td>
                                <td> 124 </td>
                                <td> 448 </td>
                                <td> 124 </td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Data collection</td>
                                <td> Adzo Lod </td>
                                <td>45</td>
                                <td>124</td>
                                <td>448</td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr> 
                            <tr>
                                <td>Data collection</td>
                                <td> Adzo Lod </td>
                                <td>45</td>
                                <td>124</td>
                                <td>448</td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                        </tbody>                   
                    </table>
                    <span class="pull-right"><a>View More</a></span>
                </div>
            </div>
            
            <div class="portlet-body">  
                <h5 class="text-center"><strong>Publisher – Summary</strong></h5>
                <div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr class="uppercase">
                                <th> Publisher ID </th>
                                <th> Submitted </th>
                                <th> Approved </th>
                                <th> Rejected </th>
                                <th> Pending </th>
                                <th> Payment </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> ADFAKDFJAJK</td>
                                <td> 45 </td>
                                <td> 124 </td>
                                <td> 448 </td>
                                <td> 124 </td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                            <tr>
                                <td> ADFAKDFJAJK </td>
                                <td> 45 </td>
                                <td> 124 </td>
                                <td> 448 </td>
                                <td> 124 </td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                            <tr>
                                <td> ADFAKDFJAJK </td>
                                <td> 45 </td>
                                <td> 124 </td>
                                <td> 448 </td>
                                <td> 124 </td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                            <tr>
                                <td> ADFAKDFJAJK </td>
                                <td> 45 </td>
                                <td> 124 </td>
                                <td> 448 </td>
                                <td> 124 </td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>                       
                        </tbody>
                    </table>
                    <span class="pull-right"><a>View More</a></span>
                </div>
            </div>
            <div class="portlet-body">  
                <h5 class="text-center"><strong>Withdrawal – Summary</strong></h5>
                <div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr class="uppercase">
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Deduction</th>
                                <th>Gross amount</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1-1-2014</td>
                                <td>Batch 124</td>
                                <td>DAFDFADFD</td>
                                <td>448</td>
                                <th>
                                    <span class="bold theme-font">$1000</span>
                                </th>
                            </tr>
                            <tr>
                                <td>1-1-2014</td>
                                <td>Batch 124</td>
                                <td>DAFDFADFD</td>
                                <td>448</td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                            <tr>
                                <td>1-1-2014</td>
                                <td>Batch 124</td>
                                <td>DAFDFADFD</td>
                                <td>448</td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                            <tr>
                                <td>1-1-2014</td>
                                <td>Batch 124</td>
                                <td>DAFDFADFD</td>
                                <td>448</td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                            <tr>
                                <td>1-1-2014</td>
                                <td>Batch 124</td>
                                <td>DAFDFADFD</td>
                                <td>448</td>
                                <td>
                                    <span class="bold theme-font">$1000</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <span class="pull-right"><a>View More</a></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>