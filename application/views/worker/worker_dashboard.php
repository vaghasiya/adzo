<?php $this->load->view('elements/worker_header', array("title"=>"Dashboard","active_menu"=>"dashboard")); ?>
<div class="page-content">
    <div class="container">
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url(); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
        <div class="page-content-inner">
            <div class="mt-content-body">
                <div class="row">
                    <?php
                    $class = $this -> session -> flashdata('class');
                    $message = $this -> session -> flashdata('message');
                    if(empty($class)) $class = "danger";
                    if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $class; ?>">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <span class="message-text"><?php echo $message; ?></span>
                        </div>
                    <?php } ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-counter="counterup" data-value="7800">0</span>
                                        <small class="font-green-sharp">$</small>
                                    </h3>
                                    <small>TOTAL PROFIT</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <div class="progress">
                                    <span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
                                        <span class="sr-only">76% progress</span>
                                    </span>
                                </div>
                                <div class="status">
                                    <div class="status-title"> progress </div>
                                    <div class="status-number"> 76% </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-red-haze">
                                        <span data-counter="counterup" data-value="1349">0</span>
                                    </h3>
                                    <small>NEW FEEDBACKS</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-like"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <div class="progress">
                                    <span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
                                        <span class="sr-only">85% change</span>
                                    </span>
                                </div>
                                <div class="status">
                                    <div class="status-title"> change </div>
                                    <div class="status-number"> 85% </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-blue-sharp">
                                        <span data-counter="counterup" data-value="567"></span>
                                    </h3>
                                    <small>NEW ORDERS</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-basket"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <div class="progress">
                                    <span style="width: 45%;" class="progress-bar progress-bar-success blue-sharp">
                                        <span class="sr-only">45% grow</span>
                                    </span>
                                </div>
                                <div class="status">
                                    <div class="status-title"> grow </div>
                                    <div class="status-number"> 45% </div>
                                </div>
                            </div>
                        </div>
                    </div>            
                </div>                                            
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="profile-sidebar">
                    <div class="portlet light profile-sidebar-portlet ">
                        <div class="profile-userpic">
                            <img src="<?php echo $this -> session -> userdata('profile_pic'); ?>" class="img-responsive" alt=""> </div>
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"><?php echo $this -> session -> userdata('first_name').' '.$this -> session -> userdata('last_name'); ?></div>
                        </div>
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="active">
                                    <a href="page_user_profile_1.html">
                                        <i class="icon-home"></i> Overview </a>
                                </li>
                                <li>
                                    <a href="page_user_profile_1_account.html">
                                        <i class="icon-settings"></i> Account Settings </a>
                                </li>
                                <li>
                                    <a href="page_user_profile_1_help.html">
                                        <i class="icon-info"></i> Help </a>
                                </li>
                            </ul>
                        </div>
                    </div>                                                    
                </div>                                                
            </div>
            <div class="col-lg-3 col-md-3 ">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption caption-md">
                            <i class="icon-bar-chart theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Tasks</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row number-stats margin-bottom-30">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-left">
                                    <div class="stat-chart">
                                    </div>
                                    <div class="stat-number">
                                        <div class="title"> Total </div>
                                        <div class="number"> 2460 </div>
                                    </div>
                                </div>
                            </div>                    
                        </div>                
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption caption-md">
                            <i class="icon-bar-chart theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Tasks available to me</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row number-stats margin-bottom-30">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-left">
                                    <div class="stat-chart">
                                    </div>
                                    <div class="stat-number">
                                        <div class="title"> Total </div>
                                        <div class="number"> 1500 </div>
                                    </div>
                                </div>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption caption-md">
                            <i class="icon-bar-chart theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Tasks in queue</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row number-stats margin-bottom-30">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-left">
                                    <div class="stat-chart">
                                    </div>
                                    <div class="stat-number">
                                        <div class="title"> Total </div>
                                        <div class="number"> 6500 </div>
                                    </div>
                                </div>
                            </div>                    
                        </div>                
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption caption-md">
                            <i class="icon-bar-chart theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Tasks Reassigned</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row number-stats margin-bottom-30">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-left">
                                    <div class="stat-chart">
                                    </div>
                                    <div class="stat-number">
                                        <div class="title"> Total </div>
                                        <div class="number"> 1584 </div>
                                    </div>
                                </div>
                            </div>                    
                        </div>                
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption caption-md">
                            <i class="icon-bar-chart theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">My favorite publisher’s tasks</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row number-stats margin-bottom-30">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-left">
                                    <div class="stat-chart">
                                    </div>
                                    <div class="stat-number">
                                        <div class="title"> Total </div>
                                        <div class="number"> 1000 </div>
                                    </div>
                                </div>
                            </div>                    
                        </div>                
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption caption-md">
                            <i class="icon-bar-chart theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Publishers online</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row number-stats margin-bottom-30">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-left">
                                    <div class="stat-chart">
                                    </div>
                                    <div class="stat-number">
                                        <div class="title"> Total </div>
                                        <div class="number"> 8815 </div>
                                    </div>
                                </div>
                            </div>                    
                        </div>                
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class=" icon-social-twitter font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Quick Actions</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_actions_pending" data-toggle="tab"> Recent </a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_actions_pending">
                                <div class="mt-actions">
                                    <div class="mt-action">
                                        <div class="mt-action-img">
                                            <img src="<?php echo base_url(); ?>assets/pages/media/users/avatar10.jpg" /> </div>
                                        <div class="mt-action-body">
                                            <div class="mt-action-row">
                                                <div class="mt-action-info ">
                                                    <div class="mt-action-icon ">
                                                        <i class="icon-magnet"></i>
                                                    </div>
                                                    <div class="mt-action-details ">
                                                        <span class="mt-action-author">Natasha Kim</span>
                                                        <p class="mt-action-desc">Dummy text of the printing</p>
                                                    </div>
                                                </div>
                                                <div class="mt-action-datetime ">
                                                    <span class="mt-action-date">3 jun</span>
                                                    <span class="mt-action-dot bg-green"></span>
                                                    <span class="mt-action-time">9:30-13:00</span>
                                                </div>
                                                <div class="mt-action-buttons ">
                                                    <div class="btn-group btn-group-circle">
                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                                        
                                </div>                                                        
                            </div>
                            <!-- END: Actions -->
                        </div>
                        <div class="tab-pane" id="tab_actions_completed">
                            <!-- BEGIN:Completed-->
                            <div class="mt-actions">
                                <div class="mt-action">
                                    <div class="mt-action-img">
                                        <img src="<?php echo base_url(); ?>assets/pages/media/users/avatar1.jpg" /> </div>
                                    <div class="mt-action-body">
                                        <div class="mt-action-row">
                                            <div class="mt-action-info ">
                                                <div class="mt-action-icon ">
                                                    <i class="icon-action-redo"></i>
                                                </div>
                                                <div class="mt-action-details ">
                                                    <span class="mt-action-author">Frank Cameron</span>
                                                    <p class="mt-action-desc">Lorem Ipsum is simply dummy</p>
                                                </div>
                                            </div>
                                            <div class="mt-action-datetime ">
                                                <span class="mt-action-date">3 jun</span>
                                                <span class="mt-action-dot bg-red"></span>
                                                <span class="mt-action-time">9:30-13:00</span>
                                            </div>
                                            <div class="mt-action-buttons ">
                                                <div class="btn-group btn-group-circle">
                                                    <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                    <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-action">
                                    <div class="mt-action-img">
                                        <img src="<?php echo base_url(); ?>assets/pages/media/users/avatar8.jpg" /> </div>
                                    <div class="mt-action-body">
                                        <div class="mt-action-row">
                                            <div class="mt-action-info ">
                                                <div class="mt-action-icon ">
                                                    <i class="icon-cup"></i>
                                                </div>
                                                <div class="mt-action-details ">
                                                    <span class="mt-action-author">Ella Davidson </span>
                                                    <p class="mt-action-desc">Text of the printing</p>
                                                </div>
                                            </div>
                                            <div class="mt-action-datetime ">
                                                <span class="mt-action-date">3 jun</span>
                                                <span class="mt-action-dot bg-green"></span>
                                                <span class="mt-action-time">9:30-13:00</span>
                                            </div>
                                            <div class="mt-action-buttons">
                                                <div class="btn-group btn-group-circle">
                                                    <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                    <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END: Completed -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption caption-md">
                            <i class="icon-bar-chart font-dark hide"></i>
                            <span class="caption-subject font-green-steel bold uppercase">Member Activity</span>
                            <span class="caption-helper">weekly stats...</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                    <input type="radio" name="options" class="toggle" id="option1">Today</label>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row number-stats margin-bottom-30">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-left">
                                    <div class="stat-chart">
                                    </div>
                                    <div class="stat-number">
                                        <div class="title"style="text-align: center;"> Total </div>
                                        <div class="number"style="text-align: center;"> 2460 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-right">
                                    <div class="stat-chart">
                                    </div>
                                    <div class="stat-number">
                                        <div class="title"> New </div>
                                        <div class="number"> 719 </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th colspan="2"> MEMBER </th>
                                        <th> Earnings </th>
                                        <th> CASES </th>
                                        <th> CLOSED </th>
                                        <th> RATE </th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="fit">
                                        <img class="user-pic rounded" src="<?php echo base_url(); ?>assets/pages/media/users/avatar4.jpg"> </td>
                                    <td>
                                        <a href="javascript:;" class="primary-link">Brain</a>
                                    </td>
                                    <td> $345 </td>
                                    <td> 45 </td>
                                    <td> 124 </td>
                                    <td>
                                        <span class="bold theme-font">80%</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fit">
                                        <img class="user-pic rounded" src="<?php echo base_url(); ?>assets/pages/media/users/avatar5.jpg"> </td>
                                    <td>
                                        <a href="javascript:;" class="primary-link">Nick</a>
                                    </td>
                                    <td> $560 </td>
                                    <td> 12 </td>
                                    <td> 24 </td>
                                    <td>
                                        <span class="bold theme-font">67%</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fit">
                                        <img class="user-pic rounded" src="<?php echo base_url(); ?>assets/pages/media/users/avatar6.jpg"> </td>
                                    <td>
                                        <a href="javascript:;" class="primary-link">Tim</a>
                                    </td>
                                    <td> $1,345 </td>
                                    <td> 450 </td>
                                    <td> 46 </td>
                                    <td>
                                        <span class="bold theme-font">98%</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fit">
                                        <img class="user-pic rounded" src="<?php echo base_url(); ?>assets/pages/media/users/avatar7.jpg"> </td>
                                    <td>
                                        <a href="javascript:;" class="primary-link">Tom</a>
                                    </td>
                                    <td> $645 </td>
                                    <td> 50 </td>
                                    <td> 89 </td>
                                    <td>
                                        <span class="bold theme-font">58%</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('elements/worker_footer'); ?>
