<?php $this->load->view('elements/worker_header', array("title"=>"Task Details","active_menu"=>"all_project","sub_menu"=>"all_task")); ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Task Details</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <?php
                $class = $this -> session -> flashdata('class');
                $message = $this -> session -> flashdata('message');
                if(empty($class)) $class = "danger";
                if(!empty($message)){ ?>
                    <div class="alert alert-<?php echo $class; ?>">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span class="message-text"><?php echo $message; ?></span>
                    </div>
                <?php }

                $base_url = base_url();
                $design_layout = '<form id="task_answer_form" onsubmit="parent.before_submit_form()" action="'.$base_url.'tasks/task-answers-submit" method="post" enctype="multipart/form-data" target="_top">' .
                    '<input name="batch_id" value="'.$batch_task['batch_id'].'" type="hidden">' .
                    '<input name="batch_task_id" value="'.$batch_task['batch_task_id'].'" type="hidden">' .
                    '<input name="auto_accept" value="'.$auto_accept.'" class="auto_accept" type="hidden">' .
                    '<input value="'. $batch_task['batch_hit_id'] .'" type="hidden">' .
                    $design_layout;
                if(!empty($batch_task)) {
                    $design_layout .= '<div style="text-align: center; width: 100%;"><input type="submit" class="btn btn-info" id="task_submit_button" value="Submit Data"/></div></form>';
                }
                ?>
                <input id="design_layout" value='<?php echo htmlentities($design_layout, ENT_QUOTES, 'utf-8'); ?>' type="hidden">
                <div class="panel" style="margin-bottom: 10px;">
                    <div class="panel panel-primary" style="margin-bottom: 0px;">
                        <div class="panel-heading">
                            <strong><?php echo $title; ?></strong>
                            <a href="javascript:;" data-toggle="modal" data-target="#task_details_modal" class="btn btn-success btn-xs pull-right">Task Details</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div id="timer" class="<?php if(empty($batch_task)){ echo "hidden"; } ?>" style="width: 240px; height: 65px;"></div>
                                </div>
                                <div class="col-md-8 text-center">
                                    <button type="button" class="btn btn-primary accept_button <?php if(!empty($batch_task)){ echo "hidden"; } ?>">Accept</button>
                                    <button type="button" class="btn btn-warning <?php if($category_type == "individual" || (!empty($batch_task)) || (count($hits) == 1)){ echo "hidden"; } ?> skip_button" style="margin-left: 15px;">Skip</button>
                                </div>
                                <div class="col-md-2">
                                    <span><strong>Reward: </strong> <span class="pull-right"><?php echo "$$reward_per_assignment"; ?></span></span>
                                    <br/>
                                    <span><strong>Time Allotted: </strong> <span class="pull-right"><?php echo "$time_per_assignment $time_per_assignment_unit"; ?></span></span>
                                    <?php if($category_type == "multiple"){ ?>
                                    <br/>
                                    <input type="checkbox" id="auto_accept" value="true" <?php if($auto_accept == "true") { echo 'checked="checked"'; } ?>/>&nbsp;&nbsp;<label for="auto_accept">Auto Accept Next Task</label>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary" style="margin-bottom: 0px;">
                        <div class="panel-body" style="padding: 0px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if($category_type == "multiple") {
                                        $design_layout = str_replace($csv_header_array, $csv_data_array[$current_hit_index], $design_layout);
                                    } ?>
                                    <iframe frameborder="0" id="preview_frame" style="width: 100%; height: 600px; max-height: 900px;" srcdoc='<?php echo htmlentities($design_layout, ENT_QUOTES, 'utf-8'); ?>'></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary" style="margin-bottom: 10px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="javascript:;" data-toggle="modal" data-target="#task_details_modal" class="btn btn-success btn-xs">Task Details</a>
                                </div>
                                <div class="col-md-8 text-center">
                                    <button type="button" class="btn btn-primary accept_button <?php if(!empty($batch_task)){ echo "hidden"; } ?>">Accept</button>
                                    <button type="button" class="btn btn-warning <?php if($category_type == "individual" || (!empty($batch_task)) || (count($hits) == 1)){ echo "hidden"; } ?> skip_button" style="margin-left: 15px;">Skip</button>
                                </div>
                                <div class="col-md-2">
                                    <a href="#broken_task_modal" data-toggle="modal" class="btn btn-success btn-xs">Broken Task</a>
                                    <a href="#violet_task_modal" data-toggle="modal" class="btn btn-primary btn-xs" style="margin-left: 15px;">Violet ToS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="task_details_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $title; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <strong>Publisher: </strong> <?php echo $publisher['first_name'].' '.$publisher['last_name']; ?>
                    </div>
                    <div class="col-md-4">
                        <strong>Share: </strong>
                        <a href="javascript:;">Facebook</a>&nbsp;&nbsp;
                        <a href="javascript:;">Twitter</a>&nbsp;&nbsp;
                        <a href="javascript:;">LinkedIn</a>
                    </div>
                    <div class="col-md-4">
                        <a href="javascript:;" id="add_to_favourite" class="btn btn-success btn-xs pull-right">Add to Favourite</a>
                    </div>
                    <div class="col-md-12 mtop10">
                        <strong>Description: </strong><?php echo $description; ?>
                    </div>
                    <div class="col-md-12 mtop10 mbottom10">
                        <strong>Keywords: </strong><?php echo $keywords_description; ?>
                    </div>
                    <div class="col-md-4">
                        <strong>Reward: </strong><?php echo $reward_per_assignment; ?>
                        <br/>
                        <strong>Create Date: </strong><?php echo date("d-m-Y H:i:s", strtotime($created_date)); ?>
                    </div>
                    <div class="col-md-4">
                        <strong>Tasks Available: </strong><?php echo count($hits); ?>
                        <br/>
                        <strong>Expire Date: </strong><?php echo date("d-m-Y H:i:s", strtotime($expire_date_time)); ?>
                    </div>
                    <div class="col-md-4">
                        <strong>Time Allotted: </strong><?php echo "$time_per_assignment $time_per_assignment_unit"; ?>
                    </div>
                    <div class="col-md-12 mtop10">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>Qualification Required</th>
                                <th class="column-size-100 text-center">My Value</th>
                                <th style="width: 275px;">Result</th>
                            </tr>
                            <?php foreach ($batch_qualifications as $qualification) { ?>
                            <tr>
                                <td>
                                    <?php
                                        echo $qualification['sq_friendly_name'] . " " . $qualification['sq_friendly_key'] . " " . $qualification['sq_value'];
                                        if($qualification['sq_type'] == "location" && (!empty($qualification['sq_state_value']))){
                                            echo "<br/>[".$qualification['sq_state_value']."]";
                                        }
                                    ?>
                                </td>
                                <td><?php echo $qualification['my_value']; ?></td>
                                <td>
                                    <span><?php echo $qualification['result']; ?></span>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if(!empty($adzo_qualification)){ ?>
                                <tr>
                                    <td class="text-bold" colspan="3">Adzo Qualification</td>
                                </tr>
                                <tr>
                                    <td><?php echo $adzo_qualification; ?></td>
                                    <td><?php if(empty($adzo_qualification_request)){ echo "-"; } else { echo $adzo_qualification_request['score']; } ?></td>
                                    <td>
                                        <span><?php if(empty($adzo_qualification_request)){ echo "You do not meet this qualification."; } else { echo $adzo_qualification_request['result']; }?></span>
                                        <br/>
                                        <a href="javascript:;" data-id="<?php echo $adzo_qualification_id; ?>" class="btn btn-success btn-xs qualification_request <?php if(!empty($adzo_qualification_request)){ echo "hidden"; } ?>">Send Request</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty($publisher_qualification)){ ?>
                                <tr>
                                    <td class="text-bold" colspan="3">Publisher Qualification</td>
                                </tr>
                                <tr>
                                    <td><?php echo $publisher_qualification; ?></td>
                                    <td><?php if(empty($publisher_qualification_request)){ echo "-"; } else { echo $publisher_qualification_request['score']; } ?></td>
                                    <td>
                                        <span><?php if(empty($publisher_qualification_request)){ echo "You do not meet this qualification."; } else { echo $publisher_qualification_request['result']; }?></span>
                                        <br/>
                                        <a href="javascript:;" data-id="<?php echo $publisher_qualification_id; ?>" class="btn btn-success btn-xs qualification_request <?php if(!empty($publisher_qualification_request)){ echo "hidden"; } ?>">Send Request</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="broken_task_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Broken Task</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="<?php echo base_url(); ?>tasks/broken-or-violet-task" method="post" style="display: inline;">
                        <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                        <input name="batch_hit_id" class="batch_hit_id" value="<?php echo $hits[0]['batch_hit_id']; ?>" type="hidden">
                        <input name="batch_task_id" value="<?php echo $batch_task['batch_task_id']; ?>" type="hidden">
                        <input type="hidden" name="task_type" value="broken_task"/>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="reason">Enter Reason</label>
                                <textarea name="reason" id="broken_task_reason" class="form-control" rows="5" required="required"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <input type="submit" class="form-control btn btn-info" value="Submit">
                            </div>
                            <div class="col-md-4">&nbsp;</div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="violet_task_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Violet ToS</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="<?php echo base_url(); ?>tasks/broken-or-violet-task" method="post" style="display: inline;">
                        <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
                        <input name="batch_hit_id" class="batch_hit_id" value="<?php echo $hits[0]['batch_hit_id']; ?>" type="hidden">
                        <input name="batch_task_id" value="<?php echo $batch_task['batch_task_id']; ?>" type="hidden">
                        <input type="hidden" name="task_type" value="violet_task"/>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="reason">Enter Reason</label>
                                <textarea name="reason" id="broken_task_reason" class="form-control" rows="5" required="required"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <input type="submit" class="form-control btn btn-info" value="Submit">
                            </div>
                            <div class="col-md-4">&nbsp;</div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>tasks/details" method="post" id="task_accept_form">
    <input type="hidden" id="details_batch_id" name="batch_id" value="<?php echo $batch_id; ?>"/>
    <input name="batch_hit_id" class="batch_hit_id" value="<?php echo $hits[0]['batch_hit_id']; ?>" type="hidden">
    <input type="hidden" id="details_start_work" name="start_work" value="true"/>
</form>

<form action="<?php echo base_url(); ?>tasks/task-expired" method="post" id="task_expired_form">
    <input type="hidden" name="batch_id" value="<?php echo $batch_id; ?>"/>
    <input type="hidden" name="batch_task_id" value="<?php echo $batch_task['batch_task_id']; ?>">
    <input type="hidden" name="batch_hit_id" value="<?php echo $batch_task['batch_hit_id']; ?>">
</form>
<?php $this->load->view('elements/worker_footer'); ?>
<script>
    var csv_header = <?php echo json_encode($csv_header_array); ?>;
    var csv_data = <?php echo json_encode($csv_data_array); ?>;
    var hits = <?php echo json_encode($hits); ?>;
    var total_hits = parseInt(<?php echo count($csv_data_array); ?>) - 1;
    var current_hit_index = 0;
    var end_date_time = new Date("<?php echo $batch_task['expire_date_time']; ?>");
    $(document).ready(function() {
        $('#timer').countdown({
            until: end_date_time,
            padZeroes: true,
            format: 'DHMS',
            onExpiry: time_finished,
            description: '<strong>Time Remaining</strong>'
        });

        $(".skip_button").on("click",function(){
            current_hit_index++;
            var design_layout_html = $("#design_layout").val();
            design_layout_html = $("<div />").html(design_layout_html).html();
            var hit = hits[current_hit_index];
            var replace_value = csv_data[current_hit_index];
//            design_layout_html = "This is sample data for ${company name} - ${location} - ${website} for testing ${location} data";
            $(csv_header).each(function (index) {
                design_layout_html = design_layout_html.replace(new RegExp("\\" + csv_header[index], "gi"), replace_value[index]);
            });
            $("#preview_frame").attr("srcdoc", design_layout_html);
            $(".batch_hit_id").val(hit.batch_hit_id);
            if(total_hits == current_hit_index) {
                $(".skip_button").hide();
            }
        });

        $("#auto_accept").on("click",function(){
            if($(this).is(":checked")) {
                $("#preview_frame").contents().find(".auto_accept").val("true");
            } else {
                $("#preview_frame").contents().find(".auto_accept").val("false");
            }
        });

        $(".accept_button").on("click",function(){
            $("#task_accept_form").submit();
        });

        $(".qualification_request").on("click",function(){
            var id = $(this).data("id");
            var post_data = {"qualification_id": id};
            ajax_request("<?php echo base_url(); ?>qualification/ajax-qualification-request", post_data, function (result, element) {
                result = JSON.parse(result);
                if(result.status == "error"){
                    swal("Opps, Error Occurred",result.message,"error");
                } else {
                    swal("Process Completed",result.message,"success");
                }
            }, undefined);
        });

        $("#add_to_favourite").on("click",function(){
            var post_data = {"publisher_id": <?php echo $publisher['publisher_id']; ?>};
            ajax_request("<?php echo base_url(); ?>worker/ajax-add-favourite-publisher", post_data, function (result, element) {
                result = JSON.parse(result);
                if(result.status == "error"){
                    swal("Opps, Error Occurred",result.message,"error");
                } else {
                    swal("Process Completed",result.message,"success");
                }
            }, undefined);
        });
    });

    function time_finished() {
        $("#task_expired_form").submit();
    }
</script>