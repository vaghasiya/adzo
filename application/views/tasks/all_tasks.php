<?php $this->load->view('elements/worker_header', array("title"=>"All Task","active_menu"=>"all_project","sub_menu"=>"all_task")); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase">All Task</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <?php
                $class = $this -> session -> flashdata('class');
                $message = $this -> session -> flashdata('message');
                if(empty($class)) $class = "danger";
                if(!empty($message)){ ?>
                    <div class="alert alert-<?php echo $class; ?>">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span class="message-text"><?php echo $message; ?></span>
                    </div>
                <?php } ?>
                <table id="all_task_table" class="table display nowrap table-striped table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Publisher</th>
                            <th>Project Title</th>
                            <th>Task<br/>Available</th>
                            <th>Rewards<br/>per Task</th>
                            <th>Duration<br/>per Task</th>
                            <th>Created<br/>Date</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($batch_list as $batch) { ?>
                            <tr>
                                <td><?php echo $batch['first_name'].' '.$batch['last_name']; ?></td>
                                <td><?php echo $batch['title']; ?></td>
                                <td><?php echo $batch['total_assignments'] + $batch['assignments_republished'] - $batch['assignments_submitted'] - $batch['assignments_pending']; ?></td>
                                <td><?php echo $batch['reward_per_assignment']; ?></td>
                                <td><?php echo $batch['time_per_assignment'].' '.$batch['time_per_assignment_unit']; ?></td>
                                <td><?php echo date("d-m-Y", strtotime($batch['created_date'])); ?></td>
                                <td class="text-center">
                                    <a class="btn btn-success btn-xs task_details" data-visibility="<?php echo $batch['hit_visibility']; ?>" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Preview</a>&nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-info btn-xs accept_task" data-id="<?php echo $batch['batch_id']; ?>" href="javascript:;">Accept</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url(); ?>tasks/details" method="post" id="details_form" target="_blank">
    <input type="hidden" id="details_batch_id" name="batch_id" value=""/>
    <input type="hidden" id="details_start_work" name="start_work" value="false"/>
</form>
<?php $this->load->view('elements/worker_footer'); ?>
<script>
    $(document).ready(function() {
        $('#all_task_table').DataTable({
            responsive: true
        });

        $("#all_task_table").on("click",".task_details",function(){
            var hit_visibility = $(this).data("visibility");
            if(hit_visibility == true){
                swal("Opps, Error Occurred","Sorry, Task preview is not available for this batch.! ! !","error");
            } else {
                var id = $(this).data("id");
                $("#details_batch_id").val(id);
                $("#details_start_work").val("false");
                $("#details_form").submit();
            }
        });

        $("#all_task_table").on("click",".accept_task",function(){
            var id = $(this).data("id");
            $("#details_start_work").val("true");
            $("#details_batch_id").val(id);
            $("#details_form").submit();
        });
    });
</script>