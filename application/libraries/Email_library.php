<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Email_library {

    public $CI;

    function __construct() {
        $this -> CI = &get_instance();
    }

    function mail_send($data)
    {
        $data['EmailFrom'] = "";
        $data['Password'] = "";

        if(empty($data['EmailFrom']) || empty($data['Password'])){
            return false;
        }
        $this->CI->load->file(FCPATH.'assets/global/plugins/PHPMailer/PHPMailerAutoload.php', true);
        //Create a new PHPMailer instance
        $mail = new PHPMailer;

        //Tell PHPMailer to use SMTP
        $mail->isSMTP();

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 0;

        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';

        //Set the hostname of the mail server
        $mail->Host = 'ssl://smtp.gmail.com';
        // use
        // $mail->Host = gethostbyname('smtp.gmail.com');
        // if your network does not support SMTP over IPv6

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 465;

        //Set the encryption system to use - ssl (deprecated) or tls
        $mail->SMTPSecure = 'ssl';

        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = "{$data['EmailFrom']}";

        //Password to use for SMTP authentication
        $mail->Password = $data['Password'];

        //Set who the message is to be sent from
        $mail->setFrom("{$data['EmailFrom']}","Atelier Digital Market");

        //Set who the message is to be sent to
        $mail->addAddress($data['to']);

        // Set email format to HTML
        $mail->isHTML(true);

        //Set the subject line
        $mail->Subject = $data['subject'];

        $mail->Body    = $data['message'];

        //Replace the plain text body with one created manually
        $mail->AltBody = 'Sorry, this is html type email and your email provider does not support html type email. Please contact your email administrator regarding this problem.';

        //send the message, check for errors
        if ($mail->send()) {
            return true;
//            echo "Message sent!";
        } else {
//            return false;
            echo "Mailer Error: " . $mail->ErrorInfo;
        }
        exit;
    }
}