<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Common_library
{
    public $CI;
    function __construct()
    {
        $this -> CI = &get_instance();
    }

    function uploadFile($field_name, $upload_path, $file_name="", $allowed_types="")
    {
        $this -> CI -> load -> library('upload');
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777, TRUE);
        }
        if(!empty($file_name)) {
            $config['file_name'] = $file_name;
        }
        if(!empty($allowed_types)) {
            $config['allowed_types'] = $allowed_types;
        }
        $config['upload_path'] = $upload_path;
        $config['overwrite'] = true;
        $config['remove_spaces'] = TRUE;
        $this->CI->upload->initialize($config);

        $FileName = "";
        if ($this->CI->upload->do_upload($field_name)) {
            $file = $this->CI->upload->data();
            $FileName = $file['file_name'];
        } else {
            $error = $this->CI->upload->display_errors();
            return $error;
        }
        return $FileName;
    }

    public function initPagination($base_url, $total_rows, $per_page = PER_PAGE) {
        $this->CI->load->library('pagination');
        $config['page_query_string'] = FALSE;
        $config['base_url'] = base_url() . $base_url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = FALSE;
        $config['prev_link'] = FALSE;
        $this->CI->pagination->initialize($config);
        return $this->CI->pagination->create_links();
    }

    public function get_seconds_from_time($time, $unit){
        $time = intval($time);
        if($unit == "Minutes"){
            $time *= 60;
        } else if($unit == "Hours"){
            $time *= 3600;
        } else if($unit == "Days"){
            $time *= 86400;
        }
        return $time;
    }

    public function get_minutes_from_time($time, $unit){
        $time = intval($time);
        if($unit == "Hours"){
            $time *= 60;
        } else if($unit == "Days"){
            $time *= 1440;
        }
        return $time;
    }

    public function validate_csv_file($csv_file, $csv_header) {
        $csv_header_array = explode(",",$csv_header);
        $columns = count($csv_header_array);
        $lines_array = explode("\n", file_get_contents($csv_file));
        $errors = array();
        if(count($lines_array) < 2){
            $errors[] = "Project-Task Data is Missing";
        }
        foreach ($lines_array as $key => $line) {
            if(empty($line)){
                continue;
            }
            $csv_line_array = explode(",", $line);
            if($key > 0 && count($csv_line_array) != $columns){
                $errors[] = "Line $key: Columns are not matched with header elements";
            } else if(($key == 0) && (trim($line) != $csv_header)) {
                $errors[] = "Header is Not Matched with Project Variables";
            }
        }
        return $errors;
    }
}