<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Withdrawals_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("withdrawals", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function batch_insert($data)
    {
        try
        {
            $this -> db -> insert_batch("withdrawals", $data);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function update($data)
    {
        $condition = array(
            'withdraw_id' => $data['withdraw_id']
        );
        try
        {
            $this -> db -> update("withdrawals", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($ID)
    {
        try
        {
            $this -> db -> where('withdraw_id', $ID) -> delete("withdrawals");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'w.withdraw_id' => $ID
        );
        $query = $this -> db -> select("w.*, u.first_name, u.last_name, u.unique_user_id")
            -> from("withdrawals as w")
            -> join("users as u","w.created_by = u.user_id")
            -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_all_data() {
        $query = $this -> db -> select("w.*, u.first_name, u.last_name, u.unique_user_id")
            -> from("withdrawals as w")
            -> join("users as u","w.created_by = u.user_id")
            -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_my_withdrawals() {
        $ID = $this -> session -> userdata('user_id');
        $condition = array(
            'w.created_by' => $ID
        );
        $query = $this -> db -> select("w.*, u.first_name, u.last_name, u.unique_user_id")
            -> from("withdrawals as w")
            -> join("users as u","w.created_by = u.user_id")
            -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }
}