<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class System_qualifications_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("system_qualifications", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function batch_insert($data)
    {
        try
        {
            $this -> db -> insert_batch("system_qualifications", $data);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function update($data)
    {
        $condition = array(
            'system_qualification_id' => $data['system_qualification_id']
        );
        try
        {
            $this -> db -> update("system_qualifications", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function batch_delete_by_project_id($data)
    {
        try
        {
            $this->db->where_in('project_id',$data);
            $this->db->delete('system_qualifications');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_project_id($data)
    {
        try
        {
            $this -> db -> where('project_id', $data) -> delete("system_qualifications");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_project_id($ID) {
        $condition = array(
            'project_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("system_qualifications") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }
}