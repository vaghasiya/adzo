<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Blocked_workers_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("blocked_workers", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data)
    {
        try
        {
            $condition = array(
                'block_id' => $data['block_id']
            );
            $this -> db -> update("blocked_workers",$data,$condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_publisher_id($data)
    {
        try
        {
            $this->db->where('publisher_id',$data);
            $this->db->delete('blocked_workers');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_worker_id($data)
    {
        try
        {
            $this->db->where('worker_id',$data);
            $this->db->delete('blocked_workers');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($data)
    {
        try
        {
            $this -> db -> where('block_id', $data) -> delete("blocked_workers");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'block_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("blocked_workers") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_by_publisher_id($ID) {
        $condition = "bw.publisher_id = $ID";
        $query = $this -> db -> select("bw.*, u.unique_user_id") -> from("blocked_workers as bw") -> join("users as u","bw.worker_id = u.user_id") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_by_worker_id($ID) {
        $condition = "worker_id = $ID";
        $query = $this -> db -> select("*") -> from("blocked_workers") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }

    function is_blocked($ID) {
        $publisher_id = $this -> session -> userdata('user_id');
        $condition = array(
            'publisher_id' => $publisher_id,
            'worker_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("blocked_workers") -> where($condition) -> get();
        $result = $query->result_array();
        if(!empty($result)) {
            return true;
        }
        return false;
    }
}