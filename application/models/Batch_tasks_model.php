<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Batch_tasks_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
        $this -> load -> model("batch_task_answers_model","batch_task_answers",true);
    }

    function insert($data)
    {
        $this -> db -> insert("batch_tasks", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data)
    {
        $condition = array(
            'batch_task_id' => $data['batch_task_id']
        );
        try
        {
            $this -> db -> update("batch_tasks", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function batch_delete_by_batch_id($data)
    {
        try
        {
            $this->db->where_in('batch_id',$data);
            $this->db->delete('batch_tasks');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_batch_id($data)
    {
        try
        {
            $this -> db -> where('batch_id', $data) -> delete("batch_tasks");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_user_id($data)
    {
        try
        {
            $this -> db -> where('user_id', $data) -> delete("batch_tasks");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($data)
    {
        try
        {
            $this -> db -> where('batch_task_id', $data) -> delete("batch_tasks");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'batch_task_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("batch_tasks") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_by_batch_id($ID) {
        $condition = array(
            'batch_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("batch_tasks") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_by_user_id($ID) {
        $condition = array(
            'user_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("batch_tasks") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_by_batch_id_user_id($batch_id, $user_id) {
        $condition = array(
            'batch_id' => $batch_id,
            'user_id' => $user_id
        );
        $query = $this -> db -> select("*") -> from("batch_tasks") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }
    
    function get_active_task_by_user_id($user_id = "") {
        if(empty($user_id)) {
            $user_id = $this -> session -> userdata('user_id');
        }
        $condition = array(
            'bt.user_id' => $user_id,
            "bt.status" => "active"
        );
        $query = $this -> db -> select("bt.*, b.title, b.time_per_assignment, b.time_per_assignment_unit, u.first_name, u.last_name")
            -> from("batch_tasks as bt")
            -> join("batches as b","bt.batch_id = b.batch_id")
            -> join("users as u","b.created_by = u.user_id")
            -> where($condition) -> get();

        $result = $query->result_array();
        return $result;
    }

    function get_reassigned_task_by_user_id($user_id = "") {
        if(empty($user_id)) {
            $user_id = $this -> session -> userdata('user_id');
        }
        $condition = array(
            'bt.user_id' => $user_id,
            "bt.status" => "reassigned"
        );
        $query = $this -> db -> select("bt.*, b.title, b.time_per_assignment, b.time_per_assignment_unit, u.first_name, u.last_name")
            -> from("batch_tasks as bt")
            -> join("batches as b","bt.batch_id = b.batch_id")
            -> join("users as u","b.created_by = u.user_id")
            -> where($condition) -> get();

        $result = $query->result_array();
        return $result;
    }

    function get_task_and_answers_by_batch_id($batch_id, $condition = "") {
        if(!empty($condition)) {
            $condition .= " AND ";
        }
        $condition .= "bt.batch_id = $batch_id";
        $query = $this -> db -> select("bt.*, bh.csv_data, u.unique_user_id as worker_id")
            -> from("batch_tasks as bt")
            -> join("batches as b","bt.batch_id = b.batch_id")
            -> join("batch_hits as bh","bt.batch_hit_id = bh.batch_hit_id")
            -> join("users as u","bt.user_id = u.user_id")
            -> where($condition) -> get();

        $result = $query->result_array();
        foreach ($result as $key => $row) {
            $answers = $this -> batch_task_answers -> get_by_batch_task_id($row['batch_task_id']);
            foreach ($answers as $answer) {
                $result[$key]['answers'][$answer['answer_key']] = $answer['answer_value'];
            }
        }
        return $result;
    }

    function get_worker_summary($batch_id) {
        $sql = "SELECT tab1.user_id, tab1.unique_user_id, tab2.submitted, tab3.approved, tab4.rejected, tab5.pending, tab6.reassigned, tab7.expired, tab8.returned_task, tab9.earnings, tab10.block_id FROM

                (SELECT DISTINCT bt.user_id, u.unique_user_id  FROM `batch_tasks` as bt JOIN users as u ON bt.user_id = u.user_id WHERE `batch_id` = '$batch_id') tab1

                LEFT JOIN

                (SELECT user_id, COUNT(*) as submitted FROM `batch_tasks` WHERE `batch_id` = '$batch_id' AND `status` IN ('submitted','approved','rejected','reassigned') GROUP BY user_id) tab2 ON tab1.user_id = tab2.user_id

                LEFT JOIN

                (SELECT user_id, COUNT(*) as approved FROM `batch_tasks` WHERE `batch_id` = '$batch_id' AND `status` = 'approved' GROUP BY user_id) tab3 ON tab1.user_id = tab3.user_id

                LEFT JOIN

                (SELECT user_id, COUNT(*) as rejected FROM `batch_tasks` WHERE `batch_id` = '$batch_id' AND `status` = 'rejected' GROUP BY user_id) tab4 ON tab1.user_id = tab4.user_id

                LEFT JOIN

                (SELECT user_id, COUNT(*) as pending FROM `batch_tasks` WHERE `batch_id` = '$batch_id' AND `status` = 'active' GROUP BY user_id) tab5 ON tab1.user_id = tab5.user_id

                LEFT JOIN

                (SELECT user_id, COUNT(*) as reassigned FROM `batch_tasks` WHERE `batch_id` = '$batch_id' AND `status` = 'reassigned' GROUP BY user_id) tab6 ON tab1.user_id = tab6.user_id

                LEFT JOIN

                (SELECT user_id, COUNT(*) as expired FROM `batch_tasks` WHERE `batch_id` = '$batch_id' AND `status` = 'expired' GROUP BY user_id) tab7 ON tab1.user_id = tab7.user_id

                LEFT JOIN

                (SELECT user_id, COUNT(*) as returned_task FROM `batch_tasks` WHERE `batch_id` = '$batch_id' AND `status` = 'returned' GROUP BY user_id) tab8 ON tab1.user_id = tab8.user_id

                LEFT JOIN

                (SELECT user_id, SUM(task_rewards) as earnings FROM `batch_tasks` WHERE `batch_id` = '$batch_id' AND `status` = 'approved' GROUP BY user_id) tab9 ON tab1.user_id = tab9.user_id

                LEFT JOIN

                (SELECT block_id, worker_id FROM `blocked_workers`) tab10 ON tab1.user_id = tab10.worker_id";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_task_summary_days_for_worker() {
        $user_id = $this -> session -> userdata('user_id');
        $start_date = date('Y-m-d', strtotime('today - 30 days'));
        $sql = "SELECT tab1.start_date, tab2.submitted, tab3.approved, tab4.rejected, tab5.review_pending, tab6.in_progress, tab7.earnings FROM

                (SELECT DISTINCT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date FROM `batch_tasks` WHERE `user_id` = $user_id AND status IN ('active','submitted','approved','rejected') AND DATE(start_date_time) >= '$start_date' ORDER BY start_date_time) tab1

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as submitted FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` IN ('submitted','approved','rejected') GROUP BY DATE_FORMAT(start_date_time,'%d-%m-%Y')) tab2 ON tab1.start_date = tab2.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as approved FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'approved' GROUP BY DATE_FORMAT(start_date_time,'%d-%m-%Y')) tab3 ON tab1.start_date = tab3.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as rejected FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'rejected' GROUP BY DATE_FORMAT(start_date_time,'%d-%m-%Y')) tab4 ON tab1.start_date = tab4.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as review_pending FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'submitted' GROUP BY DATE_FORMAT(start_date_time,'%d-%m-%Y')) tab5 ON tab1.start_date = tab5.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as in_progress FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'active' GROUP BY DATE_FORMAT(start_date_time,'%d-%m-%Y')) tab6 ON tab1.start_date = tab6.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, SUM(task_rewards) as earnings FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'approved' GROUP BY DATE_FORMAT(start_date_time,'%d-%m-%Y')) tab7 ON tab1.start_date = tab7.start_date";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_task_summary_monthly_for_worker() {
        $user_id = $this -> session -> userdata('user_id');
        $start_date = date('Y-m', strtotime('today - 12 months'))."-01";
        $sql = "SELECT tab1.start_date, tab2.submitted, tab3.approved, tab4.rejected, tab5.review_pending, tab6.in_progress, tab7.earnings FROM

                (SELECT DISTINCT DATE_FORMAT(start_date_time,'%M %Y') as start_date FROM `batch_tasks` WHERE `user_id` = $user_id AND status IN ('active','submitted','approved','rejected') AND DATE(start_date_time) >= '$start_date' ORDER BY start_date_time) tab1

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as submitted FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` IN ('submitted','approved','rejected') GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab2 ON tab1.start_date = tab2.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as approved FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'approved' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab3 ON tab1.start_date = tab3.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as rejected FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'rejected' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab4 ON tab1.start_date = tab4.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as review_pending FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'submitted' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab5 ON tab1.start_date = tab5.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as in_progress FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'active' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab6 ON tab1.start_date = tab6.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, SUM(task_rewards) as earnings FROM `batch_tasks` WHERE `user_id` = '$user_id' AND `status` = 'approved' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab7 ON tab1.start_date = tab7.start_date";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_task_statistics_for_worker($user_id = ""){
        if(empty($user_id)) {
            $user_id = $this -> session -> userdata('user_id');
        }
        $sql = "SELECT * FROM

                (SELECT COUNT(*) as submitted FROM `batch_tasks` WHERE `user_id` = $user_id AND status IN ('submitted','approved','rejected')) tab1

                JOIN

                (SELECT COUNT(*) as approved FROM `batch_tasks` WHERE `user_id` = $user_id AND status = 'approved') tab2 ON 1=1

                JOIN

                (SELECT COUNT(*) as rejected FROM `batch_tasks` WHERE `user_id` = $user_id AND status = 'rejected') tab3 ON 1=1

                JOIN

                (SELECT COUNT(*) as review_pending FROM `batch_tasks` WHERE `user_id` = $user_id AND status = 'submitted') tab4 ON 1=1

                JOIN

                (SELECT COUNT(*) as in_progress FROM `batch_tasks` WHERE `user_id` = $user_id AND status = 'active') tab5 ON 1=1

                JOIN

                (SELECT SUM(task_rewards) as earnings FROM `batch_tasks` WHERE `user_id` = $user_id AND status = 'approved') tab6 ON 1=1";

        $query = $this -> db -> query($sql);
        $result = $query->row_array();
        $result['approval_rate'] = number_format((($result['approved']) / ($result['approved'] + $result['rejected']))*100,2);
        $result['rejection_rate'] = number_format((($result['rejected']) / ($result['approved'] + $result['rejected']))*100,2);
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_publisher_summary_for_worker($summary_type = "") {
        $user_id = $this -> session -> userdata('user_id');
        $condition = "";
        if($summary_type == "week"){
            $start_date = date('Y-m-d', strtotime('today - 7 days'));
            $condition .= " AND DATE(start_date_time) > '$start_date'";
        } else if($summary_type == "month"){
            $start_date = date('Y-m-d', strtotime('today - 30 days'));
            $condition .= " AND DATE(start_date_time) > '$start_date'";
        }
        $sql = "SELECT tab1.publisher_name, tab2.submitted, tab3.approved, tab4.rejected, tab5.review_pending, tab6.in_progress, tab7.earnings FROM

                (SELECT DISTINCT u.user_id, CONCAT_WS(' ', u.first_name, u.last_name) as publisher_name FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id JOIN users as u ON b.created_by = u.user_id where bt.user_id = $user_id AND bt.status IN ('active','submitted','approved','rejected') $condition) tab1

                LEFT JOIN

                (SELECT COUNT(*) as submitted, b.created_by FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.user_id = '$user_id' AND bt.status IN ('submitted','approved','rejected') $condition GROUP BY b.created_by) tab2 ON tab1.user_id = tab2.created_by

                LEFT JOIN

                (SELECT COUNT(*) as approved, b.created_by FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.user_id = '$user_id' AND bt.status = 'approved' $condition GROUP BY b.created_by) tab3 ON tab1.user_id = tab3.created_by

                LEFT JOIN

                (SELECT COUNT(*) as rejected, b.created_by FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.user_id = '$user_id' AND bt.status = 'rejected' $condition GROUP BY b.created_by) tab4 ON tab1.user_id = tab4.created_by

                LEFT JOIN

                (SELECT COUNT(*) as review_pending, b.created_by FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.user_id = '$user_id' AND bt.status = 'submitted' $condition GROUP BY b.created_by) tab5 ON tab1.user_id = tab5.created_by

                LEFT JOIN

                (SELECT COUNT(*) as in_progress, b.created_by FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.user_id = '$user_id' AND bt.status = 'active' $condition GROUP BY b.created_by) tab6 ON tab1.user_id = tab6.created_by

                LEFT JOIN

                (SELECT SUM(task_rewards) as earnings, b.created_by FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.user_id = '$user_id' AND bt.status = 'approved' $condition GROUP BY b.created_by) tab7 ON tab1.user_id = tab7.created_by";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_task_summary_days_for_publisher() {
        $user_id = $this -> session -> userdata('user_id');
        $start_date = date('Y-m-d', strtotime('today - 30 days'));
        $sql = "SELECT tab1.start_date, tab2.submitted, tab3.approved, tab4.rejected, tab5.review_pending, tab6.in_progress, tab7.payment FROM

                (SELECT DISTINCT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = $user_id AND bt.status IN ('active','submitted','approved','rejected') AND DATE(bt.start_date_time) >= '$start_date' ORDER BY bt.start_date_time) tab1

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as submitted FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status IN ('submitted','approved','rejected') GROUP BY DATE_FORMAT(bt.start_date_time,'%d-%m-%Y')) tab2 ON tab1.start_date = tab2.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as approved FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'approved' GROUP BY DATE_FORMAT(bt.start_date_time,'%d-%m-%Y')) tab3 ON tab1.start_date = tab3.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as rejected FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'rejected' GROUP BY DATE_FORMAT(bt.start_date_time,'%d-%m-%Y')) tab4 ON tab1.start_date = tab4.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as review_pending FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'submitted' GROUP BY DATE_FORMAT(bt.start_date_time,'%d-%m-%Y')) tab5 ON tab1.start_date = tab5.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, COUNT(*) as in_progress FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'active' GROUP BY DATE_FORMAT(bt.start_date_time,'%d-%m-%Y')) tab6 ON tab1.start_date = tab6.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%d-%m-%Y') as start_date, SUM(bt.task_rewards) as payment FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'approved' GROUP BY DATE_FORMAT(start_date_time,'%d-%m-%Y')) tab7 ON tab1.start_date = tab7.start_date";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_task_summary_monthly_for_publisher() {
        $user_id = $this -> session -> userdata('user_id');
        $start_date = date('Y-m', strtotime('today - 12 months'))."-01";
        $sql = "SELECT tab1.start_date, tab2.submitted, tab3.approved, tab4.rejected, tab5.review_pending, tab6.in_progress, tab7.payment FROM

                (SELECT DISTINCT DATE_FORMAT(start_date_time,'%M %Y') as start_date FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = $user_id AND bt.status IN ('active','submitted','approved','rejected') AND DATE(start_date_time) >= '$start_date' ORDER BY start_date_time) tab1

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as submitted FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status IN ('submitted','approved','rejected') GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab2 ON tab1.start_date = tab2.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as approved FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'approved' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab3 ON tab1.start_date = tab3.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as rejected FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'rejected' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab4 ON tab1.start_date = tab4.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as review_pending FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'submitted' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab5 ON tab1.start_date = tab5.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, COUNT(*) as in_progress FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'active' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab6 ON tab1.start_date = tab6.start_date

                LEFT JOIN

                (SELECT DATE_FORMAT(start_date_time,'%M %Y') as start_date, SUM(bt.task_rewards) as payment FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'approved' GROUP BY DATE_FORMAT(start_date_time,'%M %Y')) tab7 ON tab1.start_date = tab7.start_date";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_task_statistics_for_publisher($user_id = ""){
        if(empty($user_id)) {
            $user_id = $this -> session -> userdata('user_id');
        }
        $sql = "SELECT * FROM

                (SELECT SUM(b.total_assignments) as published FROM `batches` as b WHERE b.created_by = '$user_id') tab0

                JOIN

                (SELECT COUNT(*) as submitted FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status IN ('submitted','approved','rejected')) tab1

                JOIN

                (SELECT COUNT(*) as approved FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'approved') tab2 ON 1=1

                JOIN

                (SELECT COUNT(*) as rejected FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'rejected') tab3 ON 1=1

                JOIN

                (SELECT COUNT(*) as review_pending FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'submitted') tab4 ON 1=1

                JOIN

                (SELECT COUNT(*) as in_progress FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'active') tab5 ON 1=1

                JOIN

                (SELECT SUM(task_rewards) as payment FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'approved') tab6 ON 1=1";

        $query = $this -> db -> query($sql);
        $result = $query->row_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_worker_summary_for_publisher($summary_type = "") {
        $user_id = $this -> session -> userdata('user_id');
        $condition = "";
        if($summary_type == "week"){
            $start_date = date('Y-m-d', strtotime('today - 7 days'));
            $condition .= " AND DATE(bt.start_date_time) > '$start_date'";
        } else if($summary_type == "month"){
            $start_date = date('Y-m-d', strtotime('today - 30 days'));
            $condition .= " AND DATE(bt.start_date_time) > '$start_date'";
        }
        $sql = "SELECT tab1.unique_user_id, tab2.submitted, tab3.approved, tab4.rejected, tab5.review_pending, tab6.in_progress, tab7.payment FROM

                (SELECT DISTINCT u.user_id, u.unique_user_id, CONCAT_WS(' ', u.first_name, u.last_name) as worker_name FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id JOIN users as u ON bt.user_id = u.user_id where b.created_by = $user_id AND bt.status IN ('active','submitted','approved','rejected') $condition) tab1

                LEFT JOIN

                (SELECT COUNT(*) as submitted, bt.user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status IN ('submitted','approved','rejected') $condition GROUP BY bt.user_id) tab2 ON tab1.user_id = tab2.user_id

                LEFT JOIN

                (SELECT COUNT(*) as approved, bt.user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'approved' $condition GROUP BY bt.user_id) tab3 ON tab1.user_id = tab3.user_id

                LEFT JOIN

                (SELECT COUNT(*) as rejected, bt.user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'rejected' $condition GROUP BY bt.user_id) tab4 ON tab1.user_id = tab4.user_id

                LEFT JOIN

                (SELECT COUNT(*) as review_pending, bt.user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'submitted' $condition GROUP BY bt.user_id) tab5 ON tab1.user_id = tab5.user_id

                LEFT JOIN

                (SELECT COUNT(*) as in_progress, bt.user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'active' $condition GROUP BY bt.user_id) tab6 ON tab1.user_id = tab6.user_id

                LEFT JOIN

                (SELECT SUM(task_rewards) as payment, bt.user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE b.created_by = '$user_id' AND bt.status = 'approved' $condition GROUP BY bt.user_id) tab7 ON tab1.user_id = tab7.user_id";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }
}