<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Other_qualifications_request_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("other_qualifications_request", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data)
    {
        $condition = array(
            'qualification_request_id' => $data['qualification_request_id']
        );
        try
        {
            $this -> db -> update("other_qualifications_request", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($ID)
    {
        try
        {
            $this -> db -> where('qualification_request_id', $ID) -> delete("other_qualifications_request");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_user_id($data)
    {
        try
        {
            $this -> db -> where('created_by', $data) -> delete("other_qualifications_request");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'qualification_request_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("other_qualifications_request") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_workers_request_by_qualification_id($ID, $status = "") {
        $user_id = $this -> session -> userdata('user_id');
        $condition = array(
            'qualification_id' => $ID,
            'worker_id' => $user_id
        );
        if(!empty($status)) {
            $condition['status'] = $status;
        }
        $query = $this -> db -> select("*") -> from("other_qualifications_request") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_all_pending_request() {
        $user_id = $this -> session -> userdata('user_id');
        $condition = array(
            'oqr.status' => "pending",
            'oq.created_by' => $user_id
        );
        $query = $this -> db -> select("oqr.*, oq.name, u.unique_user_id") -> from("other_qualifications_request as oqr") -> join("other_qualifications as oq", "oqr.qualification_id = oq.qualification_id") -> join("users as u", "oqr.worker_id = u.user_id") -> where($condition) -> order_by("oqr.created_date","DESC") -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_by_qualification_id($qualification_id) {
        $condition = array(
            'oqr.qualification_id' => $qualification_id
        );
        $query = $this -> db -> select("oqr.*, u.unique_user_id") -> from("other_qualifications_request as oqr") -> join("users as u", "oqr.worker_id = u.user_id") -> where($condition) -> order_by("oqr.created_date","DESC") -> get();
        $result = $query->result_array();
        return $result;
    }

    function is_worker_request_exist($qualification_id) {
        $user_id = $this -> session -> userdata('user_id');
        $condition = array(
            'qualification_id' => $qualification_id,
            'worker_id' => $user_id,
        );
        $query = $this -> db -> select("*") -> from("other_qualifications_request") -> where($condition) -> get();
        $result = $query->result_array();
        if(empty($result)) {
            return false;
        }
        return true;
    }
}