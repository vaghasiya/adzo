<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Batch_qualifications_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("batch_qualifications", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function batch_insert($data)
    {
        try
        {
            $this -> db -> insert_batch("batch_qualifications", $data);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function update($data)
    {
        $condition = array(
            'batch_qualification_id' => $data['batch_qualification_id']
        );
        try
        {
            $this -> db -> update("batch_qualifications", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function batch_delete_by_batch_id($data)
    {
        try
        {
            $this->db->where_in('batch_id',$data);
            $this->db->delete('batch_qualifications');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_batch_id($data)
    {
        try
        {
            $this -> db -> where('batch_id', $data) -> delete("batch_qualifications");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_batch_id($ID) {
        $condition = array(
            'batch_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("batch_qualifications") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }
}