<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Projects_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("projects", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data)
    {
        $condition = array(
            'project_id' => $data['project_id']
        );
        try
        {
            $this -> db -> update("projects", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function batch_delete($data)
    {
        try
        {
            $this->db->where_in('project_id',$data);
            $this->db->delete('projects');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($data)
    {
        try
        {
            $this -> db -> where('project_id', $data) -> delete("projects");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'project_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("projects") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_publishers_projects_list($fields = "*") {
        $condition = array(
            'created_by' => $this -> session -> userdata('user_id')
        );
        $query = $this -> db -> select($fields) -> from("projects") -> where($condition) -> order_by("created_date","desc") -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_projects_list() {
        $query = $this -> db -> select("*") -> from("projects") -> order_by("created_date","desc") -> get();
        $result = $query->result_array();
        return $result;
    }

    function is_project_name_available($project_name, $project_id="") {
        $user_id = $this -> session -> userdata('user_id');
        $condition = array(
            'created_by' => $user_id,
            'project_name' => $project_name
        );
        if(!empty($project_id)) {
            $condition['project_id !='] = "$project_id";
        }
        $query = $this -> db -> select("*") -> from("projects") -> where($condition) -> get();
        $result = $query->result_array();
        if(empty($result)) {
            return true;
        }
        return false;
    }

    function get_project_counter($project_name) {
        $user_id = $this -> session -> userdata('user_id');
        $query = $this -> db -> select("*") -> from("projects") -> where("created_by",$user_id) -> like("project_name",$project_name,"after") -> get();
        $result = $query->result_array();
        $counter = count($result);
        if($counter > 0){
            $counter++;
        } else {
            $counter = "1";
        }
        return " ".$counter;
    }
}