<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users_verification_request_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("users_verification_request", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data)
    {
        $condition = array(
            'verification_id' => $data['verification_id']
        );
        try
        {
            $this -> db -> update("users_verification_request", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($ID)
    {
        try
        {
            $this -> db -> where('verification_id', $ID) -> delete("users_verification_request");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'uv.verification_id' => $ID
        );
        $query = $this -> db -> select("uv.*, u.first_name, u.last_name, u.unique_user_id, u.country")
            -> from("users_verification_request as uv")
            -> join("users as u","uv.created_by = u.user_id")
            -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_all_data() {
        $query = $this -> db -> select("uv.*, u.first_name, u.last_name, u.unique_user_id, u.country")
            -> from("users_verification_request as uv")
            -> join("users as u","uv.created_by = u.user_id")
            -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_my_requests() {
        $ID = $this -> session -> userdata('user_id');
        $condition = array(
            'uv.created_by' => $ID
        );
        $query = $this -> db -> select("uv.*, u.first_name, u.last_name, u.unique_user_id, u.country")
            -> from("users_verification_request as uv")
            -> join("users as u","uv.created_by = u.user_id")
            -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }
}