<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transactions_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("transactions", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function batch_insert($data)
    {
        try
        {
            $this -> db -> insert_batch("transactions", $data);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function update($data)
    {
        $condition = array(
            'transaction_id' => $data['transaction_id']
        );
        try
        {
            $this -> db -> update("transactions", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($ID)
    {
        try
        {
            $this -> db -> where('transaction_id', $ID) -> delete("transactions");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            't.transaction_id' => $ID
        );
        $query = $this -> db -> select("t.*, u.first_name, u.last_name, u.unique_user_id, b.title")
            -> from("transactions as t")
            -> join("batches as b","t.batch_id = b.batch_id")
            -> join("users as u","t.user_id = u.user_id")
            -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_by_user_id($ID) {
        $condition = array(
            't.created_by' => $ID
        );
        $query = $this -> db -> select("t.*, u.first_name, u.last_name, u.unique_user_id, b.title")
            -> from("transactions as t")
            -> join("batches as b","t.batch_id = b.batch_id")
            -> join("users as u","t.user_id = u.user_id")
            -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_my_transactions() {
        $ID = $this -> session -> userdata('user_id');
        $condition = array(
            't.created_by' => $ID
        );
        $query = $this -> db -> select("t.*, u.first_name, u.last_name, u.unique_user_id, b.title")
            -> from("transactions as t")
            -> join("batches as b","t.batch_id = b.batch_id","LEFT")
            -> join("users as u","t.user_id = u.user_id")
            -> where($condition) -> get();
//        echo $this -> db -> last_query(); exit;
        $result = $query->result_array();
        return $result;
    }

    function generate_transaction_reference_no() {
        $transaction_reference_no = date("mdHi") . mt_rand(10,99);
        return $transaction_reference_no;
    }
}