<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Batch_task_answers_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("batch_task_answers", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function batch_insert($data)
    {
        try
        {
            $this -> db -> insert_batch("batch_task_answers", $data);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function update($data)
    {
        $condition = array(
            'batch_task_answer_id' => $data['batch_task_answer_id']
        );
        try
        {
            $this -> db -> update("batch_task_answers", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_batch_task_id($data)
    {
        try
        {
            $this -> db -> where('batch_task_id', $data) -> delete("batch_task_answers");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_batch_id($data)
    {
        try
        {
            $this -> db -> delete("batch_task_answers as bta") -> join("batch_tasks as bt","bta.batch_task_id = bt.batch_task_id") -> where('bt.batch_id', $data);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($data)
    {
        try
        {
            $this -> db -> where('batch_task_answer_id', $data) -> delete("batch_task_answers");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'batch_task_answer_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("batch_task_answers") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }


    function get_by_batch_task_id($ID) {
        $condition = array(
            'batch_task_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("batch_task_answers") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_task_headers_by_batch_id($batch_id) {
        $sql = "SELECT DISTINCT answer_key FROM batch_task_answers
	            WHERE batch_task_id IN (SELECT batch_task_id FROM batch_tasks WHERE batch_id = $batch_id)";
        $query = $this -> db -> query($sql);
        $result = $query->result_array();
        return array_column($result,"answer_key");
    }
}