<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Favourite_publishers_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("favourite_publishers", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data)
    {
        try
        {
            $condition = array(
                'favourite_id' => $data['favourite_id']
            );
            $this -> db -> update("favourite_publishers",$data,$condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_publisher_id($data)
    {
        try
        {
            $this->db->where('publisher_id',$data);
            $this->db->delete('favourite_publishers');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($data)
    {
        try
        {
            $this -> db -> where('favourite_id', $data) -> delete("favourite_publishers");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'favourite_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("favourite_publishers") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_by_worker_id($ID) {
        $condition = "fp.worker_id = $ID";
        $query = $this -> db -> select("fp.*, u.unique_user_id") -> from("favourite_publishers as fp") -> join("users as u","fp.publisher_id = u.user_id") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }

    function is_favourite($ID) {
        $worker_id = $this -> session -> userdata('user_id');
        $condition = array(
            'publisher_id' => $ID,
            'worker_id' => $worker_id
        );
        $query = $this -> db -> select("*") -> from("favourite_publishers") -> where($condition) -> get();
        $result = $query->result_array();
        if(!empty($result)) {
            return true;
        }
        return false;
    }
}