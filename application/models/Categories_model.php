<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categories_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function get_all_categories_list() {
        $query = $this -> db -> select("category_id, category_name, type, order_no, img_link") -> from("categories") -> order_by("type","DESC") -> order_by("order_no","ASC") -> get();
        return $query -> result_array();
    }

    function get_categories_list_by_type($type="multiple") {
        $condition = array(
            'type' => $type
        );
        $query = $this -> db -> select("category_id, category_name, type, order_no, img_link") -> from("categories") -> where($condition) -> order_by("type","DESC") -> order_by("order_no","ASC") -> get();
        return $query -> result_array();
    }

    function get_by_id($ID, $fields = "*") {
        $condition = array(
            'category_id' => $ID
        );
        $query = $this -> db -> select("$fields") -> from("categories") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }
}