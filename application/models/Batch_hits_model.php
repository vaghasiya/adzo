<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Batch_hits_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("batch_hits", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function batch_insert($data)
    {
        try
        {
            $this -> db -> insert_batch("batch_hits", $data);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function update($data, $escape_data = FALSE)
    {
        try
        {
            if($escape_data == TRUE) {
                foreach ($data as $key => $value) {
                    $this -> db -> set("$key", "$value", FALSE);
                }
                $this -> db -> where('batch_hit_id', $data['batch_hit_id']);
                $this -> db -> update("batch_hits");
            } else {
                $condition = array(
                    'batch_hit_id' => $data['batch_hit_id']
                );
                $this -> db -> update("batch_hits",$data,$condition);
            }
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function batch_delete_by_batch_id($data)
    {
        try
        {
            $this->db->where_in('batch_id',$data);
            $this->db->delete('batch_hits');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_batch_id($data)
    {
        try
        {
            $this -> db -> where('batch_id', $data) -> delete("batch_hits");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'batch_hit_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("batch_hits") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_by_batch_id($ID, $condition="") {
        if(!empty($condition)) {
            $condition .= " AND ";
        }
        $condition .= "batch_id = $ID AND assignments_pending > 0";
        $query = $this -> db -> select("*") -> from("batch_hits") -> where($condition) -> get();
//        echo $this -> db -> last_query();
        $result = $query->result_array();
        return $result;
    }
}