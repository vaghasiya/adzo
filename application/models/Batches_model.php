<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Batches_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("batches", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data, $escape_data = FALSE)
    {
        try
        {
            if($escape_data == TRUE) {
                foreach ($data as $key => $value) {
                    $this -> db -> set("$key", "$value", FALSE);
                }
                $this -> db -> where('batch_id', $data['batch_id']);
                $this -> db -> update("batches");
            } else {
                $condition = array(
                    'batch_id' => $data['batch_id']
                );
                $this -> db -> update("batches",$data,$condition);
            }
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function batch_delete($data)
    {
        try
        {
            $this->db->where_in('batch_id',$data);
            $this->db->delete('batches');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($data)
    {
        try
        {
            $this -> db -> where('batch_id', $data) -> delete("batches");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'batch_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("batches") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_publishers_batch_list($fields = "*", $condition = "") {
        if(!empty($condition)) {
            $condition .= " AND ";
        }
        $condition .= "created_by = ".$this -> session -> userdata('user_id');
        $query = $this -> db -> select("$fields") -> from("batches") -> where($condition) -> order_by("created_date","desc") -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_batches_list($fields="b.*", $condition="") {
        $this -> db -> select("$fields") -> from("batches as b") -> join("users as u","b.created_by = u.user_id");
        if(!empty($condition)) {
            $this -> db -> where($condition);
        }
        $query = $this -> db -> order_by("created_date","desc") -> get();
        $result = $query->result_array();
        return $result;
    }

    function get_batches_list_by_status($status, $fields = "b.*", $condition="") {
        $this -> db -> select("$fields") -> from("batches as b") -> join("users as u","b.created_by = u.user_id");
        $this -> db -> where("b.status",$status);
        if(!empty($condition)) {
            $this -> db -> where($condition);
        }
        $this -> db -> order_by("created_date","desc");
        $query = $this -> db -> get();
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query(); exit;
        return $result;
    }

    function is_batch_name_available($batch_name, $batch_id="") {
        $user_id = $this -> session -> userdata('user_id');
        $condition = array(
            'created_by' => $user_id,
            'batch_name' => $batch_name
        );
        if(!empty($batch_id)) {
            $condition['batch_id !='] = "$batch_id";
        }
        $query = $this -> db -> select("*") -> from("batches") -> where($condition) -> get();
        $result = $query->result_array();
        if(empty($result)) {
            return true;
        }
        return false;
    }

    function get_batch_counter($batch_name) {
        $user_id = $this -> session -> userdata('user_id');
        $query = $this -> db -> select("*") -> from("batches") -> where("created_by",$user_id) -> like("batch_name",$batch_name,"after") -> get();
        $result = $query->result_array();
        $counter = count($result);
        if($counter > 0){
            $counter++;
        } else {
            $counter = 1;
        }
        return " ".$counter;
    }

    function get_available_batch_id_by_user_id($user_id="") {
        if(empty($user_id)) {
            $user_id = $this -> session -> userdata('user_id');
        }
        $sql = "SELECT DISTINCT batch_id FROM batch_hits
                WHERE assignments_pending > 0 AND batch_hit_id NOT IN (SELECT batch_hit_id FROM batch_tasks WHERE user_id = $user_id)";
        $query = $this -> db -> query($sql);
        $result = $query->result_array();
        return $result;
    }
}