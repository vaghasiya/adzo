<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Country_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get_country_list()
    {
        $query = $this -> db -> select("*") -> from("country") -> order_by("country_nice_name") -> get();
        return $query -> result_array();
    }

    function get_by_id($ID)
    {
        $condition = array(
            'country_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("country") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_india_state_list(){
        $query = $this -> db -> select("*") -> from("country_states_india") -> order_by("state_id") -> get();
        return $query -> result_array();
    }

    function get_usa_state_list(){
        $query = $this -> db -> select("*") -> from("country_states_usa") -> order_by("state_id") -> get();
        return $query -> result_array();
    }
}