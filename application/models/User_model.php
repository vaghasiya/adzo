<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('encryption');
    }

    function isLogin($user_type="")
    {
        $user_id = $this -> session -> userdata('user_id');
        $user_name = $this -> session-> userdata('user_name');
        $current_user_type = $this -> session-> userdata('user_type');
        if((!empty($user_id)) && (!empty($user_name)) && ($user_type == $current_user_type)) {
            return true;
        }
        else
        {
            $session_items = array('user_id', 'user_name', 'first_name', 'last_name', 'email', 'user_type', 'profile_pic', 'unique_user_id');
            $this -> session -> unset_userdata($session_items);
            $this -> session -> set_flashdata('message','You are not logged in. Please Login Now.!!!');
            $this -> session -> set_flashdata("modal", "login_modal");
            redirect(base_url());
        }
    }

    function isExist($data)
    {
        $condition = "";
        if (!empty($data['user_name'])) {
            $condition .= "user_name = '{$data['user_name']}'";
        }
        if (!empty($data['email'])) {
            $condition .= "email = '{$data['email']}'";
        }
        if (!empty($data['user_id'])) {
            $condition .= " AND user_id != '{$data['user_id']}'";
        }
        if (!empty($data['user_type'])) {
            $condition .= " AND user_type = '{$data['user_type']}'";
        }
        $this->db->select("*");
        $this->db->from("users");
        if(!empty($condition)) {
            $this->db->where($condition);
        }
        $query = $this->db->get()->result_array();
        if (empty($query)) {
            return false;
        }
        return true;
    }

    function authenticate($username,$password)
    {
        if($username != "" && $password != ""){
            $condition = array(
                "user_name" => $username,
            );
            $query = $this -> db -> select("user_id, user_name, password, first_name, last_name, email, user_type, profile_pic, unique_user_id")->from("users")->where($condition)->get();
            $user = $query->row_array();
//             echo $this -> db -> last_query(); exit;
            if(!empty($user)){
                $original_password = $this -> encryption -> decrypt($user['password']);
//                echo $original_password; exit;
                if($password === $original_password){
                    return $user;
                }
            }
        }
        return array();
    }

    function insert($data)
    {
        $this -> db -> insert("users", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data, $escape_data = FALSE)
    {
        try
        {
            if($escape_data == TRUE) {
                foreach ($data as $key => $value) {
                    $this -> db -> set("$key", "$value", FALSE);
                }
                $this -> db -> where('user_id', $data['user_id']);
                $this -> db -> update("users");
            } else {
                $condition = array(
                    'user_id' => $data['user_id']
                );
                $this -> db -> update("users",$data,$condition);
            }
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function batch_delete($data)
    {
        try
        {
            $this->db->where_in('user_id',$data);
            $this->db->delete('users');
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }
    
    function delete($data)
    {
        try
        {
            $this -> db -> where('user_id', $data) -> delete("users");
            //echo $this -> db -> last_query();
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function isSafeDelete($IDs)
    {
        return true;
    }

    function get_all_data($condition = "")
    {
        $this -> db -> select("*, '' AS password") -> from("users");
        if(!empty($condition)) {
            $this -> db -> where($condition);
        }
        $query = $this -> db -> get();
        return $query -> result_array();
    }

    function get_by_id($ID)
    {
        $condition = array(
            'user_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("users") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_by_username($user_name)
    {
        $query = $this -> db -> select("*")->from("users")->where("user_name",$user_name)->get();
        $result = $query->row_array();
        if(!empty($result)){
            return $result;
        }
        return array();
    }

    function get_by_email($email)
    {
        $query = $this -> db -> select("*")->from("users")->where("email",$email)->get();
        $result = $query->row_array();
        if(!empty($result)){
            return $result;
        }
        return array();
    }

    function get_active_user_data($fields)
    {
        $ID = $this -> session -> userdata('user_id');
        $condition = array(
            'user_id' => $ID
        );
        $query = $this -> db -> select("$fields") -> from("users") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_user_data($ID, $fields)
    {
        $condition = array(
            'user_id' => $ID
        );
        $query = $this -> db -> select("$fields") -> from("users") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_worker_summary_for_admin() {
        $sql = "SELECT tab1.*, '' as password, COALESCE(tab2.submitted,0) as submitted, COALESCE(tab3.approved,0) as approved, COALESCE(tab4.rejected,0) as rejected, COALESCE(tab5.review_pending,0) as review_pending, COALESCE(tab6.in_progress,0) as in_progress, COALESCE(tab7.reassigned,0) as reassigned, COALESCE(tab8.expired,0) as expired, COALESCE(tab9.returned_task,0) as returned_task, COALESCE(tab10.earning,0) as earning FROM

                (SELECT * FROM `users` WHERE user_type = 'worker') tab1

                LEFT JOIN

                (SELECT COUNT(*) as submitted, user_id FROM `batch_tasks` WHERE status IN ('submitted','approved','rejected','reassigned') GROUP BY user_id) tab2 ON tab1.user_id = tab2.user_id

                LEFT JOIN

                (SELECT COUNT(*) as approved, user_id FROM `batch_tasks` WHERE status = 'approved' GROUP BY user_id) tab3 ON tab1.user_id = tab3.user_id

                LEFT JOIN

                (SELECT COUNT(*) as rejected, user_id FROM `batch_tasks` WHERE status = 'rejected' GROUP BY user_id) tab4 ON tab1.user_id = tab4.user_id

                LEFT JOIN

                (SELECT COUNT(*) as review_pending, user_id FROM `batch_tasks` WHERE status = 'submitted' GROUP BY user_id) tab5 ON tab1.user_id = tab5.user_id

                LEFT JOIN

                (SELECT COUNT(*) as in_progress, user_id FROM `batch_tasks` WHERE status = 'active' GROUP BY user_id) tab6 ON tab1.user_id = tab6.user_id

                LEFT JOIN

                (SELECT COUNT(*) as reassigned, user_id FROM `batch_tasks` WHERE status = 'reassigned' GROUP BY user_id) tab7 ON tab1.user_id = tab7.user_id

                LEFT JOIN

                (SELECT COUNT(*) as expired, user_id FROM `batch_tasks` WHERE status = 'expired' GROUP BY user_id) tab8 ON tab1.user_id = tab8.user_id

                LEFT JOIN

                (SELECT COUNT(*) as returned_task, user_id FROM `batch_tasks` WHERE status = 'returned' GROUP BY user_id) tab9 ON tab1.user_id = tab9.user_id

                LEFT JOIN

                (SELECT SUM(task_rewards) as earning, user_id FROM `batch_tasks` WHERE status = 'approved' GROUP BY user_id) tab10 ON tab1.user_id = tab10.user_id";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_publisher_summary_for_admin() {
        $sql = "SELECT tab1.*, '' as password, COALESCE(tab2.submitted,0) as submitted, COALESCE(tab3.approved,0) as approved, COALESCE(tab4.rejected,0) as rejected, COALESCE(tab5.review_pending,0) as review_pending, COALESCE(tab6.in_progress,0) as in_progress, COALESCE(tab7.reassigned,0) as reassigned, COALESCE(tab8.expired,0) as expired, COALESCE(tab9.returned_task,0) as returned_task, COALESCE(tab10.payments,0) as payments, COALESCE(tab11.published,0) as published FROM

                (SELECT * FROM `users` WHERE user_type = 'publisher') tab1

                LEFT JOIN

                (SELECT COUNT(*) as submitted, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status IN ('submitted','approved','rejected','reassigned') GROUP BY b.created_by) tab2 ON tab1.user_id = tab2.user_id

                LEFT JOIN

                (SELECT COUNT(*) as approved, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status = 'approved' GROUP BY b.created_by) tab3 ON tab1.user_id = tab3.user_id

                LEFT JOIN

                (SELECT COUNT(*) as rejected, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status = 'rejected' GROUP BY b.created_by) tab4 ON tab1.user_id = tab4.user_id

                LEFT JOIN

                (SELECT COUNT(*) as review_pending, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status = 'submitted' GROUP BY b.created_by) tab5 ON tab1.user_id = tab5.user_id

                LEFT JOIN

                (SELECT COUNT(*) as in_progress, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status = 'active' GROUP BY b.created_by) tab6 ON tab1.user_id = tab6.user_id

                LEFT JOIN

                (SELECT COUNT(*) as reassigned, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status = 'reassigned' GROUP BY b.created_by) tab7 ON tab1.user_id = tab7.user_id

                LEFT JOIN

                (SELECT COUNT(*) as expired, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status = 'expired' GROUP BY b.created_by) tab8 ON tab1.user_id = tab8.user_id

                LEFT JOIN

                (SELECT COUNT(*) as returned_task, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status = 'returned' GROUP BY b.created_by) tab9 ON tab1.user_id = tab9.user_id

                LEFT JOIN

                (SELECT SUM(bt.task_rewards) as payments, b.created_by as user_id FROM `batch_tasks` as bt JOIN batches as b ON bt.batch_id = b.batch_id WHERE bt.status = 'approved' GROUP BY b.created_by) tab10 ON tab1.user_id = tab10.user_id

                LEFT JOIN

                (SELECT SUM(b.total_assignments) as published, b.created_by as user_id FROM batches as b GROUP BY b.created_by) tab11 ON tab1.user_id = tab11.user_id";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_country_wise_summary_for_admin($user_type = "") {
        $sql = "SELECT tab1.country, tab2.registered, tab3.verified, tab4.unverified, tab5.suspended, tab6.active FROM

                (SELECT DISTINCT country FROM `users` WHERE user_type = '$user_type' ORDER BY country) tab1

                LEFT JOIN

                (SELECT country, COUNT(*) as registered FROM `users` WHERE user_type = '$user_type' GROUP BY country) tab2 ON tab1.country = tab2.country

                LEFT JOIN

                (SELECT country, COUNT(*) as verified FROM `users` WHERE user_type = '$user_type' AND email_verified = '1' AND document_verified = '1' GROUP BY country) tab3 ON tab1.country = tab3.country

                LEFT JOIN

                (SELECT country, COUNT(*) as unverified FROM `users` WHERE user_type = '$user_type' AND (email_verified = '0' OR document_verified = '0') GROUP BY country) tab4 ON tab1.country = tab4.country

                LEFT JOIN

                (SELECT country, COUNT(*) as suspended FROM `users` WHERE user_type = '$user_type' AND status = '0' GROUP BY country) tab5 ON tab1.country = tab5.country

                LEFT JOIN

                (SELECT country, COUNT(*) as active FROM `users` WHERE user_type = '$user_type' AND status = '1' GROUP BY country) tab6 ON tab1.country = tab6.country";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }

    function get_registration_analytics_for_admin($user_type = "") {
        $sql = "SELECT COALESCE(tab1.registered,0) as registered, COALESCE(tab2.verified,0) as verified, COALESCE(tab3.unverified,0) as unverified, COALESCE(tab4.suspended,0) as suspended, COALESCE(tab5.active,0) as active  FROM

                (SELECT COUNT(*) as registered FROM `users` WHERE user_type = '$user_type') tab1

                JOIN

                (SELECT COUNT(*) as verified FROM `users` WHERE user_type = '$user_type' AND email_verified = '1' AND document_verified = '1') tab2 ON 1=1

                JOIN

                (SELECT COUNT(*) as unverified FROM `users` WHERE user_type = '$user_type' AND (email_verified = '0' OR document_verified = '0')) tab3 ON 1=1

                JOIN

                (SELECT COUNT(*) as suspended FROM `users` WHERE user_type = '$user_type' AND status = '0') tab4 ON 1=1

                JOIN

                (SELECT COUNT(*) as active FROM `users` WHERE user_type = '$user_type' AND status = '1') tab5 ON 1=1";

        $query = $this -> db -> query($sql);
        $result = $query->row_array();
//        echo "<pre>";
//        echo $this -> db -> last_query();
//        print_r($result); exit;
        return $result;
    }
}