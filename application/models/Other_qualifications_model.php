<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Other_qualifications_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    function insert($data)
    {
        $this -> db -> insert("other_qualifications", $data);
        $id = $this -> db -> insert_id();
        return $id;
    }

    function update($data)
    {
        $condition = array(
            'qualification_id' => $data['qualification_id']
        );
        try
        {
            $this -> db -> update("other_qualifications", $data, $condition);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete($ID)
    {
        try
        {
            $this -> db -> where('qualification_id', $ID) -> delete("other_qualifications");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function delete_by_user_id($data)
    {
        try
        {
            $this -> db -> where('created_by', $data) -> delete("other_qualifications");
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    function get_by_id($ID) {
        $condition = array(
            'qualification_id' => $ID
        );
        $query = $this -> db -> select("*") -> from("other_qualifications") -> where($condition) -> get();
        $result = $query->row_array();
        return $result;
    }

    function get_my_qualifications() {
        $ID = $this -> session -> userdata('user_id');
        $sql = "SELECT tab1.*, tab2.request_approved, tab3.request_pending FROM

        (SELECT * FROM `other_qualifications` WHERE `created_by` = '$ID') tab1

        LEFT JOIN

        (SELECT qualification_id, COUNT(*) as request_approved FROM `other_qualifications_request` WHERE `status` = 'approved' GROUP BY qualification_id) tab2 ON tab1.qualification_id = tab2.qualification_id

        LEFT JOIN

        (SELECT qualification_id, COUNT(*) as request_pending FROM `other_qualifications_request` WHERE `status` = 'pending' GROUP BY qualification_id) tab3 ON tab1.qualification_id = tab3.qualification_id";

        $query = $this -> db -> query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_admin_qualifications() {
        $condition = array(
            'level' => "admin"
        );
        $query = $this -> db -> select("*") -> from("other_qualifications") -> where($condition) -> get();
        $result = $query->result_array();
        return $result;
    }

    function is_name_available($name, $id="") {
        $user_id = $this -> session -> userdata('user_id');
        $condition = array(
            'created_by' => $user_id,
            'name' => $name
        );
        if(!empty($id)) {
            $condition['qualification_id !='] = "$id";
        }
        $query = $this -> db -> select("*") -> from("other_qualifications") -> where($condition) -> get();
        $result = $query->result_array();
        if(empty($result)) {
            return true;
        }
        return false;
    }
}