<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Banks_list_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get_banks_list()
    {
        $query = $this -> db -> select("*") -> from("banks_list") -> order_by("bank_id","ASC") -> get();
        return $query -> result_array();
    }
}