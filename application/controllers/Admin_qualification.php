<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_qualification extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> database();
        $this -> load -> library('common_library');
        $this -> load -> model("projects_model","projects",true);
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_qualifications_model","batch_qualifications",true);
        $this -> load -> model("system_qualifications_model","system_qualifications",true);
        $this -> load -> model("other_qualifications_model","other_qualifications",true);
        $this -> load -> model("other_qualifications_request_model","other_qualifications_request",true);
        $this -> load -> model("categories_model","categories",true);
        $this -> load -> model("country_model","country",true);
    }

    function manage() {
        $this -> user -> isLogin("admin");
        $data['qualifications_list'] = $this -> other_qualifications -> get_my_qualifications();
        $data['request_list'] = $this -> other_qualifications_request -> get_all_pending_request();
        $this -> load -> view('qualifications/manage_qualifications_admin',$data);
    }

    function create() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        if($this -> other_qualifications -> is_name_available($data['name'])) {
            $data['level'] = $this->session->userdata('user_type');
            $data['created_by'] = $this->session->userdata('user_id');
            $data['created_date'] = date("Y-m-d H:i:s");
            $this->other_qualifications->insert($data);
            $this->session->set_flashdata("message", "Qualification added successfully");
            $this -> session -> set_flashdata("class", "success");
        } else {
            $this -> session -> set_flashdata("message", "Qualification name already exist, try another name.! ! !");
        }
        redirect(base_url() . 'admin-qualification/manage');
    }

    function update_details() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['qualification_id'])) {
            if($this -> other_qualifications -> is_name_available($data['name'], $data['qualification_id'])) {
                $this->other_qualifications->update($data);
                $message = "Qualification updated successfully.";
                $this->session->set_flashdata("class", "success");
            } else {
                $message = "Qualification name already exist, try another name.! ! !";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin-qualification/manage');
    }

    function delete_qualification() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['qualification_id'])) {
            $this -> other_qualifications -> delete($data['qualification_id']);
            $message = "Qualification deleted successfully.";
            $this -> session -> set_flashdata("class", "success");
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin-qualification/manage');
    }

    function details()
    {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        if(empty($data['qualification_id'])) {
            $data['qualification_id'] = $this -> session -> flashdata("qualification_id");
            if(empty($data['qualification_id'])) {
                $this->session->set_flashdata("message", "Invalid Request Found.! ! !");
                redirect(base_url() . 'admin-qualification/manage');
            }
        }
        $data = $this -> other_qualifications -> get_by_id($data['qualification_id']);
        $data['request_list'] = $this -> other_qualifications_request -> get_by_qualification_id($data['qualification_id']);
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("qualifications/details_admin",$data);
    }

    function reject_request() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['qualification_request_id'])) {
            $update = array(
                "qualification_request_id" => $data['qualification_request_id'],
                "score" => "0",
                "status" => "rejected"
            );
            $this -> other_qualifications_request -> update($update);
            $this -> session -> set_flashdata("class", "success");
            $message = "Request Rejected Successfully.";
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . "admin-qualification/manage");
    }

    function update_request() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        if(empty($data['qualification_id'])) {
            $data['redirect_to'] = "admin-qualification/manage";
        } else {
            $this -> session -> set_flashdata("qualification_id", $data['qualification_id']);
            $data['redirect_to'] = "admin-qualification/details";
        }
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['qualification_request_id'])) {
            $update = array(
                "qualification_request_id" => $data['qualification_request_id'],
                "score" => $data['score'],
                "status" => "approved"
            );
            $this -> other_qualifications_request -> update($update);
            $this -> session -> set_flashdata("class", "success");
            $message = "Request Approved and Score Updated Successfully.";
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . $data['redirect_to']);
    }

    function remove_request() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['qualification_request_id'])) {
            $this -> other_qualifications_request -> delete($data['qualification_request_id']);
            $this -> session -> set_flashdata("class", "success");
            $message = "Request Removed Successfully.";
        }
        $this -> session -> set_flashdata("qualification_id", $data['qualification_id']);
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . "admin-qualification/details");
    }
}
