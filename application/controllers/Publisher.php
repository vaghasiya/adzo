<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publisher extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> library('encryption');
        $this -> load -> library('common_library');
        $this -> load -> database();
        $this -> load -> model("country_model","country",true);
        $this -> load -> model("banks_list_model","banks_list",true);
        $this -> load -> model("blocked_workers_model","blocked_workers",true);
        $this -> load -> model("projects_model","projects",true);
        $this -> load -> model("system_qualifications_model","system_qualifications",true);
        $this -> load -> model("categories_model","categories",true);
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("batch_task_answers_model","batch_task_answers",true);
        $this -> load -> model("transactions_model","transactions",true);
    }

    function index() {
        $data = $this -> session -> userdata();
        echo "<pre>"; print_r($data); exit;
        $this -> dashboard();
    }

    function dashboard() {
        $data['multiple_categories_list'] = $this -> categories -> get_categories_list_by_type("multiple");
        $data['individual_categories_list'] = $this -> categories -> get_categories_list_by_type("individual");
        $user_id = $this -> session -> userdata('user_id');
        $user_type = $this -> session -> userdata('user_type');
        if((!empty($user_id)) && ($user_type == "publisher")) {
            $data['projects_list'] = $this -> projects -> get_publishers_projects_list("project_id,project_name,category_type,title,csv_file,created_date,updated_date");
        }
//        echo "<pre>"; print_r($data); exit;
        $this->load->view('publisher/publisher_dashboard',$data);
    }

    function login() {
        $this->load->view('publisher/login');
    }

    function profile() {
        $this -> user -> isLogin("publisher");
        $this->load->view('publisher/profile');
    }
    
    function blocked_workers() {
        $this -> user -> isLogin("publisher");
        $user_id = $this -> session -> userdata('user_id');
        $data['blocked_workers_list'] = $this -> blocked_workers -> get_by_publisher_id($user_id);
//        echo "<pre>"; print_r($data); exit;
        $this->load->view('publisher/blocked_workers',$data);
    }

    function ajax_block_worker() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $result = array("status"=>"error", "message"=>"Sorry, Invalid Request Found. ! ! !");
        if(!empty($data['worker_id'])) {
            $is_blocked = $this -> blocked_workers -> is_blocked($data['worker_id']);
            if($is_blocked){
                $result['message'] = "Sorry, this worker is already blocked.! ! !";
            } else {
                $block = array(
                    "publisher_id" => $this->session->userdata('user_id'),
                    "worker_id" => $data['worker_id'],
                    "created_date" => date("Y-m-d H:i:s")
                );
                $this->blocked_workers->insert($block);
                $result['message'] = "Worker Blocked Successfully.";
                $result['status'] = "success";
            }
        }
        echo json_encode($result); exit(0);
    }

    function ajax_unblock_worker() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $result = array("status"=>"error", "message"=>"Sorry, Invalid Request Found. ! ! !");
        if(!empty($data['worker_id'])) {
            $this -> blocked_workers -> delete_by_worker_id($data['worker_id']);
            $result['message'] = "Worker Unblocked Successfully.";
            $result['status'] = "success";
        }
        echo json_encode($result); exit(0);
    }

    function unblock_worker() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $message = "Sorry, Invalid Request Found.! ! !";
        if(!empty($data['block_id'])) {
            $this -> blocked_workers -> delete($data['block_id']);
            $message = "Worker Unblocked Successfully.";
            $this -> session -> set_flashdata("class", "success");
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'publisher/blocked-workers');
    }

    function ajax_pay_bonus() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $result = array("status"=>"error", "message"=>"Sorry, Invalid Request Found. ! ! !");
        if((!empty($data['worker_id'])) && ($data['total_amount'] > 0)) {
            $user = $this -> user -> get_by_id($this -> session -> userdata('user_id'));
            if($user['balance'] >= $data['total_amount']){
                $update = array(
                    "user_id" => $user['user_id'],
                    "balance" => $user['balance'] - $data['total_amount']
                );
                $this -> user -> update($update);
                $update = array(
                    "user_id" => $data['worker_id'],
                    "balance" => "balance + ".$data['bonus_amount']
                );
                $this -> user -> update($update, TRUE);
                $update = array(
                    "user_id" => 1,
                    "balance" => "balance + ".$data['adzo_fees_amount']
                );
                $this -> user -> update($update, TRUE);
                $transaction_reference_no = $this -> transactions -> generate_transaction_reference_no();
                $transaction = array();
                $transaction[] = array(
                    "transaction_reference_no" => $transaction_reference_no,
                    "batch_id" => $data['batch_id'],
                    "user_id" => $data['worker_id'],
                    "details" => "Bonus Paid",
                    "type" => "debit",
                    "adzo_fees_percentage" => $data['adzo_fees_percentage'],
                    "adzo_fees_amount" => $data['adzo_fees_amount'],
                    "total_amount" => $data['total_amount'],
                    "transaction_amount" => $data['total_amount'],
                    "paid_via" => "adzo_account",
                    "created_by" => $this -> session -> userdata('user_id'),
                    "created_date" => date("Y-m-d H:i:s")
                );
                $transaction[] = array(
                    "transaction_reference_no" => $transaction_reference_no,
                    "batch_id" => $data['batch_id'],
                    "user_id" => $this -> session -> userdata('user_id'),
                    "details" => "Bonus Received",
                    "type" => "credit",
                    "adzo_fees_percentage" => $data['adzo_fees_percentage'],
                    "adzo_fees_amount" => $data['adzo_fees_amount'],
                    "total_amount" => $data['total_amount'],
                    "transaction_amount" => $data['bonus_amount'],
                    "paid_via" => "adzo_account",
                    "created_by" => $data['worker_id'],
                    "created_date" => date("Y-m-d H:i:s")
                );
                $transaction[] = array(
                    "transaction_reference_no" => $transaction_reference_no,
                    "batch_id" => $data['batch_id'],
                    "user_id" => $this -> session -> userdata('user_id'),
                    "details" => "Bonus Fees Received",
                    "type" => "credit",
                    "adzo_fees_percentage" => $data['adzo_fees_percentage'],
                    "adzo_fees_amount" => $data['adzo_fees_amount'],
                    "total_amount" => $data['total_amount'],
                    "transaction_amount" => $data['adzo_fees_amount'],
                    "paid_via" => "adzo_account",
                    "created_by" => 1,
                    "created_date" => date("Y-m-d H:i:s")
                );
                $this -> transactions -> batch_insert($transaction);
                $result['message'] = "Bonus Paid Successfully.";
                $result['status'] = "success";
            } else {
                $result['message'] = "Sorry, you do not have enough balance in your adzo account.! ! !";
            }
        }
        echo json_encode($result); exit(0);
    }

    function task_summary() {
        $this -> user -> isLogin("publisher");
        $data['days_list'] = $this -> batch_tasks -> get_task_summary_days_for_publisher();
        $data['monthly_list'] = $this -> batch_tasks -> get_task_summary_monthly_for_publisher();
        $data['task_statistics'] = $this -> batch_tasks -> get_task_statistics_for_publisher();
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("publisher/tasks_summary",$data);
    }

    function worker_summary() {
        $this -> user -> isLogin("publisher");
        $data['week_list'] = $this -> batch_tasks -> get_worker_summary_for_publisher("week");
        $data['month_list'] = $this -> batch_tasks -> get_worker_summary_for_publisher("month");
        $data['life_time_list'] = $this -> batch_tasks -> get_worker_summary_for_publisher("life_time");
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("publisher/worker_summary",$data);
    }

    function transaction_summary() {
        $this -> user -> isLogin("publisher");
        $data['transactions_list'] = $this -> transactions -> get_my_transactions();
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("publisher/transaction_summary",$data);
    }

    function my_profile() {
        $this -> user -> isLogin("publisher");
        $user_id = $this -> session -> userdata('user_id');
        $data = $this -> user -> get_by_id($user_id);
        $data['country_list'] = $this -> country -> get_country_list();
        $data['india_state_list'] = $this -> country -> get_india_state_list();
        $data['usa_state_list'] = $this -> country -> get_usa_state_list();
        $data['banks_list'] = $this -> banks_list -> get_banks_list();
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("publisher/my_profile",$data);
    }

    function update_profile() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $message = "Sorry, Invalid Request Found.! ! !";
        if(!empty($data)) {
            $data['user_id'] = $this -> session -> userdata('user_id');
            $message = "Profile Updated Successfully";
            if (($_FILES['profile_pic']['error'] == 0)) {
                $ext = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
                $result = $this->common_library->uploadFile("profile_pic", FCPATH . PATH_PROFILE_PIC, $data['user_id'] . '.' . $ext, "jpg|png|jpeg");
                if (!empty($result) && (is_array($result))) {
                    $message .= " But Sorry, Photo Uploading Failed.";
                } else {
                    $data['profile_pic'] = $data['user_id'] . '.' . $ext;
                    $this -> session -> set_userdata("profile_pic",base_url().PATH_PROFILE_PIC."/".$data['profile_pic']);
                    $message = "Profile Updated Successfully";
                }
            } else if (($_FILES['profile_pic']['error'] != 4)) {
                $message .= " But Sorry, Photo Uploading Failed.";
            }
            $this->user->update($data);
            $this->session->set_flashdata("class", "success");
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'publisher/my-profile');
    }

    function change_password() {
        $this -> user -> isLogin("publisher");
        $this -> load -> view("publisher/change_password");
    }

    function update_password() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $message = "Sorry, Invalid Request Found.! ! !";
        if((!empty($data['password'])) && (!empty($data['new_password'])) && (!empty($data['confirm_password']))) {
            if($data['new_password'] === $data['confirm_password']) {
                $data['user_name'] = $this->session->userdata('user_name');
                $user = $this -> user -> authenticate($data['user_name'], $data['password']);
                if (!empty($user)) {
                    $update = array(
                        "user_id" => $user['user_id'],
                        "password" => $this->encryption->encrypt($data['new_password'])
                    );
                    $this -> user -> update($update);
                    $message = "Password Changed Successfully";
                    $this->session->set_flashdata("class", "success");
                } else {
                    $message = "Sorry, Original Password Not Matched";
                }
            } else {
                $message = "Sorry, New Password Not Matched With Confirm Password";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'publisher/change-password');
    }
}
