<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Individual_task extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> database();
    }

    function file_upload() {
        $this -> load -> view("individual_task/file_upload");
    }

    function free_text() {
        $this -> load -> view("individual_task/free_text");
    }

    function multi_choice_image() {
        $this -> load -> view("individual_task/multi_choice_image");
    }

    function multi_choice_text() {
        $this -> load -> view("individual_task/multi_choice_text");
    }

    function single_choice_image() {
        $this -> load -> view("individual_task/single_choice_image");
    }

    function single_choice_text() {
        $this -> load -> view("individual_task/single_choice_text");
    }

    function preview() {
        $this -> load -> view("individual_task/preview");
    }
}
