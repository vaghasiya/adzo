<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_manage_publishers extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> library('common_library');
        $this -> load -> database();
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("transactions_model","transactions",true);
        $this -> load -> model("withdrawals_model","withdrawals",true);
    }

    function index() {
        $this -> user -> isLogin("admin");
        $data['users_list'] = $this -> user -> get_publisher_summary_for_admin();
        $data['country_summary_list'] = $this -> user -> get_country_wise_summary_for_admin("publisher");
        $data['users_count'] = $this -> user -> get_registration_analytics_for_admin("publisher");
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("admin/manage_publishers",$data);
    }
}
