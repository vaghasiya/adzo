<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_manage_transactions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> library('common_library');
        $this -> load -> database();
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("transactions_model","transactions",true);
        $this -> load -> model("withdrawals_model","withdrawals",true);
    }

    function index() {
        $this -> user -> isLogin("admin");
        $data['transactions_list'] = $this -> transactions -> get_my_transactions();
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("admin/manage_transactions",$data);
    }
}
