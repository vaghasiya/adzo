<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> database();
        $this -> load -> library('common_library');
        $this -> load -> library('email_library');
        $this -> load -> library('encryption');
        $this -> load -> model("country_model","country",true);
    }

    function login_process() {
        $user_id = $this->session->userdata('user_id');
        $redirect_to = "";
        if(empty($user_id)) {
            $data = $this -> input -> post();
            $redirect_to = $data['redirect_to'];
            $user = $this -> user -> authenticate($data['user_name'], $data['password']);
            if(!empty($user)){
                if(!empty($user['profile_pic'])) {
                    $user['profile_pic'] = base_url().PATH_PROFILE_PIC."/".$user['profile_pic'];
                } else {
                    $user['profile_pic'] = base_url().DEFAULT_PROFILE_PIC;
                }
                $this -> session -> set_userdata($user);
                if($user['user_type'] == "publisher") {
                    redirect(base_url() . 'publisher/dashboard');
                } else if($user['user_type'] == "worker") {
                    redirect(base_url() . 'worker/dashboard');
                } else if($user['user_type'] == "admin") {
                    redirect(base_url() . 'admin/dashboard');
                }
            } else {
                $this -> session -> set_flashdata('message','Invalid User Name or Password.!!!');
                $this -> session -> set_flashdata("modal", "login_modal");
            }
        }
        redirect(base_url().$redirect_to);
    }

    function logout() {
        $session_items = array('user_id', 'user_name', 'first_name', 'last_name', 'email', 'user_type', 'profile_pic', 'unique_user_id');
        $this -> session -> unset_userdata($session_items);
        $this -> session -> set_flashdata('message','Logout Successfully.');
        $this -> session -> set_flashdata("class", "success");
//        $this -> session -> set_flashdata("modal", "login_modal");
        redirect(base_url());
    }

    function register_process() {
        $data = $this -> input -> post();
        $message = "Record Already Exist. Please Use Other Email Address.!!!";
        if(!$this -> user -> isExist($data)){
            $email_data['to'] = $data['email'];
            $email_data['to'] = "siddharthjogia@gmail.com";
            $email_data['subject'] = "Verify Email for Adzo Digital Market";
            $email_data['message'] = $this -> load -> view("elements/template_register_email", $data, true);
           // echo "<pre>"; print_r($data); exit;
            if($this -> email_library -> mail_send($email_data)) {
                $data['password'] = $this->encryption->encrypt($data['password']);
                $data['register_date'] = date("Y-m-d H:i:s");
                $id = $this -> user -> insert($data);
                $this -> session -> set_flashdata("class", "success");
                $message = "Thanks for Registration. Please Check Confirmation Email.";
            } else {
                $message = "Sorry, System is down. Please Try Again Later.!!!";
            }
        }
        $this -> session -> set_flashdata("modal", "signup_modal");
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url());
    }

    function forget_password_process() {
        $data = $this -> input -> post();
        $redirect_to = $data['redirect_to'];
        $message = "Record Not Exist. Please Provide Correct Details.!!!";
        $data['user'] = $this -> user -> get_by_username($data['user_name']);
        if((!empty($data['user'])) && $data['email'] === $data['user']['email']){
            $data['user']['password'] = $this -> encryption -> decrypt($data['user']['password']);
            $email_data['to'] = $data['email'];
            $email_data['to'] = "siddharthjogia@gmail.com";
            $email_data['subject'] = "Forget Password for Adzo Digital Market";
            $email_data['message'] = $this -> load -> view("elements/template_forget_password", $data, true);
//            echo "<pre>"; print_r($data); exit;
            if($this -> email_library -> mail_send($email_data)) {
                $this -> session -> set_flashdata("class", "success");
                $message = "Your Password has been Sent in Mail.";
            } else {
                $message = "Sorry, System is down. Please Try Again Later.!!!";
            }
        }
        $this -> session -> set_flashdata("modal", "forget_password_modal");
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url().$redirect_to);
    }

    function complete_registration($email) {
        $user = $this -> user -> get_by_email(base64_decode($email));
        if($user['email_verified'] == '0' && $user['status'] == '0') {
            $data['country_list'] = $this -> country -> get_country_list();
            $data['india_state_list'] = $this -> country -> get_india_state_list();
            $data['usa_state_list'] = $this -> country -> get_usa_state_list();
            $data['user'] = $user;
            $this -> load -> view("website/registration_details",$data);
        } else {
            $this -> session -> set_flashdata("modal", "signup_modal");
            $this -> session -> set_flashdata("message", "Invalid Link Found. Please Contact Administrator.!!!");
            redirect(base_url());
        }
    }

    function complete_registration_process() {
        $data = $this -> input -> post();
        $user = $this -> user -> get_by_username($data['user_name']);
        $user_type = $this -> session -> userdata('user_type');
//        $data['FILES'] = $_FILES;
//        echo "<pre>"; print_r($data); exit;
        if(!empty($user_type)){
            $this -> session -> set_flashdata("message", "Invalid Link Found");
            if($user_type == "publisher") {
                redirect(base_url() . 'publisher/dashboard');
            } else if($user_type == "worker") {
                redirect(base_url() . 'worker/dashboard');
            } else if($user_type == "admin") {
                redirect(base_url() . 'admin/dashboard');
            }
        }
        if(empty($user)) {
            $user = $this -> user -> get_by_id($data['user_id']);
            if($user['email_verified'] == '0') {
                $message = "Registration Process Completed. You can login now.";
                $data['email_verified'] = '1';
                $data['status'] = '1';
                $data['unique_user_id'] = date("ymd", strtotime($user['register_date'])) . $data['user_id'];
                $this->user->update($data);
                if (($_FILES['profile_pic']['error'] == 0)) {
                    $ext = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
                    $result = $this->common_library->uploadFile("profile_pic", FCPATH . PATH_PROFILE_PIC, $data['user_id'] . '.' . $ext, "jpg|png|jpeg");
                    if (!empty($result) && (is_array($result))) {
                        $message .= " But Sorry, Photo Uploading Failed.";
                    } else {
                        $update['user_id'] = $data['user_id'];
                        $update['profile_pic'] = $data['user_id'] . '.' . $ext;
                        $this->user->update($update);
                        $message = "Registration Process Complete. You can login now.";
                    }
                } else if (($_FILES['profile_pic']['error'] != 4)) {
                    $message .= " But Sorry, Photo Uploading Failed.";
                }
                $this->session->set_flashdata("class", "success");
            } else {
                $message = "Sorry, You have already completed registration process.";
            }
            $this -> session -> set_flashdata("modal", "login_modal");
            $this -> session -> set_flashdata("message", $message);
            redirect(base_url());
        } else {
            $this -> session -> set_flashdata("message", "Sorry, Occupied User Name Found. Please Try Again with Other User Name.");
            redirect(base_url() . 'complete-registration/' . base64_encode($data['email']));
        }
    }

    function set_password($user_name, $password) {
        $user = $this -> user -> get_by_username($user_name);
        if(empty($user)) {
            echo "User Not Found.!!!";
        } else {
            $update['user_id'] = $user['user_id'];
            $update['password'] = $this -> encryption -> encrypt($password);
            $this -> user -> update($update);
            echo "User Name: $user_name<br>Password: $password";
        }
    }
}
