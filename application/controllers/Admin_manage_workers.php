<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_manage_workers extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> library('common_library');
        $this -> load -> database();
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("transactions_model","transactions",true);
        $this -> load -> model("withdrawals_model","withdrawals",true);
        $this -> load -> model("users_verification_request_model","users_verification_request",true);
    }

    function index() {
        $this -> user -> isLogin("admin");
        $data['withdrawal_list'] = $this -> withdrawals -> get_all_data();
        $data['verification_list'] = $this -> users_verification_request -> get_all_data();
        $data['users_list'] = $this -> user -> get_worker_summary_for_admin();
        $data['country_summary_list'] = $this -> user -> get_country_wise_summary_for_admin("worker");
        $data['users_count'] = $this -> user -> get_registration_analytics_for_admin("worker");
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("admin/manage_workers",$data);
    }

    function approve_verification_request() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
//        echo "<pre>"; print_r($data); exit;
        $message = "Invalid Request";
        if(!empty($data['verification_id'])) {
            $record = $this -> users_verification_request -> get_by_id($data['verification_id']);
//            echo "<pre>"; print_r($data);
//            echo "<pre>"; print_r($record); exit;
            if($record['status'] == "pending") {
                $update = array(
                    "user_id" => $record['created_by'],
                    "document_verified" => "1"
                );
                $this->user->update($update);
                $update = array(
                    "verification_id" => $data['verification_id'],
                    "comments" => "Document Verification Completed",
                    "status" => "approved",
                    "updated_date" => date("Y-m-d H:i:s")
                );
                $this->users_verification_request->update($update);
                $this->session->set_flashdata("class", "success");
                $message = "Request Approved Successfully";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin-manage-workers');
    }

    function reject_verification_request() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Invalid Request";
        if(!empty($data['verification_id'])) {
            $record = $this -> users_verification_request -> get_by_id($data['verification_id']);
            if($record['status'] == "pending") {
                $update = array(
                    "user_id" => $record['created_by'],
                    "document_verified" => "0"
                );
                $this->user->update($update);
                $update = array(
                    "verification_id" => $data['verification_id'],
                    "comments" => $data['comments'],
                    "status" => "rejected",
                    "updated_date" => date("Y-m-d H:i:s")
                );
                $this->users_verification_request->update($update);
                $this->session->set_flashdata("class", "success");
                $message = "Request Rejected Successfully";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin-manage-workers');
    }

    function approve_withdrawal_request() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
//        echo "<pre>"; print_r($data); exit;
        $message = "Invalid Request";
        if(!empty($data['withdraw_id'])) {
            $record = $this -> withdrawals -> get_by_id($data['withdraw_id']);
            if($record['status'] == "pending") {
                $update = array(
                    "user_id" => 1,
                    "balance" => "balance - " . $record['total_amount']
                );
                $this->user->update($update, TRUE);
                $transaction_reference_no = $this->transactions->generate_transaction_reference_no();
                $transaction = array();
                $transaction[] = array(
                    "transaction_reference_no" => $transaction_reference_no,
                    "user_id" => 1,
                    "details" => "Withdraw Transaction: " . $record['withdraw_reference_no'],
                    "type" => "debit",
                    "adzo_fees_amount" => $record['adzo_fees_amount'],
                    "total_amount" => $record['total_amount'],
                    "transaction_amount" => $record['total_amount'],
                    "paid_via" => "adzo_account",
                    "created_by" => 1,
                    "created_date" => date("Y-m-d H:i:s")
                );
                $transaction[] = array(
                    "transaction_reference_no" => $transaction_reference_no,
                    "user_id" => $this->session->userdata('user_id'),
                    "details" => "Withdraw Transaction: " . $record['withdraw_reference_no'],
                    "type" => "credit",
                    "adzo_fees_amount" => $record['adzo_fees_amount'],
                    "total_amount" => $record['total_amount'],
                    "transaction_amount" => $record['total_amount'],
                    "paid_via" => "adzo_account",
                    "created_by" => $record['created_by'],
                    "created_date" => date("Y-m-d H:i:s")
                );
                $this->transactions->batch_insert($transaction);
                $update = array(
                    "withdraw_id" => $data['withdraw_id'],
                    "transaction_reference_no" => $transaction_reference_no,
                    "comments" => "Withdrawal Request Approved",
                    "status" => "approved",
                    "updated_date" => date("Y-m-d H:i:s")
                );
                $this->withdrawals->update($update);
                $this->session->set_flashdata("class", "success");
                $message = "Request Approved Successfully";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin-manage-workers');
    }

    function reject_withdrawal_request() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Invalid Request";
        if(!empty($data['withdraw_id'])) {
            $record = $this -> withdrawals -> get_by_id($data['withdraw_id']);
            if($record['status'] == "pending") {
                $update = array(
                    "user_id" => $record['created_by'],
                    "balance" => "balance + " . $record['withdrawal_amount']
                );
                $this->user->update($update, TRUE);
                $update = array(
                    "withdraw_id" => $data['withdraw_id'],
                    "comments" => $data['comments'],
                    "status" => "rejected",
                    "updated_date" => date("Y-m-d H:i:s")
                );
                $this->withdrawals->update($update);
                $this->session->set_flashdata("class", "success");
                $message = "Request Rejected Successfully";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin-manage-workers');
    }
}
