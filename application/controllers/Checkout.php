<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> database();
    }

    function preview_before_checkout() {
        $this->load->view('checkout/preview_before_checkout');
    }

    function payment() {
        $this->load->view('checkout/payment');
    }
}
