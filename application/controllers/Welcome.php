<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> database();
    }

    function index()
    {
        $user_type = $this -> session -> userdata('user_type');
        if(!empty($user_type)) {
            if($user_type == "publisher") {
                redirect(base_url() . 'publisher/dashboard');
            } else if($user_type == "worker") {
                redirect(base_url() . 'worker/dashboard');
            } else if($user_type == "admin"){
                redirect(base_url() . 'admin/dashboard');
            } else {
                redirect(base_url() . 'user/logout');
            }
        }
        $this->load->view('website/index');
    }
}
