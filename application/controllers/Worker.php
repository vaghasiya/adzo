<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Worker extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> library('encryption');
        $this -> load -> library('common_library');
        $this -> load -> database();
        $this -> load -> model("country_model","country",true);
        $this -> load -> model("banks_list_model","banks_list",true);
        $this -> load -> model("favourite_publishers_model","favourite_publishers",true);
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("batch_task_answers_model","batch_task_answers",true);
        $this -> load -> model("batch_qualifications_model","batch_qualifications",true);
        $this -> load -> model("other_qualifications_model","other_qualifications",true);
        $this -> load -> model("transactions_model","transactions",true);
        $this -> load -> model("withdrawals_model","withdrawals",true);
        $this -> load -> model("users_verification_request_model","users_verification_request",true);
    }

    function index() {
        $data = $this -> session -> userdata();
        echo "<pre>"; print_r($data); exit;
        $this -> dashboard();
    }

    function dashboard() {
        $this -> user -> isLogin("worker");
        $this->load->view('worker/worker_dashboard');
    }

    function my_tasks() {
        $this -> user -> isLogin("worker");
        $data['records_list'] = $this -> batch_tasks -> get_active_task_by_user_id();
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("worker/my_tasks",$data);
    }

    function reassigned_tasks() {
        $this -> user -> isLogin("worker");
        $data['records_list'] = $this -> batch_tasks -> get_reassigned_task_by_user_id();
        $this -> load -> view("worker/reassigned_tasks",$data);
    }

    function favourite_publishers() {
        $this -> user -> isLogin("worker");
        $user_id = $this -> session -> userdata('user_id');
        $data['favourite_publishers_list'] = $this -> favourite_publishers -> get_by_worker_id($user_id);
        $this -> load -> view("worker/favourite_publishers",$data);
    }

    function ajax_add_favourite_publisher() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        $result = array("status"=>"error", "message"=>"Sorry, Invalid Request Found. ! ! !");
        if(!empty($data['publisher_id'])) {
            $is_favourite = $this -> favourite_publishers -> is_favourite($data['publisher_id']);
            if($is_favourite){
                $result['message'] = "Sorry, this publisher is already exist in your favourite list.!!!";
            } else {
                $favourite = array(
                    "worker_id" => $this->session->userdata('user_id'),
                    "publisher_id" => $data['publisher_id'],
                    "created_date" => date("Y-m-d H:i:s")
                );
                $this->favourite_publishers->insert($favourite);
                $result['message'] = "Publisher Added in your Favourite List Successfully.";
                $result['status'] = "success";
            }
        }
        echo json_encode($result); exit(0);
    }

    function remove_favourite_publisher() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        $message = "Sorry, Invalid Request Found.! ! !";
        if(!empty($data['favourite_id'])) {
            $this -> favourite_publishers -> delete($data['favourite_id']);
            $message = "Publisher Removed from Your Favourite List Successfully.";
            $this -> session -> set_flashdata("class", "success");
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'worker/favourite-publishers');
    }

    function my_profile() {
        $this -> user -> isLogin("worker");
        $user_id = $this -> session -> userdata('user_id');
        $data = $this -> user -> get_by_id($user_id);
        $data['country_list'] = $this -> country -> get_country_list();
        $data['india_state_list'] = $this -> country -> get_india_state_list();
        $data['usa_state_list'] = $this -> country -> get_usa_state_list();
        $data['banks_list'] = $this -> banks_list -> get_banks_list();
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("wprofile/my_profile",$data);
    }

    function update_profile() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        $message = "Sorry, Invalid Request Found.! ! !";
        if(!empty($data)) {
//            echo "<pre>"; print_r($data); exit;
            $data['user_id'] = $this -> session -> userdata('user_id');
            $message = "Profile Updated Successfully";
            if (($_FILES['profile_pic']['error'] == 0)) {
                $ext = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
                $result = $this->common_library->uploadFile("profile_pic", FCPATH . PATH_PROFILE_PIC, $data['user_id'] . '.' . $ext, "jpg|png|jpeg");
                if (!empty($result) && (is_array($result))) {
                    $message .= " But Sorry, Photo Uploading Failed.";
                } else {
                    $data['profile_pic'] = $data['user_id'] . '.' . $ext;
                    $this -> session -> set_userdata("profile_pic",base_url().PATH_PROFILE_PIC."/".$data['profile_pic']);
                    $message = "Profile Updated Successfully";
                }
            } else if (($_FILES['profile_pic']['error'] != 4)) {
                $message .= " But Sorry, Photo Uploading Failed.";
            }
            $this->user->update($data);
            $this->session->set_flashdata("class", "success");
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'worker/my-profile');
    }

    function verification_request() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        $message = "Sorry, Invalid Request Found.! ! !";
        if(!empty($data)) {
            $data['user_id'] = $this -> session -> userdata('user_id');
            if (($_FILES['document1']['error'] != 0)) {
                $message = "Document 1 Not Uploaded Properly";
            } else if(($_FILES['document2']['error'] != 0)) {
                $message = "Document 2 Not Uploaded Properly";
            } else if(($_FILES['document3']['error'] != 0)) {
                $message = "Document 3 Not Uploaded Properly";
            } else {
                $verification_reference_no = date("mdHi") . mt_rand(10,99);
                $verification = array(
                    "verification_reference_no" => $verification_reference_no,
                    "full_name" => $data['full_name'],
                    "document_number" => $data['document_number'],
                    "birthdate" => date("Y-m-d", strtotime($data['birthdate'])),
                    "document1" => "",
                    "document2" => "",
                    "document3" => "",
                    "created_by" => $data['user_id'],
                    "created_date" => date("Y-m-d H:i:s")
                );
//                echo "<pre>"; print_r($data);
//                echo "<pre>"; print_r($verification); exit;
                $ext1 = pathinfo($_FILES['document1']['name'], PATHINFO_EXTENSION);
                $result = $this->common_library->uploadFile("document1", FCPATH . PATH_USER_DOCUMENTS, $verification_reference_no . '_1.' . $ext1, "jpg|png|jpeg");
                if (!empty($result) && (is_array($result))) {
                    $message = "Document 1 Uploading Failed.";
                } else {
                    $verification['document1'] = $verification_reference_no . '_1.' . $ext1;
                    $ext2 = pathinfo($_FILES['document2']['name'], PATHINFO_EXTENSION);
                    $result = $this->common_library->uploadFile("document2", FCPATH . PATH_USER_DOCUMENTS, $verification_reference_no . '_2.' . $ext2, "jpg|png|jpeg");
                    if (!empty($result) && (is_array($result))) {
                        $message = "Document 2 Uploading Failed.";
                    } else {
                        $verification['document2'] = $verification_reference_no . '_2.' . $ext2;
                        $ext3 = pathinfo($_FILES['document3']['name'], PATHINFO_EXTENSION);
                        $result = $this->common_library->uploadFile("document3", FCPATH . PATH_USER_DOCUMENTS, $verification_reference_no . '_3.' . $ext3, "jpg|png|jpeg");
                        if (!empty($result) && (is_array($result))) {
                            $message = "Document 3 Uploading Failed.";
                        } else {
                            $verification['document3'] = $verification_reference_no . '_3.' . $ext3;
//                            echo "<pre>"; print_r($verification); exit;
                            $this -> users_verification_request -> insert($verification);
                            $message = "Verification Request Submitted Successfully";
                        }
                    }
                }
            }
            $this->session->set_flashdata("class", "success");
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'worker/my-profile');
    }

    function change_password() {
        $this -> user -> isLogin("worker");
        $this -> load -> view("wprofile/change_password");
    }

    function update_password() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        $message = "Sorry, Invalid Request Found.! ! !";
        if((!empty($data['password'])) && (!empty($data['new_password'])) && (!empty($data['confirm_password']))) {
            if($data['new_password'] === $data['confirm_password']) {
                $data['user_name'] = $this->session->userdata('user_name');
                $user = $this -> user -> authenticate($data['user_name'], $data['password']);
                if (!empty($user)) {
                    $update = array(
                        "user_id" => $user['user_id'],
                        "password" => $this->encryption->encrypt($data['new_password'])
                    );
                    $this -> user -> update($update);
                    $message = "Password Changed Successfully";
                    $this->session->set_flashdata("class", "success");
                } else {
                    $message = "Sorry, Original Password Not Matched";
                }
            } else {
                $message = "Sorry, New Password Not Matched With Confirm Password";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'worker/change-password');
    }

    function task_summary() {
        $this -> user -> isLogin("worker");
        $data['days_list'] = $this -> batch_tasks -> get_task_summary_days_for_worker();
        $data['monthly_list'] = $this -> batch_tasks -> get_task_summary_monthly_for_worker();
        $data['task_statistics'] = $this -> batch_tasks -> get_task_statistics_for_worker();
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("wprofile/tasks_summary",$data);
    }

    function publisher_summary() {
        $this -> user -> isLogin("worker");
        $data['week_list'] = $this -> batch_tasks -> get_publisher_summary_for_worker("week");
        $data['month_list'] = $this -> batch_tasks -> get_publisher_summary_for_worker("month");
        $data['life_time_list'] = $this -> batch_tasks -> get_publisher_summary_for_worker("life_time");
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("wprofile/publisher_summary",$data);
    }

    function transaction_summary() {
        $this -> user -> isLogin("worker");
        $data['transactions_list'] = $this -> transactions -> get_my_transactions();
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("wprofile/transaction_summary",$data);
    }

    function withdrawal_summary() {
        $this -> user -> isLogin("worker");
        $data['withdrawal_list'] = $this -> withdrawals -> get_my_withdrawals();
        $data['user'] = $this -> user -> get_active_user_data("balance");
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("wprofile/withdrawal_summary",$data);
    }

    function withdraw_request() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        $user = $this -> user -> get_active_user_data("balance");
        $message = "Invalid Request Found";
        if(($user['balance'] >= MINIMUM_BALANCE_FOR_WITHDRAWAL) && ($data['withdrawal_amount'] >= MINIMUM_BALANCE_FOR_WITHDRAWAL) && (($data['withdrawal_amount'] - ADZO_WITHDRAW_CHARGES) == $data['total_amount'])) {
            $update = array(
                "user_id" => $this -> session -> userdata('user_id'),
                "balance" => "balance - ".$data['withdrawal_amount']
            );
            $this -> user -> update($update, TRUE);
            $withdraw = array(
                "withdraw_reference_no" => date("mdHi") . mt_rand(10,99),
                "withdrawal_amount" => $data['withdrawal_amount'],
                "adzo_fees_amount" => ADZO_WITHDRAW_CHARGES,
                "total_amount" => $data['total_amount'],
                "created_by" => $this -> session -> userdata('user_id'),
                "created_date" => date("Y-m-d H:i:s"),
            );
            $this -> withdrawals -> insert($withdraw);
            $this -> session -> set_flashdata("class", "success");
            $message = "Request Submitted Successfully";
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'worker/withdrawal-summary');
    }

    function message() {
        $this -> load -> view("worker/message");
    }

    function my_account() {
        $this -> load -> view("worker/my_account");
    }

    function individual() {
        $this -> load -> view("worker/individual");
    }

    function my_favorite() {
        $this -> load -> view("worker/my_favorite");
    }

    function bank_account() {
        $this -> load -> view("wprofile/bank_account");
    }

    function changeaddr() {
        $this -> load -> view("wprofile/changeaddr");
    }

    function changemail() {
        $this -> load -> view("wprofile/changemail");
    }

    function changemobi() {
        $this -> load -> view("wprofile/changemobi");
    }

    function changename() {
        $this -> load -> view("wprofile/changename");
    }

    function p7days() {
        $this -> load -> view("wprofile/p7days");
    }

    function p30days() {
        $this -> load -> view("wprofile/p30days");
    }

    function paypal() {
        $this -> load -> view("wprofile/paypal");
    }

    function task_monthly() {
        $this -> load -> view("wprofile/task_monthly");
    }


    function tasks_date() {
        $this -> load -> view("wprofile/tasks_date");
    }

    function transaction() {
        $this -> load -> view("wprofile/transaction");
    }

    function transfer_earning() {
        $this -> load -> view("wprofile/transfer_earning");
    }

    function upi() {
        $this -> load -> view("wprofile/upi");
    }

    function vrifypage() {
        $this -> load -> view("wprofile/vrifypage");
    }

    function withdrawal() {
        $this -> load -> view("wprofile/withdrawal");
    }
}
