<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> helper('download');
        $this -> load -> database();
        $this -> load -> library('common_library');
        $this -> load -> model("projects_model","projects",true);
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("batch_task_answers_model","batch_task_answers",true);
        $this -> load -> model("batch_qualifications_model","batch_qualifications",true);
        $this -> load -> model("system_qualifications_model","system_qualifications",true);
        $this -> load -> model("other_qualifications_model","other_qualifications",true);
        $this -> load -> model("transactions_model","transactions",true);
        $this -> load -> model("categories_model","categories",true);
        $this -> load -> model("country_model","country",true);
    }

    function list_projects() {
        $this -> load -> view('project/list');
    }

    function manage() {
        $this -> load -> view('project/manage');
    }

    function create_project() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(empty($data['category_id'])) {
            $data['category_id'] = $this -> session -> flashdata("category_id");
            if(empty($data['category_id'])) {
                $this -> session -> set_flashdata("message", "Invalid Request Found.!!!");
                redirect(base_url() . 'publisher/dashboard');
            }
        }
        $data = $this -> categories -> get_by_id($data['category_id']);
        $data['project_name'] .= $this -> projects -> get_project_counter($data['project_name']);
        $data['project_name'] = trim($data['project_name']);
        $data['country_list'] = $this -> country -> get_country_list();
        $data['india_state_list'] = $this -> country -> get_india_state_list();
        $data['usa_state_list'] = $this -> country -> get_usa_state_list();
        $data['publisher_qualifications_list'] = $this -> other_qualifications -> get_my_qualifications();
        $data['admin_qualifications_list'] = $this -> other_qualifications -> get_admin_qualifications();
//            echo "<pre>"; print_r($data); exit;
        $this -> load -> view('project/create_project', $data);
    }

    function save_project() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
//        echo "<pre>"; print_r($data); exit;
        $system_qualification_list = $data['system_qualification'];
        unset($data['system_qualification']);
        $data['seconds_per_assignment'] = $this -> common_library -> get_seconds_from_time($data['time_per_assignment'], $data['time_per_assignment_unit']);
        $data['hit_expire_minutes'] = $this -> common_library -> get_minutes_from_time($data['hit_expire'], $data['hit_expire_unit']);
        $data['expire_date_time'] = date("Y-m-d H:i:s",strtotime('+'.$data['hit_expire_minutes'].' minutes'));
        $data['created_by'] = $this -> session -> userdata('user_id');
        $data['created_date'] = date("Y-m-d H:i:s");
        $data['updated_date'] = date("Y-m-d H:i:s");
        $data['total_hits'] = 1;
        $data['total_assignments'] = $data['number_of_assignment'];
        $data['total_rewards_amount'] = floatval($data['total_assignments']) * floatval($data['reward_per_assignment']);
        $data['adzo_fees_percentage'] = ADZO_FEES_PERCENTAGE;
        $data['adzo_fees_amount'] = (floatval(ADZO_FEES_PERCENTAGE) * floatval($data['total_rewards_amount'])/100);
        $data['total_amount'] = floatval($data['total_rewards_amount']) + floatval($data['adzo_fees_amount']);
        $data['project_id'] = $this -> projects -> insert($data);
//        $data['project_id'] = "0";
        foreach ($system_qualification_list as $key => $system_qualification) {
            $system_qualification_list[$key] = json_decode($system_qualification,true);
            $system_qualification_list[$key]['project_id'] = $data['project_id'];
            if(!empty($system_qualification_list[$key]['sq_state_value'])) {
                $system_qualification_list[$key]['sq_state_value'] = implode(",",$system_qualification_list[$key]['sq_state_value']);
            }
        }
//        echo "<pre>"; print_r($system_qualification_list); exit;
        $this -> system_qualifications -> batch_insert($system_qualification_list);
        $this -> session -> set_flashdata("class", "success");
        $this -> session -> set_flashdata("message", "Project created successfully.");
        redirect(base_url() . 'publisher/dashboard');
    }

    function edit_project() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(empty($data['project_id'])) {
            $this -> session -> set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'publisher/dashboard');
        }
        $data = $this -> projects -> get_by_id($data['project_id']);
        $data['system_qualifications'] = $this->system_qualifications->get_by_project_id($data['project_id']);
        $data['category'] = $this -> categories -> get_by_id($data['category_id'], "category_name");
        $data['country_list'] = $this -> country -> get_country_list();
        $data['india_state_list'] = $this -> country -> get_india_state_list();
        $data['usa_state_list'] = $this -> country -> get_usa_state_list();
        $data['publisher_qualifications_list'] = $this -> other_qualifications -> get_my_qualifications();
        $data['admin_qualifications_list'] = $this -> other_qualifications -> get_admin_qualifications();
//        echo "<pre>";print_r($data);exit;
//        header("X-XSS-Protection: 0");
        $this->load->view('project/edit_project', $data);
    }

    function update_project() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(empty($data['project_id'])) {
            $this -> session -> set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'publisher/dashboard');
        }
        $message = "Sorry, Update process failed.! ! !";
        $system_qualification_list = $data['system_qualification'];
        unset($data['system_qualification']);
        $data['seconds_per_assignment'] = $this -> common_library -> get_seconds_from_time($data['time_per_assignment'], $data['time_per_assignment_unit']);
        $data['hit_expire_minutes'] = $this -> common_library -> get_minutes_from_time($data['hit_expire'], $data['hit_expire_unit']);
        $data['expire_date_time'] = date("Y-m-d H:i:s",strtotime('+'.$data['hit_expire_minutes'].' minutes'));
        $data['is_project_edited'] = "1";
        $data['updated_date'] = date("Y-m-d H:i:s");
        $data['total_hits'] = 1;
        $data['total_assignments'] = $data['number_of_assignment'];
        $data['total_rewards_amount'] = floatval($data['total_assignments']) * floatval($data['reward_per_assignment']);
        $data['adzo_fees_percentage'] = ADZO_FEES_PERCENTAGE;
        $data['adzo_fees_amount'] = (floatval(ADZO_FEES_PERCENTAGE) * floatval($data['total_rewards_amount'])/100);
        $data['total_amount'] = floatval($data['total_rewards_amount']) + floatval($data['adzo_fees_amount']);
        if($this -> projects -> update($data)){
            $this -> system_qualifications -> delete_by_project_id($data['project_id']);
            foreach ($system_qualification_list as $key => $system_qualification) {
                $system_qualification_list[$key] = json_decode($system_qualification,true);
                $system_qualification_list[$key]['project_id'] = $data['project_id'];
                if(!empty($system_qualification_list[$key]['sq_state_value'])) {
                    $system_qualification_list[$key]['sq_state_value'] = implode(",",$system_qualification_list[$key]['sq_state_value']);
                }
            }
            $this -> system_qualifications -> batch_insert($system_qualification_list);
            $this -> session -> set_flashdata("class", "success");
            $message = "Project updated successfully.";
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'publisher/dashboard');
    }

    function copy_project() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['project_id'])) {
            if($this -> projects -> is_project_name_available(trim($data['project_name']))){
                $project = $this -> projects -> get_by_id($data['project_id']);
                $system_qualifications = $this -> system_qualifications -> get_by_project_id($data['project_id']);
                $project['project_id'] = "";
                $project['project_name'] = $data['project_name'];
                $project['csv_file'] = "";
                $project['status'] = "created";
                $project['is_project_edited'] = "0";
                $project['latest_batch_id'] = "0";
                $project['expire_date_time'] = date("Y-m-d H:i:s",strtotime('+'.$data['hit_expire_minutes'].' minutes'));
                $project['updated_date'] = date("Y-m-d H:i:s");
                $project['created_by'] = $this -> session -> userdata('user_id');
                $project['created_date'] = date("Y-m-d H:i:s");
                $project['project_id'] = $this -> projects -> insert($project);
                foreach ($system_qualifications as $key => $system_qualification) {
                    $system_qualifications[$key]['system_qualification_id'] = "";
                    $system_qualifications[$key]['project_id'] = $project['project_id'];
                }
                $this -> system_qualifications -> batch_insert($system_qualifications);
                $this -> session -> set_flashdata("class", "success");
                $message = "Project copied successfully.";
            } else {
                $message = "Sorry, Duplicate project name found.! ! !";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'publisher/dashboard');
    }

    function delete_project() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['project_id'])) {
            $this -> projects -> delete($data['project_id']);
            $this -> system_qualifications -> delete_by_project_id($data['project_id']);
            $message = "Project deleted successfully.";
            $this -> session -> set_flashdata("class", "success");
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'publisher/dashboard');
    }

    function publish_project() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
//        $data['project_id'] = 7;
        if(empty($data['project_id'])) {
            $this -> session -> set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'publisher/dashboard');
        }
        $data = $this -> projects -> get_by_id($data['project_id']);
        $data['system_qualifications'] = $this->system_qualifications->get_by_project_id($data['project_id']);
        $batch_counter = $this -> batches -> get_batch_counter($data['project_name']);
        $data['batch_name'] = trim($data['project_name'].$batch_counter);
        $data['category'] = $this -> categories -> get_by_id($data['category_id'], "category_name");
        if($data['category_type'] == "multiple") {
            $data['csv_header'] = explode(",", "\${" . implode("},\${", explode(",", $data['csv_header'])) . "}");
            $lines_array = explode("\n", file_get_contents(FCPATH . "assets/uploads/csv/" . $data['csv_file']));
            $data['csv_data'] = array();
            foreach ($lines_array as $key => $line) {
                if (empty($line)) {
                    continue;
                }
                if ($key > 0) {
                    $data['csv_data'][] = explode(",", trim($line));
                }
            }
        } else {
            $data['csv_header'] = array();
            $data['csv_data'] = array();
        }
        $data['user'] = $this -> user -> get_active_user_data("balance");
        $this -> load -> view("project/preview_and_checkout",$data);
    }

    function checkout() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['project_id'])) {
            if($this -> batches -> is_batch_name_available(trim($data['batch_name']))){
                $project = $this -> projects -> get_by_id($data['project_id']);
                $user_update['user_id'] = $this -> session -> userdata('user_id');
                $user = $this -> user -> get_by_id($user_update['user_id']);
                if($data['use_wallet'] == "true" && $user['balance'] >= $project['total_amount']){
                    $system_qualifications = $this -> system_qualifications -> get_by_project_id($data['project_id']);
                    $project['batch_name'] = $data['batch_name'];
                    $project['batch_description'] = $data['batch_description'];
                    $project['status'] = "in progress";
                    $project['updated_date'] = date("Y-m-d H:i:s");
                    $project['created_by'] = $this -> session -> userdata('user_id');
                    $project['created_date'] = date("Y-m-d H:i:s");
                    unset($project['is_project_edited'],$project['latest_batch_id']);
//                    echo "<pre>"; print_r($project); exit;
                    $project['batch_id'] = $this -> batches -> insert($project);
//                    $project['batch_id'] = 1; // FOR TESTING ONLY
//                    echo "<pre>"; print_r($data); exit;
                    $batch_hits = array();
                    if($project['category_type'] == "multiple") {
                        $lines_array = explode("\n", file_get_contents(FCPATH . "assets/uploads/csv/" . $project['csv_file']));
                        foreach ($lines_array as $key => $line) {
                            if (empty($line)) {
                                continue;
                            }
                            if ($key > 0) {
                                $batch_hits[] = array(
                                    "batch_id" => $project['batch_id'],
                                    "csv_header" => $project['csv_header'],
                                    "csv_data" => trim($line),
                                    "assignments_pending" => $project['number_of_assignment']
                                );
                            }
                        }
                    } else {
                        $batch_hits[] = array(
                            "batch_id" => $project['batch_id'],
                            "csv_header" => "",
                            "csv_data" => "",
                            "assignments_pending" => $project['number_of_assignment']
                        );
                    }
//                    echo "<pre>"; print_r($batch_hits); exit;
                    $this -> batch_hits -> batch_insert($batch_hits);
                    foreach ($system_qualifications as $key => $system_qualification) {
                        $system_qualifications[$key]['batch_id'] = $project['batch_id'];
                        unset($system_qualifications[$key]['project_id'],$system_qualifications[$key]['system_qualification_id']);
                    }
                    $this -> batch_qualifications -> batch_insert($system_qualifications);
                    $project_update['project_id'] = $project['project_id'];
                    $project_update['is_project_edited'] = "0";
                    $project_update['latest_batch_id'] = $project['batch_id'];
                    $this -> projects -> update($project_update);
                    $user_update['balance'] = $user['balance'] - $project['total_amount'];
                    $this -> user -> update($user_update);
                    $update = array(
                        "user_id" => 1,
                        "balance" => "balance + ".$project['total_amount']
                    );
                    $this -> user -> update($update, TRUE);
                    $transaction_reference_no = $this -> transactions -> generate_transaction_reference_no();
                    $transaction = array();
                    $transaction[] = array(
                        "transaction_reference_no" => $transaction_reference_no,
                        "batch_id" => $project['batch_id'],
                        "user_id" => 1,
                        "details" => "Publish Project: ".$project['title'],
                        "type" => "debit",
                        "adzo_fees_percentage" => $project['adzo_fees_percentage'],
                        "adzo_fees_amount" => $project['adzo_fees_amount'],
                        "total_amount" => $project['total_amount'],
                        "transaction_amount" => $project['total_amount'],
                        "paid_via" => "adzo_account",
                        "created_by" => $this -> session -> userdata('user_id'),
                        "created_date" => date("Y-m-d H:i:s")
                    );
                    $transaction[] = array(
                        "transaction_reference_no" => $transaction_reference_no,
                        "batch_id" => $project['batch_id'],
                        "user_id" => $this -> session -> userdata('user_id'),
                        "details" => "Publish Project: ".$project['title'],
                        "type" => "credit",
                        "adzo_fees_percentage" => $project['adzo_fees_percentage'],
                        "adzo_fees_amount" => $project['adzo_fees_amount'],
                        "total_amount" => $project['total_amount'],
                        "transaction_amount" => $project['total_amount'],
                        "paid_via" => "adzo_account",
                        "created_by" => 1,
                        "created_date" => date("Y-m-d H:i:s")
                    );
                    $this -> transactions -> batch_insert($transaction);
                    $this -> session -> set_flashdata("class", "success");
                    $message = "Project Published Successfully.";
                } else if($data['use_wallet'] == "false") {
                    // perform payment gateway process here...
                    $message = "Payment gateway is not completed.";
                } else {
                    $message = "Sorry, you do not have enough balance";
                }
            } else {
                $message = "Sorry, Duplicate project name found.! ! !";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'publisher/dashboard');
    }

    function ajax_check_project_name()
    {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $result = array();
        if(!empty($data['project_name'])){
            $result['status'] = $this -> projects -> is_project_name_available(trim($data['project_name']), $data['project_id']);
        }
        echo json_encode($result); exit(0);
    }

    function ajax_check_batch_name()
    {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $result = array();
        if(!empty($data['batch_name'])){
            $result['status'] = $this -> batches -> is_batch_name_available(trim($data['batch_name']), $data['batch_id']);
        }
        echo json_encode($result); exit(0);
    }

    function ajax_upload_csv()
    {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        $result = array("status"=>"error", "message"=>"Sorry, Invalid Request Found. ! ! !");
        if((!empty($data['project_id'])) && (!empty($_FILES['csv_file']))){
            if (($_FILES['csv_file']['error'] == 0)) {
                $project = $this -> projects -> get_by_id($data['project_id']);
                $csv_errors = $this -> common_library -> validate_csv_file($_FILES['csv_file']['tmp_name'], $project['csv_header']);
                if(empty($csv_errors)) {
                    $file_name = date("ymdHis").mt_rand(0,9).'.csv';
                    $upload_result = $this -> common_library -> uploadFile("csv_file", FCPATH . "assets/uploads/csv", $file_name, "csv");
                    if (!empty($upload_result) && (is_array($upload_result))) {
                        $result['message'] = "File Uploading Failed";
                    } else {
                        $lines_array = explode("\n", file_get_contents(FCPATH . "assets/uploads/csv/" . $file_name));
                        $total_hits = 0;
                        foreach ($lines_array as $key => $line) {
                            if(empty($line)){
                                continue;
                            }
                            if($key > 0) {
                                $total_hits++;
                            }
                        }
                        $update['project_id'] = $data['project_id'];
                        $update['csv_file'] = $file_name;
                        $update['total_hits'] = $total_hits;
                        $update['total_assignments'] = intval($total_hits) * intval($project['number_of_assignment']);
                        $update['total_rewards_amount'] = floatval($update['total_assignments']) * floatval($project['reward_per_assignment']);
                        $update['adzo_fees_percentage'] = ADZO_FEES_PERCENTAGE;
                        $update['adzo_fees_amount'] = (floatval(ADZO_FEES_PERCENTAGE) * floatval($update['total_rewards_amount'])/100);
                        $update['total_amount'] = floatval($update['total_rewards_amount']) + floatval($update['adzo_fees_amount']);
                        $this -> projects -> update($update);
                        $result['status'] = "success";
                        $result['message'] = "File validation completed";
                    }
                } else {
                    $result['errors'] = $csv_errors;
                    $result['message'] = "File validation completed with errors";
                }
            } else if (($_FILES['csv_file']['error'] != 4)) {
                $result['message'] = "File Uploading Failed";
            }
        }
        echo json_encode($result); exit(0);
    }

    function download_sample_data() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(!empty($data['project_id'])){
            $project = $this -> projects -> get_by_id($data['project_id']);
            if($data['download_latest_csv_file'] == "true") {
                force_download(FCPATH . "assets/uploads/csv/" . $project['csv_file'], null);
            } else {
                $csv_data = $project['csv_header'] . "\r\n";
                $columns = explode(",", $project['csv_header']);
                for ($i = 1; $i < 4; $i++) {
                    $temp_data = array();
                    foreach ($columns as $column) {
                        $temp_data[] = "Hit{$i}_{$column}_Data";
                    }
                    $csv_data .= implode(",", $temp_data) . "\r\n";
                }
                force_download($project['project_name'] . " - Sample Data.csv", $csv_data);
            }
        } else {
            $this -> session -> set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'publisher/dashboard');
        }
    }

    function result() {
        $this -> load -> view("project/result");
    }

    function approved() {
        $this -> load -> view("project/approved");
    }

    function rejected() {
        $this -> load -> view("project/rejected");
    }

    function task_summary() {
        $this -> load -> view("project/task_summary");
    }

    function qualification() {
        $this -> load -> view("project/qualification");
    }

    function request() {
        $this -> load -> view("project/request");
    }

    function example() {
        $this -> load -> view("project/example");
    }

    function confirm() {
        $this -> load -> view("project/confirm");
    }

    function individual_task() {
        $this -> load -> view("project/individual_task");
    }

    function preview_finish() {
        $this -> load -> view("project/checkout");
    }
}
