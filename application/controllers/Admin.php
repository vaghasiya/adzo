<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> library('encryption');
        $this -> load -> library('common_library');
        $this -> load -> database();
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("transactions_model","transactions",true);
        $this -> load -> model("withdrawals_model","withdrawals",true);
    }

    function index() {
        $this -> dashboard();
    }

    function dashboard() {
        $this -> user -> isLogin("admin");
        $this -> load -> view("admin/dashboard");
    }

    function ajax_suspend_user() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $result = array("status"=>"error", "message"=>"Sorry, Invalid Request Found. ! ! !");
        if(!empty($data['user_id'])) {
            $user = $this -> user -> get_user_data($data['user_id'],"status");
            if(empty($user['status'])){
                $result['message'] = "Sorry, this user is already suspended.! ! !";
            } else {
                $update = array(
                    "user_id" => $data['user_id'],
                    "status" => "0"
                );
                $this -> user -> update($update);
                $result['message'] = "User Suspended Successfully.";
                $result['status'] = "success";
            }
        }
        echo json_encode($result); exit(0);
    }

    function ajax_revoke_user() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $result = array("status"=>"error", "message"=>"Sorry, Invalid Request Found. ! ! !");
        if(!empty($data['user_id'])) {
            $user = $this -> user -> get_user_data($data['user_id'],"status");
            if($user['status'] == "1"){
                $result['message'] = "Sorry, this user is already active.! ! !";
            } else {
                $update = array(
                    "user_id" => $data['user_id'],
                    "status" => "1"
                );
                $this -> user -> update($update);
                $result['message'] = "User Suspension Removed Successfully.";
                $result['status'] = "success";
            }
        }
        echo json_encode($result); exit(0);
    }

    function change_password() {
        $this -> user -> isLogin("admin");
        $this -> load -> view("admin/change_password");
    }

    function update_password() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Sorry, Invalid Request Found.! ! !";
        if((!empty($data['password'])) && (!empty($data['new_password'])) && (!empty($data['confirm_password']))) {
            if($data['new_password'] === $data['confirm_password']) {
                $data['user_name'] = $this->session->userdata('user_name');
                $user = $this -> user -> authenticate($data['user_name'], $data['password']);
                if (!empty($user)) {
                    $update = array(
                        "user_id" => $user['user_id'],
                        "password" => $this->encryption->encrypt($data['new_password'])
                    );
                    $this -> user -> update($update);
                    $message = "Password Changed Successfully";
                    $this->session->set_flashdata("class", "success");
                } else {
                    $message = "Sorry, Original Password Not Matched";
                }
            } else {
                $message = "Sorry, New Password Not Matched With Confirm Password";
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin/change-password');
    }

    function results() {
        $this -> load -> view("admin/results");
    }

    function login() {
        $this -> load -> view("admin/login");
    }

    function worker_registration() {
        $this -> load -> view("admin/worker_registration");
    }

    function publisher_registration() {
        $this -> load -> view("admin/publisher_registration");
    }

    function view_more() {
        $this -> load -> view("admin/view_more");
    }

    function manage_batch() {
        $this -> load -> view("admin/manage_batches");
    }

    function transaction_management(){
        $this -> load -> view("admin/transaction_management");
    }

    function saved_project() {
        $this -> load -> view("admin/saved_project");
    }
}
