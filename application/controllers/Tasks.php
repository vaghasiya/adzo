<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> database();
        $this -> load -> model("blocked_workers_model","blocked_workers",true);
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("batch_task_answers_model","batch_task_answers",true);
        $this -> load -> model("batch_qualifications_model","batch_qualifications",true);
        $this -> load -> model("other_qualifications_model","other_qualifications",true);
        $this -> load -> model("other_qualifications_request_model","other_qualifications_request",true);
        $this -> load -> model("transactions_model","transactions",true);
    }

    function all_tasks() {
        $date = date("Y-m-d H:i:s");
        $condition = "b.expire_date_time > '$date'";
        $user_id = $this -> session -> userdata('user_id');
        if(!empty($user_id)) {
            $block_id_array = $this -> blocked_workers -> get_by_worker_id($user_id);
            $batch_id_array = $this -> batches -> get_available_batch_id_by_user_id($user_id);
            if(!empty($batch_id_array)) {
                $condition .= " AND b.batch_id IN ('".implode("','",array_column($batch_id_array,"batch_id"))."')";
                if(!empty($block_id_array)) {
                    $condition .= " AND b.created_by NOT IN ('".implode("','",array_column($block_id_array,"publisher_id"))."')";
                }
                $data['batch_list'] = $this -> batches -> get_batches_list_by_status("in progress","b.batch_id, b.title, b.hit_visibility, b.total_assignments, b.assignments_submitted, b.assignments_pending, b.assignments_republished, b.reward_per_assignment, b.time_per_assignment, b.time_per_assignment_unit, b.created_date, u.first_name, u.last_name",$condition);
            } else {
                $data['batch_list'] = array();
            }
        } else {
            $data['batch_list'] = $this -> batches -> get_batches_list_by_status("in progress","b.batch_id, b.title, b.hit_visibility, b.total_assignments, b.assignments_submitted, b.assignments_pending, b.assignments_republished, b.reward_per_assignment, b.time_per_assignment, b.time_per_assignment_unit, b.created_date, u.first_name, u.last_name",$condition);
        }
//        echo "<pre>"; print_r($data); exit;
        $this->load->view('tasks/all_tasks',$data);
    }

    function details() {
        $this -> user -> isLogin("worker");
        $post_data = $this -> input -> post();
        if(empty($post_data['auto_accept'])) {
            $post_data['auto_accept'] = $this -> session -> flashdata("auto_accept");
        }
        if(empty($post_data['start_work'])) {
            $post_data['start_work'] = $this -> session -> flashdata("start_work");
        }
        if(empty($post_data['batch_id'])) {
            $post_data['batch_id'] = $this -> session -> flashdata("batch_id");
        }
        $today = date("Y-m-d H:i:s");
        $is_worker_qualified = true;
//        echo "<pre>"; print_r($post_data);exit;
        $data = $this -> batches -> get_by_id($post_data['batch_id']);
        if(empty($data)) {
            $this -> session -> set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'tasks/all-tasks');
        }
        $user_id = $this -> session -> userdata('user_id');
        if(strtotime($data['expire_date_time']) < strtotime($today)){
            $update['batch_id'] = $data['batch_id'];
            $update['status'] = "expired";
            $this -> batches -> update($update);
            $this -> session -> set_flashdata("message", "Sorry, your selected task is expired.! ! !");
            redirect(base_url() . 'tasks/all-tasks');
        }
        $batch_tasks = $this -> batch_tasks -> get_by_batch_id_user_id($data['batch_id'],$user_id);
        if(empty($batch_tasks)){
            $data['hits'] = $this -> batch_hits -> get_by_batch_id($data['batch_id']);
        } else {
            $users_batch_hit_id_array = array_column($batch_tasks, "batch_hit_id");
            $data['hits'] = $this -> batch_hits -> get_by_batch_id($data['batch_id'],"batch_hit_id NOT IN ('".implode("','",$users_batch_hit_id_array)."')");
//            echo "<pre>"; print_r($data['hits']); exit;
        }
        if(empty($data['hits'])) {
            // worker may completed all the tasks (All tasks - worker's submitted task)
            $this -> session -> set_flashdata("message", "Sorry, you do not have any task for this project.! ! !");
            redirect(base_url() . 'tasks/all-tasks');
        }
        $task_statistics = $this -> batch_tasks -> get_task_statistics_for_worker();
        $data['publisher'] = $this -> user -> get_user_data($data['created_by'], "user_id as publisher_id, first_name, last_name");
        $worker = $this -> user -> get_user_data($this -> session -> userdata('user_id'), "user_id as worker_id, country, state");
        $data['batch_qualifications'] = $this -> batch_qualifications -> get_by_batch_id($data['batch_id']);
        $batch_qualified = false;
        foreach ($data['batch_qualifications'] as $key => $batch_qualification) {
            $batch_qualified = false;
            if($batch_qualification['sq_type'] == "location"){
                if(empty($worker['state'])) {
                    $data['batch_qualifications'][$key]['my_value'] = $worker['country'];
                } else {
                    $data['batch_qualifications'][$key]['my_value'] = $worker['country']." [".$worker['state']."]";
                }
                if($batch_qualification['sq_value'] == "All"){
                    $batch_qualified = true;
                } else {
                    if($worker['country'] == $batch_qualification['sq_value']){
                        if(empty($batch_qualification['sq_state_value'])){
                            $batch_qualified = true;
                        } else if(in_array($worker['state'],explode(",",$batch_qualification['sq_state_value']))){
                            $batch_qualified = true;
                        } else {
                            $batch_qualified = false;
                        }
                    }
                }
            } else {
                if ($batch_qualification['sq_type'] == "hit_approval_rate") {
                    $data['batch_qualifications'][$key]['my_value'] = $task_statistics['approval_rate'];
                } else if ($batch_qualification['sq_type'] == "hit_rejection_rate") {
                    $data['batch_qualifications'][$key]['my_value'] = $task_statistics['rejection_rate'];
                } else if ($batch_qualification['sq_type'] == "total_hits_approved") {
                    $data['batch_qualifications'][$key]['my_value'] = $task_statistics['approved'];
                }
                if (($batch_qualification['sq_key'] == "greater_than") && ($data['batch_qualifications'][$key]['my_value'] > $batch_qualification['sq_value'])) {
                    $batch_qualified = true;
                } else if (($batch_qualification['sq_key'] == "greater_than_equal_to") && ($data['batch_qualifications'][$key]['my_value'] >= $batch_qualification['sq_value'])) {
                    $batch_qualified = true;
                } else if (($batch_qualification['sq_key'] == "less_than") && ($data['batch_qualifications'][$key]['my_value'] < $batch_qualification['sq_value'])) {
                    $batch_qualified = true;
                } else if (($batch_qualification['sq_key'] == "less_than_equal_to") && ($data['batch_qualifications'][$key]['my_value'] <= $batch_qualification['sq_value'])) {
                    $batch_qualified = true;
                }
            }
            if($batch_qualified == true) {
                $data['batch_qualifications'][$key]['result'] = "You meet this qualification.";
            } else {
                $is_worker_qualified = false;
                $data['batch_qualifications'][$key]['result'] = "You do not meet this qualification.";
            }
        }
//        echo "<pre>"; print_r($task_statistics);
//        echo "<pre>"; print_r($data['batch_qualifications']); exit;
        if(!empty($data['adzo_qualification_id'])) {
            $qualification = $this->other_qualifications->get_by_id($data['adzo_qualification_id']);
            $data['adzo_qualification'] = $qualification['name'] . ' is ' . str_replace("_"," ",$data['adzo_qualification_key']) . ' ' . $data['adzo_qualification_value'];
            $data['adzo_qualification_request'] = $this->other_qualifications_request->get_workers_request_by_qualification_id($data['adzo_qualification_id'],"approved");
            if(!empty($data['adzo_qualification_request'])) {
                $adzo_qualified = false;
                if(($data['adzo_qualification_key'] == "greater_than") && ($data['adzo_qualification_request']['score'] > $data['adzo_qualification_value'])) {
                    $adzo_qualified = true;
                } else if(($data['adzo_qualification_key'] == "greater_than_equal_to") && ($data['adzo_qualification_request']['score'] >= $data['adzo_qualification_value'])) {
                    $adzo_qualified = true;
                } else if(($data['adzo_qualification_key'] == "less_than") && ($data['adzo_qualification_request']['score'] < $data['adzo_qualification_value'])) {
                    $adzo_qualified = true;
                } else if(($data['adzo_qualification_key'] == "less_than_equal_to") && ($data['adzo_qualification_request']['score'] <= $data['adzo_qualification_value'])) {
                    $adzo_qualified = true;
                }
                if($adzo_qualified == true) {
                    $data['adzo_qualification_request']['result'] = "You meet this qualification.";
                } else {
                    $is_worker_qualified = false;
                    $data['adzo_qualification_request']['result'] = "You do not meet this qualification.";
                }
            }
        }
        if(!empty($data['publisher_qualification_id'])) {
            $qualification = $this->other_qualifications->get_by_id($data['publisher_qualification_id']);
            $data['publisher_qualification'] = $qualification['name'] . ' is ' . str_replace("_"," ",$data['publisher_qualification_key']) . ' ' . $data['publisher_qualification_value'];
            $data['publisher_qualification_request'] = $this->other_qualifications_request->get_workers_request_by_qualification_id($data['publisher_qualification_id'],"approved");
            if(!empty($data['publisher_qualification_request'])) {
                $publisher_qualified = false;
                if(($data['publisher_qualification_key'] == "greater_than") && ($data['publisher_qualification_request']['score'] > $data['publisher_qualification_value'])) {
                    $publisher_qualified = true;
                } else if(($data['publisher_qualification_key'] == "greater_than_equal_to") && ($data['publisher_qualification_request']['score'] >= $data['publisher_qualification_value'])) {
                    $publisher_qualified = true;
                } else if(($data['publisher_qualification_key'] == "less_than") && ($data['publisher_qualification_request']['score'] < $data['publisher_qualification_value'])) {
                    $publisher_qualified = true;
                } else if(($data['publisher_qualification_key'] == "less_than_equal_to") && ($data['publisher_qualification_request']['score'] <= $data['publisher_qualification_value'])) {
                    $publisher_qualified = true;
                }
                if($publisher_qualified == true) {
                    $data['publisher_qualification_request']['result'] = "You meet this qualification.";
                } else {
                    $is_worker_qualified = false;
                    $data['publisher_qualification_request']['result'] = "You do not meet this qualification.";
                }
            }
        }
//        echo "<pre>"; print_r($data['adzo_qualification_request']);
//        echo "<pre>"; print_r($data['publisher_qualification_request']);
//        echo "<pre>"; print_r($data); exit;
//        echo "<pre>"; print_r($post_data);
//        echo "<pre>"; print_r($data);
        if($post_data['start_work'] == "true"){
            if($is_worker_qualified == false){
                $this -> session -> set_flashdata("message", "Sorry, you do not meet all qualifications. Please check qualifications list in Task-Details page.! ! !");
                redirect(base_url() . 'tasks/all-tasks');
            }
//            echo "<pre>"; print_r($data); exit;
            if(empty($post_data['batch_hit_id'])) {
                $post_data['batch_hit_id'] = $data['hits'][0]['batch_hit_id'];
            } else {
                $batch_hit_id = array_column($data['hits'],"batch_hit_id");
                if (!in_array($post_data['batch_hit_id'], $batch_hit_id)) {
//                    echo "<pre>"; print_r($batch_hit_id);
                    $post_data['batch_hit_id'] = $data['hits'][0]['batch_hit_id'];
                }
            }
//            echo "<pre>"; print_r($post_data); exit;
            $data['task_expire_date_time'] = date("Y-m-d H:i:s",strtotime("+".$data['seconds_per_assignment']." seconds"));
            $data['batch_task'] = array(
                "batch_id" => $post_data['batch_id'],
                "user_id" => $user_id,
                "batch_hit_id" => $post_data['batch_hit_id'],
                "start_date_time" => date("Y-m-d H:i:s"),
                "expire_date_time" => $data['task_expire_date_time'],
                "task_rewards" => $data['reward_per_assignment'],
                "created_by" => $user_id,
                "created_date" => date("Y-m-d H:i:s")
            );
            $data['batch_task']['batch_task_id'] = $this -> batch_tasks -> insert($data['batch_task']);
            $update = array(
                "batch_id" => $data['batch_id'],
                "assignments_pending" => $data['assignments_pending'] + 1
            );
            $this -> batches -> update($update);
            $update = array(
                "batch_hit_id" => $post_data['batch_hit_id'],
                "assignments_pending" => "assignments_pending - 1"
            );
            $this -> batch_hits -> update($update, TRUE);
        } else if($data['hit_visibility'] == "true"){
            $this -> session -> set_flashdata("message", "Sorry, task preview is disabled.! ! !");
            redirect(base_url() . 'tasks/all-tasks');
        }
//        echo "<pre>"; print_r($data); exit;
        if($data['category_type'] == "multiple") {
            $data['current_hit_index'] = 0;
            foreach ($data['hits'] as $key => $value) {
                $data['hits'][$key]['csv_data_array'] = explode(",", trim($value['csv_data']));
                if($data['batch_task']['batch_hit_id'] == $value['batch_hit_id']){
                    $data['current_hit_index'] = $key;
                }
            }
            $data['csv_header_array'] = explode(",", "\${" . implode("},\${", explode(",", $data['csv_header'])) . "}");
            $data['csv_data_array'] = array_column($data['hits'],"csv_data_array");
        }
        if(!empty($post_data['auto_accept'])) {
            $data['auto_accept'] = $post_data['auto_accept'];
        } else {
            $data['auto_accept'] = "false";
        }
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("tasks/details",$data);
    }

    function work_details() {
        $this -> user -> isLogin("worker");
        $post_data = $this -> input -> post();
        if(empty($post_data['batch_task_id'])) {
            $this -> session -> set_flashdata("message", "Sorry, invalid request found.! ! !");
            redirect(base_url() . 'tasks/my-tasks');
        }
        $today = date("Y-m-d H:i:s");
        $batch_task = $this -> batch_tasks -> get_by_id($post_data['batch_task_id']);
        if(strtotime($batch_task['expire_date_time']) < strtotime($today)){
            if($batch_task['status'] == "reassigned") {
                $update = array(
                    "batch_task_id" => $batch_task['batch_task_id'],
                    "status" => "submitted"
                );
                $this->batch_tasks->update($update);

                $update = array(
                    "batch_id" => $batch_task['batch_id'],
                    "assignments_pending" => "assignments_pending - 1",
                    "assignments_submitted" => "assignments_submitted + 1",
                    "assignments_pending_review" => "assignments_pending_review + 1"
                );
                $this->batches->update($update, TRUE);
                $this -> check_batch_status($batch_task['batch_id']);
                $this->session->set_flashdata("message", "Sorry, your selected task is expired.! ! !");
                redirect(base_url() . 'worker/reassigned-tasks');
            } else {
                $update = array(
                    "batch_task_id" => $batch_task['batch_task_id'],
                    "status" => "expired"
                );
                $this->batch_tasks->update($update);

                $update = array(
                    "batch_id" => $batch_task['batch_id'],
                    "assignments_pending" => "assignments_pending - 1"
                );
                $this->batches->update($update, TRUE);

                $update = array(
                    "batch_hit_id" => $batch_task['batch_hit_id'],
                    "assignments_pending" => "assignments_pending + 1"
                );
                $this->batch_hits->update($update, TRUE);
                $this -> check_batch_status($batch_task['batch_id']);
                $this->session->set_flashdata("message", "Sorry, your selected task is expired.! ! !");
                redirect(base_url() . 'worker/my-tasks');
            }
        }
        $data = $this -> batches -> get_by_id($batch_task['batch_id']);
        $data['batch_task'] = $batch_task;
        $data['hits'][] = $this -> batch_hits -> get_by_id($batch_task['batch_hit_id']);
//        echo "<pre>"; print_r($data); exit;
        $data['publisher'] = $this -> user -> get_user_data($data['created_by'], "user_id as publisher_id, first_name, last_name");
        $data['batch_qualifications'] = $this -> batch_qualifications -> get_by_batch_id($data['batch_id']);
        if(!empty($data['adzo_qualification_id'])) {
            $qualification = $this->other_qualifications->get_by_id($data['adzo_qualification_id']);
            $data['adzo_qualification'] = $qualification['name'] . ' is ' . str_replace("_"," ",$data['adzo_qualification_key']) . ' ' . $data['adzo_qualification_value'];
        }
        if(!empty($data['publisher_qualification_id'])) {
            $qualification = $this->other_qualifications->get_by_id($data['publisher_qualification_id']);
            $data['publisher_qualification'] = $qualification['name'] . ' is ' . str_replace("_"," ",$data['publisher_qualification_key']) . ' ' . $data['publisher_qualification_value'];
        }
        if($data['category_type'] == "multiple") {
            $data['current_hit_index'] = 0;
            foreach ($data['hits'] as $key => $value) {
                $data['hits'][$key]['csv_data_array'] = explode(",", trim($value['csv_data']));
                if($data['batch_task']['batch_hit_id'] == $value['batch_hit_id']){
                    $data['current_hit_index'] = $key;
                }
            }
            $data['csv_header_array'] = explode(",", "\${" . implode("},\${", explode(",", $data['csv_header'])) . "}");
            $data['csv_data_array'] = array_column($data['hits'],"csv_data_array");
        }
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("tasks/details",$data);
    }

    function task_answers_submit() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        if(empty($data['batch_task_id'])) {
            $this -> session -> set_flashdata("message", "Sorry, invalid request found.! ! !");
            redirect(base_url() . 'tasks/my-tasks');
        }
        $batch_task_id = $data['batch_task_id'];
        $batch_id = $data['batch_id'];
        $auto_accept = $data['auto_accept'];
        unset($data['batch_task_id'], $data['batch_id'], $data['auto_accept']);
        $answers_array = array();
        foreach ($data as $key => $value) {
            $answers_array[] = array(
                "batch_task_id" => $batch_task_id,
                "answer_key" => $key,
                "answer_value" => $value
            );
        }
        if(!empty($answers_array)) {
            $this -> batch_task_answers -> delete_by_batch_task_id($batch_task_id);
            $this -> batch_task_answers -> batch_insert($answers_array);
            $update = array(
                'batch_task_id' => $batch_task_id,
                'end_date_time' => date("Y-m-d H:i:s"),
                'status' => "submitted"
            );
            $this -> batch_tasks -> update($update);
            $update = array(
                "batch_id" => $batch_id,
                "assignments_submitted" => "assignments_submitted + 1",
                "assignments_pending_review" => "assignments_pending_review + 1",
                "assignments_pending" => "assignments_pending - 1"
            );
            $this -> batches -> update($update, TRUE);
            $this -> check_batch_status($batch_id);
            $this -> session -> set_flashdata("message", "Task completed successfully and sent for review.");
            $this -> session -> set_flashdata("class", "success");
        } else {
            $this -> session -> set_flashdata("message", "Sorry, you have not submitted anything for that task.");
        }
        if($auto_accept == "true"){
            $this -> session -> set_flashdata("message", "");
            $this -> session -> set_flashdata("auto_accept", "true");
            $this -> session -> set_flashdata("start_work", "true");
        }
        $this -> session -> set_flashdata("batch_id", $batch_id);
        redirect(base_url() . 'tasks/details');
    }

    function task_expired() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        if(empty($data['batch_task_id'])) {
            $this -> session -> set_flashdata("message", "Sorry, invalid request found.! ! !");
            redirect(base_url() . 'tasks/my-tasks');
        }
        $update = array(
            "batch_id" => $data['batch_id'],
            "assignments_pending" => "assignments_pending - 1"
        );
        $this -> batches -> update($update, TRUE);

        $update = array(
            "batch_hit_id" => $data['batch_hit_id'],
            "assignments_pending" => "assignments_pending + 1"
        );
        $this -> batch_hits -> update($update, TRUE);

        $update = array(
            "batch_task_id" => $data['batch_task_id'],
            "status" => "returned"
        );
        $this -> batch_tasks -> update($update);
        $this -> session -> set_flashdata("message", "Sorry, Your task got expired and returned to publisher.");
        redirect(base_url() . 'tasks/all-tasks');
    }
    
    function return_my_task() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        if(empty($data['batch_task_id'])) {
            $this -> session -> set_flashdata("message", "Sorry, invalid request found.! ! !");
            redirect(base_url() . 'tasks/my-tasks');
        }
        $update = array(
            "batch_id" => $data['batch_id'],
            "assignments_pending" => "assignments_pending - 1"
        );
        $this -> batches -> update($update, TRUE);

        $update = array(
            "batch_hit_id" => $data['batch_hit_id'],
            "assignments_pending" => "assignments_pending + 1"
        );
        $this -> batch_hits -> update($update, TRUE);

        $update = array(
            "batch_task_id" => $data['batch_task_id'],
            "status" => "returned"
        );
        $this -> batch_tasks -> update($update);

//        $this -> batch_task_answers -> delete_by_batch_task_id($data['batch_task_id']);
//        $this -> batch_tasks -> delete($data['batch_task_id']);
        $this -> session -> set_flashdata("message", "Your task returned successfully.");
        $this -> session -> set_flashdata("class", "success");
        redirect(base_url() . 'worker/my-tasks');
    }

    function approve_task() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(empty($data['batch_id']) || empty($data['batch_task_id']) || empty($data['worker_id'])) {
            $this->session->set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'batch/manage');
        }
        $batch = $this -> batches -> get_by_id($data['batch_id']);
//        echo "<pre>"; print_r($batch);
//        echo "<pre>"; print_r($data); exit;
        $update = array(
            "batch_id" => $data['batch_id'],
            "assignments_approved" => "assignments_approved + 1",
            "assignments_pending_review" => "assignments_pending_review - 1"
        );
        $this -> batches -> update($update, TRUE);

        $update = array(
            "batch_task_id" => $data['batch_task_id'],
            "status" => "approved"
        );
        $this -> batch_tasks -> update($update);
        $update = array(
            "user_id" => $data['worker_id'],
            "balance" => "balance + ".$batch['reward_per_assignment']
        );
        $this -> user -> update($update, TRUE);
        $transaction_reference_no = $this -> transactions -> generate_transaction_reference_no();
        $transaction = array();
        $transaction[] = array(
            "transaction_reference_no" => $transaction_reference_no,
            "batch_id" => $data['batch_id'],
            "user_id" => 1,
            "details" => "Task Completed - ".$batch['title'],
            "type" => "credit",
            "total_amount" => $batch['reward_per_assignment'],
            "transaction_amount" => $batch['reward_per_assignment'],
            "paid_via" => "adzo_account",
            "created_by" => $data['worker_id'],
            "created_date" => date("Y-m-d H:i:s")
        );
        $transaction[] = array(
            "transaction_reference_no" => $transaction_reference_no,
            "batch_id" => $data['batch_id'],
            "user_id" => $data['worker_id'],
            "details" => "Task Completed - ".$batch['title'],
            "type" => "debit",
            "total_amount" => $batch['reward_per_assignment'],
            "transaction_amount" => $batch['reward_per_assignment'],
            "paid_via" => "adzo_account",
            "created_by" => 1,
            "created_date" => date("Y-m-d H:i:s")
        );
        $this -> transactions -> batch_insert($transaction);
//        $this -> check_batch_status($data['batch_id']);
        $this -> session -> set_flashdata("batch_id", $data['batch_id']);
        $this -> session -> set_flashdata("message", "Task approved successfully.");
        $this -> session -> set_flashdata("class", "success");
        redirect(base_url() . 'batch/result');
    }

    function reapprove_task() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(empty($data['batch_id']) || empty($data['batch_task_id'])) {
            $this->session->set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'batch/manage');
        }
        $batch = $this -> batches -> get_by_id($data['batch_id']);
//        echo "<pre>"; print_r($data);
//        echo "<pre>"; print_r($batch); exit;
        $batch_task = $this -> batch_tasks -> get_by_id($data['batch_task_id']);
        if($batch_task['is_republished'] == 0) {
            $update = array(
                "batch_id" => $data['batch_id'],
                "assignments_approved" => "assignments_approved + 1",
                "assignments_rejected" => "assignments_rejected - 1"
            );
            $this->batches->update($update, TRUE);

            $update = array(
                "batch_task_id" => $data['batch_task_id'],
                "status" => "approved"
            );
            $this->batch_tasks->update($update);
            $update = array(
                "user_id" => $data['worker_id'],
                "balance" => "balance + ".$batch['reward_per_assignment']
            );
            $this -> user -> update($update, TRUE);
            $transaction_reference_no = $this -> transactions -> generate_transaction_reference_no();
            $transaction = array();
            $transaction[] = array(
                "transaction_reference_no" => $transaction_reference_no,
                "batch_id" => $data['batch_id'],
                "user_id" => 1,
                "details" => "Task Completed - ".$batch['title'],
                "type" => "credit",
                "total_amount" => $batch['reward_per_assignment'],
                "transaction_amount" => $batch['reward_per_assignment'],
                "paid_via" => "adzo_account",
                "created_by" => $data['worker_id'],
                "created_date" => date("Y-m-d H:i:s")
            );
            $transaction[] = array(
                "transaction_reference_no" => $transaction_reference_no,
                "batch_id" => $data['batch_id'],
                "user_id" => $data['worker_id'],
                "details" => "Task Completed - ".$batch['title'],
                "type" => "debit",
                "total_amount" => $batch['reward_per_assignment'],
                "transaction_amount" => $batch['reward_per_assignment'],
                "paid_via" => "adzo_account",
                "created_by" => 1,
                "created_date" => date("Y-m-d H:i:s")
            );
            $this -> transactions -> batch_insert($transaction);
            $this->session->set_flashdata("message", "Task reapproved successfully.");
            $this->session->set_flashdata("class", "success");
        } else {
            $this -> session -> set_flashdata("message", "Sorry, You have already republished this task.! ! !");
        }
        $this->session->set_flashdata("batch_id", $data['batch_id']);
        redirect(base_url() . 'batch/result');
    }

    function republish_task() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(empty($data['batch_id']) || empty($data['batch_task_id'])) {
            $this->session->set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'batch/manage');
        }
        $batch_task = $this -> batch_tasks -> get_by_id($data['batch_task_id']);
        if($batch_task['is_republished'] == 0) {
//            $update = array(
//                "batch_id" => $data['batch_id'],
//                "assignments_submitted" => "assignments_approved - 1",
//                "assignments_rejected" => "assignments_rejected - 1"
//            );

            $update = array(
                "batch_id" => $data['batch_id'],
                "assignments_republished" => "assignments_republished + 1"
            );
            $this -> batches -> update($update, TRUE);

            $update = array(
                "batch_hit_id" => $batch_task['batch_hit_id'],
                "assignments_pending" => "assignments_pending + 1"
            );
            $this -> batch_hits -> update($update, TRUE);

            $update = array(
                "batch_task_id" => $data['batch_task_id'],
                "is_republished" => "1"
            );
            $this->batch_tasks->update($update);
            $this -> check_batch_status($data['batch_id']);
            $this->session->set_flashdata("message", "Task republished successfully.");
            $this->session->set_flashdata("class", "success");
        } else {
            $this -> session -> set_flashdata("message", "Sorry, You have already republished this task.! ! !");
        }
        $this->session->set_flashdata("batch_id", $data['batch_id']);
        redirect(base_url() . 'batch/result');
    }

    function reject_task() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(empty($data['batch_id']) || empty($data['batch_task_id'])) {
            $this->session->set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'batch/manage');
        }
        $update = array(
            "batch_id" => $data['batch_id'],
            "assignments_rejected" => "assignments_rejected + 1",
            "assignments_pending_review" => "assignments_pending_review - 1"
        );
        $this -> batches -> update($update, TRUE);

        $update = array(
            "batch_task_id" => $data['batch_task_id'],
            "status" => "rejected"
        );
        $this -> batch_tasks -> update($update);
        $this -> check_batch_status($data['batch_id']);
        $this -> session -> set_flashdata("batch_id", $data['batch_id']);
        $this -> session -> set_flashdata("message", "Task rejected successfully.");
        $this -> session -> set_flashdata("class", "success");
        redirect(base_url() . 'batch/result');
    }

    function reassign_task() {
        $this -> user -> isLogin("publisher");
        $data = $this -> input -> post();
        if(empty($data['batch_id']) || empty($data['batch_task_id'])) {
            $this->session->set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'batch/manage');
        }
        $update = array(
            "batch_id" => $data['batch_id'],
            "assignments_pending" => "assignments_pending + 1",
            "assignments_submitted" => "assignments_submitted - 1",
            "assignments_pending_review" => "assignments_pending_review - 1"
        );
        $this->batches->update($update, TRUE);
        $batch = $this->batches->get_by_id($data['batch_id']);
        $update = array(
            "batch_task_id" => $data['batch_task_id'],
            "expire_date_time" => date("Y-m-d H:i:s", strtotime('+' . $batch['seconds_per_assignment'] . ' seconds')),
            "status" => "reassigned"
        );
        $this->batch_tasks->update($update);
        $this -> check_batch_status($data['batch_id']);
        $this->session->set_flashdata("message", "Task reassigned successfully.");
        $this->session->set_flashdata("class", "success");
        $this->session->set_flashdata("batch_id", $data['batch_id']);
        redirect(base_url() . 'batch/result');
    }

    function return_reassigned_task() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        if(empty($data['batch_task_id'])) {
            $this -> session -> set_flashdata("message", "Sorry, invalid request found.! ! !");
            redirect(base_url() . 'tasks/my-tasks');
        }
//        echo "<pre>"; print_r($data); exit;
        $update = array(
            "batch_id" => $data['batch_id'],
            "assignments_pending" => "assignments_pending - 1",
            "assignments_submitted" => "assignments_submitted + 1",
            "assignments_pending_review" => "assignments_pending_review + 1"
        );
        $this -> batches -> update($update, TRUE);

        $update = array(
            "batch_task_id" => $data['batch_task_id'],
            "end_date_time" => date("Y-m-d H:i:s"),
            "status" => "submitted"
        );
        $this -> batch_tasks -> update($update);
        $this -> check_batch_status($data['batch_id']);
//        $this -> batch_task_answers -> delete_by_batch_task_id($data['batch_task_id']);
//        $this -> batch_tasks -> delete($data['batch_task_id']);
        $this -> session -> set_flashdata("message", "Your task returned successfully.");
        $this -> session -> set_flashdata("class", "success");
        redirect(base_url() . 'worker/reassigned-tasks');
    }

    function check_batch_status($batch_id){
        $batch = $this->batches->get_by_id($batch_id);
        $remaining = $batch['total_assignments'] + $batch['assignments_republished'] - $batch['assignments_submitted'] - $batch['assignments_pending'] - $batch['assignments_cancelled'];
        $update = array();
        $today = date("Y-m-d H:i:s");
        if(strtotime($batch['expire_date_time']) < strtotime($today)){
            $update = array(
                "batch_id" => $batch['batch_id'],
                "status" => "expired"
            );
        } else if($batch['status'] == "cancelled"){
            $update = array();
        } else if(($batch['assignments_pending'] > 0) || ($remaining > 0)){
            $update = array(
                "batch_id" => $batch['batch_id'],
                "status" => "in progress"
            );
        } else if(($batch['assignments_pending_review'] > 0) && ($remaining == 0)){
            $update = array(
                "batch_id" => $batch['batch_id'],
                "status" => "review pending"
            );
        } else if(($batch['assignments_pending_review'] == 0) && ($batch['assignments_pending'] == 0) && ($remaining == 0)) {
            $update = array(
                "batch_id" => $batch['batch_id'],
                "status" => "completed"
            );
        }
        if(!empty($update)){
            $this -> batches -> update($update);
        }
    }

    function broken_or_violet_task() {
        $this -> user -> isLogin("worker");
        $data = $this -> input -> post();
        if(empty($data['batch_hit_id'])) {
            $this -> session -> set_flashdata("message", "Sorry, invalid request found.! ! !");
            redirect(base_url() . 'tasks/all-tasks');
        }
        if(!empty($data['batch_task_id'])) {
            $update = array(
                "batch_id" => $data['batch_id'],
                "assignments_pending" => "assignments_pending - 1"
            );
            $this->batches->update($update, TRUE);

            $update = array(
                "batch_hit_id" => $data['batch_hit_id'],
                "assignments_pending" => "assignments_pending + 1"
            );
            $this->batch_hits->update($update, TRUE);

            $update = array(
                "batch_task_id" => $data['batch_task_id'],
                "status" => "returned"
            );
            $this->batch_tasks->update($update);
        }
        $this -> session -> set_flashdata("message", "Your request submitted to admin and publisher successfully.");
        $this -> session -> set_flashdata("class", "success");
        redirect(base_url() . 'tasks/all-tasks');
    }
}
