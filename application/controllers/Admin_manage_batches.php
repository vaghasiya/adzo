<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_manage_batches extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> helper('download');
        $this -> load -> library('common_library');
        $this -> load -> database();
        $this -> load -> model("projects_model","projects",true);
        $this -> load -> model("batches_model","batches",true);
        $this -> load -> model("batch_hits_model","batch_hits",true);
        $this -> load -> model("batch_tasks_model","batch_tasks",true);
        $this -> load -> model("batch_task_answers_model","batch_task_answers",true);
        $this -> load -> model("batch_qualifications_model","batch_qualifications",true);
        $this -> load -> model("system_qualifications_model","system_qualifications",true);
        $this -> load -> model("other_qualifications_model","other_qualifications",true);
        $this -> load -> model("categories_model","categories",true);
        $this -> load -> model("country_model","country",true);
    }

    function index() {
        $this -> user -> isLogin("admin");
        $fields = "b.batch_id,
        b.batch_name,
        b.total_assignments,
        b.assignments_submitted,
        b.assignments_approved,
        b.assignments_rejected,
        b.assignments_pending,
        b.assignments_pending_review,
        b.assignments_cancelled,
        b.assignments_republished,
        b.created_by,
        b.created_date,
        u.unique_user_id,
        u.first_name,
        u.last_name";
        $data['batch_in_progress_list'] = $this -> batches -> get_all_batches_list($fields, "b.status = 'in progress'");
        $data['batch_review_pending_list'] = $this -> batches -> get_all_batches_list($fields, "b.status = 'review pending'");
        $data['batch_reviewed_list'] = $this -> batches -> get_all_batches_list($fields, "b.status = 'completed' or b.status = 'expired' or b.status = 'cancelled'");
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("admin/manage_batches",$data);
    }

    function result()
    {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        if(empty($data['batch_id'])) {
            $data['batch_id'] = $this -> session -> flashdata("batch_id");
            if(empty($data['batch_id'])) {
                $this->session->set_flashdata("message", "Invalid Request Found.! ! !");
                redirect(base_url() . 'admin-manage-batches');
            }
        }
        $data = $this -> batches -> get_by_id($data['batch_id']);
        $data['result_headers_list'] = $this -> batch_task_answers -> get_task_headers_by_batch_id($data['batch_id']);
        $data['submitted_list'] = $this -> batch_tasks -> get_task_and_answers_by_batch_id($data['batch_id'], "bt.status = 'submitted'");
        $data['approved_list'] = $this -> batch_tasks -> get_task_and_answers_by_batch_id($data['batch_id'], "bt.status = 'approved'");
        $data['rejected_list'] = $this -> batch_tasks -> get_task_and_answers_by_batch_id($data['batch_id'], "bt.status = 'rejected'");
        $data['worker_summary_list'] = $this -> batch_tasks -> get_worker_summary($data['batch_id']);
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("batch/result_admin",$data);
    }

    function cancel_batch() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['batch_id'])) {
            $batch = $this -> batches -> get_by_id($data['batch_id']);
            if($batch['status'] == "in progress") {
                $data['assignments_cancelled'] = $batch['total_assignments'] + $batch['assignments_republished'] - $batch['assignments_submitted'] - $batch['assignments_pending'];
                if ($batch['assignments_pending_review'] > 0) {
                    $data['status'] = "review pending";
                } else {
                    $data['status'] = "cancelled";
                }
//                echo "<pre>"; print_r($batch);
//                echo "<pre>"; print_r($data); exit;
                $this->batches->update($data);
                $message = "Batch cancelled successfully.";
                $this->session->set_flashdata("class", "success");
            }
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin-manage-batches');
    }

    function delete_batch() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        $message = "Invalid Request Found.! ! !";
        if(!empty($data['batch_id'])) {
//            echo "<pre>"; print_r($data); exit;
            $this -> batch_hits -> delete_by_batch_id($data['batch_id']);
            $this -> batch_task_answers -> delete_by_batch_id($data['batch_id']);
            $this -> batch_tasks -> delete_by_batch_id($data['batch_id']);
            $this -> batch_qualifications -> delete_by_batch_id($data['batch_id']);
            $this -> batches -> delete($data['batch_id']);
            $message = "Batch deleted successfully.";
            $this -> session -> set_flashdata("class", "success");
        }
        $this -> session -> set_flashdata("message", $message);
        redirect(base_url() . 'admin-manage-batches');
    }

    function summary()
    {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        if(empty($data['batch_id'])) {
            $data['batch_id'] = $this -> session -> flashdata("batch_id");
            if(empty($data['batch_id'])) {
                $this->session->set_flashdata("message", "Invalid Request Found.! ! !");
                redirect(base_url() . 'admin-manage-batches');
            }
        }
        $data = $this -> batches -> get_by_id($data['batch_id']);
        $data['worker_summary_list'] = $this -> batch_tasks -> get_worker_summary($data['batch_id']);
//        echo "<pre>"; print_r($data); exit;
        $this -> load -> view("batch/summary_admin",$data);
    }

    function download_batch_data() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
        if(empty($data['batch_id'])){
            $this -> session -> set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'publisher/dashboard');
        }
        $batch = $this -> batches -> get_by_id($data['batch_id']);
//        echo "<pre>"; print_r($batch); exit;
        force_download(FCPATH . "assets/uploads/csv/" . $batch['csv_file'], null);
    }

    function download_batch_result() {
        $this -> user -> isLogin("admin");
        $data = $this -> input -> post();
//        $data['batch_id'] = 1;
        if(empty($data['batch_id'])){
            $this -> session -> set_flashdata("message", "Invalid Request Found.! ! !");
            redirect(base_url() . 'publisher/dashboard');
        }
        $batch = $this -> batches -> get_by_id($data['batch_id']);
        $result_headers_list = $this -> batch_task_answers -> get_task_headers_by_batch_id($data['batch_id']);
        $data['submitted_list'] = $this -> batch_tasks -> get_task_and_answers_by_batch_id($data['batch_id'], "bt.status = 'submitted'");
        $data['approved_list'] = $this -> batch_tasks -> get_task_and_answers_by_batch_id($data['batch_id'], "bt.status = 'approved'");
        $data['rejected_list'] = $this -> batch_tasks -> get_task_and_answers_by_batch_id($data['batch_id'], "bt.status = 'rejected'");
        $csv_header = explode(",", $batch['csv_header']);
        $result_headers_list[] = "Task-Status";
        if(!empty($batch['csv_header'])) {
            $data['result_headers_list'] = $csv_header;
            foreach ($result_headers_list as $header_key) {
                $data['result_headers_list'][] = $header_key;
            }
        } else {
            $data['result_headers_list'] = $result_headers_list;
        }
        $csv_data = implode(",", $data['result_headers_list']) . "\r\n";

        foreach ($data['submitted_list'] as $row) {
            $temp_data = array();
            if(!empty($row['csv_data'])) {
                foreach (explode(",", $row['csv_data']) as $header_key) {
                    $temp_data[] = $header_key;
                }
            }
            foreach ($result_headers_list as $column) {
                if(empty($row['answers'][$column])) {
                    if($column == "Task-Status"){
                        $temp_data[] = "Review-Pending";
                    } else {
                        $temp_data[] = "-";
                    }
                } else {
                    $temp_data[] = $row['answers'][$column];
                }
            }
            $csv_data .= implode(",", $temp_data) . "\r\n";
        }
        foreach ($data['approved_list'] as $row) {
            $temp_data = array();
            if(!empty($row['csv_data'])) {
                foreach (explode(",", $row['csv_data']) as $header_key) {
                    $temp_data[] = $header_key;
                }
            }
            foreach ($result_headers_list as $column) {
                if(empty($row['answers'][$column])) {
                    if($column == "Task-Status"){
                        $temp_data[] = "Approved";
                    } else {
                        $temp_data[] = "-";
                    }
                } else {
                    $temp_data[] = $row['answers'][$column];
                }
            }
            $csv_data .= implode(",", $temp_data) . "\r\n";
        }
        foreach ($data['rejected_list'] as $row) {
            $temp_data = array();
            if(!empty($row['csv_data'])) {
                foreach (explode(",", $row['csv_data']) as $header_key) {
                    $temp_data[] = $header_key;
                }
            }
            foreach ($result_headers_list as $column) {
                if(empty($row['answers'][$column])) {
                    if($column == "Task-Status"){
                        $temp_data[] = "Rejected";
                    } else {
                        $temp_data[] = "-";
                    }
                } else {
                    $temp_data[] = $row['answers'][$column];
                }
            }
            $csv_data .= implode(",", $temp_data) . "\r\n";
        }

        force_download("Result - " . $batch['batch_name'] . " - " . date("Y-m-d") . ".csv", $csv_data);
    }
}
